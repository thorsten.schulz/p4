<?xml version="1.0" encoding="us-ascii" standalone="no"?>
<romimage xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" partition="0" xmlns="http://www.sysgo.com/xsd/p4/romimage-3.7.xsd">
  <!--!!! Automatically created file. !!!-->
  <!--!!! CAUTION: !!!-->
  <!--!!! Manual changes will be overridden on the next file generation. !!!-->
  <psp align="0x00001000" anchor="-1" header="0" resource="/opt/pikeos-4.2/target/x86/amd64/object/bsp/x86-64/kernel-tracesys-smp.bin"/>
  <sigma0>
    <segment map="true" readonly="true" type="text">
      <file format="elf" name="ssw" offset="-1" size="-1"/>
    </segment>
    <segment map="false" readonly="false" type="data">
      <file format="elf" name="ssw" offset="-1" size="-1"/>
    </segment>
  </sigma0>
  <tags>
    <tag key="UK_TRACE_POST_MORTEM" value="0"/>
    <tag key="UK_TRACE_START_KERNEL_TRACING" value="0"/>
    <tag key="UK_TRACE_MEM_SIZE" value="0x100000"/>
    <tag key="UK_TRACE_MEM_PHYS" value="0xffffffff"/>
    <tag key="UK_TRACE_CONTROL_SIZE" value="0x8000"/>
    <tag key="UK_TRACE_DEFAULT_BUFFER_SIZE" value="0x10000"/>
    <tag key="UK_TRACE_KERNEL_BUFFER_SIZE" value="0x30000"/>
    <tag key="UK_LOG_LEVEL" value="3"/>
    <tag key="0x302" value="1"/>
    <tag key="0x306" value="1"/>
    <tag key="PSP_CHECKSUM" value="1"/>
    <tag key="PSP_CONSOLE_PORT" value="1"/>
    <tag key="PSP_BAUDRATE" value="115200"/>
    <tag key="PSP_DEBUG_PORT" value="2"/>
  </tags>
  <files>
    <file align="0x00001000" name="server" resource="/home/thorsten/Projekte/workspace.codeo/cuzz.pool/pikeos-native/object/trdpStw.elf"/>
    <file align="0x00001000" name="linux.kernel" resource="/home/thorsten/Projekte/p4/nanoStw.int/../trdp-P4.sys/boot/linux.kernel"/>
    <file align="0x00001000" name="linux.params" resource="/home/thorsten/Projekte/p4/nanoStw.int/../trdp-P4.sys/boot/linux.params"/>
    <file align="0x00001000" name="linux.initrd" resource="/home/thorsten/Projekte/p4/nanoStw.int/../trdp-P4.sys/boot/linux.initrd"/>
    <file align="0x00001000" name="monitor" resource="/opt/pikeos-4.2/target/x86/amd64//driver/object/monitor.elf"/>
    <file align="0x00001000" name="traceserver" resource="/opt/pikeos-4.2/target/x86/amd64//driver/object/traceserver.elf"/>
    <file align="0x00001000" name="muxa" resource="/opt/pikeos-4.2/target/x86/amd64//driver/object/muxa.elf"/>
    <file align="0x00001000" name="ssw" resource="/opt/pikeos-4.2/target/x86/amd64/pssw/object/standard/pssw.elf"/>
    <file align="0x00001000" name="VMIT" resource="vmit.mod"/>
    <file align="0x00001000" name="8250.elf" resource="/opt/pikeos-4.2/target/x86/amd64/driver/object/8250.elf"/>
    <file align="0x00001000" name="e1000.elf" resource="/opt/pikeos-4.2/target/x86/amd64/driver/object/e1000.elf"/>
    <file align="4096" name="eStw.xml" resource="/home/thorsten/Projekte/workspace.codeo/cuzz.pool/files/eStw.xml"/>
  </files>
  <properties>
    <prop_dir name="app">
      <prop_dir name="Scade_Stw">
        <prop_dir name="server">
          <prop_dir name="args">
            <prop_uint32 data="1" name="numargs"/>
            <prop_string data="nanonet.Stw" name="argv1"/>
          </prop_dir>
        </prop_dir>
      </prop_dir>
      <prop_dir name="service">
        <prop_dir name="8250">
          <prop_string data="ser0" name="provider"/>
        </prop_dir>
        <prop_dir name="e1000">
          <prop_string data="eth0" name="provider"/>
        </prop_dir>
      </prop_dir>
    </prop_dir>
    <prop_dir name="board">
      <prop_dir name="drv">
        <prop_dir name="misc">
          <prop_dir name="muxa">
            <prop_dir name="configuration">
              <prop_dir name="eth">
                <prop_bin data="192.168.201.3" name="TargetIP"/>
                <prop_bin data="192.168.201.1" name="HostIP"/>
                <prop_bin data="192.168.201.1" name="GatewayIP"/>
                <prop_bin data="255.255.255.0" name="NetMask"/>
                <prop_uint32 data="1500" name="TargetPort"/>
                <prop_uint32 data="1500" name="HostPort"/>
              </prop_dir>
              <prop_dir name="ser">
                <prop_string data="" name="HostDevice"/>
                <prop_uint32 data="0" name="Baudrate"/>
              </prop_dir>
            </prop_dir>
            <prop_dir name="channellist">
              <prop_dir name="0">
                <prop_string data="" name="Name"/>
                <prop_uint32 data="1502" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="1">
                <prop_string data="apex" name="Name"/>
                <prop_uint32 data="1503" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="2">
                <prop_string data="posix-stdio" name="Name"/>
                <prop_uint32 data="1504" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="3">
                <prop_string data="posix-dbg" name="Name"/>
                <prop_uint32 data="1505" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="4">
                <prop_string data="" name="Name"/>
                <prop_uint32 data="1506" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="5">
                <prop_string data="" name="Name"/>
                <prop_uint32 data="1507" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="6">
                <prop_string data="" name="Name"/>
                <prop_uint32 data="1508" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="7">
                <prop_string data="linux" name="Name"/>
                <prop_uint32 data="1509" name="Port"/>
                <prop_string data="telnet" name="Protocol"/>
              </prop_dir>
              <prop_dir name="8">
                <prop_string data="" name="Name"/>
                <prop_uint32 data="1510" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="9">
                <prop_string data="" name="Name"/>
                <prop_uint32 data="1511" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="10">
                <prop_string data="" name="Name"/>
                <prop_uint32 data="1512" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="11">
                <prop_string data="traceserver" name="Name"/>
                <prop_uint32 data="1513" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="12">
                <prop_string data="Monitor" name="Name"/>
                <prop_uint32 data="1514" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="13">
                <prop_string data="mon_con" name="Name"/>
                <prop_uint32 data="1515" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
              <prop_dir name="14">
                <prop_string data="pikeos-remote-shell" name="Name"/>
                <prop_uint32 data="1516" name="Port"/>
                <prop_string data="" name="Protocol"/>
              </prop_dir>
            </prop_dir>
            <prop_string data="eth" name="Mode"/>
            <prop_string data="version" name="Verbosity"/>
            <prop_string data="MUXA" name="Name"/>
            <prop_uint32 data="0x10001" name="Version"/>
            <prop_string data="eth0:0" name="Device"/>
            <prop_string data="127.0.0.1" name="Hostname"/>
            <prop_uint32 data="1501" name="ControlPort"/>
          </prop_dir>
        </prop_dir>
      </prop_dir>
    </prop_dir>
    <prop_dir name="psp">
      <prop_dir name="io">
        <prop_dir name="IDE0">
          <prop_portmap name="command" paddr="0x1f0" psize="0x8"/>
          <prop_portmap name="control" paddr="0x3f6" psize="0x1"/>
        </prop_dir>
        <prop_dir name="IDE1">
          <prop_portmap name="command" paddr="0x170" psize="0x8"/>
          <prop_portmap name="control" paddr="0x376" psize="0x1"/>
        </prop_dir>
        <prop_portmap name="KEYBOARD" paddr="0x60" psize="0x10"/>
        <prop_portmap name="RTC" paddr="0x70" psize="0x10"/>
        <prop_portmap name="COM1" paddr="0x3f8" psize="0x8"/>
        <prop_portmap name="COM2" paddr="0x2f8" psize="0x8"/>
        <prop_portmap name="PCICONF" paddr="0xcf8" psize="0x8"/>
      </prop_dir>
      <prop_dir name="int">
        <prop_interrupt data="0" name="8253"/>
        <prop_interrupt data="1" name="KEYBOARD"/>
        <prop_interrupt data="2" name="CASCADE"/>
        <prop_interrupt data="3" name="COM2"/>
        <prop_interrupt data="4" name="COM1"/>
        <prop_interrupt data="5" name="IRQ5"/>
        <prop_interrupt data="6" name="FLOPPY"/>
        <prop_interrupt data="7" name="IRQ7"/>
        <prop_interrupt data="8" name="RTC"/>
        <prop_interrupt data="9" name="IRQ9"/>
        <prop_interrupt data="10" name="IRQ10"/>
        <prop_interrupt data="11" name="IRQ11"/>
        <prop_interrupt data="12" name="MOUSE"/>
        <prop_interrupt data="13" name="IRQ13"/>
        <prop_interrupt data="14" name="IDE0"/>
        <prop_interrupt data="15" name="IDE1"/>
        <prop_interrupt data="16" name="TIMER"/>
      </prop_dir>
      <prop_dir name="8250">
        <prop_link data="config/provider/ser0/io/0" name="ser0_0"/>
      </prop_dir>
      <prop_uint32 data="0x00010001" name="Version"/>
    </prop_dir>
    <prop_dir name="config">
      <prop_dir name="provider">
        <prop_dir name="pci">
          <prop_dir name="priv">
            <prop_uint32 data="0" name="print_devices"/>
            <prop_uint32 data="0" name="write_trace"/>
            <prop_uint32 data="0" name="write_enable"/>
          </prop_dir>
          <prop_dir name="device">
            <prop_dir name="eth0_0">
              <prop_string data="byclass/020000/0000" name="pci_name"/>
              <prop_uint32 data="0" name="disable_msi"/>
              <prop_uint32 data="0" name="disable_msix"/>
            </prop_dir>
          </prop_dir>
        </prop_dir>
        <prop_dir name="con">
          <prop_uint64 data="200000" name="poll_rate"/>
        </prop_dir>
        <prop_dir name="ser0">
          <prop_dir name="base">
            <prop_dir name="logging">
              <prop_uint32 data="0" name="verbosity"/>
            </prop_dir>
            <prop_dir name="diag">
              <prop_uint32 data="1" name="verbosity"/>
              <prop_bool data="true" name="config"/>
              <prop_bool data="true" name="resource"/>
              <prop_bool data="true" name="io"/>
              <prop_bool data="true" name="trace"/>
            </prop_dir>
            <prop_size data="0x00200000" name="heap_size"/>
            <prop_uint32 data="0" name="max_timer_count"/>
          </prop_dir>
          <prop_dir name="char">
            <prop_dir name="thread_model">
              <prop_uint32 data="2" name="type"/>
              <prop_uint32 data="8192" name="max_transfer_size"/>
            </prop_dir>
            <prop_uint32 data="1" name="max_file_count"/>
            <prop_uint32 data="1" name="max_fd_count"/>
          </prop_dir>
          <prop_dir name="device">
            <prop_dir name="0">
              <prop_dir name="base">
                <prop_string data="0" name="name"/>
                <prop_uint32 data="3" name="access_mode"/>
                <prop_bool data="false" name="shared"/>
                <prop_uint64 data="0x0FFFFFFFFFFFFFFF" name="read_timeout"/>
                <prop_uint64 data="0x0FFFFFFFFFFFFFFF" name="write_timeout"/>
                <prop_uint32 data="0" name="ref_io_id"/>
              </prop_dir>
              <prop_dir name="ser">
                <prop_uint32 data="0" name="phys_if"/>
                <prop_uint32 data="115200" name="baud_rate"/>
                <prop_uint32 data="2" name="baud_tolerance"/>
                <prop_uint32 data="8" name="data_bits"/>
                <prop_uint32 data="1" name="stop_bits"/>
                <prop_uint32 data="0" name="parity"/>
                <prop_uint32 data="0" name="flow_control"/>
                <prop_uint32 data="0" name="rxfifo_trg"/>
                <prop_uint32 data="512" name="read_fifo_size"/>
                <prop_uint32 data="128" name="write_fifo_size"/>
              </prop_dir>
            </prop_dir>
          </prop_dir>
          <prop_dir name="io">
            <prop_dir name="0">
              <prop_dir name="ser">
                <prop_uint32 data="0" name="clock_speed"/>
              </prop_dir>
              <prop_dir name="base">
                <prop_uint32 data="0" name="io_id"/>
              </prop_dir>
              <prop_dir name="sys">
                <prop_dir name="resource">
                  <prop_link data="psp/io/COM1" name="iores0"/>
                </prop_dir>
                <prop_dir name="interrupt">
                  <prop_link data="psp/int/COM1" name="irq0"/>
                </prop_dir>
              </prop_dir>
            </prop_dir>
          </prop_dir>
          <prop_dir name="priv">
            <prop_dir name="io">
              <prop_dir name="0">
                <prop_uint32 data="1" name="reg_multiplier"/>
                <prop_uint32 data="0" name="address_swap_mask"/>
                <prop_uint32 data="16" name="sampling_rate"/>
              </prop_dir>
            </prop_dir>
          </prop_dir>
        </prop_dir>
        <prop_dir name="eth0">
          <prop_dir name="base">
            <prop_dir name="logging">
              <prop_uint32 data="0" name="verbosity"/>
            </prop_dir>
            <prop_dir name="diag">
              <prop_uint32 data="1" name="verbosity"/>
              <prop_bool data="true" name="config"/>
              <prop_bool data="true" name="resource"/>
              <prop_bool data="true" name="io"/>
              <prop_bool data="true" name="trace"/>
            </prop_dir>
            <prop_size data="0x00300000" name="heap_size"/>
            <prop_uint32 data="10" name="max_timer_count"/>
          </prop_dir>
          <prop_dir name="char">
            <prop_dir name="thread_model">
              <prop_uint32 data="2" name="type"/>
              <prop_uint32 data="1522" name="max_transfer_size"/>
            </prop_dir>
            <prop_uint32 data="4" name="max_file_count"/>
            <prop_uint32 data="4" name="max_fd_count"/>
          </prop_dir>
          <prop_dir name="device">
            <prop_dir name="0">
              <prop_dir name="base">
                <prop_string data="dev0" name="name"/>
                <prop_uint32 data="3" name="access_mode"/>
                <prop_bool data="false" name="shared"/>
                <prop_uint64 data="0x0FFFFFFFFFFFFFFF" name="read_timeout"/>
                <prop_uint64 data="0x0FFFFFFFFFFFFFFF" name="write_timeout"/>
                <prop_uint32 data="0" name="ref_io_id"/>
              </prop_dir>
              <prop_dir name="net">
                <prop_dir name="vchan">
                  <prop_dir name="0">
                    <prop_string data="0" name="name"/>
                    <prop_bin data="00:00:00:00:00:00" name="mac_address"/>
                    <prop_uint32 data="32" name="receive_queue_depth"/>
                    <prop_uint32 data="32" name="send_queue_depth"/>
                    <prop_bool data="false" name="enable_mcast"/>
                    <prop_uint32 data="0" name="mcast_table_size"/>
                  </prop_dir>
                  <prop_dir name="1">
                    <prop_string data="1" name="name"/>
                    <prop_bin data="00:00:00:00:00:00" name="mac_address"/>
                    <prop_uint32 data="32" name="receive_queue_depth"/>
                    <prop_uint32 data="32" name="send_queue_depth"/>
                    <prop_bool data="true" name="enable_mcast"/>
                    <prop_uint32 data="32" name="mcast_table_size"/>
                  </prop_dir>
                  <prop_dir name="2">
                    <prop_string data="2" name="name"/>
                    <prop_bin data="00:00:00:00:00:00" name="mac_address"/>
                    <prop_uint32 data="32" name="receive_queue_depth"/>
                    <prop_uint32 data="32" name="send_queue_depth"/>
                    <prop_bool data="true" name="enable_mcast"/>
                    <prop_uint32 data="32" name="mcast_table_size"/>
                  </prop_dir>
                  <prop_dir name="3">
                    <prop_string data="3" name="name"/>
                    <prop_bin data="00:00:00:00:00:00" name="mac_address"/>
                    <prop_uint32 data="32" name="receive_queue_depth"/>
                    <prop_uint32 data="32" name="send_queue_depth"/>
                    <prop_bool data="false" name="enable_mcast"/>
                    <prop_uint32 data="0" name="mcast_table_size"/>
                  </prop_dir>
                </prop_dir>
                <prop_uint32 data="512" name="mbuf_pool_size"/>
                <prop_bin data="00:00:00:00:00:00" name="mac_address"/>
                <prop_uint32 data="64" name="receive_queue_depth"/>
                <prop_uint32 data="64" name="send_queue_depth"/>
                <prop_bool data="false" name="enable_mcast"/>
                <prop_uint32 data="0" name="mcast_table_size"/>
              </prop_dir>
            </prop_dir>
          </prop_dir>
          <prop_dir name="io">
            <prop_dir name="0">
              <prop_dir name="net">
                <prop_bool data="true" name="aneg"/>
              </prop_dir>
              <prop_dir name="base">
                <prop_uint32 data="0" name="io_id"/>
              </prop_dir>
              <prop_dir name="pci">
                <prop_string data="byclass/020000/0000" name="name"/>
              </prop_dir>
            </prop_dir>
          </prop_dir>
        </prop_dir>
      </prop_dir>
    </prop_dir>
  </properties>
</romimage>
