This a variant to run the nanoStw on ElinOS, as a partition in PikeOS.

In the integrated system, run trdp.sh, so the device files are correctly created first.

Peer Projects:
  trdpStw.app   (Scade integration)
  trdp-P4.sys   (ELinOS system)
  nanoStwX.int  (integration project for fitlet)
  nanoStw.int   (integration project for Qemu)

