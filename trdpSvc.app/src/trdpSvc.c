/*
 ============================================================================
 Name        : trdp-svc.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Version     :
 Copyright   : (c) 2017-2019 with terms of EUPL
 Description : TRDP-stub to wire up Scade models
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
//#include <sys/types.h>
//#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>
#include <tau_xmarshall.h>
#include <tau_xsession.h>

#include "trdpSvc.h"

/* use TAU_XMARSHALL_MAP() in the actual application, here we keep a copy of that */
uint8_t __TAU_XTYPE_MAP[TAU_XTYPE_MAP_SIZE]; /* will be filled from SHM */


static void dprint(const char* lead, const char* msg, int putNL) {
	if ( lead) fputs(lead, stderr);
	if (  msg) fputs(msg,  stderr);
	if (putNL) fputc('\n', stderr);
}

static TRDP_ERR_T init_shm(
		int argc, char **argv,
		TAU_XSESSION_T **trdp, int *fdq,
		struct shm_struct **_shm, struct trdpSvcRec **_itemI, struct trdpSvcRec **_itemO) {

	const char* fnw = QPORTSVCW_FILENAME;
	const char* fnr = QPORTSVCR_FILENAME;
	const char* fnshm = SHM_FILENAME;

	TRDP_ERR_T result = TRDP_NO_ERR;
	struct shm_struct *shm;
	char *xml;
	size_t xlength;
	int fdm;
	uint32_t pages, magic, ifaces;

	if (!trdp | !fdq | !_shm) return TRDP_PARAM_ERR;

	int opt;
	while ((opt = getopt(argc, argv, XSERVICE_OPTARGSTR)) != -1) {
		switch (opt) {
		case 'r':
		   fnr = optarg;
		   break;
		case 'w':
		   fnw = optarg;
		   break;
		case 's':
		   fnshm = optarg;
		   break;
		default: /* '?' */
		   dprint(argv[0], " usage: %s" XSERVICE_OPTARG_USAGE "\n", 0);
		   exit(EXIT_FAILURE);
		}
	}

	vos_printLog(VOS_LOG_INFO, "SHM-INIT: Blocking for signal paths."); /* in Linux, the open-call is blocking */
	fdq[QSVCR] = open(fnr, O_RDONLY /* | O_NONBLOCK */);
	if (!fdq[QSVCR]) {
		vos_printLog(VOS_LOG_ERROR, "Opening \"%s\": %m", fnr);
		return TRDP_MEM_ERR;
	}
	fdq[QSVCW] = open(fnw, O_WRONLY /* | O_NONBLOCK */);
	if (!fdq[QSVCW]) {
		vos_printLog(VOS_LOG_ERROR, "Opening \"%s\": %m", fnw);
		return TRDP_MEM_ERR;
	}

	if (sizeof(magic) != read(fdq[QSVCR], &magic, sizeof(magic))) return TRDP_MEM_ERR;
	if (((magic ^ SHM_MAGIC) & ~0xFF)) return TRDP_MEM_ERR;
	pages = magic & 0xFF;
	vos_printLog(VOS_LOG_DBG, "Found %08X in the pipe => %d pages.", magic, pages);

	fdm = !strchr(fnshm+1, '/') /* Posix-shm-files start with a slash and must not contain further slashes */
		? shm_open(fnshm, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP)
		: open(fnshm, O_RDWR);  /* PikeOS SHM are mapped through device files */

	if (!fdm) {
		vos_printLog(VOS_LOG_ERROR, "Opening \"%s\": %m", fnshm);
		return TRDP_MEM_ERR;
	}

	shm = mmap(NULL, pages * PAGESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fdm, 0);
	close(fdm);  /* once mapped, the fd becomes redundant */
	if (!shm) {
		vos_printLog(VOS_LOG_ERROR, "Mapping \"%s\": %m", fnshm);
		return TRDP_MEM_ERR;
	}

	if ( shm->magic != SHM_MAGIC ) {
		vos_printLog(VOS_LOG_ERROR, "Mapping \"%s\" brought no real magic (0x%08lX).", fnshm, shm->magic);
		return TRDP_MEM_ERR;
	}
	if ( shm->sizeIf != MAX_INTERFACES ) {
		vos_printLog(VOS_LOG_ERROR, "Mapping \"%s\" brought unexpected values (%d != %ld).",
				fnshm, MAX_INTERFACES, shm->sizeIf);
		return TRDP_MEM_ERR;
	}

	/* the payload block of the SHM contains an array of service records. */
	struct trdpSvcRec *item = (struct trdpSvcRec *)shm->payload;
	/*  The last record always exists and contains information about this instance's xml-config-(file) */
	item += shm->sizeIn + shm->sizeOut;
	if ( !item->offset ) return TRDP_PARAM_ERR;
	xml = (char *)shm + item->offset;
	/* if xlength is >MAX_XML_FILENAME_LENGTH, we can load the config directly from SHM: */
	xlength = (item->size <= MAX_XML_FILENAME_LENGTH) ? 0 : item->size;

	/* copy the type-map to be used by xmarshall */
	memcpy(__TAU_XTYPE_MAP, shm->xtype_map, TAU_XTYPE_MAP_SIZE);

	vos_printLog(VOS_LOG_INFO, "LOADING \"%s\":\n", xlength ? "<from SHM>" : xml);
	if (TRDP_NO_ERR != (result = tau_xsession_load( xml , xlength, dprint)))
		return result;
	for (ifaces=0; ifaces<MAX_INTERFACES && *shm->xnetif[ifaces]; ifaces++) {
		vos_printLog(VOS_LOG_INFO, "INTERFACE[%d]: \"%s\"\n", ifaces, shm->xnetif[ifaces]);
		if (TRDP_NO_ERR != (result = tau_xsession_init( &trdp[ifaces], shm->xnetif[ifaces], NULL )))
			return result;
	}

	item = (struct trdpSvcRec *)shm->payload;
	/* now, subscribe / publish the I/Os of the peer application */
	for (int i=0; i<shm->sizeIn; i++, item++) if (item->iface < ifaces) {
		if (item->ComId) {
			result = tau_xsession_subscribe( trdp[item->iface], item->ComId, &item->ID, 1, NULL );
			if (TRDP_NO_ERR != result) return result;
			vos_printLog(VOS_LOG_INFO, "SUBSCRIBED[%d]: <%d>%d[%d]\n", item->ID, item->iface, item->ComId, item->size);
		}
	} else {
		vos_printLog(VOS_LOG_ERROR, "Service item %d/%ld referenced an out-of-index interface %d/%d.\n",
				i, shm->sizeIn, item->iface, ifaces);
		return TRDP_MEM_ERR;
	}
	/* item-pointer is NOT reset, since out-items follow the in-items */
	for (int i=0; i<shm->sizeOut; i++, item++) if (item->iface < ifaces) {
		if (item->ComId) {
			result = tau_xsession_publish( trdp[item->iface], item->ComId, &item->ID, 1,
					(UINT8 *)shm + item->offset, item->size, NULL );
			if (TRDP_NO_ERR != result) return result;
			vos_printLog(VOS_LOG_INFO, "PUBLISHED[%d]: <%d>%d[%d]\n", item->ID, item->iface, item->ComId, item->size);
		}
	} else {
		vos_printLog(VOS_LOG_ERROR, "Service item %d/%ld referenced an out-of-index interface %d/%d.\n",
				i, shm->sizeOut, item->iface, ifaces);
		return TRDP_MEM_ERR;
	}
	/* return the init-token */
	write(fdq[QSVCW], &magic, sizeof(magic));

	/* only set "return-values" at the end, when we are successful */
	*_itemI =  (struct trdpSvcRec *)shm->payload;
	*_itemO = ((struct trdpSvcRec *)shm->payload) + shm->sizeIn;
	*_shm = shm;
	return result;
}

int main(int argc, char **argv) {
	static TAU_XSESSION_T *trdp[MAX_INTERFACES];

	fprintf(stderr, "%s @ %s \n", __FILE__, __TIME__);

	TRDP_ERR_T result;
	struct shm_struct *shm;
	int fdq[2];
	VOS_TIMEVAL_T tv2, tv1, tv3;
	uint64_t t2=0, t3=0, t=15;
	struct trdpSvcRec *itemIn = NULL;
	struct trdpSvcRec *itemOut = NULL;

	/* after this, I need to clean up */
	if (TRDP_NO_ERR == (result = init_shm( argc, argv, trdp, fdq, &shm, &itemIn, &itemOut ))) {

		TRDP_PD_INFO_T info[shm->sizeIn]; /* have a stash for meta-data from received packets */
		VOS_TIMEVAL_T deadline;
		uint32_t cmd[1] = { 0, };
		size_t red = 0;
		do {

			if (red != sizeof(cmd) || !(cmd[0] & QSVC_GETCOM)) {
				red = read(fdq[QSVCR], cmd, sizeof(cmd));
			}
			vos_getTime(&tv1);
			if (red == sizeof(cmd) && (cmd[0] & QSVC_GETCOM)) {

				/* iterate all inputs */
				for (int i=0; i<shm->sizeIn; i++) if (itemIn[i].ComId && itemIn[i].ID >= 0) {
					UINT32 temp;
					tau_xsession_getCom( trdp[itemIn[i].iface], itemIn[i].ID,
							(UINT8 *)shm + itemIn[i].offset, itemIn[i].size, &temp, info+i);
				}

				/* return the lock */
				write(fdq[QSVCW], cmd, sizeof(cmd));
				cmd[0] = 0;
				vos_getTime(&tv2);
				vos_subTime(&tv2, &tv1);
				t2 += tv2.tv_usec;

				/* next read blocks for the Scade model thread */
				red = read(fdq[QSVCR], cmd, sizeof(cmd));
				vos_getTime(&tv1);
			}

			if (red == sizeof(cmd) && (cmd[0] & QSVC_SETCOM)) {

				/* export data of model to TRDP */
				for (int i=0; i<shm->sizeOut; i++) if (itemOut[i].ComId && itemOut[i].ID >= 0) {
					tau_xsession_setCom( trdp[itemOut[i].iface], itemOut[i].ID,
							(UINT8 *)shm + itemOut[i].offset, itemOut[i].size);
				}
				if (cmd[0] == QSVC_SETCOM) {
					write(fdq[QSVCW], &cmd, sizeof(cmd));
					cmd[0] = 0;
				}

				/* iterate all requests for input data */
				for (int i=0; i<shm->sizeIn; i++) {
					if (itemIn[i].request && itemIn[i].iface < MAX_INTERFACES)
						tau_xsession_request( trdp[itemIn[i].iface], itemIn[i].ID);
				}
			} else
				usleep(10000); /* give it minimal sleep in odd conditions */

			/* period handling is left to the safe application */
			deadline.tv_sec  = shm->deadline / 1000000000L;
			deadline.tv_usec = (shm->deadline % 1000000000L)/1000L;
			vos_getTime(&tv3);  /* in Linux, sys-start-based */
			VOS_TIMEVAL_T left = deadline;
			vos_subTime( &left, &tv3 );
			vos_subTime( &tv3, &tv1 );

			t3 += tv3.tv_usec;
			if (++t==16) {
				printf("%5ld us / %5ld us ->| %5ld\r",
						t2/16, (t2+t3)/16, left.tv_sec*1000+left.tv_usec/1000
						); fflush(stdout);
				t=0; t2=0; t3=0;
			}
			tau_xsession_cycle_until(deadline);

		} while (1);
	} else
		vos_printLog(VOS_LOG_ERROR, "%s\n", tau_getResultString(result));

	tau_xsession_delete( NULL );

	return result;
}
