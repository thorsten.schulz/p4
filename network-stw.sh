#!/bin/bash

# This part becomes relevant, when working with QEMU.
# It may fully differ on real setups
# ECN3 is only relevant for the second train.

if [[ $UID != 0 ]]; then
	echo "Please run this network script as root!"
	exit 1
fi

# allow br-qemu
mkdir -p /etc/qemu ; [[ -r /etc/qemu/bridge ]] || ( touch /etc/qemu/bridge.conf ; chgrp kvm /etc/qemu/bridge.conf ) ; [[ $(grep -w "br-qemu" /etc/qemu/bridge.conf) ]] || echo "allow br-qemu" | cat - /etc/qemu/bridge.conf |  tee /etc/qemu/bridge.conf > /dev/null

#allow br-up
mkdir -p /etc/qemu ; [[ -r /etc/qemu/bridge ]] || ( touch /etc/qemu/bridge.conf ; chgrp kvm /etc/qemu/bridge.conf ) ; [[ $(grep -w "br-up" /etc/qemu/bridge.conf) ]] || echo "allow br-up" | cat - /etc/qemu/bridge.conf |  tee /etc/qemu/bridge.conf > /dev/null

#allow br-ecn2
mkdir -p /etc/qemu ; [[ -r /etc/qemu/bridge ]] || ( touch /etc/qemu/bridge.conf ; chgrp kvm /etc/qemu/bridge.conf ) ; [[ $(grep -w "br-ecn2" /etc/qemu/bridge.conf) ]] || echo "allow br-ecn2" | cat - /etc/qemu/bridge.conf |  tee /etc/qemu/bridge.conf > /dev/null

#allow br-ecn3
mkdir -p /etc/qemu ; [[ -r /etc/qemu/bridge ]] || ( touch /etc/qemu/bridge.conf ; chgrp kvm /etc/qemu/bridge.conf ) ; [[ $(grep -w "br-ecn3" /etc/qemu/bridge.conf) ]] || echo "allow br-ecn3" | cat - /etc/qemu/bridge.conf |  tee /etc/qemu/bridge.conf > /dev/null

#clear interfaces
echo "Reseting links, may echo a failure"
ip link del ve1-ecn3
ip link del br-ecn3
ip link del ve1-ecn2
ip link del br-ecn2
ip link del br-qemu
ip link del wg-up
ip link del br-up

#create interfaces
ip link add br-qemu type bridge
ip link add br-up type bridge
ip link add br-ecn2 type bridge
#ip link add ve1-ecn2 type veth peer name ve2-ecn2
#ip link set ve1-ecn2 master br-ecn2
ip link add br-ecn3 type bridge
#ip link add ve1-ecn3 type veth peer name ve2-ecn3
#ip link set ve1-ecn3 master br-ecn3
#ip link add wg-up type wireguard

#hand out addresses
ip addr add 192.168.201.1/24 dev br-qemu
#ip addr add 172.22.0.1/24 dev br-up      #some router on the Uplink at the OCC
#ip addr add 172.21.0.10/24 dev wg-up     #core GG instance
#move address from WG to clear wireguard
ip addr add 172.21.0.10/20 dev br-up

#train-2
#ip addr add 10.0.17.1/24 dev br-ecn2    #some router on ECN
#ip addr add 10.0.17.10/24 dev br-ecn2    #stw
ip addr add 10.0.17.11/24 dev br-ecn2    #Ctrl
ip addr add 10.0.17.18/24 dev br-ecn2    #DMI-A
#ip addr add 10.0.17.19/24 dev br-ecn2    #UI-A
#ip addr add 10.0.18.18/24 dev br-ecn2    #DMI-B
#ip addr add 10.0.18.19/24 dev br-ecn2    #UI-B

#train-3
#ip addr add 10.0.25.1/24 dev br-ecn3    #some router on ECN
#ip addr add 10.0.25.10/32 dev br-ecn3    #stw
ip addr add 10.0.25.11/32 dev br-ecn3    #Ctrl
ip addr add 10.0.25.18/32 dev br-ecn3    #DMI-A
#ip addr add 10.0.25.19/32 dev br-ecn3    #UI-A
#ip addr add 10.0.26.18/32 dev br-ecn2    #DMI-B
#ip addr add 10.0.26.19/32 dev br-ecn2    #UI-B

#wg addconf wg-up recipes-core/non-root-initramfs/files/overlay/etc/trdp/wg?gg.conf

ip link set br-up up
ip link set br-qemu up
#ip link set ve1-ecn2 up
#ip link set ve2-ecn2 up
ip link set br-ecn2 up
#ip link set ve1-ecn3 up
#ip link set ve2-ecn3 up
ip link set br-ecn3 up
#ip link set wg-up up

#tell the kernel where to find our peers
#ip route add 172.21.2.0/24 via 172.21.0.102 dev wg-up    #route to STW of DB train via its router (instead of NATing)
#ip route add 172.21.1.0/24 via 172.21.0.101 dev wg-up
#ip route add 172.21.3.0/24 via 172.21.0.103 dev wg-up


