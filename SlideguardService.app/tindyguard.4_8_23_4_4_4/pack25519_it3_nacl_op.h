/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _pack25519_it3_nacl_op_H_
#define _pack25519_it3_nacl_op_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::pack25519_it3/ */
extern void pack25519_it3_nacl_op(
  /* _L5/, mi1/ */
  kcg_int64 mi1,
  /* _L1/, t/ */
  kcg_int64 t,
  /* _L13/, makku/ */
  kcg_int64 *makku,
  /* _L12/, mo/ */
  kcg_int64 *mo);



#endif /* _pack25519_it3_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack25519_it3_nacl_op.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

