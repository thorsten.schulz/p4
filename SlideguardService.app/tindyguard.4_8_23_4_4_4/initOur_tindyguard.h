/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _initOur_tindyguard_H_
#define _initOur_tindyguard_H_

#include "kcg_types.h"
#include "singleChunk_hash_blake2s.h"
#include "single_hash_blake2s.h"
#include "keyPair_nacl_box_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  kcg_bool init;
  kcg_size /* @1/_/v3/ */ v3_times_3_size;
  SSM_ST_key_retry /* key_retry: */ key_retry_state_nxt;
  kcg_bool /* key_retry: */ key_retry_reset_nxt;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_keyPair_nacl_box_20 /* key_retry:makeKeys:_L1=(nacl::box::keyPair#2)/ */ Context_keyPair_2;
  /* ----------------- no clocks of observable data ------------------ */
} outC_initOur_tindyguard;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::initOur/ */
extern void initOur_tindyguard(
  /* priv/ */
  Key32_slideTypes *priv,
  /* again/ */
  kcg_bool *again,
  /* salted/ */
  KeySalted_tindyguardTypes *salted,
  /* _L40/, failed/, failure/ */
  kcg_bool *failed,
  outC_initOur_tindyguard *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void initOur_reset_tindyguard(outC_initOur_tindyguard *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void initOur_init_tindyguard(outC_initOur_tindyguard *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::initOur/
  @1: (times#3)
*/

#endif /* _initOur_tindyguard_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** initOur_tindyguard.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

