/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _PingPong_tindyguard_test_H_
#define _PingPong_tindyguard_test_H_

#include "kcg_types.h"
#include "filter1_tindyguard_data_23.h"
#include "initPeer_tindyguard.h"
#include "echoTake_icmp_23.h"
#include "initOur_tindyguard.h"
#include "echoRequest_icmp_23.h"
#include "bindings_tindyguard_4_8_23_4_4_4.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  kcg_bool /* trigger/ */ mem_trigger;
  kcg_bool init;
  SSM_ST_SM1 /* SM1: */ SM1_state_nxt;
  length_t_udp /* echoRsplen/ */ echoRsplen;
  array_uint32_16_23 /* echoRspmsg/ */ echoRspmsg;
  size_tindyguardTypes /* goodEchos/ */ goodEchos;
  size_tindyguardTypes /* badEchos/ */ badEchos;
  KeySalted_tindyguardTypes /* ownKey/ */ ownKey;
  size_tindyguardTypes /* reqEchos/ */ reqEchos;
  _3_array /* _L884/, _L892/, updatedPeers/ */ _L884;
  _3_array /* _L907/, initialPeers/ */ _L907;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_initOur_tindyguard /* SM1:peer_setup:_L10=(tindyguard::initOur#3)/ */ Context_initOur_3;
  outC_bindings_tindyguard_4_8_23_4_4_4 /* _L2=(tindyguard::bindings#1)/ */ Context_bindings_1;
  outC_echoRequest_icmp_23 /* IfBlock1:then:_L4=(icmp::echoRequest#2)/ */ Context_echoRequest_2;
  /* ----------------- no clocks of observable data ------------------ */
} outC_PingPong_tindyguard_test;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::test::PingPong/ */
extern void PingPong_tindyguard_test(
  /* trigger/ */
  kcg_bool trigger,
  /* _L54/, shutdown/ */
  kcg_bool shutdown,
  /* Echos/, _L883/ */
  size_tindyguardTypes *Echos,
  /* Seqoo/ */
  kcg_uint32 *Seqoo,
  /* Rechos/, _L895/ */
  size_tindyguardTypes *Rechos,
  /* Bechos/, _L906/ */
  size_tindyguardTypes *Bechos,
  /* _L59/, fail/, failure/ */
  kcg_bool *failure,
  outC_PingPong_tindyguard_test *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void PingPong_reset_tindyguard_test(outC_PingPong_tindyguard_test *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void PingPong_init_tindyguard_test(outC_PingPong_tindyguard_test *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _PingPong_tindyguard_test_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** PingPong_tindyguard_test.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

