/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "pack25519_it1_nacl_op.h"

/* nacl::op::pack25519_it1/ */
void pack25519_it1_nacl_op(
  /* @1/_L1/, @1/p/, _L1/, _L13/, t/ */
  gf_nacl_op *t,
  /* @1/_L8/, @1/po/, _L11/, to/ */
  gf_nacl_op *to)
{
  kcg_size idx;
  gf_nacl_op tmp;
  /* _L27/ */
  kcg_int64 _L27;
  /* _L2/ */
  kcg_int64 _L2;
  /* _L3/ */
  array_int64_15 _L3;
  /* @1/_L7/ */
  kcg_uint64 _L7_sel25519_1;

  _L2 = kcg_lit_int64(-18);
  /* _L2= */
  for (idx = 0; idx < 15; idx++) {
    _L27 = _L2;
    /* _L2=(nacl::op::pack25519_it3#1)/ */
    pack25519_it3_nacl_op(_L27, (*t)[idx], &_L2, &_L3[idx]);
  }
  _L27 = (*t)[15] - kcg_lit_int64(0x7fff) - _L2;
  _L7_sel25519_1 = kcg_lnot_uint64(
      /* @1/_L6= */(kcg_uint64)
        (kcg_lit_int32(1) - /* _L26= */(kcg_int32)
            (kcg_lit_uint64(1) & (/* _L23= */(kcg_uint64) _L27 >> kcg_lit_uint64(16))) -
          kcg_lit_int32(1)));
  kcg_copy_array_int64_15(&tmp[0], &_L3);
  tmp[15] = _L27;
  /* @1/_L8= */
  for (idx = 0; idx < 16; idx++) {
    (*to)[idx] = /* @2/_L17= */(kcg_int64)
        (/* @2/_L14= */(kcg_uint64) (*t)[idx] ^ ((/* @2/_L14= */(kcg_uint64)
                (*t)[idx] ^ /* @2/_L15= */(kcg_uint64) tmp[idx]) &
            /* @2/_L16= */(kcg_uint64) /* @1/_L11= */(kcg_int64) _L7_sel25519_1));
  }
}

/*
  Expanded instances for: nacl::op::pack25519_it1/
  @1: (nacl::op::sel25519#1)
  @2: @1/(nacl::op::sel25519_it1#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack25519_it1_nacl_op.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

