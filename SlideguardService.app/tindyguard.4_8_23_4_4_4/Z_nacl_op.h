/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */
#ifndef _Z_nacl_op_H_
#define _Z_nacl_op_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::Z/ */
extern void Z_nacl_op(
  /* _L1/, a/ */
  gf_nacl_op *a,
  /* _L2/, b/ */
  gf_nacl_op *b,
  /* _L3/, d/ */
  gf_nacl_op *d);



#endif /* _Z_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Z_nacl_op.h
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

