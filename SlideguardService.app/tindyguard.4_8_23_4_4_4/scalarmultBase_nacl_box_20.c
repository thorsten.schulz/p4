/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "scalarmultBase_nacl_box_20.h"

/* nacl::box::scalarmultBase/ */
void scalarmultBase_nacl_box_20(
  /* _L2/, our/ */
  KeyPair32_slideTypes *our_20,
  /* _L3/, q/ */
  array_uint32_8 *q_20,
  /* _L5/, again/ */
  kcg_bool *again_20,
  /* _L6/, failed/ */
  kcg_bool *failed_20,
  outC_scalarmultBase_nacl_box_20 *outC)
{
  /* _L5=(nacl::box::scalarmult#2)/ */
  scalarmult_nacl_box_20(
    our_20,
    (Key32_slideTypes *) &_9_nacl_box,
    kcg_true,
    again_20,
    q_20,
    failed_20,
    &outC->Context_scalarmult_2);
}

#ifndef KCG_USER_DEFINED_INIT
void scalarmultBase_init_nacl_box_20(outC_scalarmultBase_nacl_box_20 *outC)
{
  /* _L5=(nacl::box::scalarmult#2)/ */
  scalarmult_init_nacl_box_20(&outC->Context_scalarmult_2);
}
#endif /* KCG_USER_DEFINED_INIT */


void scalarmultBase_reset_nacl_box_20(outC_scalarmultBase_nacl_box_20 *outC)
{
  /* _L5=(nacl::box::scalarmult#2)/ */
  scalarmult_reset_nacl_box_20(&outC->Context_scalarmult_2);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarmultBase_nacl_box_20.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

