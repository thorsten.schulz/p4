/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _assertGoodKey_nacl_box_H_
#define _assertGoodKey_nacl_box_H_

#include "kcg_types.h"
#include "cmp_M_uint32_8.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::box::assertGoodKey/ */
extern kcg_bool assertGoodKey_nacl_box(/* _L3/, k/ */ Key32_slideTypes *k);

/*
  Expanded instances for: nacl::box::assertGoodKey/
  @1: (math::IntToBool#1)
*/

#endif /* _assertGoodKey_nacl_box_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** assertGoodKey_nacl_box.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

