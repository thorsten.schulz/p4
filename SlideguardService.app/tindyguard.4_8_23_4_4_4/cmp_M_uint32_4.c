/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "cmp_M_uint32_4.h"

/* M::cmp/ */
int_slideTypes cmp_M_uint32_4(
  /* _L2/, x/ */
  array_uint32_4 *x_uint32_4,
  /* _L3/, y/ */
  array_uint32_4 *y_uint32_4)
{
  kcg_uint32 tmp;
  kcg_size idx;
  /* _L9/, failed/ */
  int_slideTypes failed_uint32_4;

  tmp = kcg_lit_uint32(0);
  /* _L1= */
  for (idx = 0; idx < 4; idx++) {
    tmp = tmp | ((*x_uint32_4)[idx] ^ (*y_uint32_4)[idx]);
  }
  failed_uint32_4 = /* _L10= */(kcg_int32)
      (((/* _L12= */(kcg_uint64) tmp - kcg_lit_uint64(1)) >> kcg_lit_uint64(
            32)) & kcg_lit_uint64(1)) - kcg_lit_int32(1);
  return failed_uint32_4;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** cmp_M_uint32_4.c
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

