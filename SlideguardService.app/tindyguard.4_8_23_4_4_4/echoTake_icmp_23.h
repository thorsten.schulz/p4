/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _echoTake_icmp_23_H_
#define _echoTake_icmp_23_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "check_ip_23.h"
#include "ipsum_BEH_ip_23.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* icmp::echoTake/ */
extern void echoTake_icmp_23(
  /* _L1/, len/ */
  length_t_udp len_23,
  /* _L2/, msg/ */
  array_uint32_16_23 *msg_23,
  /* _L50/, host/ */
  IpAddress_udp *host_23,
  /* lenoo/ */
  length_t_udp *lenoo_23,
  /* msgoo/ */
  array_uint32_16_23 *msgoo_23,
  /* seqoo/ */
  kcg_uint32 *seqoo_23,
  /* _L33/, src/ */
  IpAddress_udp *src_23,
  /* id/ */
  kcg_uint32 *id_23,
  /* _L60/, fail1/, failed/ */
  kcg_bool *failed_23,
  /* badChecksum/ */
  kcg_bool *badChecksum_23,
  /* badPayload/ */
  kcg_bool *badPayload_23);



#endif /* _echoTake_icmp_23_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** echoTake_icmp_23.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

