/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "A_nacl_op.h"

/* nacl::op::A/ */
void A_nacl_op(
  /* _L1/, a/ */
  gf_nacl_op *a,
  /* _L2/, b/ */
  gf_nacl_op *b,
  /* _L3/, s/ */
  gf_nacl_op *s)
{
  kcg_size idx;

  /* _L3= */
  for (idx = 0; idx < 16; idx++) {
    (*s)[idx] = (*a)[idx] + (*b)[idx];
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** A_nacl_op.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

