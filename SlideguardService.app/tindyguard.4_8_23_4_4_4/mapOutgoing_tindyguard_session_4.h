/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */
#ifndef _mapOutgoing_tindyguard_session_4_H_
#define _mapOutgoing_tindyguard_session_4_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::mapOutgoing/ */
extern void mapOutgoing_tindyguard_session_4(
  /* _L7/, sid/ */
  size_tindyguardTypes sid_4,
  /* _L5/, length/ */
  array_int32_4 *length_4,
  /* _L8/, txm_sid/ */
  array_int32_4 *txm_sid_4,
  /* _L2/, length_not_taken/ */
  array_int32_4 *length_not_taken_4,
  /* _L3/, length_picked/ */
  array_int32_4 *length_picked_4);



#endif /* _mapOutgoing_tindyguard_session_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapOutgoing_tindyguard_session_4.h
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

