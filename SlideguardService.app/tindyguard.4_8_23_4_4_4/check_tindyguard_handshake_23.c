/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "check_tindyguard_handshake_23.h"

/* tindyguard::handshake::check/ */
void check_tindyguard_handshake_23(
  /* length/ */
  length_t_udp length_23,
  /* hdrmsg/ */
  array_uint32_16_24 *hdrmsg_23,
  /* endpoint/ */
  peer_t_udp *endpoint_23,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_23,
  /* requireMac2/ */
  kcg_bool requireMac2_23,
  /* lengthoo/ */
  length_t_udp *lengthoo_23,
  /* our/ */
  kcg_uint32 *our_23,
  outC_check_tindyguard_handshake_23 *outC)
{
  Mac_hash_blake2s tmp;
  array_uint32_16_3 tmp1;
  array_uint32_16 tmp2;
  Mac_hash_blake2s tmp3;
  array_uint32_16_2 tmp4;
  Mac_hash_blake2s tmp5;
  array_uint32_16_4 tmp6;
  array_uint32_16 tmp7;
  array_uint32_16_2 tmp8;
  Mac_hash_blake2s tmp9;
  array_uint32_16_3 tmp10;
  array_uint32_16 tmp11;
  array_uint32_16_2 tmp12;
  array_uint32_16 tmp13;
  kcg_bool tmp14;
  Mac_hash_blake2s tmp15;
  array_uint32_16_4 tmp16;
  array_uint32_16 tmp17;
  array_uint32_16_2 tmp18;
  kcg_bool tmp19;
  Mac_hash_blake2s tmp20;
  array_uint32_16_3 tmp21;
  array_uint32_16 tmp22;
  array_uint32_16_2 tmp23;
  array_uint32_16 tmp24;
  /* IfBlock1:else: */
  kcg_bool else_clock_IfBlock1_23;
  /* IfBlock1:else:else:then:_L23/,
     IfBlock1:else:else:then:_L46/,
     IfBlock1:else:else:then:_L47/,
     IfBlock1:else:else:then:m_hdr/ */
  array_uint32_48 _L23_then_else_else_IfBlock1_23;
  /* IfBlock1:else:else:then:IfBlock3: */
  kcg_bool IfBlock3_clock_then_else_else_IfBlock1_23;
  /* @5/_L20/, @5/failed/, IfBlock1:else:else:then:IfBlock3:then:then:_L31/ */
  kcg_bool _L31_then_then_IfBlock3_then_else_else_IfBlock1_23;
  /* IfBlock1:else:else:else: */
  kcg_bool else_clock_else_else_IfBlock1_23;
  /* IfBlock1:else:else:else:else: */
  kcg_bool else_clock_else_else_else_IfBlock1_23;
  /* IfBlock1:else:else:, IfBlock2:else: */
  kcg_bool else_clock_else_IfBlock1_23;
  /* @1/_L20/,
     @1/failed/,
     IfBlock1:else:then:IfBlock3:then:then:_L31/,
     IfBlock2:else:else: */
  kcg_bool _L31_then_then_IfBlock3_then_else_IfBlock1_23;
  /* IfBlock1:else:then:IfBlock3:, IfBlock2: */
  kcg_bool IfBlock3_clock_then_else_IfBlock1_23;
  /* IfBlock1:else:then:_L22/,
     IfBlock1:else:then:_L26/,
     IfBlock1:else:then:m_hdr/ */
  array_uint32_64 _L26_then_else_IfBlock1_23;
  /* IfBlock1:, dejavu/ */
  kcg_bool dejavu_23;
  kcg_size idx;

  IfBlock3_clock_then_else_IfBlock1_23 = length_23 > kcg_lit_int32(1488);
  /* IfBlock2: */
  if (IfBlock3_clock_then_else_IfBlock1_23) {
    dejavu_23 = kcg_true;
  }
  else {
    else_clock_else_IfBlock1_23 = length_23 == kcg_lit_int32(148) &&
      (*hdrmsg_23)[0][12] == MsgType_Initiation_tindyguard;
    /* IfBlock2:else: */
    if (else_clock_else_IfBlock1_23) {
      /* IfBlock2:else:then:_L3=(tindyguard::handshake::dejavu_check#7)/ */
      dejavu_check_tindyguard_handshake_16_4(
        (Mac_hash_blake2s *) &(*hdrmsg_23)[2][9],
        &dejavu_23,
        &outC->Context_dejavu_check_7);
    }
    else {
      _L31_then_then_IfBlock3_then_else_IfBlock1_23 = length_23 ==
        kcg_lit_int32(92) && (*hdrmsg_23)[0][12] == MsgType_Response_tindyguard;
      /* IfBlock2:else:else: */
      if (_L31_then_then_IfBlock3_then_else_IfBlock1_23) {
        /* IfBlock2:else:else:then:_L3=(tindyguard::handshake::dejavu_check#6)/ */
        dejavu_check_tindyguard_handshake_16_4(
          (Mac_hash_blake2s *) &(*hdrmsg_23)[1][11],
          &dejavu_23,
          &outC->Context_dejavu_check_6);
      }
      else {
        dejavu_23 = kcg_false;
      }
    }
  }
  /* IfBlock1: */
  if (dejavu_23) {
    *lengthoo_23 = nil_udp;
    *our_23 = kcg_lit_uint32(0);
  }
  else {
    else_clock_IfBlock1_23 = length_23 == kcg_lit_int32(148) &&
      (*hdrmsg_23)[0][12] == MsgType_Initiation_tindyguard;
    /* IfBlock1:else: */
    if (else_clock_IfBlock1_23) {
      kcg_copy_StreamChunk_slideTypes(&tmp1[0], &(*sks_23).hcache.salt);
      kcg_copy_chunk_t_udp(&_L26_then_else_IfBlock1_23[0], &(*hdrmsg_23)[0]);
      kcg_copy_chunk_t_udp(&_L26_then_else_IfBlock1_23[16], &(*hdrmsg_23)[1]);
      kcg_copy_chunk_t_udp(&_L26_then_else_IfBlock1_23[32], &(*hdrmsg_23)[2]);
      kcg_copy_chunk_t_udp(&_L26_then_else_IfBlock1_23[48], &(*hdrmsg_23)[3]);
      kcg_copy_StreamChunk_slideTypes(
        &tmp1[1],
        (StreamChunk_slideTypes *) &_L26_then_else_IfBlock1_23[12]);
      kcg_copy_array_uint32_13(
        &tmp2[0],
        (array_uint32_13 *) &_L26_then_else_IfBlock1_23[28]);
      for (idx = 0; idx < 3; idx++) {
        tmp2[idx + 13] = kcg_lit_uint32(0);
      }
      kcg_copy_StreamChunk_slideTypes(&tmp1[2], &tmp2);
      /* IfBlock1:else:then:_L24=(hash::blake2s::hash128#11)/ */
      hash128_hash_blake2s_3(
        &tmp1,
        kcg_lit_int32(116),
        kcg_lit_int32(32),
        &tmp);
      IfBlock3_clock_then_else_IfBlock1_23 =
        /* IfBlock1:else:then:_L6=(M::cmp#11)/ */
        cmp_M_uint32_4(
          &tmp,
          (array_uint32_4 *) &_L26_then_else_IfBlock1_23[41]) == kcg_lit_int32(
          0);
      /* IfBlock1:else:then:IfBlock3: */
      if (IfBlock3_clock_then_else_IfBlock1_23) {
        /* IfBlock1:else:then:IfBlock3:then: */
        if (requireMac2_23) {
          kcg_copy_array_uint32_16(
            &tmp6[1],
            (array_uint32_16 *) &_L26_then_else_IfBlock1_23[12]);
          kcg_copy_array_uint32_16(
            &tmp6[2],
            (array_uint32_16 *) &_L26_then_else_IfBlock1_23[28]);
          kcg_copy_StreamChunk_slideTypes(&tmp8[0], &(*sks_23).cookies[0].content);
          kcg_copy_StreamChunk_slideTypes(
            &tmp8[1],
            (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
          tmp8[1][0] = (*endpoint_23).addr.addr;
          tmp8[1][1] = /* @2/_L3= */(kcg_uint32) (*endpoint_23).port;
          /* @2/_L10=(hash::blake2s::hash128#1)/ */
          hash128_hash_blake2s_2(
            &tmp8,
            kcg_lit_int32(8),
            kcg_lit_int32(32),
            (Mac_hash_blake2s *) &tmp7[0]);
          for (idx = 0; idx < 12; idx++) {
            tmp7[idx + 4] = kcg_lit_uint32(0);
          }
          kcg_copy_array_uint32_16(&tmp6[0], &tmp7);
          kcg_copy_StreamChunk_slideTypes(
            &tmp6[3],
            (array_uint32_16 *) &ZeroChunk_slideTypes);
          tmp6[3][0] = _L26_then_else_IfBlock1_23[44];
          /* @1/_L26=(hash::blake2s::hash128#2)/ */
          hash128_hash_blake2s_4(
            &tmp6,
            kcg_lit_int32(132),
            kcg_lit_int32(32),
            &tmp5);
          _L31_then_then_IfBlock3_then_else_IfBlock1_23 =
            /* @1/_L28=(M::cmp#1)/ */
            cmp_M_uint32_4(
              &tmp5,
              (array_uint32_4 *) &_L26_then_else_IfBlock1_23[45]) !=
            kcg_lit_int32(0);
          if (_L31_then_then_IfBlock3_then_else_IfBlock1_23) {
            kcg_copy_array_uint32_16(
              &tmp16[1],
              (array_uint32_16 *) &_L26_then_else_IfBlock1_23[12]);
            kcg_copy_array_uint32_16(
              &tmp16[2],
              (array_uint32_16 *) &_L26_then_else_IfBlock1_23[28]);
            kcg_copy_StreamChunk_slideTypes(&tmp18[0], &(*sks_23).cookies[1].content);
            kcg_copy_StreamChunk_slideTypes(
              &tmp18[1],
              (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
            tmp18[1][0] = (*endpoint_23).addr.addr;
            tmp18[1][1] = /* @4/_L3= */(kcg_uint32) (*endpoint_23).port;
            /* @4/_L10=(hash::blake2s::hash128#1)/ */
            hash128_hash_blake2s_2(
              &tmp18,
              kcg_lit_int32(8),
              kcg_lit_int32(32),
              (Mac_hash_blake2s *) &tmp17[0]);
            for (idx = 0; idx < 12; idx++) {
              tmp17[idx + 4] = kcg_lit_uint32(0);
            }
            kcg_copy_array_uint32_16(&tmp16[0], &tmp17);
            kcg_copy_StreamChunk_slideTypes(
              &tmp16[3],
              (array_uint32_16 *) &ZeroChunk_slideTypes);
            tmp16[3][0] = _L26_then_else_IfBlock1_23[44];
            /* @3/_L26=(hash::blake2s::hash128#2)/ */
            hash128_hash_blake2s_4(
              &tmp16,
              kcg_lit_int32(132),
              kcg_lit_int32(32),
              &tmp15);
            tmp14 = /* @3/_L28=(M::cmp#1)/ */
              cmp_M_uint32_4(
                &tmp15,
                (array_uint32_4 *) &_L26_then_else_IfBlock1_23[45]) !=
              kcg_lit_int32(0);
          }
          else {
            tmp14 = kcg_false;
          }
          /* IfBlock1:else:then:IfBlock3:then:then:_L19= */
          if (tmp14) {
            *lengthoo_23 = nil_udp;
          }
          else {
            *lengthoo_23 = length_23;
          }
        }
        else {
          *lengthoo_23 = length_23;
        }
      }
      else {
        *lengthoo_23 = nil_udp;
      }
      *our_23 = kcg_lit_uint32(0);
    }
    else {
      else_clock_else_IfBlock1_23 = length_23 == kcg_lit_int32(92) &&
        (*hdrmsg_23)[0][12] == MsgType_Response_tindyguard;
      /* IfBlock1:else:else: */
      if (else_clock_else_IfBlock1_23) {
        kcg_copy_StreamChunk_slideTypes(&tmp4[0], &(*sks_23).hcache.salt);
        kcg_copy_chunk_t_udp(&_L23_then_else_else_IfBlock1_23[0], &(*hdrmsg_23)[0]);
        kcg_copy_chunk_t_udp(&_L23_then_else_else_IfBlock1_23[16], &(*hdrmsg_23)[1]);
        kcg_copy_chunk_t_udp(&_L23_then_else_else_IfBlock1_23[32], &(*hdrmsg_23)[2]);
        kcg_copy_StreamChunk_slideTypes(
          &tmp4[1],
          (StreamChunk_slideTypes *) &_L23_then_else_else_IfBlock1_23[12]);
        tmp4[1][15] = kcg_lit_uint32(0);
        /* IfBlock1:else:else:then:_L33=(hash::blake2s::hash128#10)/ */
        hash128_hash_blake2s_2(
          &tmp4,
          kcg_lit_int32(60),
          kcg_lit_int32(32),
          &tmp3);
        IfBlock3_clock_then_else_else_IfBlock1_23 =
          /* IfBlock1:else:else:then:_L25=(M::cmp#10)/ */
          cmp_M_uint32_4(
            &tmp3,
            (array_uint32_4 *) &_L23_then_else_else_IfBlock1_23[27]) ==
          kcg_lit_int32(0);
        /* IfBlock1:else:else:then:IfBlock3: */
        if (IfBlock3_clock_then_else_else_IfBlock1_23) {
          /* IfBlock1:else:else:then:IfBlock3:then: */
          if (requireMac2_23) {
            kcg_copy_array_uint32_16(
              &tmp10[1],
              (array_uint32_16 *) &_L23_then_else_else_IfBlock1_23[12]);
            kcg_copy_StreamChunk_slideTypes(&tmp12[0], &(*sks_23).cookies[0].content);
            kcg_copy_StreamChunk_slideTypes(
              &tmp12[1],
              (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
            tmp12[1][0] = (*endpoint_23).addr.addr;
            tmp12[1][1] = /* @6/_L3= */(kcg_uint32) (*endpoint_23).port;
            /* @6/_L10=(hash::blake2s::hash128#1)/ */
            hash128_hash_blake2s_2(
              &tmp12,
              kcg_lit_int32(8),
              kcg_lit_int32(32),
              (Mac_hash_blake2s *) &tmp11[0]);
            for (idx = 0; idx < 12; idx++) {
              tmp11[idx + 4] = kcg_lit_uint32(0);
            }
            kcg_copy_array_uint32_16(&tmp10[0], &tmp11);
            kcg_copy_array_uint32_3(
              &tmp13[0],
              (array_uint32_3 *) &_L23_then_else_else_IfBlock1_23[28]);
            for (idx = 0; idx < 13; idx++) {
              tmp13[idx + 3] = kcg_lit_uint32(0);
            }
            kcg_copy_array_uint32_16(&tmp10[2], &tmp13);
            /* @5/_L26=(hash::blake2s::hash128#1)/ */
            hash128_hash_blake2s_3(
              &tmp10,
              kcg_lit_int32(76),
              kcg_lit_int32(32),
              &tmp9);
            _L31_then_then_IfBlock3_then_else_else_IfBlock1_23 =
              /* @5/_L28=(M::cmp#1)/ */
              cmp_M_uint32_4(
                &tmp9,
                (array_uint32_4 *) &_L23_then_else_else_IfBlock1_23[31]) !=
              kcg_lit_int32(0);
            if (_L31_then_then_IfBlock3_then_else_else_IfBlock1_23) {
              kcg_copy_array_uint32_16(
                &tmp21[1],
                (array_uint32_16 *) &_L23_then_else_else_IfBlock1_23[12]);
              kcg_copy_StreamChunk_slideTypes(&tmp23[0], &(*sks_23).cookies[1].content);
              kcg_copy_StreamChunk_slideTypes(
                &tmp23[1],
                (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
              tmp23[1][0] = (*endpoint_23).addr.addr;
              tmp23[1][1] = /* @8/_L3= */(kcg_uint32) (*endpoint_23).port;
              /* @8/_L10=(hash::blake2s::hash128#1)/ */
              hash128_hash_blake2s_2(
                &tmp23,
                kcg_lit_int32(8),
                kcg_lit_int32(32),
                (Mac_hash_blake2s *) &tmp22[0]);
              for (idx = 0; idx < 12; idx++) {
                tmp22[idx + 4] = kcg_lit_uint32(0);
              }
              kcg_copy_array_uint32_16(&tmp21[0], &tmp22);
              kcg_copy_array_uint32_3(
                &tmp24[0],
                (array_uint32_3 *) &_L23_then_else_else_IfBlock1_23[28]);
              for (idx = 0; idx < 13; idx++) {
                tmp24[idx + 3] = kcg_lit_uint32(0);
              }
              kcg_copy_array_uint32_16(&tmp21[2], &tmp24);
              /* @7/_L26=(hash::blake2s::hash128#1)/ */
              hash128_hash_blake2s_3(
                &tmp21,
                kcg_lit_int32(76),
                kcg_lit_int32(32),
                &tmp20);
              tmp19 = /* @7/_L28=(M::cmp#1)/ */
                cmp_M_uint32_4(
                  &tmp20,
                  (array_uint32_4 *) &_L23_then_else_else_IfBlock1_23[31]) !=
                kcg_lit_int32(0);
            }
            else {
              tmp19 = kcg_false;
            }
            /* IfBlock1:else:else:then:IfBlock3:then:then:_L19= */
            if (tmp19) {
              *lengthoo_23 = nil_udp;
            }
            else {
              *lengthoo_23 = length_23;
            }
            *our_23 = (*hdrmsg_23)[0][14];
          }
          else {
            *lengthoo_23 = length_23;
            *our_23 = (*hdrmsg_23)[0][14];
          }
        }
        else {
          *lengthoo_23 = nil_udp;
          *our_23 = kcg_lit_uint32(0);
        }
      }
      else {
        else_clock_else_else_IfBlock1_23 = length_23 == kcg_lit_int32(64) &&
          (*hdrmsg_23)[0][12] == MsgType_Cookie_tindyguard;
        /* IfBlock1:else:else:else: */
        if (else_clock_else_else_IfBlock1_23) {
          *lengthoo_23 = length_23;
          *our_23 = (*hdrmsg_23)[0][13];
        }
        else {
          else_clock_else_else_else_IfBlock1_23 = length_23 >= kcg_lit_int32(
              32) && length_23 % kcg_lit_int32(16) == kcg_lit_int32(0) &&
            (*hdrmsg_23)[0][12] == MsgType_Data_tindyguard;
          /* IfBlock1:else:else:else:else: */
          if (else_clock_else_else_else_IfBlock1_23) {
            *lengthoo_23 = length_23;
            *our_23 = (*hdrmsg_23)[0][13];
          }
          else {
            *lengthoo_23 = nil_udp;
            *our_23 = kcg_lit_uint32(0);
          }
        }
      }
    }
  }
}

#ifndef KCG_USER_DEFINED_INIT
void check_init_tindyguard_handshake_23(
  outC_check_tindyguard_handshake_23 *outC)
{
  /* IfBlock2:else:else:then:_L3=(tindyguard::handshake::dejavu_check#6)/ */
  dejavu_check_init_tindyguard_handshake_16_4(&outC->Context_dejavu_check_6);
  /* IfBlock2:else:then:_L3=(tindyguard::handshake::dejavu_check#7)/ */
  dejavu_check_init_tindyguard_handshake_16_4(&outC->Context_dejavu_check_7);
}
#endif /* KCG_USER_DEFINED_INIT */


void check_reset_tindyguard_handshake_23(
  outC_check_tindyguard_handshake_23 *outC)
{
  /* IfBlock2:else:else:then:_L3=(tindyguard::handshake::dejavu_check#6)/ */
  dejavu_check_reset_tindyguard_handshake_16_4(&outC->Context_dejavu_check_6);
  /* IfBlock2:else:then:_L3=(tindyguard::handshake::dejavu_check#7)/ */
  dejavu_check_reset_tindyguard_handshake_16_4(&outC->Context_dejavu_check_7);
}

/*
  Expanded instances for: tindyguard::handshake::check/
  @1: (tindyguard::handshake::checkCookieInit#2)
  @2: @1/(tindyguard::handshake::bakeCookie#1)
  @3: (tindyguard::handshake::checkCookieInit#3)
  @4: @3/(tindyguard::handshake::bakeCookie#1)
  @5: (tindyguard::handshake::checkCookieRsp#1)
  @6: @5/(tindyguard::handshake::bakeCookie#1)
  @7: (tindyguard::handshake::checkCookieRsp#2)
  @8: @7/(tindyguard::handshake::bakeCookie#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** check_tindyguard_handshake_23.c
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

