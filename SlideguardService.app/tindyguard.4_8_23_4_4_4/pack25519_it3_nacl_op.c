/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "pack25519_it3_nacl_op.h"

/* nacl::op::pack25519_it3/ */
void pack25519_it3_nacl_op(
  /* _L5/, mi1/ */
  kcg_int64 mi1,
  /* _L1/, t/ */
  kcg_int64 t,
  /* _L13/, makku/ */
  kcg_int64 *makku,
  /* _L12/, mo/ */
  kcg_int64 *mo)
{
  /* _L4/ */
  kcg_int64 _L4;

  _L4 = t - kcg_lit_int64(0xffff) - mi1;
  *makku = /* _L13= */(kcg_int64)
      ((/* _L11= */(kcg_uint64) _L4 >> kcg_lit_uint64(16)) & kcg_lit_uint64(1));
  *mo = /* _L12= */(kcg_int64)
      (kcg_lit_uint64(0xffff) & /* _L11= */(kcg_uint64) _L4);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack25519_it3_nacl_op.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

