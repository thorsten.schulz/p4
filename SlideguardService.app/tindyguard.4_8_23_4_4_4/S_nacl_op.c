/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "S_nacl_op.h"

/* nacl::op::S/ */
void S_nacl_op(/* _L1/, a/ */ gf_nacl_op *a, /* _L4/, o/ */ gf_nacl_op *o)
{
  /* _L4=(nacl::op::M#1)/ */ M_nacl_op(a, a, o);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** S_nacl_op.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

