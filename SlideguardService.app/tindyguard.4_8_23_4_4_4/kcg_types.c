/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

#include "kcg_types.h"

#ifdef kcg_use_array_uint8_12
kcg_bool kcg_comp_array_uint8_12(array_uint8_12 *kcg_c1, array_uint8_12 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 12; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint8_12 */

#ifdef kcg_use_array_int32_4
kcg_bool kcg_comp_array_int32_4(array_int32_4 *kcg_c1, array_int32_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int32_4 */

#ifdef kcg_use_array_uint32_16_24_4
kcg_bool kcg_comp_array_uint32_16_24_4(
  array_uint32_16_24_4 *kcg_c1,
  array_uint32_16_24_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_16_24(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16_24_4 */

#ifdef kcg_use_array_uint32_64
kcg_bool kcg_comp_array_uint32_64(
  array_uint32_64 *kcg_c1,
  array_uint32_64 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 64; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_64 */

#ifdef kcg_use_array_uint32_17
kcg_bool kcg_comp_array_uint32_17(
  array_uint32_17 *kcg_c1,
  array_uint32_17 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 17; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_17 */

#ifdef kcg_use_array_uint32_5
kcg_bool kcg_comp_array_uint32_5(array_uint32_5 *kcg_c1, array_uint32_5 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 5; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_5 */

#ifdef kcg_use_array_uint32_8_3
kcg_bool kcg_comp_array_uint32_8_3(
  array_uint32_8_3 *kcg_c1,
  array_uint32_8_3 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 3; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_8(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_8_3 */

#ifdef kcg_use_array_uint8_32
kcg_bool kcg_comp_array_uint8_32(array_uint8_32 *kcg_c1, array_uint8_32 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 32; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint8_32 */

#ifdef kcg_use_array
kcg_bool kcg_comp_array(array *kcg_c1, array *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 16; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_addrs_t_udp(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array */

#ifdef kcg_use_array_int64_15
kcg_bool kcg_comp_array_int64_15(array_int64_15 *kcg_c1, array_int64_15 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 15; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int64_15 */

#ifdef kcg_use_array_uint32_8
kcg_bool kcg_comp_array_uint32_8(array_uint32_8 *kcg_c1, array_uint32_8 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 8; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_8 */

#ifdef kcg_use_array_uint32_2
kcg_bool kcg_comp_array_uint32_2(array_uint32_2 *kcg_c1, array_uint32_2 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 2; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_2 */

#ifdef kcg_use_array_uint32_16_2
kcg_bool kcg_comp_array_uint32_16_2(
  array_uint32_16_2 *kcg_c1,
  array_uint32_16_2 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 2; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_16(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16_2 */

#ifdef kcg_use_u848_slideTypes
kcg_bool kcg_comp_u848_slideTypes(
  u848_slideTypes *kcg_c1,
  u848_slideTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 8; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint8_4(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_u848_slideTypes */

#ifdef kcg_use_array_uint32_16_4
kcg_bool kcg_comp_array_uint32_16_4(
  array_uint32_16_4 *kcg_c1,
  array_uint32_16_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_16(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16_4 */

#ifdef kcg_use_array_uint32_13
kcg_bool kcg_comp_array_uint32_13(
  array_uint32_13 *kcg_c1,
  array_uint32_13 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 13; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_13 */

#ifdef kcg_use_array_uint32_7
kcg_bool kcg_comp_array_uint32_7(array_uint32_7 *kcg_c1, array_uint32_7 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 7; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_7 */

#ifdef kcg_use__2_array
kcg_bool kcg_comp__2_array(_2_array *kcg_c1, _2_array *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 2; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_CookieJar_tindyguardTypes(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use__2_array */

#ifdef kcg_use_array_uint32_48
kcg_bool kcg_comp_array_uint32_48(
  array_uint32_48 *kcg_c1,
  array_uint32_48 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 48; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_48 */

#ifdef kcg_use_array_uint32_16_23
kcg_bool kcg_comp_array_uint32_16_23(
  array_uint32_16_23 *kcg_c1,
  array_uint32_16_23 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 23; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_16(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16_23 */

#ifdef kcg_use_array_uint8_17
kcg_bool kcg_comp_array_uint8_17(array_uint8_17 *kcg_c1, array_uint8_17 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 17; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint8_17 */

#ifdef kcg_use_array_uint32_16
kcg_bool kcg_comp_array_uint32_16(
  array_uint32_16 *kcg_c1,
  array_uint32_16 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 16; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16 */

#ifdef kcg_use_array_uint32_4
kcg_bool kcg_comp_array_uint32_4(array_uint32_4 *kcg_c1, array_uint32_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_4 */

#ifdef kcg_use_array_bool_4
kcg_bool kcg_comp_array_bool_4(array_bool_4 *kcg_c1, array_bool_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_bool_4 */

#ifdef kcg_use_array_uint32_33
kcg_bool kcg_comp_array_uint32_33(
  array_uint32_33 *kcg_c1,
  array_uint32_33 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 33; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_33 */

#ifdef kcg_use__3_array
kcg_bool kcg_comp__3_array(_3_array *kcg_c1, _3_array *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 8; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_Peer_tindyguardTypes(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use__3_array */

#ifdef kcg_use_array_uint32_8_2
kcg_bool kcg_comp_array_uint32_8_2(
  array_uint32_8_2 *kcg_c1,
  array_uint32_8_2 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 2; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_8(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_8_2 */

#ifdef kcg_use_array_uint32_1
kcg_bool kcg_comp_array_uint32_1(array_uint32_1 *kcg_c1, array_uint32_1 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 1; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_1 */

#ifdef kcg_use_array_int64_31
kcg_bool kcg_comp_array_int64_31(array_int64_31 *kcg_c1, array_int64_31 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 31; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int64_31 */

#ifdef kcg_use_array_uint32_12
kcg_bool kcg_comp_array_uint32_12(
  array_uint32_12 *kcg_c1,
  array_uint32_12 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 12; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_12 */

#ifdef kcg_use_array_uint32_6
kcg_bool kcg_comp_array_uint32_6(array_uint32_6 *kcg_c1, array_uint32_6 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 6; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_6 */

#ifdef kcg_use_array_uint8_16
kcg_bool kcg_comp_array_uint8_16(array_uint8_16 *kcg_c1, array_uint8_16 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 16; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint8_16 */

#ifdef kcg_use__4_array
kcg_bool kcg_comp__4_array(_4_array *kcg_c1, _4_array *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_Session_tindyguardTypes(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use__4_array */

#ifdef kcg_use_array_uint8_4
kcg_bool kcg_comp_array_uint8_4(array_uint8_4 *kcg_c1, array_uint8_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint8_4 */

#ifdef kcg_use_array_uint32_16_1
kcg_bool kcg_comp_array_uint32_16_1(
  array_uint32_16_1 *kcg_c1,
  array_uint32_16_1 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 1; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_16(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16_1 */

#ifdef kcg_use_array_uint32_16_3
kcg_bool kcg_comp_array_uint32_16_3(
  array_uint32_16_3 *kcg_c1,
  array_uint32_16_3 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 3; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_16(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16_3 */

#ifdef kcg_use_array_int32_4_4
kcg_bool kcg_comp_array_int32_4_4(
  array_int32_4_4 *kcg_c1,
  array_int32_4_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_int32_4(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int32_4_4 */

#ifdef kcg_use_array_uint8_4_4
kcg_bool kcg_comp_array_uint8_4_4(
  array_uint8_4_4 *kcg_c1,
  array_uint8_4_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint8_4(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint8_4_4 */

#ifdef kcg_use_array_uint32_4_4
kcg_bool kcg_comp_array_uint32_4_4(
  array_uint32_4_4 *kcg_c1,
  array_uint32_4_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_4(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_4_4 */

#ifdef kcg_use_array_uint8_16_10
kcg_bool kcg_comp_array_uint8_16_10(
  array_uint8_16_10 *kcg_c1,
  array_uint8_16_10 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 10; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint8_16(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint8_16_10 */

#ifdef kcg_use_array_uint32_16_23_4
kcg_bool kcg_comp_array_uint32_16_23_4(
  array_uint32_16_23_4 *kcg_c1,
  array_uint32_16_23_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_16_23(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16_23_4 */

#ifdef kcg_use__5_array
kcg_bool kcg_comp__5_array(_5_array *kcg_c1, _5_array *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_peer_t_udp(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use__5_array */

#ifdef kcg_use_array_uint32_16_23_4_4
kcg_bool kcg_comp_array_uint32_16_23_4_4(
  array_uint32_16_23_4_4 *kcg_c1,
  array_uint32_16_23_4_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_16_23_4(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16_23_4_4 */

#ifdef kcg_use_array_uint32_16_24
kcg_bool kcg_comp_array_uint32_16_24(
  array_uint32_16_24 *kcg_c1,
  array_uint32_16_24 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 24; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_16(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_16_24 */

#ifdef kcg_use_array_uint32_8_5
kcg_bool kcg_comp_array_uint32_8_5(
  array_uint32_8_5 *kcg_c1,
  array_uint32_8_5 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 5; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_Key32_slideTypes(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_8_5 */

#ifdef kcg_use_gf_nacl_op
kcg_bool kcg_comp_gf_nacl_op(gf_nacl_op *kcg_c1, gf_nacl_op *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 16; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_gf_nacl_op */

#ifdef kcg_use_array_uint32_4_4_16
kcg_bool kcg_comp_array_uint32_4_4_16(
  array_uint32_4_4_16 *kcg_c1,
  array_uint32_4_4_16 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 16; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_uint32_4_4(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_4_4_16 */

#ifdef kcg_use_array_uint32_3
kcg_bool kcg_comp_array_uint32_3(array_uint32_3 *kcg_c1, array_uint32_3 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 3; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_3 */

#ifdef kcg_use_array_bool_4_4
kcg_bool kcg_comp_array_bool_4_4(array_bool_4_4 *kcg_c1, array_bool_4_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ && kcg_comp_array_bool_4(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_bool_4_4 */

#ifdef kcg_use_array_uint32_32
kcg_bool kcg_comp_array_uint32_32(
  array_uint32_32 *kcg_c1,
  array_uint32_32 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 32; kcg_ci++) {
    kcg_equ = kcg_equ && (*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci];
  }
  return kcg_equ;
}
#endif /* kcg_use_array_uint32_32 */

#ifdef kcg_use_header_ip
kcg_bool kcg_comp_header_ip(header_ip *kcg_c1, header_ip *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_c1->daddr == kcg_c2->daddr;
  kcg_equ = kcg_equ && kcg_c1->saddr == kcg_c2->saddr;
  kcg_equ = kcg_equ && kcg_c1->check == kcg_c2->check;
  kcg_equ = kcg_equ && kcg_c1->protocol == kcg_c2->protocol;
  kcg_equ = kcg_equ && kcg_c1->ttl == kcg_c2->ttl;
  kcg_equ = kcg_equ && kcg_c1->frag_off == kcg_c2->frag_off;
  kcg_equ = kcg_equ && kcg_c1->id == kcg_c2->id;
  kcg_equ = kcg_equ && kcg_c1->tot_len == kcg_c2->tot_len;
  kcg_equ = kcg_equ && kcg_c1->tos == kcg_c2->tos;
  kcg_equ = kcg_equ && kcg_c1->ihl_version == kcg_c2->ihl_version;
  return kcg_equ;
}
#endif /* kcg_use_header_ip */

#ifdef kcg_use_Session_tindyguardTypes
kcg_bool kcg_comp_Session_tindyguardTypes(
  Session_tindyguardTypes *kcg_c1,
  Session_tindyguardTypes *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_c1->sentKeepAlive == kcg_c2->sentKeepAlive;
  kcg_equ = kcg_equ && kcg_c1->gotKeepAlive == kcg_c2->gotKeepAlive;
  kcg_equ = kcg_equ && kcg_c1->transmissive == kcg_c2->transmissive;
  kcg_equ = kcg_equ && kcg_comp_Peer_tindyguardTypes(
      &kcg_c1->peer,
      &kcg_c2->peer);
  kcg_equ = kcg_equ && kcg_comp_State_tindyguard_handshake(
      &kcg_c1->handshake,
      &kcg_c2->handshake);
  kcg_equ = kcg_equ && kcg_c1->pid == kcg_c2->pid;
  kcg_equ = kcg_equ && kcg_c1->sTime == kcg_c2->sTime;
  kcg_equ = kcg_equ && kcg_c1->txTime == kcg_c2->txTime;
  kcg_equ = kcg_equ && kcg_c1->tx_cnt == kcg_c2->tx_cnt;
  kcg_equ = kcg_equ && kcg_c1->rx_cnt == kcg_c2->rx_cnt;
  kcg_equ = kcg_equ && kcg_c1->rx_bytes == kcg_c2->rx_bytes;
  kcg_equ = kcg_equ && kcg_c1->their == kcg_c2->their;
  kcg_equ = kcg_equ && kcg_c1->our == kcg_c2->our;
  kcg_equ = kcg_equ && kcg_c1->to_cnt_cache == kcg_c2->to_cnt_cache;
  kcg_equ = kcg_equ && kcg_c1->to_cnt == kcg_c2->to_cnt;
  kcg_equ = kcg_equ && kcg_c1->ot_cnt == kcg_c2->ot_cnt;
  kcg_equ = kcg_equ && kcg_comp_secret_tindyguardTypes(&kcg_c1->to, &kcg_c2->to);
  kcg_equ = kcg_equ && kcg_comp_secret_tindyguardTypes(&kcg_c1->ot, &kcg_c2->ot);
  return kcg_equ;
}
#endif /* kcg_use_Session_tindyguardTypes */

#ifdef kcg_use_Peer_tindyguardTypes
kcg_bool kcg_comp_Peer_tindyguardTypes(
  Peer_tindyguardTypes *kcg_c1,
  Peer_tindyguardTypes *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_comp_CookieJar_tindyguardTypes(
      &kcg_c1->cookie,
      &kcg_c2->cookie);
  kcg_equ = kcg_equ && kcg_c1->inhibitInit == kcg_c2->inhibitInit;
  kcg_equ = kcg_equ && kcg_c1->bestSession == kcg_c2->bestSession;
  kcg_equ = kcg_equ && kcg_c1->timedOut == kcg_c2->timedOut;
  kcg_equ = kcg_equ && kcg_comp_TAI_tindyguardTypes(&kcg_c1->tai, &kcg_c2->tai);
  kcg_equ = kcg_equ && kcg_comp_peer_t_udp(&kcg_c1->endpoint, &kcg_c2->endpoint);
  kcg_equ = kcg_equ && kcg_comp_PreHashes_tindyguardTypes(
      &kcg_c1->hcache,
      &kcg_c2->hcache);
  kcg_equ = kcg_equ && kcg_comp_range_t_udp(&kcg_c1->allowed, &kcg_c2->allowed);
  kcg_equ = kcg_equ && kcg_comp_psk_tindyguardTypes(
      &kcg_c1->preshared,
      &kcg_c2->preshared);
  kcg_equ = kcg_equ && kcg_comp_pub_tindyguardTypes(&kcg_c1->tpub, &kcg_c2->tpub);
  return kcg_equ;
}
#endif /* kcg_use_Peer_tindyguardTypes */

#ifdef kcg_use_KeySalted_tindyguardTypes
kcg_bool kcg_comp_KeySalted_tindyguardTypes(
  KeySalted_tindyguardTypes *kcg_c1,
  KeySalted_tindyguardTypes *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_comp__2_array(&kcg_c1->cookies, &kcg_c2->cookies);
  kcg_equ = kcg_equ && kcg_comp_PreHashes_tindyguardTypes(
      &kcg_c1->hcache,
      &kcg_c2->hcache);
  kcg_equ = kcg_equ && kcg_comp_KeyPair32_slideTypes(&kcg_c1->key, &kcg_c2->key);
  return kcg_equ;
}
#endif /* kcg_use_KeySalted_tindyguardTypes */

#ifdef kcg_use_IpAddress_udp
kcg_bool kcg_comp_IpAddress_udp(IpAddress_udp *kcg_c1, IpAddress_udp *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_c1->addr == kcg_c2->addr;
  return kcg_equ;
}
#endif /* kcg_use_IpAddress_udp */

#ifdef kcg_use_PreHashes_tindyguardTypes
kcg_bool kcg_comp_PreHashes_tindyguardTypes(
  PreHashes_tindyguardTypes *kcg_c1,
  PreHashes_tindyguardTypes *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_comp_Hash_hash_blake2s(
      &kcg_c1->cookieHKey,
      &kcg_c2->cookieHKey);
  kcg_equ = kcg_equ && kcg_comp_Hash_hash_blake2s(&kcg_c1->hash1, &kcg_c2->hash1);
  kcg_equ = kcg_equ && kcg_comp_StreamChunk_slideTypes(
      &kcg_c1->salt,
      &kcg_c2->salt);
  return kcg_equ;
}
#endif /* kcg_use_PreHashes_tindyguardTypes */

#ifdef kcg_use_scalMulAcc_nacl_box
kcg_bool kcg_comp_scalMulAcc_nacl_box(
  scalMulAcc_nacl_box *kcg_c1,
  scalMulAcc_nacl_box *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_comp_gf_nacl_op(&kcg_c1->a4, &kcg_c2->a4);
  kcg_equ = kcg_equ && kcg_comp_gf_nacl_op(&kcg_c1->a3, &kcg_c2->a3);
  kcg_equ = kcg_equ && kcg_comp_gf_nacl_op(&kcg_c1->a2, &kcg_c2->a2);
  kcg_equ = kcg_equ && kcg_comp_gf_nacl_op(&kcg_c1->a1, &kcg_c2->a1);
  kcg_equ = kcg_equ && kcg_c1->i == kcg_c2->i;
  return kcg_equ;
}
#endif /* kcg_use_scalMulAcc_nacl_box */

#ifdef kcg_use_State_tindyguard_handshake
kcg_bool kcg_comp_State_tindyguard_handshake(
  State_tindyguard_handshake *kcg_c1,
  State_tindyguard_handshake *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_comp_HashChunk_hash_blake2s(
      &kcg_c1->chainingKey,
      &kcg_c2->chainingKey);
  kcg_equ = kcg_equ && kcg_comp_Hash_hash_blake2s(&kcg_c1->ihash, &kcg_c2->ihash);
  kcg_equ = kcg_equ && kcg_comp_Key32_slideTypes(&kcg_c1->their, &kcg_c2->their);
  kcg_equ = kcg_equ && kcg_comp_KeyPair32_slideTypes(
      &kcg_c1->ephemeral,
      &kcg_c2->ephemeral);
  return kcg_equ;
}
#endif /* kcg_use_State_tindyguard_handshake */

#ifdef kcg_use_KeyPair32_slideTypes
kcg_bool kcg_comp_KeyPair32_slideTypes(
  KeyPair32_slideTypes *kcg_c1,
  KeyPair32_slideTypes *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_comp_Key32_slideTypes(&kcg_c1->pk_y, &kcg_c2->pk_y);
  kcg_equ = kcg_equ && kcg_comp_Key32_slideTypes(&kcg_c1->sk_x, &kcg_c2->sk_x);
  return kcg_equ;
}
#endif /* kcg_use_KeyPair32_slideTypes */

#ifdef kcg_use_range_t_udp
kcg_bool kcg_comp_range_t_udp(range_t_udp *kcg_c1, range_t_udp *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_comp_IpAddress_udp(&kcg_c1->mask, &kcg_c2->mask);
  kcg_equ = kcg_equ && kcg_comp_IpAddress_udp(&kcg_c1->net, &kcg_c2->net);
  return kcg_equ;
}
#endif /* kcg_use_range_t_udp */

#ifdef kcg_use_peer_t_udp
kcg_bool kcg_comp_peer_t_udp(peer_t_udp *kcg_c1, peer_t_udp *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_c1->mtime == kcg_c2->mtime;
  kcg_equ = kcg_equ && kcg_c1->time == kcg_c2->time;
  kcg_equ = kcg_equ && kcg_c1->port == kcg_c2->port;
  kcg_equ = kcg_equ && kcg_comp_IpAddress_udp(&kcg_c1->addr, &kcg_c2->addr);
  return kcg_equ;
}
#endif /* kcg_use_peer_t_udp */

#ifdef kcg_use_CookieJar_tindyguardTypes
kcg_bool kcg_comp_CookieJar_tindyguardTypes(
  CookieJar_tindyguardTypes *kcg_c1,
  CookieJar_tindyguardTypes *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_c1->opened == kcg_c2->opened;
  kcg_equ = kcg_equ && kcg_comp_StreamChunk_slideTypes(
      &kcg_c1->content,
      &kcg_c2->content);
  return kcg_equ;
}
#endif /* kcg_use_CookieJar_tindyguardTypes */

#ifdef kcg_use_addrs_t_udp
kcg_bool kcg_comp_addrs_t_udp(addrs_t_udp *kcg_c1, addrs_t_udp *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ && kcg_c1->broadcast == kcg_c2->broadcast;
  kcg_equ = kcg_equ && kcg_c1->p2p == kcg_c2->p2p;
  kcg_equ = kcg_equ && kcg_c1->mc == kcg_c2->mc;
  kcg_equ = kcg_equ && kcg_c1->up == kcg_c2->up;
  kcg_equ = kcg_equ && kcg_comp_IpAddress_udp(&kcg_c1->ip, &kcg_c2->ip);
  return kcg_equ;
}
#endif /* kcg_use_addrs_t_udp */

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_types.c
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

