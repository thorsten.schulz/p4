/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _hashChunk_hash_blake2s_2_8_H_
#define _hashChunk_hash_blake2s_2_8_H_

#include "kcg_types.h"
#include "stream_it_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hashChunk/ */
extern void hashChunk_hash_blake2s_2_8(
  /* _L64/, msg/ */
  array_uint32_16_2 *msg_2_8,
  /* _L66/, len/ */
  size_slideTypes len_2_8,
  /* _L70/, keybytes/ */
  size_slideTypes keybytes_2_8,
  /* _L65/, chunk/ */
  HashChunk_hash_blake2s *chunk_2_8);



#endif /* _hashChunk_hash_blake2s_2_8_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hashChunk_hash_blake2s_2_8.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

