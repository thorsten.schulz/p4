/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */
#ifndef _KCG_CONSTS_H_
#define _KCG_CONSTS_H_

#include "kcg_types.h"

/* icmp::ECHOREPLY/ */
#define ECHOREPLY_icmp (kcg_lit_uint8(0))

/* udp::ipBroadcast/ */
extern const IpAddress_udp ipBroadcast_udp;

/* icmp::payload/ */
extern const array_uint32_5 payload_icmp;

/* icmp::ECHO/ */
#define ECHO_icmp (kcg_lit_uint8(8))

/* icmp::payloadlen/ */
#define payloadlen_icmp (kcg_lit_int32(250) - kcg_lit_int32(16))

/* udp::chunkBytes/ */
#define chunkBytes_udp (kcg_lit_int32(16) * kcg_lit_int32(4))

/* udp::chunkLength/ */
#define chunkLength_udp (kcg_lit_int32(16))

/* ip::HEADER_icmp/ */
extern const header_ip HEADER_icmp_ip;

/* ip::PROTOCOL_icmp/ */
#define PROTOCOL_icmp_ip (kcg_lit_uint8(1))

/* udp::anyAddress/ */
extern const range_t_udp anyAddress_udp;

/* icmp::HeaderBytes/ */
#define HeaderBytes_icmp (kcg_lit_int32(16))

/* ip::headerBytes/ */
#define headerBytes_ip (kcg_lit_int32(20))

/* tindyguard::test::Slider_endpoint/ */
extern const peer_t_udp Slider_endpoint_tindyguard_test;

/* tindyguard::test::rpi_pub/ */
extern const pub_tindyguardTypes rpi_pub_tindyguard_test;

/* tindyguard::test::rpi_allowed/ */
extern const range_t_udp rpi_allowed_tindyguard_test;

/* tindyguard::test::rpi_endpoint/ */
extern const peer_t_udp rpi_endpoint_tindyguard_test;

/* tindyguard::test::Lani_endpoint/ */
extern const peer_t_udp Lani_endpoint_tindyguard_test;

/* tindyguard::test::Slider_priv/ */
extern const secret_tindyguardTypes Slider_priv_tindyguard_test;

/* tindyguard::test::BigZ_endpoint/ */
extern const peer_t_udp BigZ_endpoint_tindyguard_test;

/* tindyguard::test::Lani_pub/ */
extern const pub_tindyguardTypes Lani_pub_tindyguard_test;

/* tindyguard::test::net10_allowed/ */
extern const range_t_udp net10_allowed_tindyguard_test;

/* tindyguard::test::preshared/ */
extern const secret_tindyguardTypes preshared_tindyguard_test;

/* tindyguard::test::ChickenJoeLocal_endpoint/ */
extern const peer_t_udp ChickenJoeLocal_endpoint_tindyguard_test;

/* tindyguard::test::BigZ_pub/ */
extern const pub_tindyguardTypes BigZ_pub_tindyguard_test;

/* tindyguard::test::ChickenJoe_allowed/ */
extern const range_t_udp ChickenJoe_allowed_tindyguard_test;

/* tindyguard::test::ChickenJoe_ip/ */
extern const IpAddress_udp ChickenJoe_ip_tindyguard_test;

/* tindyguard::test::ChickenJoe_pub/ */
extern const pub_tindyguardTypes ChickenJoe_pub_tindyguard_test;

/* tindyguard::test::BigZ_allowed/ */
extern const range_t_udp BigZ_allowed_tindyguard_test;

/* tindyguard::test::BigZ_ip/ */
extern const IpAddress_udp BigZ_ip_tindyguard_test;

/* tindyguard::test::rpi_ip/ */
extern const IpAddress_udp rpi_ip_tindyguard_test;

/* tindyguard::test::Slider_ip/ */
extern const IpAddress_udp Slider_ip_tindyguard_test;

/* tindyguard::test::dimTxBuffer/ */
#define dimTxBuffer_tindyguard_test (kcg_lit_int32(4))

/* tindyguard::test::dimRx/ */
#define dimRx_tindyguard_test (kcg_lit_int32(4))

/* tindyguard::test::dimSession/ */
#define dimSession_tindyguard_test (kcg_lit_int32(4))

/* tindyguard::test::dimData/ */
#define dimData_tindyguard_test (kcg_lit_int32(23))

/* tindyguard::test::dimPeer/ */
#define dimPeer_tindyguard_test (kcg_lit_int32(8))

/* tindyguard::test::dimLatch/ */
#define dimLatch_tindyguard_test (kcg_lit_int32(4))

/* tindyguardTypes::EmptyKey/ */
extern const KeySalted_tindyguardTypes EmptyKey_tindyguardTypes;

/* tindyguard::MAC1_label/ */
extern const array_uint32_2 MAC1_label_tindyguard;

/* tindyguard::CONSTRUCTION_IDENTIFIER_hash/ */
extern const array_uint32_8 CONSTRUCTION_IDENTIFIER_hash_tindyguard;

/* tindyguard::COOKIE_label/ */
extern const array_uint32_2 COOKIE_label_tindyguard;

/* udp::invalidSocket/ */
#define invalidSocket_udp (kcg_lit_int32(-1))

/* udp::addrsLength/ */
#define addrsLength_udp (kcg_lit_int32(16))

/* tindyguard::data::RXoffs/ */
#define RXoffs_tindyguard_data (kcg_lit_int32(48))

/* tindyguard::conf::dimDejavuDepth/ */
#define dimDejavuDepth_tindyguard_conf (kcg_lit_uint32(4))

/* tindyguard::conf::dimDejavuBuckets/ */
#define dimDejavuBuckets_tindyguard_conf (kcg_lit_uint32(16))

/* tindyguard::MsgType_Cookie/ */
#define MsgType_Cookie_tindyguard (kcg_lit_uint32(3))

/* tindyguard::conf::retryHandshake/ */
#define retryHandshake_tindyguard_conf (kcg_lit_int32(20))

/* tindyguard::WG_Retry_Handshake/ */
#define WG_Retry_Handshake_tindyguard (kcg_lit_int32(20))

/* tindyguard::conf::retryKey/ */
#define retryKey_tindyguard_conf (kcg_lit_int32(3))

/* slideTypes::StreamChunkBytes/ */
#define StreamChunkBytes_slideTypes (kcg_lit_int32(16) * kcg_lit_int32(4))

/* tindyguard::conf::signalKeepAlive/ */
#define signalKeepAlive_tindyguard_conf kcg_true

/* tindyguard::MsgType_Data/ */
#define MsgType_Data_tindyguard (kcg_lit_uint32(4))

/* nacl::onetime::macLength/ */
#define macLength_nacl_onetime (kcg_lit_int32(16))

/* tindyguardTypes::FatalTAIage/ */
#define FatalTAIage_tindyguardTypes                                           \
  (kcg_lit_uint64(60) * kcg_lit_uint64(60) * kcg_lit_uint64(24))

/* tindyguard::CONSTRUCTION_chunk/ */
extern const StreamChunk_slideTypes CONSTRUCTION_chunk_tindyguard;

/* tindyguard::MsgType_Initiation/ */
#define MsgType_Initiation_tindyguard (kcg_lit_uint32(1))

/* nacl::onetime::kMask32/ */
extern const array_uint32_4 kMask32_nacl_onetime;

/* nacl::onetime::minusp/ */
extern const array_uint8_17 minusp_nacl_onetime;

/* nacl::onetime::macBytes/ */
#define macBytes_nacl_onetime (kcg_lit_uint32(16))

/* nacl::core::sigma/ */
extern const array_uint32_4 sigma_nacl_core;

/* nacl::core::chacha::NonceBytes/ */
#define NonceBytes_nacl_core_chacha (kcg_lit_uint32(12))

/* nacl::core::chacha::DEFAULT_Rounds/ */
#define DEFAULT_Rounds_nacl_core_chacha (kcg_lit_uint32(20))

/* tindyguardTypes::EmptySession/ */
extern const Session_tindyguardTypes EmptySession_tindyguardTypes;

/* tindyguard::conf::cookieEdible/ */
#define cookieEdible_tindyguard_conf                                          \
  (kcg_lit_int64(120) * kcg_lit_int64(1000000000))

/* tindyguard::WG_Cookie_Rotten_After_Time/ */
#define WG_Cookie_Rotten_After_Time_tindyguard                                \
  (kcg_lit_int64(120) * kcg_lit_int64(1000000000))

/* hash::libsha::HMAC_outer_xor/ */
#define HMAC_outer_xor_hash_libsha (kcg_lit_uint32(0x5c5c5c5c))

/* hash::libsha::HMAC_inner_xor/ */
#define HMAC_inner_xor_hash_libsha (kcg_lit_uint32(0x36363636))

/* hash::blake2s::sigma/ */
extern const array_uint8_16_10 sigma_hash_blake2s;

/* hash::blake2s::hBlockLength/ */
#define hBlockLength_hash_blake2s (kcg_lit_int32(64) / kcg_lit_int32(4))

/* hash::blake2s::IV/ */
extern const array_uint32_8 IV_hash_blake2s;

/* hash::blake2s::hBlockBytes/ */
#define hBlockBytes_hash_blake2s (kcg_lit_int32(64))

/* hash::blake2s::Bytes/ */
#define Bytes_hash_blake2s (kcg_lit_int32(32))

/* slideTypes::ZeroChunk/ */
extern const StreamChunk_slideTypes ZeroChunk_slideTypes;

/* slideTypes::ZeroKeyPair/ */
extern const KeyPair32_slideTypes ZeroKeyPair_slideTypes;

/* nacl::box::badKeys/ */
#define badKeys_nacl_box (kcg_lit_int32(5))

/* nacl::box::badKey/ */
extern const array_uint32_8_5 badKey_nacl_box;

/* nacl::box::bad2/ */
extern const Key32_slideTypes bad2_nacl_box;

/* nacl::box::bad1/ */
extern const Key32_slideTypes bad1_nacl_box;

/* nacl::box::invKey/ */
extern const Key32_slideTypes invKey_nacl_box;

/* nacl::box::oneKey/ */
extern const Key32_slideTypes oneKey_nacl_box;

/* nacl::box::zeroKey/ */
extern const Key32_slideTypes zeroKey_nacl_box;

/* nacl::op::_121665/ */
extern const gf_nacl_op _121665_nacl_op;

/* nacl::box::turnsInvEquiv/ */
#define turnsInvEquiv_nacl_box (kcg_lit_int32(6))

/* nacl::box::TURNS_scalarM_Single/ */
#define TURNS_scalarM_Single_nacl_box (kcg_lit_int32(21))

/* nacl::box::initialScalMulAcc/ */
extern const scalMulAcc_nacl_box initialScalMulAcc_nacl_box;

/* nacl::op::gf0/ */
extern const gf_nacl_op gf0_nacl_op;

/* nacl::op::gf1/ */
extern const gf_nacl_op gf1_nacl_op;

/* nacl::box::_9/ */
extern const array_uint32_8 _9_nacl_box;

/* nacl::box::TURNS_scalarM_Donna/ */
#define TURNS_scalarM_Donna_nacl_box (kcg_lit_int32(20))

/* tindyguard::conf::fixedEphemeral/ */
extern const secret_tindyguardTypes fixedEphemeral_tindyguard_conf;

/* tindyguard::conf::isFixedE/ */
#define isFixedE_tindyguard_conf kcg_false

/* tindyguard::MsgType_Response/ */
#define MsgType_Response_tindyguard (kcg_lit_uint32(2))

/* tindyguard::handshake::dimHS/ */
#define dimHS_tindyguard_handshake (kcg_lit_int32(4))

/* tindyguardTypes::EmptyPeer/ */
extern const Peer_tindyguardTypes EmptyPeer_tindyguardTypes;

/* udp::emptyPeer/ */
extern const peer_t_udp emptyPeer_udp;

/* udp::ipAny/ */
extern const IpAddress_udp ipAny_udp;

/* tindyguardTypes::EmptyJar/ */
extern const CookieJar_tindyguardTypes EmptyJar_tindyguardTypes;

/* tindyguard::handshake::EmptyState/ */
extern const State_tindyguard_handshake EmptyState_tindyguard_handshake;

/* tindyguard::session::waitForMoreBeforeKeepAlive/ */
#define waitForMoreBeforeKeepAlive_tindyguard_session                         \
  (kcg_lit_int64(10) * kcg_lit_int64(1000000000))

/* tindyguard::session::waitForResponseBeforeRetry/ */
#define waitForResponseBeforeRetry_tindyguard_session                         \
  (kcg_lit_int64(5) * kcg_lit_int64(1000000000) + kcg_lit_int64(5) *          \
    kcg_lit_int64(1000000000) / kcg_lit_int64(20))

/* tindyguard::WG_Rekey_After_Time/ */
#define WG_Rekey_After_Time_tindyguard                                        \
  (kcg_lit_int64(120) * kcg_lit_int64(1000000000))

/* tindyguard::WG_Keepalive_Timeout/ */
#define WG_Keepalive_Timeout_tindyguard                                       \
  (kcg_lit_int64(10) * kcg_lit_int64(1000000000))

/* tindyguard::WG_Reject_After_Time/ */
#define WG_Reject_After_Time_tindyguard                                       \
  (kcg_lit_int64(180) * kcg_lit_int64(1000000000))

/* tindyguard::WG_Rekey_Timeout/ */
#define WG_Rekey_Timeout_tindyguard                                           \
  (kcg_lit_int64(5) * kcg_lit_int64(1000000000))

/* tindyguard::Ticks_per_Sec/ */
#define Ticks_per_Sec_tindyguard (kcg_lit_int64(1000000000))

/* udp::Zero/ */
extern const chunk_t_udp Zero_udp;

/* tindyguardTypes::PeerFromRxPacket/ */
#define PeerFromRxPacket_tindyguardTypes (kcg_lit_int32(-1) - kcg_lit_int32(1))

/* tindyguard::session::nil/ */
#define nil_tindyguard_session (kcg_lit_int32(-1))

/* tindyguard::conf::queueToAnySession/ */
#define queueToAnySession_tindyguard_conf kcg_true

/* slideTypes::StreamChunkLength/ */
#define StreamChunkLength_slideTypes (kcg_lit_int32(16))

/* hash::blake2s::Length/ */
#define Length_hash_blake2s (kcg_lit_int32(8))

/* slideTypes::KeyLength/ */
#define KeyLength_slideTypes (kcg_lit_int32(32) / kcg_lit_int32(4))

/* nacl::core::KeyLength/ */
#define KeyLength_nacl_core (kcg_lit_int32(32) / kcg_lit_int32(4))

/* nacl::core::KeyBytes/ */
#define KeyBytes_nacl_core (kcg_lit_int32(32))

/* tindyguardTypes::InvalidPeer/ */
#define InvalidPeer_tindyguardTypes (kcg_lit_int32(-1))

/* tindyguardTypes::nil/ */
#define nil_tindyguardTypes (kcg_lit_int32(-1))

/* udp::nil/ */
#define nil_udp (kcg_lit_int32(-1))

#endif /* _KCG_CONSTS_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_consts.h
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

