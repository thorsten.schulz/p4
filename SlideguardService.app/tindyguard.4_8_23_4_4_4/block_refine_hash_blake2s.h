/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _block_refine_hash_blake2s_H_
#define _block_refine_hash_blake2s_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::block_refine/ */
extern void block_refine_hash_blake2s(
  /* _L19/, _L20/, h/ */
  array_uint32_8 *h,
  /* _L23/, cumulated/ */
  size_slideTypes cumulated,
  /* @1/m/,
     @10/m/,
     @11/m/,
     @12/m/,
     @13/m/,
     @14/m/,
     @15/m/,
     @16/m/,
     @17/m/,
     @18/m/,
     @19/m/,
     @2/m/,
     @20/m/,
     @21/m/,
     @22/m/,
     @23/m/,
     @24/m/,
     @25/m/,
     @26/m/,
     @27/m/,
     @28/m/,
     @29/m/,
     @3/m/,
     @30/m/,
     @31/m/,
     @32/m/,
     @33/m/,
     @34/m/,
     @35/m/,
     @36/m/,
     @37/m/,
     @38/m/,
     @39/m/,
     @4/m/,
     @40/m/,
     @41/m/,
     @42/m/,
     @43/m/,
     @44/m/,
     @45/m/,
     @46/m/,
     @47/m/,
     @48/m/,
     @49/m/,
     @5/m/,
     @50/m/,
     @51/m/,
     @52/m/,
     @53/m/,
     @54/m/,
     @55/m/,
     @56/m/,
     @57/m/,
     @58/m/,
     @59/m/,
     @6/m/,
     @60/m/,
     @61/m/,
     @62/m/,
     @63/m/,
     @64/m/,
     @65/m/,
     @66/m/,
     @67/m/,
     @68/m/,
     @69/m/,
     @7/m/,
     @70/m/,
     @71/m/,
     @72/m/,
     @73/m/,
     @74/m/,
     @75/m/,
     @76/m/,
     @77/m/,
     @78/m/,
     @79/m/,
     @8/m/,
     @80/m/,
     @81/m/,
     @82/m/,
     @83/m/,
     @84/m/,
     @85/m/,
     @86/m/,
     @87/m/,
     @88/m/,
     @89/m/,
     @9/m/,
     @90/m/,
     m/ */
  array_uint32_16 *m,
  /* _L31/, more/ */
  kcg_bool more,
  /* _L16/, hoo/ */
  array_uint32_8 *hoo);

/*
  Expanded instances for: hash::blake2s::block_refine/
  @1: (hash::blake2s::ROUND#1)
  @2: @1/(hash::blake2s::G#6)
  @3: @1/(hash::blake2s::G#7)
  @4: @1/(hash::blake2s::G#8)
  @5: @1/(hash::blake2s::G#9)
  @6: @1/(hash::blake2s::G#5)
  @7: @1/(hash::blake2s::G#4)
  @8: @1/(hash::blake2s::G#3)
  @9: @1/(hash::blake2s::G#1)
  @10: (hash::blake2s::ROUND#2)
  @11: @10/(hash::blake2s::G#6)
  @12: @10/(hash::blake2s::G#7)
  @13: @10/(hash::blake2s::G#8)
  @14: @10/(hash::blake2s::G#9)
  @15: @10/(hash::blake2s::G#5)
  @16: @10/(hash::blake2s::G#4)
  @17: @10/(hash::blake2s::G#3)
  @18: @10/(hash::blake2s::G#1)
  @19: (hash::blake2s::ROUND#4)
  @20: @19/(hash::blake2s::G#6)
  @21: @19/(hash::blake2s::G#7)
  @22: @19/(hash::blake2s::G#8)
  @23: @19/(hash::blake2s::G#9)
  @24: @19/(hash::blake2s::G#5)
  @25: @19/(hash::blake2s::G#4)
  @26: @19/(hash::blake2s::G#3)
  @27: @19/(hash::blake2s::G#1)
  @28: (hash::blake2s::ROUND#3)
  @29: @28/(hash::blake2s::G#6)
  @30: @28/(hash::blake2s::G#7)
  @31: @28/(hash::blake2s::G#8)
  @32: @28/(hash::blake2s::G#9)
  @33: @28/(hash::blake2s::G#5)
  @34: @28/(hash::blake2s::G#4)
  @35: @28/(hash::blake2s::G#3)
  @36: @28/(hash::blake2s::G#1)
  @37: (hash::blake2s::ROUND#12)
  @38: @37/(hash::blake2s::G#6)
  @39: @37/(hash::blake2s::G#7)
  @40: @37/(hash::blake2s::G#8)
  @41: @37/(hash::blake2s::G#9)
  @42: @37/(hash::blake2s::G#5)
  @43: @37/(hash::blake2s::G#4)
  @44: @37/(hash::blake2s::G#3)
  @45: @37/(hash::blake2s::G#1)
  @46: (hash::blake2s::ROUND#11)
  @47: @46/(hash::blake2s::G#6)
  @48: @46/(hash::blake2s::G#7)
  @49: @46/(hash::blake2s::G#8)
  @50: @46/(hash::blake2s::G#9)
  @51: @46/(hash::blake2s::G#5)
  @52: @46/(hash::blake2s::G#4)
  @53: @46/(hash::blake2s::G#3)
  @54: @46/(hash::blake2s::G#1)
  @55: (hash::blake2s::ROUND#10)
  @56: @55/(hash::blake2s::G#6)
  @57: @55/(hash::blake2s::G#7)
  @58: @55/(hash::blake2s::G#8)
  @59: @55/(hash::blake2s::G#9)
  @60: @55/(hash::blake2s::G#5)
  @61: @55/(hash::blake2s::G#4)
  @62: @55/(hash::blake2s::G#3)
  @63: @55/(hash::blake2s::G#1)
  @64: (hash::blake2s::ROUND#9)
  @65: @64/(hash::blake2s::G#6)
  @66: @64/(hash::blake2s::G#7)
  @67: @64/(hash::blake2s::G#8)
  @68: @64/(hash::blake2s::G#9)
  @69: @64/(hash::blake2s::G#5)
  @70: @64/(hash::blake2s::G#4)
  @71: @64/(hash::blake2s::G#3)
  @72: @64/(hash::blake2s::G#1)
  @73: (hash::blake2s::ROUND#14)
  @74: @73/(hash::blake2s::G#6)
  @75: @73/(hash::blake2s::G#7)
  @76: @73/(hash::blake2s::G#8)
  @77: @73/(hash::blake2s::G#9)
  @78: @73/(hash::blake2s::G#5)
  @79: @73/(hash::blake2s::G#4)
  @80: @73/(hash::blake2s::G#3)
  @81: @73/(hash::blake2s::G#1)
  @82: (hash::blake2s::ROUND#13)
  @83: @82/(hash::blake2s::G#6)
  @84: @82/(hash::blake2s::G#7)
  @85: @82/(hash::blake2s::G#8)
  @86: @82/(hash::blake2s::G#9)
  @87: @82/(hash::blake2s::G#5)
  @88: @82/(hash::blake2s::G#4)
  @89: @82/(hash::blake2s::G#3)
  @90: @82/(hash::blake2s::G#1)
*/

#endif /* _block_refine_hash_blake2s_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block_refine_hash_blake2s.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

