/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */
#ifndef _S_nacl_op_H_
#define _S_nacl_op_H_

#include "kcg_types.h"
#include "M_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::S/ */
extern void S_nacl_op(
  /* _L1/, a/ */
  gf_nacl_op *a,
  /* _L4/, o/ */
  gf_nacl_op *o);



#endif /* _S_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** S_nacl_op.h
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

