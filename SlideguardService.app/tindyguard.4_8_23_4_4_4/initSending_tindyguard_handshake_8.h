/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _initSending_tindyguard_handshake_8_H_
#define _initSending_tindyguard_handshake_8_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "init_tindyguard_handshake.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_init_tindyguard_handshake /* _L3=(tindyguard::handshake::init#1)/ */ Context_init_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_initSending_tindyguard_handshake_8;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::handshake::initSending/ */
extern void initSending_tindyguard_handshake_8(
  /* s/ */
  Session_tindyguardTypes *s_8,
  /* _L8/, pid/ */
  size_tindyguardTypes pid_8,
  /* _L15/, knownPeers/ */
  _3_array *knownPeers_8,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8,
  /* socket/ */
  fd_t_udp socket_8,
  /* @1/now/, now/ */
  kcg_int64 now_8,
  /* @1/soo/, _L12/, soo/ */
  Session_tindyguardTypes *soo_8,
  /* @1/IfBlock1:, @1/failed/, _L10/, fdfail/ */
  kcg_bool *fdfail_8,
  /* _L6/, again/ */
  kcg_bool *again_8,
  outC_initSending_tindyguard_handshake_8 *outC);

extern void initSending_reset_tindyguard_handshake_8(
  outC_initSending_tindyguard_handshake_8 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void initSending_init_tindyguard_handshake_8(
  outC_initSending_tindyguard_handshake_8 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::handshake::initSending/
  @1: (tindyguard::session::updateTx#1)
*/

#endif /* _initSending_tindyguard_handshake_8_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** initSending_tindyguard_handshake_8.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

