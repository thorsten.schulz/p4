/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "discardSent_tindyguard_session_4.h"

/* tindyguard::session::discardSent/ */
void discardSent_tindyguard_session_4(
  /* @1/_L1/, @1/i/, _L8/, slots/ */
  size_tindyguardTypes slots_4,
  /* _L1/, lenbuf/ */
  size_tindyguardTypes lenbuf_4,
  /* _L2/, taken/ */
  array_bool_4 *taken_4,
  /* _L11/, avail/ */
  size_tindyguardTypes *avail_4,
  /* _L7/, lenbufoo/ */
  size_tindyguardTypes *lenbufoo_4)
{
  size_tindyguardTypes acc;
  kcg_size idx;

  *lenbufoo_4 = lenbuf_4;
  /* _L4= */
  for (idx = 0; idx < 4; idx++) {
    acc = *lenbufoo_4;
    /* @2/_L3= */
    if ((*taken_4)[idx]) {
      *lenbufoo_4 = nil_udp;
    }
    else {
      *lenbufoo_4 = acc;
    }
    /* _L4= */
    if (!!(*taken_4)[idx]) {
      break;
    }
  }
  /* _L11= */
  if (*lenbufoo_4 != nil_udp) {
    *avail_4 = slots_4 - kcg_lit_int32(1);
  }
  else {
    *avail_4 = slots_4;
  }
}

/*
  Expanded instances for: tindyguard::session::discardSent/
  @2: (tindyguard::session::anyone_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** discardSent_tindyguard_session_4.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

