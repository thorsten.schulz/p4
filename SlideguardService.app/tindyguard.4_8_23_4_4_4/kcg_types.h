/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */
#ifndef _KCG_TYPES_H_
#define _KCG_TYPES_H_

#include "stddef.h"

#define KCG_MAPW_CPY

#include "../user_macros.h"

#ifndef kcg_char
#define kcg_char kcg_char
typedef char kcg_char;
#endif /* kcg_char */

#ifndef kcg_bool
#define kcg_bool kcg_bool
typedef unsigned char kcg_bool;
#endif /* kcg_bool */

#ifndef kcg_float32
#define kcg_float32 kcg_float32
typedef float kcg_float32;
#endif /* kcg_float32 */

#ifndef kcg_float64
#define kcg_float64 kcg_float64
typedef double kcg_float64;
#endif /* kcg_float64 */

#ifndef kcg_size
#define kcg_size kcg_size
typedef ptrdiff_t kcg_size;
#endif /* kcg_size */

#ifndef kcg_uint64
#define kcg_uint64 kcg_uint64
typedef unsigned long long kcg_uint64;
#endif /* kcg_uint64 */

#ifndef kcg_uint32
#define kcg_uint32 kcg_uint32
typedef unsigned long kcg_uint32;
#endif /* kcg_uint32 */

#ifndef kcg_uint16
#define kcg_uint16 kcg_uint16
typedef unsigned short kcg_uint16;
#endif /* kcg_uint16 */

#ifndef kcg_uint8
#define kcg_uint8 kcg_uint8
typedef unsigned char kcg_uint8;
#endif /* kcg_uint8 */

#ifndef kcg_int64
#define kcg_int64 kcg_int64
typedef signed long long kcg_int64;
#endif /* kcg_int64 */

#ifndef kcg_int32
#define kcg_int32 kcg_int32
typedef signed long kcg_int32;
#endif /* kcg_int32 */

#ifndef kcg_int16
#define kcg_int16 kcg_int16
typedef signed short kcg_int16;
#endif /* kcg_int16 */

#ifndef kcg_int8
#define kcg_int8 kcg_int8
typedef signed char kcg_int8;
#endif /* kcg_int8 */

#ifndef kcg_lit_float32
#define kcg_lit_float32(kcg_C1) ((kcg_float32) (kcg_C1))
#endif /* kcg_lit_float32 */

#ifndef kcg_lit_float64
#define kcg_lit_float64(kcg_C1) ((kcg_float64) (kcg_C1))
#endif /* kcg_lit_float64 */

#ifndef kcg_lit_size
#define kcg_lit_size(kcg_C1) ((kcg_size) (kcg_C1))
#endif /* kcg_lit_size */

#ifndef kcg_lit_uint64
#define kcg_lit_uint64(kcg_C1) ((kcg_uint64) (kcg_C1))
#endif /* kcg_lit_uint64 */

#ifndef kcg_lit_uint32
#define kcg_lit_uint32(kcg_C1) ((kcg_uint32) (kcg_C1))
#endif /* kcg_lit_uint32 */

#ifndef kcg_lit_uint16
#define kcg_lit_uint16(kcg_C1) ((kcg_uint16) (kcg_C1))
#endif /* kcg_lit_uint16 */

#ifndef kcg_lit_uint8
#define kcg_lit_uint8(kcg_C1) ((kcg_uint8) (kcg_C1))
#endif /* kcg_lit_uint8 */

#ifndef kcg_lit_int64
#define kcg_lit_int64(kcg_C1) ((kcg_int64) (kcg_C1))
#endif /* kcg_lit_int64 */

#ifndef kcg_lit_int32
#define kcg_lit_int32(kcg_C1) ((kcg_int32) (kcg_C1))
#endif /* kcg_lit_int32 */

#ifndef kcg_lit_int16
#define kcg_lit_int16(kcg_C1) ((kcg_int16) (kcg_C1))
#endif /* kcg_lit_int16 */

#ifndef kcg_lit_int8
#define kcg_lit_int8(kcg_C1) ((kcg_int8) (kcg_C1))
#endif /* kcg_lit_int8 */

#ifndef kcg_false
#define kcg_false ((kcg_bool) 0)
#endif /* kcg_false */

#ifndef kcg_true
#define kcg_true ((kcg_bool) 1)
#endif /* kcg_true */

#ifndef kcg_lsl_uint64
#define kcg_lsl_uint64(kcg_C1, kcg_C2)                                        \
  ((kcg_uint64) ((kcg_C1) << (kcg_C2)) & 0xffffffffffffffff)
#endif /* kcg_lsl_uint64 */

#ifndef kcg_lsl_uint32
#define kcg_lsl_uint32(kcg_C1, kcg_C2)                                        \
  ((kcg_uint32) ((kcg_C1) << (kcg_C2)) & 0xffffffff)
#endif /* kcg_lsl_uint32 */

#ifndef kcg_lsl_uint16
#define kcg_lsl_uint16(kcg_C1, kcg_C2)                                        \
  ((kcg_uint16) ((kcg_C1) << (kcg_C2)) & 0xffff)
#endif /* kcg_lsl_uint16 */

#ifndef kcg_lsl_uint8
#define kcg_lsl_uint8(kcg_C1, kcg_C2)                                         \
  ((kcg_uint8) ((kcg_C1) << (kcg_C2)) & 0xff)
#endif /* kcg_lsl_uint8 */

#ifndef kcg_lnot_uint64
#define kcg_lnot_uint64(kcg_C1) ((kcg_C1) ^ 0xffffffffffffffff)
#endif /* kcg_lnot_uint64 */

#ifndef kcg_lnot_uint32
#define kcg_lnot_uint32(kcg_C1) ((kcg_C1) ^ 0xffffffff)
#endif /* kcg_lnot_uint32 */

#ifndef kcg_lnot_uint16
#define kcg_lnot_uint16(kcg_C1) ((kcg_C1) ^ 0xffff)
#endif /* kcg_lnot_uint16 */

#ifndef kcg_lnot_uint8
#define kcg_lnot_uint8(kcg_C1) ((kcg_C1) ^ 0xff)
#endif /* kcg_lnot_uint8 */

#ifndef kcg_assign
#include "kcg_assign.h"
#endif /* kcg_assign */

#ifndef kcg_assign_struct
#define kcg_assign_struct kcg_assign
#endif /* kcg_assign_struct */

#ifndef kcg_assign_array
#define kcg_assign_array kcg_assign
#endif /* kcg_assign_array */

/* nacl::box::scalarmult/SM1: */
typedef enum kcg_tag__9_SSM_TR_SM1 {
  SSM_TR_no_trans_SM1,
  SSM_TR_firstPart_donna_1_firstPart_SM1,
  SSM_TR_firstPart_runSingle_2_firstPart_SM1,
  SSM_TR_firstPart_runPart_3_firstPart_SM1,
  SSM_TR_finalPart_fin_1_finalPart_SM1,
  SSM_TR_runPart_finalPart_1_runPart_SM1,
  SSM_TR_runSingle_fin_1_runSingle_SM1,
  SSM_TR_donna_fin_1_donna_SM1
} _9_SSM_TR_SM1;
/* nacl::box::scalarmult/SM1: */
typedef enum kcg_tag__8_SSM_ST_SM1 {
  SSM_st_firstPart_SM1,
  SSM_st_finalPart_SM1,
  SSM_st_runPart_SM1,
  SSM_st_runSingle_SM1,
  SSM_st_fin_SM1,
  SSM_st_donna_SM1
} _8_SSM_ST_SM1;
/* tindyguard::session::player/SM2: */
typedef enum kcg_tag_SSM_TR_SM2 {
  SSM_TR_no_trans_SM2,
  SSM_TR_work_out_idle_1_work_out_SM2,
  SSM_TR_idle_work_out_1_idle_SM2
} SSM_TR_SM2;
/* tindyguard::session::player/SM2: */
typedef enum kcg_tag_SSM_ST_SM2 {
  SSM_st_work_out_SM2,
  SSM_st_idle_SM2
} SSM_ST_SM2;
/* tindyguard::session::player/SM1: */
typedef enum kcg_tag__7_SSM_TR_SM1 {
  _10_SSM_TR_no_trans_SM1,
  SSM_TR_reset_respond_1_reset_SM1,
  SSM_TR_reset_initiate_2_reset_SM1,
  SSM_TR_respond_reset_1_respond_SM1,
  SSM_TR_respond_await_ackn_2_respond_SM1,
  SSM_TR_initiate_await_response_1_initiate_SM1,
  SSM_TR_initiate_reset_2_initiate_SM1,
  SSM_TR_await_response_reset_1_1_await_response_SM1,
  SSM_TR_await_response_initiate_2_1_await_response_SM1,
  SSM_TR_await_response_Initiator_inTransmission_2_await_response_SM1,
  SSM_TR_await_ackn_reset_1_await_ackn_SM1,
  SSM_TR_await_ackn_Responder_inTransmission_2_await_ackn_SM1,
  SSM_TR_Initiator_inTransmission_reset_1_Initiator_inTransmission_SM1,
  SSM_TR_Responder_inTransmission_reset_1_Responder_inTransmission_SM1
} _7_SSM_TR_SM1;
/* tindyguard::session::player/SM1: */
typedef enum kcg_tag__6_SSM_ST_SM1 {
  SSM_st_reset_SM1,
  SSM_st_respond_SM1,
  SSM_st_initiate_SM1,
  SSM_st_await_response_SM1,
  SSM_st_await_ackn_SM1,
  SSM_st_Initiator_inTransmission_SM1,
  SSM_st_Responder_inTransmission_SM1
} _6_SSM_ST_SM1;
/* tindyguard::bindings/wipe_machine: */
typedef enum kcg_tag_SSM_TR_wipe_machine {
  SSM_TR_no_trans_wipe_machine,
  SSM_TR_main_main_1_main_wipe_machine
} SSM_TR_wipe_machine;
/* tindyguard::bindings/wipe_machine: */
typedef enum kcg_tag_SSM_ST_wipe_machine {
  SSM_st_main_wipe_machine
} SSM_ST_wipe_machine;
/* tindyguard::bindings/wipe_machine:main:ride_machine: */
typedef enum kcg_tag_SSM_TR_ride_machine_main_wipe_machine {
  SSM_TR_no_trans_ride_machine_main_wipe_machine,
  SSM_TR_setup_cleanup_1_setup_ride_machine_main_wipe_machine,
  SSM_TR_setup_ride_2_setup_ride_machine_main_wipe_machine,
  SSM_TR_cleanup_setup_1_cleanup_ride_machine_main_wipe_machine,
  SSM_TR_ride_cleanup_1_ride_ride_machine_main_wipe_machine,
  SSM_TR_ride_setup_2_ride_ride_machine_main_wipe_machine
} SSM_TR_ride_machine_main_wipe_machine;
/* tindyguard::bindings/wipe_machine:main:ride_machine: */
typedef enum kcg_tag_SSM_ST_ride_machine_main_wipe_machine {
  SSM_st_setup_ride_machine_main_wipe_machine,
  SSM_st_cleanup_ride_machine_main_wipe_machine,
  SSM_st_ride_ride_machine_main_wipe_machine
} SSM_ST_ride_machine_main_wipe_machine;
/* tindyguard::initOur/key_retry: */
typedef enum kcg_tag_SSM_TR_key_retry {
  SSM_TR_no_trans_key_retry,
  SSM_TR_makeKeys_permanentWeakKey_1_makeKeys_key_retry,
  SSM_TR_makeKeys_makeKeys_2_makeKeys_key_retry
} SSM_TR_key_retry;
/* tindyguard::initOur/key_retry: */
typedef enum kcg_tag_SSM_ST_key_retry {
  SSM_st_makeKeys_key_retry,
  SSM_st_permanentWeakKey_key_retry
} SSM_ST_key_retry;
/* tindyguard::test::PingPong/SM1: */
typedef enum kcg_tag_SSM_TR_SM1 {
  _11_SSM_TR_no_trans_SM1,
  SSM_TR_peer_setup_peer_setup_done_1_peer_setup_SM1
} SSM_TR_SM1;
/* tindyguard::test::PingPong/SM1: */
typedef enum kcg_tag_SSM_ST_SM1 {
  SSM_st_peer_setup_SM1,
  SSM_st_peer_setup_done_SM1
} SSM_ST_SM1;
typedef kcg_bool array_bool_4[4];

typedef array_bool_4 array_bool_4_4[4];

/* slideTypes::uint/ */
typedef kcg_uint32 uint_slideTypes;

typedef kcg_uint32 array_uint32_32[32];

typedef kcg_uint32 array_uint32_3[3];

/* nacl::core::chacha::Nonce/ */
typedef array_uint32_3 Nonce_nacl_core_chacha;

/* tindyguardTypes::TAI/ */
typedef array_uint32_3 TAI_tindyguardTypes;

typedef kcg_uint32 array_uint32_6[6];

typedef kcg_uint32 array_uint32_12[12];

typedef kcg_uint32 array_uint32_1[1];

typedef kcg_uint32 array_uint32_33[33];

typedef kcg_uint32 array_uint32_4[4];

/* hash::blake2s::Mac/ */
typedef array_uint32_4 Mac_hash_blake2s;

/* slideTypes::u324/ */
typedef array_uint32_4 u324_slideTypes;

typedef array_uint32_4 array_uint32_4_4[4];

typedef array_uint32_4_4 array_uint32_4_4_16[16];

typedef kcg_uint32 array_uint32_16[16];

/* nacl::core::chacha::State/ */
typedef array_uint32_16 State_nacl_core_chacha;

/* hash::blake2s::u32_16/ */
typedef array_uint32_16 u32_16_hash_blake2s;

/* udp::chunk_t/ */
typedef array_uint32_16 chunk_t_udp;

/* hash::blake2s::HashChunk/ */
typedef array_uint32_16 HashChunk_hash_blake2s;

/* slideTypes::StreamChunk/ */
typedef array_uint32_16 StreamChunk_slideTypes;

typedef array_uint32_16 array_uint32_16_24[24];

typedef array_uint32_16_24 array_uint32_16_24_4[4];

typedef array_uint32_16 array_uint32_16_3[3];

typedef array_uint32_16 array_uint32_16_1[1];

typedef array_uint32_16 array_uint32_16_23[23];

typedef array_uint32_16_23 array_uint32_16_23_4[4];

typedef array_uint32_16_23_4 array_uint32_16_23_4_4[4];

typedef array_uint32_16 array_uint32_16_4[4];

typedef array_uint32_16 array_uint32_16_2[2];

typedef kcg_uint32 array_uint32_48[48];

typedef kcg_uint32 array_uint32_7[7];

typedef kcg_uint32 array_uint32_13[13];

typedef kcg_uint32 array_uint32_2[2];

typedef kcg_uint32 array_uint32_8[8];

/* nacl::core::Key/ */
typedef array_uint32_8 Key_nacl_core;

/* tindyguardTypes::psk/ */
typedef array_uint32_8 psk_tindyguardTypes;

/* tindyguardTypes::pub/ */
typedef array_uint32_8 pub_tindyguardTypes;

/* tindyguardTypes::secret/ */
typedef array_uint32_8 secret_tindyguardTypes;

/* hash::blake2s::Hash/ */
typedef array_uint32_8 Hash_hash_blake2s;

/* tindyguardTypes::PreHashes/ */
typedef struct kcg_tag_PreHashes_tindyguardTypes {
  StreamChunk_slideTypes salt;
  Hash_hash_blake2s hash1;
  Hash_hash_blake2s cookieHKey;
} PreHashes_tindyguardTypes;

/* slideTypes::Key32/ */
typedef array_uint32_8 Key32_slideTypes;

typedef Key32_slideTypes array_uint32_8_5[5];

/* slideTypes::KeyPair32/ */
typedef struct kcg_tag_KeyPair32_slideTypes {
  Key32_slideTypes sk_x;
  Key32_slideTypes pk_y;
} KeyPair32_slideTypes;

/* tindyguard::handshake::State/ */
typedef struct kcg_tag_State_tindyguard_handshake {
  KeyPair32_slideTypes ephemeral;
  Key32_slideTypes their;
  Hash_hash_blake2s ihash;
  HashChunk_hash_blake2s chainingKey;
} State_tindyguard_handshake;

typedef array_uint32_8 array_uint32_8_2[2];

typedef array_uint32_8 array_uint32_8_3[3];

/* udp::IpAddress/ */
typedef struct kcg_tag_IpAddress_udp { kcg_uint32 addr; } IpAddress_udp;

/* udp::addrs_t/ */
typedef struct kcg_tag_addrs_t_udp {
  IpAddress_udp ip;
  kcg_bool up;
  kcg_bool mc;
  kcg_bool p2p;
  kcg_bool broadcast;
} addrs_t_udp;

typedef addrs_t_udp array[16];

/* udp::range_t/ */
typedef struct kcg_tag_range_t_udp {
  IpAddress_udp net;
  IpAddress_udp mask;
} range_t_udp;

typedef kcg_uint32 array_uint32_5[5];

typedef kcg_uint32 array_uint32_17[17];

typedef kcg_uint32 array_uint32_64[64];

typedef kcg_uint8 array_uint8_4[4];

typedef array_uint8_4 array_uint8_4_4[4];

/* slideTypes::st32x8/, slideTypes::u848/ */
typedef array_uint8_4 u848_slideTypes[8];

typedef kcg_uint8 array_uint8_16[16];

/* nacl::onetime::Mac/ */
typedef array_uint8_16 Mac_nacl_onetime;

typedef array_uint8_16 array_uint8_16_10[10];

typedef kcg_uint8 array_uint8_17[17];

typedef kcg_uint8 array_uint8_32[32];

typedef kcg_uint8 array_uint8_12[12];

/* nacl::op::car25519/, nacl::op::gf/, nacl::op::unpack25519/ */
typedef kcg_int64 gf_nacl_op[16];

/* tindyguardTypes::CookieJar/ */
typedef struct kcg_tag_CookieJar_tindyguardTypes {
  StreamChunk_slideTypes content;
  kcg_int64 opened;
} CookieJar_tindyguardTypes;

typedef CookieJar_tindyguardTypes _2_array[2];

/* tindyguardTypes::KeySalted/ */
typedef struct kcg_tag_KeySalted_tindyguardTypes {
  KeyPair32_slideTypes key;
  PreHashes_tindyguardTypes hcache;
  _2_array cookies;
} KeySalted_tindyguardTypes;

typedef kcg_int64 array_int64_31[31];

typedef kcg_int64 array_int64_15[15];

/* udp::fd_t/ */
typedef kcg_int32 fd_t_udp;

/* sys::time_t/ */
typedef kcg_int32 time_t_sys;

/* udp::port_t/ */
typedef kcg_int32 port_t_udp;

/* udp::peer_t/ */
typedef struct kcg_tag_peer_t_udp {
  IpAddress_udp addr;
  port_t_udp port;
  time_t_sys time;
  kcg_int64 mtime;
} peer_t_udp;

typedef peer_t_udp _5_array[4];

/* slideTypes::size/ */
typedef kcg_int32 size_slideTypes;

/* slideTypes::int/ */
typedef kcg_int32 int_slideTypes;

/* nacl::box::scalMulAcc/ */
typedef struct kcg_tag_scalMulAcc_nacl_box {
  int_slideTypes i;
  gf_nacl_op a1;
  gf_nacl_op a2;
  gf_nacl_op a3;
  gf_nacl_op a4;
} scalMulAcc_nacl_box;

/* udp::length_t/ */
typedef kcg_int32 length_t_udp;

/* ip::header/ */
typedef struct kcg_tag_header_ip {
  kcg_uint8 ihl_version;
  kcg_uint8 tos;
  length_t_udp tot_len;
  kcg_uint16 id;
  kcg_uint16 frag_off;
  kcg_uint8 ttl;
  kcg_uint8 protocol;
  kcg_uint16 check;
  kcg_uint32 saddr;
  kcg_uint32 daddr;
} header_ip;

/* tindyguardTypes::size/ */
typedef kcg_int32 size_tindyguardTypes;

/* tindyguardTypes::Peer/ */
typedef struct kcg_tag_Peer_tindyguardTypes {
  pub_tindyguardTypes tpub;
  psk_tindyguardTypes preshared;
  range_t_udp allowed;
  PreHashes_tindyguardTypes hcache;
  peer_t_udp endpoint;
  TAI_tindyguardTypes tai;
  kcg_bool timedOut;
  size_tindyguardTypes bestSession;
  kcg_bool inhibitInit;
  CookieJar_tindyguardTypes cookie;
} Peer_tindyguardTypes;

typedef Peer_tindyguardTypes _3_array[8];

/* tindyguardTypes::Session/ */
typedef struct kcg_tag_Session_tindyguardTypes {
  secret_tindyguardTypes ot;
  secret_tindyguardTypes to;
  kcg_uint64 ot_cnt;
  kcg_uint64 to_cnt;
  kcg_uint64 to_cnt_cache;
  kcg_uint32 our;
  kcg_uint32 their;
  kcg_int64 rx_bytes;
  size_tindyguardTypes rx_cnt;
  size_tindyguardTypes tx_cnt;
  kcg_int64 txTime;
  kcg_int64 sTime;
  size_tindyguardTypes pid;
  State_tindyguard_handshake handshake;
  Peer_tindyguardTypes peer;
  kcg_bool transmissive;
  kcg_bool gotKeepAlive;
  kcg_bool sentKeepAlive;
} Session_tindyguardTypes;

typedef Session_tindyguardTypes _4_array[4];

/* tindyguardTypes::int/ */
typedef kcg_int32 int_tindyguardTypes;

typedef kcg_int32 array_int32_4[4];

typedef array_int32_4 array_int32_4_4[4];

#ifndef kcg_copy_header_ip
#define kcg_copy_header_ip(kcg_C1, kcg_C2)                                    \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (header_ip)))
#endif /* kcg_copy_header_ip */

#ifndef kcg_copy_Session_tindyguardTypes
#define kcg_copy_Session_tindyguardTypes(kcg_C1, kcg_C2)                      \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (Session_tindyguardTypes)))
#endif /* kcg_copy_Session_tindyguardTypes */

#ifndef kcg_copy_Peer_tindyguardTypes
#define kcg_copy_Peer_tindyguardTypes(kcg_C1, kcg_C2)                         \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (Peer_tindyguardTypes)))
#endif /* kcg_copy_Peer_tindyguardTypes */

#ifndef kcg_copy_KeySalted_tindyguardTypes
#define kcg_copy_KeySalted_tindyguardTypes(kcg_C1, kcg_C2)                    \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (KeySalted_tindyguardTypes)))
#endif /* kcg_copy_KeySalted_tindyguardTypes */

#ifndef kcg_copy_IpAddress_udp
#define kcg_copy_IpAddress_udp(kcg_C1, kcg_C2)                                \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (IpAddress_udp)))
#endif /* kcg_copy_IpAddress_udp */

#ifndef kcg_copy_PreHashes_tindyguardTypes
#define kcg_copy_PreHashes_tindyguardTypes(kcg_C1, kcg_C2)                    \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (PreHashes_tindyguardTypes)))
#endif /* kcg_copy_PreHashes_tindyguardTypes */

#ifndef kcg_copy_scalMulAcc_nacl_box
#define kcg_copy_scalMulAcc_nacl_box(kcg_C1, kcg_C2)                          \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (scalMulAcc_nacl_box)))
#endif /* kcg_copy_scalMulAcc_nacl_box */

#ifndef kcg_copy_State_tindyguard_handshake
#define kcg_copy_State_tindyguard_handshake(kcg_C1, kcg_C2)                   \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (State_tindyguard_handshake)))
#endif /* kcg_copy_State_tindyguard_handshake */

#ifndef kcg_copy_KeyPair32_slideTypes
#define kcg_copy_KeyPair32_slideTypes(kcg_C1, kcg_C2)                         \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (KeyPair32_slideTypes)))
#endif /* kcg_copy_KeyPair32_slideTypes */

#ifndef kcg_copy_range_t_udp
#define kcg_copy_range_t_udp(kcg_C1, kcg_C2)                                  \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (range_t_udp)))
#endif /* kcg_copy_range_t_udp */

#ifndef kcg_copy_peer_t_udp
#define kcg_copy_peer_t_udp(kcg_C1, kcg_C2)                                   \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (peer_t_udp)))
#endif /* kcg_copy_peer_t_udp */

#ifndef kcg_copy_CookieJar_tindyguardTypes
#define kcg_copy_CookieJar_tindyguardTypes(kcg_C1, kcg_C2)                    \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (CookieJar_tindyguardTypes)))
#endif /* kcg_copy_CookieJar_tindyguardTypes */

#ifndef kcg_copy_addrs_t_udp
#define kcg_copy_addrs_t_udp(kcg_C1, kcg_C2)                                  \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (addrs_t_udp)))
#endif /* kcg_copy_addrs_t_udp */

#ifndef kcg_copy_array_uint8_12
#define kcg_copy_array_uint8_12(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_12)))
#endif /* kcg_copy_array_uint8_12 */

#ifndef kcg_copy_array_int32_4
#define kcg_copy_array_int32_4(kcg_C1, kcg_C2)                                \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_int32_4)))
#endif /* kcg_copy_array_int32_4 */

#ifndef kcg_copy_array_uint32_16_24_4
#define kcg_copy_array_uint32_16_24_4(kcg_C1, kcg_C2)                         \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_24_4)))
#endif /* kcg_copy_array_uint32_16_24_4 */

#ifndef kcg_copy_array_uint32_64
#define kcg_copy_array_uint32_64(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_64)))
#endif /* kcg_copy_array_uint32_64 */

#ifndef kcg_copy_array_uint32_17
#define kcg_copy_array_uint32_17(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_17)))
#endif /* kcg_copy_array_uint32_17 */

#ifndef kcg_copy_array_uint32_5
#define kcg_copy_array_uint32_5(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_5)))
#endif /* kcg_copy_array_uint32_5 */

#ifndef kcg_copy_array_uint32_8_3
#define kcg_copy_array_uint32_8_3(kcg_C1, kcg_C2)                             \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_8_3)))
#endif /* kcg_copy_array_uint32_8_3 */

#ifndef kcg_copy_array_uint8_32
#define kcg_copy_array_uint8_32(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_32)))
#endif /* kcg_copy_array_uint8_32 */

#ifndef kcg_copy_array
#define kcg_copy_array(kcg_C1, kcg_C2)                                        \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array)))
#endif /* kcg_copy_array */

#ifndef kcg_copy_array_int64_15
#define kcg_copy_array_int64_15(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_int64_15)))
#endif /* kcg_copy_array_int64_15 */

#ifndef kcg_copy_array_uint32_8
#define kcg_copy_array_uint32_8(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_8)))
#endif /* kcg_copy_array_uint32_8 */

#ifndef kcg_copy_array_uint32_2
#define kcg_copy_array_uint32_2(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_2)))
#endif /* kcg_copy_array_uint32_2 */

#ifndef kcg_copy_array_uint32_16_2
#define kcg_copy_array_uint32_16_2(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_2)))
#endif /* kcg_copy_array_uint32_16_2 */

#ifndef kcg_copy_u848_slideTypes
#define kcg_copy_u848_slideTypes(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (u848_slideTypes)))
#endif /* kcg_copy_u848_slideTypes */

#ifndef kcg_copy_array_uint32_16_4
#define kcg_copy_array_uint32_16_4(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_4)))
#endif /* kcg_copy_array_uint32_16_4 */

#ifndef kcg_copy_array_uint32_13
#define kcg_copy_array_uint32_13(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_13)))
#endif /* kcg_copy_array_uint32_13 */

#ifndef kcg_copy_array_uint32_7
#define kcg_copy_array_uint32_7(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_7)))
#endif /* kcg_copy_array_uint32_7 */

#ifndef kcg_copy__2_array
#define kcg_copy__2_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_2_array)))
#endif /* kcg_copy__2_array */

#ifndef kcg_copy_array_uint32_48
#define kcg_copy_array_uint32_48(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_48)))
#endif /* kcg_copy_array_uint32_48 */

#ifndef kcg_copy_array_uint32_16_23
#define kcg_copy_array_uint32_16_23(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_23)))
#endif /* kcg_copy_array_uint32_16_23 */

#ifndef kcg_copy_array_uint8_17
#define kcg_copy_array_uint8_17(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_17)))
#endif /* kcg_copy_array_uint8_17 */

#ifndef kcg_copy_array_uint32_16
#define kcg_copy_array_uint32_16(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16)))
#endif /* kcg_copy_array_uint32_16 */

#ifndef kcg_copy_array_uint32_4
#define kcg_copy_array_uint32_4(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_4)))
#endif /* kcg_copy_array_uint32_4 */

#ifndef kcg_copy_array_bool_4
#define kcg_copy_array_bool_4(kcg_C1, kcg_C2)                                 \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_bool_4)))
#endif /* kcg_copy_array_bool_4 */

#ifndef kcg_copy_array_uint32_33
#define kcg_copy_array_uint32_33(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_33)))
#endif /* kcg_copy_array_uint32_33 */

#ifndef kcg_copy__3_array
#define kcg_copy__3_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_3_array)))
#endif /* kcg_copy__3_array */

#ifndef kcg_copy_array_uint32_8_2
#define kcg_copy_array_uint32_8_2(kcg_C1, kcg_C2)                             \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_8_2)))
#endif /* kcg_copy_array_uint32_8_2 */

#ifndef kcg_copy_array_uint32_1
#define kcg_copy_array_uint32_1(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_1)))
#endif /* kcg_copy_array_uint32_1 */

#ifndef kcg_copy_array_int64_31
#define kcg_copy_array_int64_31(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_int64_31)))
#endif /* kcg_copy_array_int64_31 */

#ifndef kcg_copy_array_uint32_12
#define kcg_copy_array_uint32_12(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_12)))
#endif /* kcg_copy_array_uint32_12 */

#ifndef kcg_copy_array_uint32_6
#define kcg_copy_array_uint32_6(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_6)))
#endif /* kcg_copy_array_uint32_6 */

#ifndef kcg_copy_array_uint8_16
#define kcg_copy_array_uint8_16(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_16)))
#endif /* kcg_copy_array_uint8_16 */

#ifndef kcg_copy__4_array
#define kcg_copy__4_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_4_array)))
#endif /* kcg_copy__4_array */

#ifndef kcg_copy_array_uint8_4
#define kcg_copy_array_uint8_4(kcg_C1, kcg_C2)                                \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_4)))
#endif /* kcg_copy_array_uint8_4 */

#ifndef kcg_copy_array_uint32_16_1
#define kcg_copy_array_uint32_16_1(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_1)))
#endif /* kcg_copy_array_uint32_16_1 */

#ifndef kcg_copy_array_uint32_16_3
#define kcg_copy_array_uint32_16_3(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_3)))
#endif /* kcg_copy_array_uint32_16_3 */

#ifndef kcg_copy_array_int32_4_4
#define kcg_copy_array_int32_4_4(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_int32_4_4)))
#endif /* kcg_copy_array_int32_4_4 */

#ifndef kcg_copy_array_uint8_4_4
#define kcg_copy_array_uint8_4_4(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_4_4)))
#endif /* kcg_copy_array_uint8_4_4 */

#ifndef kcg_copy_array_uint32_4_4
#define kcg_copy_array_uint32_4_4(kcg_C1, kcg_C2)                             \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_4_4)))
#endif /* kcg_copy_array_uint32_4_4 */

#ifndef kcg_copy_array_uint8_16_10
#define kcg_copy_array_uint8_16_10(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_16_10)))
#endif /* kcg_copy_array_uint8_16_10 */

#ifndef kcg_copy_array_uint32_16_23_4
#define kcg_copy_array_uint32_16_23_4(kcg_C1, kcg_C2)                         \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_23_4)))
#endif /* kcg_copy_array_uint32_16_23_4 */

#ifndef kcg_copy__5_array
#define kcg_copy__5_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_5_array)))
#endif /* kcg_copy__5_array */

#ifndef kcg_copy_array_uint32_16_23_4_4
#define kcg_copy_array_uint32_16_23_4_4(kcg_C1, kcg_C2)                       \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_23_4_4)))
#endif /* kcg_copy_array_uint32_16_23_4_4 */

#ifndef kcg_copy_array_uint32_16_24
#define kcg_copy_array_uint32_16_24(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_24)))
#endif /* kcg_copy_array_uint32_16_24 */

#ifndef kcg_copy_array_uint32_8_5
#define kcg_copy_array_uint32_8_5(kcg_C1, kcg_C2)                             \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_8_5)))
#endif /* kcg_copy_array_uint32_8_5 */

#ifndef kcg_copy_gf_nacl_op
#define kcg_copy_gf_nacl_op(kcg_C1, kcg_C2)                                   \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (gf_nacl_op)))
#endif /* kcg_copy_gf_nacl_op */

#ifndef kcg_copy_array_uint32_4_4_16
#define kcg_copy_array_uint32_4_4_16(kcg_C1, kcg_C2)                          \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_4_4_16)))
#endif /* kcg_copy_array_uint32_4_4_16 */

#ifndef kcg_copy_array_uint32_3
#define kcg_copy_array_uint32_3(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_3)))
#endif /* kcg_copy_array_uint32_3 */

#ifndef kcg_copy_array_bool_4_4
#define kcg_copy_array_bool_4_4(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_bool_4_4)))
#endif /* kcg_copy_array_bool_4_4 */

#ifndef kcg_copy_array_uint32_32
#define kcg_copy_array_uint32_32(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_32)))
#endif /* kcg_copy_array_uint32_32 */

#ifdef kcg_use_header_ip
#ifndef kcg_comp_header_ip
extern kcg_bool kcg_comp_header_ip(header_ip *kcg_c1, header_ip *kcg_c2);
#endif /* kcg_comp_header_ip */
#endif /* kcg_use_header_ip */

#ifdef kcg_use_Session_tindyguardTypes
#ifndef kcg_comp_Session_tindyguardTypes
extern kcg_bool kcg_comp_Session_tindyguardTypes(
  Session_tindyguardTypes *kcg_c1,
  Session_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_Session_tindyguardTypes */
#endif /* kcg_use_Session_tindyguardTypes */

#ifdef kcg_use_Peer_tindyguardTypes
#ifndef kcg_comp_Peer_tindyguardTypes
extern kcg_bool kcg_comp_Peer_tindyguardTypes(
  Peer_tindyguardTypes *kcg_c1,
  Peer_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_Peer_tindyguardTypes */
#endif /* kcg_use_Peer_tindyguardTypes */

#ifdef kcg_use_KeySalted_tindyguardTypes
#ifndef kcg_comp_KeySalted_tindyguardTypes
extern kcg_bool kcg_comp_KeySalted_tindyguardTypes(
  KeySalted_tindyguardTypes *kcg_c1,
  KeySalted_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_KeySalted_tindyguardTypes */
#endif /* kcg_use_KeySalted_tindyguardTypes */

#ifndef kcg_comp_IpAddress_udp
extern kcg_bool kcg_comp_IpAddress_udp(
  IpAddress_udp *kcg_c1,
  IpAddress_udp *kcg_c2);
#define kcg_use_IpAddress_udp
#endif /* kcg_comp_IpAddress_udp */

#ifdef kcg_use_PreHashes_tindyguardTypes
#ifndef kcg_comp_PreHashes_tindyguardTypes
extern kcg_bool kcg_comp_PreHashes_tindyguardTypes(
  PreHashes_tindyguardTypes *kcg_c1,
  PreHashes_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_PreHashes_tindyguardTypes */
#endif /* kcg_use_PreHashes_tindyguardTypes */

#ifdef kcg_use_scalMulAcc_nacl_box
#ifndef kcg_comp_scalMulAcc_nacl_box
extern kcg_bool kcg_comp_scalMulAcc_nacl_box(
  scalMulAcc_nacl_box *kcg_c1,
  scalMulAcc_nacl_box *kcg_c2);
#endif /* kcg_comp_scalMulAcc_nacl_box */
#endif /* kcg_use_scalMulAcc_nacl_box */

#ifdef kcg_use_State_tindyguard_handshake
#ifndef kcg_comp_State_tindyguard_handshake
extern kcg_bool kcg_comp_State_tindyguard_handshake(
  State_tindyguard_handshake *kcg_c1,
  State_tindyguard_handshake *kcg_c2);
#endif /* kcg_comp_State_tindyguard_handshake */
#endif /* kcg_use_State_tindyguard_handshake */

#ifdef kcg_use_KeyPair32_slideTypes
#ifndef kcg_comp_KeyPair32_slideTypes
extern kcg_bool kcg_comp_KeyPair32_slideTypes(
  KeyPair32_slideTypes *kcg_c1,
  KeyPair32_slideTypes *kcg_c2);
#endif /* kcg_comp_KeyPair32_slideTypes */
#endif /* kcg_use_KeyPair32_slideTypes */

#ifdef kcg_use_range_t_udp
#ifndef kcg_comp_range_t_udp
extern kcg_bool kcg_comp_range_t_udp(range_t_udp *kcg_c1, range_t_udp *kcg_c2);
#endif /* kcg_comp_range_t_udp */
#endif /* kcg_use_range_t_udp */

#ifdef kcg_use_peer_t_udp
#ifndef kcg_comp_peer_t_udp
extern kcg_bool kcg_comp_peer_t_udp(peer_t_udp *kcg_c1, peer_t_udp *kcg_c2);
#endif /* kcg_comp_peer_t_udp */
#endif /* kcg_use_peer_t_udp */

#ifdef kcg_use_CookieJar_tindyguardTypes
#ifndef kcg_comp_CookieJar_tindyguardTypes
extern kcg_bool kcg_comp_CookieJar_tindyguardTypes(
  CookieJar_tindyguardTypes *kcg_c1,
  CookieJar_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_CookieJar_tindyguardTypes */
#endif /* kcg_use_CookieJar_tindyguardTypes */

#ifdef kcg_use_addrs_t_udp
#ifndef kcg_comp_addrs_t_udp
extern kcg_bool kcg_comp_addrs_t_udp(addrs_t_udp *kcg_c1, addrs_t_udp *kcg_c2);
#endif /* kcg_comp_addrs_t_udp */
#endif /* kcg_use_addrs_t_udp */

#ifdef kcg_use_array_uint8_12
#ifndef kcg_comp_array_uint8_12
extern kcg_bool kcg_comp_array_uint8_12(
  array_uint8_12 *kcg_c1,
  array_uint8_12 *kcg_c2);
#endif /* kcg_comp_array_uint8_12 */
#endif /* kcg_use_array_uint8_12 */

#ifdef kcg_use_array_int32_4
#ifndef kcg_comp_array_int32_4
extern kcg_bool kcg_comp_array_int32_4(
  array_int32_4 *kcg_c1,
  array_int32_4 *kcg_c2);
#endif /* kcg_comp_array_int32_4 */
#endif /* kcg_use_array_int32_4 */

#ifdef kcg_use_array_uint32_16_24_4
#ifndef kcg_comp_array_uint32_16_24_4
extern kcg_bool kcg_comp_array_uint32_16_24_4(
  array_uint32_16_24_4 *kcg_c1,
  array_uint32_16_24_4 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_24_4 */
#endif /* kcg_use_array_uint32_16_24_4 */

#ifdef kcg_use_array_uint32_64
#ifndef kcg_comp_array_uint32_64
extern kcg_bool kcg_comp_array_uint32_64(
  array_uint32_64 *kcg_c1,
  array_uint32_64 *kcg_c2);
#endif /* kcg_comp_array_uint32_64 */
#endif /* kcg_use_array_uint32_64 */

#ifdef kcg_use_array_uint32_17
#ifndef kcg_comp_array_uint32_17
extern kcg_bool kcg_comp_array_uint32_17(
  array_uint32_17 *kcg_c1,
  array_uint32_17 *kcg_c2);
#endif /* kcg_comp_array_uint32_17 */
#endif /* kcg_use_array_uint32_17 */

#ifndef kcg_comp_array_uint32_5
extern kcg_bool kcg_comp_array_uint32_5(
  array_uint32_5 *kcg_c1,
  array_uint32_5 *kcg_c2);
#define kcg_use_array_uint32_5
#endif /* kcg_comp_array_uint32_5 */

#ifdef kcg_use_array_uint32_8_3
#ifndef kcg_comp_array_uint32_8_3
extern kcg_bool kcg_comp_array_uint32_8_3(
  array_uint32_8_3 *kcg_c1,
  array_uint32_8_3 *kcg_c2);
#endif /* kcg_comp_array_uint32_8_3 */
#endif /* kcg_use_array_uint32_8_3 */

#ifdef kcg_use_array_uint8_32
#ifndef kcg_comp_array_uint8_32
extern kcg_bool kcg_comp_array_uint8_32(
  array_uint8_32 *kcg_c1,
  array_uint8_32 *kcg_c2);
#endif /* kcg_comp_array_uint8_32 */
#endif /* kcg_use_array_uint8_32 */

#ifdef kcg_use_array
#ifndef kcg_comp_array
extern kcg_bool kcg_comp_array(array *kcg_c1, array *kcg_c2);
#endif /* kcg_comp_array */
#endif /* kcg_use_array */

#ifdef kcg_use_array_int64_15
#ifndef kcg_comp_array_int64_15
extern kcg_bool kcg_comp_array_int64_15(
  array_int64_15 *kcg_c1,
  array_int64_15 *kcg_c2);
#endif /* kcg_comp_array_int64_15 */
#endif /* kcg_use_array_int64_15 */

#ifndef kcg_comp_array_uint32_8
extern kcg_bool kcg_comp_array_uint32_8(
  array_uint32_8 *kcg_c1,
  array_uint32_8 *kcg_c2);
#define kcg_use_array_uint32_8
#endif /* kcg_comp_array_uint32_8 */

#ifdef kcg_use_array_uint32_2
#ifndef kcg_comp_array_uint32_2
extern kcg_bool kcg_comp_array_uint32_2(
  array_uint32_2 *kcg_c1,
  array_uint32_2 *kcg_c2);
#endif /* kcg_comp_array_uint32_2 */
#endif /* kcg_use_array_uint32_2 */

#ifdef kcg_use_array_uint32_16_2
#ifndef kcg_comp_array_uint32_16_2
extern kcg_bool kcg_comp_array_uint32_16_2(
  array_uint32_16_2 *kcg_c1,
  array_uint32_16_2 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_2 */
#endif /* kcg_use_array_uint32_16_2 */

#ifdef kcg_use_u848_slideTypes
#ifndef kcg_comp_u848_slideTypes
extern kcg_bool kcg_comp_u848_slideTypes(
  u848_slideTypes *kcg_c1,
  u848_slideTypes *kcg_c2);
#endif /* kcg_comp_u848_slideTypes */
#endif /* kcg_use_u848_slideTypes */

#ifdef kcg_use_array_uint32_16_4
#ifndef kcg_comp_array_uint32_16_4
extern kcg_bool kcg_comp_array_uint32_16_4(
  array_uint32_16_4 *kcg_c1,
  array_uint32_16_4 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_4 */
#endif /* kcg_use_array_uint32_16_4 */

#ifdef kcg_use_array_uint32_13
#ifndef kcg_comp_array_uint32_13
extern kcg_bool kcg_comp_array_uint32_13(
  array_uint32_13 *kcg_c1,
  array_uint32_13 *kcg_c2);
#endif /* kcg_comp_array_uint32_13 */
#endif /* kcg_use_array_uint32_13 */

#ifdef kcg_use_array_uint32_7
#ifndef kcg_comp_array_uint32_7
extern kcg_bool kcg_comp_array_uint32_7(
  array_uint32_7 *kcg_c1,
  array_uint32_7 *kcg_c2);
#endif /* kcg_comp_array_uint32_7 */
#endif /* kcg_use_array_uint32_7 */

#ifdef kcg_use__2_array
#ifndef kcg_comp__2_array
extern kcg_bool kcg_comp__2_array(_2_array *kcg_c1, _2_array *kcg_c2);
#endif /* kcg_comp__2_array */
#endif /* kcg_use__2_array */

#ifdef kcg_use_array_uint32_48
#ifndef kcg_comp_array_uint32_48
extern kcg_bool kcg_comp_array_uint32_48(
  array_uint32_48 *kcg_c1,
  array_uint32_48 *kcg_c2);
#endif /* kcg_comp_array_uint32_48 */
#endif /* kcg_use_array_uint32_48 */

#ifdef kcg_use_array_uint32_16_23
#ifndef kcg_comp_array_uint32_16_23
extern kcg_bool kcg_comp_array_uint32_16_23(
  array_uint32_16_23 *kcg_c1,
  array_uint32_16_23 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_23 */
#endif /* kcg_use_array_uint32_16_23 */

#ifdef kcg_use_array_uint8_17
#ifndef kcg_comp_array_uint8_17
extern kcg_bool kcg_comp_array_uint8_17(
  array_uint8_17 *kcg_c1,
  array_uint8_17 *kcg_c2);
#endif /* kcg_comp_array_uint8_17 */
#endif /* kcg_use_array_uint8_17 */

#ifdef kcg_use_array_uint32_16
#ifndef kcg_comp_array_uint32_16
extern kcg_bool kcg_comp_array_uint32_16(
  array_uint32_16 *kcg_c1,
  array_uint32_16 *kcg_c2);
#endif /* kcg_comp_array_uint32_16 */
#endif /* kcg_use_array_uint32_16 */

#ifndef kcg_comp_array_uint32_4
extern kcg_bool kcg_comp_array_uint32_4(
  array_uint32_4 *kcg_c1,
  array_uint32_4 *kcg_c2);
#define kcg_use_array_uint32_4
#endif /* kcg_comp_array_uint32_4 */

#ifdef kcg_use_array_bool_4
#ifndef kcg_comp_array_bool_4
extern kcg_bool kcg_comp_array_bool_4(
  array_bool_4 *kcg_c1,
  array_bool_4 *kcg_c2);
#endif /* kcg_comp_array_bool_4 */
#endif /* kcg_use_array_bool_4 */

#ifdef kcg_use_array_uint32_33
#ifndef kcg_comp_array_uint32_33
extern kcg_bool kcg_comp_array_uint32_33(
  array_uint32_33 *kcg_c1,
  array_uint32_33 *kcg_c2);
#endif /* kcg_comp_array_uint32_33 */
#endif /* kcg_use_array_uint32_33 */

#ifdef kcg_use__3_array
#ifndef kcg_comp__3_array
extern kcg_bool kcg_comp__3_array(_3_array *kcg_c1, _3_array *kcg_c2);
#endif /* kcg_comp__3_array */
#endif /* kcg_use__3_array */

#ifdef kcg_use_array_uint32_8_2
#ifndef kcg_comp_array_uint32_8_2
extern kcg_bool kcg_comp_array_uint32_8_2(
  array_uint32_8_2 *kcg_c1,
  array_uint32_8_2 *kcg_c2);
#endif /* kcg_comp_array_uint32_8_2 */
#endif /* kcg_use_array_uint32_8_2 */

#ifdef kcg_use_array_uint32_1
#ifndef kcg_comp_array_uint32_1
extern kcg_bool kcg_comp_array_uint32_1(
  array_uint32_1 *kcg_c1,
  array_uint32_1 *kcg_c2);
#endif /* kcg_comp_array_uint32_1 */
#endif /* kcg_use_array_uint32_1 */

#ifdef kcg_use_array_int64_31
#ifndef kcg_comp_array_int64_31
extern kcg_bool kcg_comp_array_int64_31(
  array_int64_31 *kcg_c1,
  array_int64_31 *kcg_c2);
#endif /* kcg_comp_array_int64_31 */
#endif /* kcg_use_array_int64_31 */

#ifdef kcg_use_array_uint32_12
#ifndef kcg_comp_array_uint32_12
extern kcg_bool kcg_comp_array_uint32_12(
  array_uint32_12 *kcg_c1,
  array_uint32_12 *kcg_c2);
#endif /* kcg_comp_array_uint32_12 */
#endif /* kcg_use_array_uint32_12 */

#ifdef kcg_use_array_uint32_6
#ifndef kcg_comp_array_uint32_6
extern kcg_bool kcg_comp_array_uint32_6(
  array_uint32_6 *kcg_c1,
  array_uint32_6 *kcg_c2);
#endif /* kcg_comp_array_uint32_6 */
#endif /* kcg_use_array_uint32_6 */

#ifdef kcg_use_array_uint8_16
#ifndef kcg_comp_array_uint8_16
extern kcg_bool kcg_comp_array_uint8_16(
  array_uint8_16 *kcg_c1,
  array_uint8_16 *kcg_c2);
#endif /* kcg_comp_array_uint8_16 */
#endif /* kcg_use_array_uint8_16 */

#ifdef kcg_use__4_array
#ifndef kcg_comp__4_array
extern kcg_bool kcg_comp__4_array(_4_array *kcg_c1, _4_array *kcg_c2);
#endif /* kcg_comp__4_array */
#endif /* kcg_use__4_array */

#ifdef kcg_use_array_uint8_4
#ifndef kcg_comp_array_uint8_4
extern kcg_bool kcg_comp_array_uint8_4(
  array_uint8_4 *kcg_c1,
  array_uint8_4 *kcg_c2);
#endif /* kcg_comp_array_uint8_4 */
#endif /* kcg_use_array_uint8_4 */

#ifdef kcg_use_array_uint32_16_1
#ifndef kcg_comp_array_uint32_16_1
extern kcg_bool kcg_comp_array_uint32_16_1(
  array_uint32_16_1 *kcg_c1,
  array_uint32_16_1 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_1 */
#endif /* kcg_use_array_uint32_16_1 */

#ifdef kcg_use_array_uint32_16_3
#ifndef kcg_comp_array_uint32_16_3
extern kcg_bool kcg_comp_array_uint32_16_3(
  array_uint32_16_3 *kcg_c1,
  array_uint32_16_3 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_3 */
#endif /* kcg_use_array_uint32_16_3 */

#ifdef kcg_use_array_int32_4_4
#ifndef kcg_comp_array_int32_4_4
extern kcg_bool kcg_comp_array_int32_4_4(
  array_int32_4_4 *kcg_c1,
  array_int32_4_4 *kcg_c2);
#endif /* kcg_comp_array_int32_4_4 */
#endif /* kcg_use_array_int32_4_4 */

#ifdef kcg_use_array_uint8_4_4
#ifndef kcg_comp_array_uint8_4_4
extern kcg_bool kcg_comp_array_uint8_4_4(
  array_uint8_4_4 *kcg_c1,
  array_uint8_4_4 *kcg_c2);
#endif /* kcg_comp_array_uint8_4_4 */
#endif /* kcg_use_array_uint8_4_4 */

#ifdef kcg_use_array_uint32_4_4
#ifndef kcg_comp_array_uint32_4_4
extern kcg_bool kcg_comp_array_uint32_4_4(
  array_uint32_4_4 *kcg_c1,
  array_uint32_4_4 *kcg_c2);
#endif /* kcg_comp_array_uint32_4_4 */
#endif /* kcg_use_array_uint32_4_4 */

#ifdef kcg_use_array_uint8_16_10
#ifndef kcg_comp_array_uint8_16_10
extern kcg_bool kcg_comp_array_uint8_16_10(
  array_uint8_16_10 *kcg_c1,
  array_uint8_16_10 *kcg_c2);
#endif /* kcg_comp_array_uint8_16_10 */
#endif /* kcg_use_array_uint8_16_10 */

#ifdef kcg_use_array_uint32_16_23_4
#ifndef kcg_comp_array_uint32_16_23_4
extern kcg_bool kcg_comp_array_uint32_16_23_4(
  array_uint32_16_23_4 *kcg_c1,
  array_uint32_16_23_4 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_23_4 */
#endif /* kcg_use_array_uint32_16_23_4 */

#ifdef kcg_use__5_array
#ifndef kcg_comp__5_array
extern kcg_bool kcg_comp__5_array(_5_array *kcg_c1, _5_array *kcg_c2);
#endif /* kcg_comp__5_array */
#endif /* kcg_use__5_array */

#ifdef kcg_use_array_uint32_16_23_4_4
#ifndef kcg_comp_array_uint32_16_23_4_4
extern kcg_bool kcg_comp_array_uint32_16_23_4_4(
  array_uint32_16_23_4_4 *kcg_c1,
  array_uint32_16_23_4_4 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_23_4_4 */
#endif /* kcg_use_array_uint32_16_23_4_4 */

#ifdef kcg_use_array_uint32_16_24
#ifndef kcg_comp_array_uint32_16_24
extern kcg_bool kcg_comp_array_uint32_16_24(
  array_uint32_16_24 *kcg_c1,
  array_uint32_16_24 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_24 */
#endif /* kcg_use_array_uint32_16_24 */

#ifdef kcg_use_array_uint32_8_5
#ifndef kcg_comp_array_uint32_8_5
extern kcg_bool kcg_comp_array_uint32_8_5(
  array_uint32_8_5 *kcg_c1,
  array_uint32_8_5 *kcg_c2);
#endif /* kcg_comp_array_uint32_8_5 */
#endif /* kcg_use_array_uint32_8_5 */

#ifdef kcg_use_gf_nacl_op
#ifndef kcg_comp_gf_nacl_op
extern kcg_bool kcg_comp_gf_nacl_op(gf_nacl_op *kcg_c1, gf_nacl_op *kcg_c2);
#endif /* kcg_comp_gf_nacl_op */
#endif /* kcg_use_gf_nacl_op */

#ifdef kcg_use_array_uint32_4_4_16
#ifndef kcg_comp_array_uint32_4_4_16
extern kcg_bool kcg_comp_array_uint32_4_4_16(
  array_uint32_4_4_16 *kcg_c1,
  array_uint32_4_4_16 *kcg_c2);
#endif /* kcg_comp_array_uint32_4_4_16 */
#endif /* kcg_use_array_uint32_4_4_16 */

#ifdef kcg_use_array_uint32_3
#ifndef kcg_comp_array_uint32_3
extern kcg_bool kcg_comp_array_uint32_3(
  array_uint32_3 *kcg_c1,
  array_uint32_3 *kcg_c2);
#endif /* kcg_comp_array_uint32_3 */
#endif /* kcg_use_array_uint32_3 */

#ifdef kcg_use_array_bool_4_4
#ifndef kcg_comp_array_bool_4_4
extern kcg_bool kcg_comp_array_bool_4_4(
  array_bool_4_4 *kcg_c1,
  array_bool_4_4 *kcg_c2);
#endif /* kcg_comp_array_bool_4_4 */
#endif /* kcg_use_array_bool_4_4 */

#ifdef kcg_use_array_uint32_32
#ifndef kcg_comp_array_uint32_32
extern kcg_bool kcg_comp_array_uint32_32(
  array_uint32_32 *kcg_c1,
  array_uint32_32 *kcg_c2);
#endif /* kcg_comp_array_uint32_32 */
#endif /* kcg_use_array_uint32_32 */

#define kcg_comp_Key32_slideTypes kcg_comp_array_uint32_8

#define kcg_copy_Key32_slideTypes kcg_copy_array_uint32_8

#define kcg_comp_Hash_hash_blake2s kcg_comp_array_uint32_8

#define kcg_copy_Hash_hash_blake2s kcg_copy_array_uint32_8

#define kcg_comp_StreamChunk_slideTypes kcg_comp_array_uint32_16

#define kcg_copy_StreamChunk_slideTypes kcg_copy_array_uint32_16

#define kcg_comp_HashChunk_hash_blake2s kcg_comp_array_uint32_16

#define kcg_copy_HashChunk_hash_blake2s kcg_copy_array_uint32_16

#define kcg_comp_secret_tindyguardTypes kcg_comp_array_uint32_8

#define kcg_copy_secret_tindyguardTypes kcg_copy_array_uint32_8

#define kcg_comp_pub_tindyguardTypes kcg_comp_array_uint32_8

#define kcg_copy_pub_tindyguardTypes kcg_copy_array_uint32_8

#define kcg_comp_psk_tindyguardTypes kcg_comp_array_uint32_8

#define kcg_copy_psk_tindyguardTypes kcg_copy_array_uint32_8

#define kcg_comp_TAI_tindyguardTypes kcg_comp_array_uint32_3

#define kcg_copy_TAI_tindyguardTypes kcg_copy_array_uint32_3

#define kcg_comp_chunk_t_udp kcg_comp_array_uint32_16

#define kcg_copy_chunk_t_udp kcg_copy_array_uint32_16

#define kcg_comp_u32_16_hash_blake2s kcg_comp_array_uint32_16

#define kcg_copy_u32_16_hash_blake2s kcg_copy_array_uint32_16

#define kcg_comp_Key_nacl_core kcg_comp_array_uint32_8

#define kcg_copy_Key_nacl_core kcg_copy_array_uint32_8

#define kcg_comp_State_nacl_core_chacha kcg_comp_array_uint32_16

#define kcg_copy_State_nacl_core_chacha kcg_copy_array_uint32_16

#define kcg_comp_Nonce_nacl_core_chacha kcg_comp_array_uint32_3

#define kcg_copy_Nonce_nacl_core_chacha kcg_copy_array_uint32_3

#define kcg_comp_Mac_nacl_onetime kcg_comp_array_uint8_16

#define kcg_copy_Mac_nacl_onetime kcg_copy_array_uint8_16

#define kcg_comp_u324_slideTypes kcg_comp_array_uint32_4

#define kcg_copy_u324_slideTypes kcg_copy_array_uint32_4

#define kcg_comp_Mac_hash_blake2s kcg_comp_array_uint32_4

#define kcg_copy_Mac_hash_blake2s kcg_copy_array_uint32_4

#endif /* _KCG_TYPES_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_types.h
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

