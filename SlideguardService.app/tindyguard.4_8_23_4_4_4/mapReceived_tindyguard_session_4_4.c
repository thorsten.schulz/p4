/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapReceived_tindyguard_session_4_4.h"

/* tindyguard::session::mapReceived/ */
void mapReceived_tindyguard_session_4_4(
  /* _L12/, sid/ */
  size_tindyguardTypes sid_4_4,
  /* _L1/, length/ */
  array_int32_4 *length_4_4,
  /* _L2/, our/ */
  array_uint32_4 *our_4_4,
  /* _L11/, s/ */
  Session_tindyguardTypes *s_4_4,
  /* _L6/, residual_length_or_sid/ */
  array_int32_4 *residual_length_or_sid_4_4,
  /* _L10/, length_for_session/ */
  array_int32_4 *length_for_session_4_4,
  /* _L8/, for_init_response/ */
  kcg_bool *for_init_response_4_4)
{
  kcg_bool acc;
  kcg_size idx;
  /* @1/IfBlock1:then:_L24/ */
  kcg_bool _L24_mapRxd_it_1_then_IfBlock1_4;
  /* @1/IfBlock1:then:_L19/,
     @1/IfBlock1:then:_L22/,
     @1/IfBlock1:then:take_packet/ */
  kcg_bool _L22_mapRxd_it_1_then_IfBlock1_4;
  /* @1/IfBlock1:then:_L1/ */
  kcg_bool _L1_mapRxd_it_1_then_IfBlock1_4;
  /* @1/IfBlock1:then:_L5/ */
  kcg_bool _L5_mapRxd_it_1_then_IfBlock1_4;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_mapRxd_it_1_4;

  *for_init_response_4_4 = kcg_false;
  /* _L8= */
  for (idx = 0; idx < 4; idx++) {
    acc = *for_init_response_4_4;
    IfBlock1_clock_mapRxd_it_1_4 = (*length_4_4)[idx] >= kcg_lit_int32(4);
    /* @1/IfBlock1: */
    if (IfBlock1_clock_mapRxd_it_1_4) {
      _L24_mapRxd_it_1_then_IfBlock1_4 = (*length_4_4)[idx] == kcg_lit_int32(148);
      _L1_mapRxd_it_1_then_IfBlock1_4 = (*s_4_4).pid ==
        InvalidPeer_tindyguardTypes && _L24_mapRxd_it_1_then_IfBlock1_4 && !acc;
      _L5_mapRxd_it_1_then_IfBlock1_4 = (*our_4_4)[idx] == (*s_4_4).our;
      _L22_mapRxd_it_1_then_IfBlock1_4 = _L5_mapRxd_it_1_then_IfBlock1_4 ||
        _L1_mapRxd_it_1_then_IfBlock1_4;
      *for_init_response_4_4 = _L1_mapRxd_it_1_then_IfBlock1_4 || acc;
      /* @1/IfBlock1:then:_L8= */
      if (_L22_mapRxd_it_1_then_IfBlock1_4) {
        (*residual_length_or_sid_4_4)[idx] = sid_4_4;
        (*length_for_session_4_4)[idx] = (*length_4_4)[idx];
      }
      else {
        /* @1/IfBlock1:then:_L30= */
        if (!(_L5_mapRxd_it_1_then_IfBlock1_4 ||
            _L24_mapRxd_it_1_then_IfBlock1_4) && sid_4_4 == kcg_lit_int32(3)) {
          (*residual_length_or_sid_4_4)[idx] = nil_udp;
        }
        else {
          (*residual_length_or_sid_4_4)[idx] = (*length_4_4)[idx];
        }
        (*length_for_session_4_4)[idx] = nil_udp;
      }
    }
    else {
      *for_init_response_4_4 = acc;
      (*residual_length_or_sid_4_4)[idx] = (*length_4_4)[idx];
      (*length_for_session_4_4)[idx] = nil_udp;
    }
  }
}

/*
  Expanded instances for: tindyguard::session::mapReceived/
  @1: (tindyguard::session::mapRxd_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapReceived_tindyguard_session_4_4.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

