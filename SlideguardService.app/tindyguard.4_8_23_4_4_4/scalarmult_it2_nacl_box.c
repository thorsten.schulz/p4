/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "scalarmult_it2_nacl_box.h"

/* nacl::box::scalarmult_it2/ */
void scalarmult_it2_nacl_box(
  /* i/ */
  kcg_uint8 i,
  /* akku/ */
  scalMulAcc_nacl_box *akku,
  /* b/ */
  kcg_uint8 b,
  /* x/ */
  gf_nacl_op *x,
  /* akkoo/ */
  scalMulAcc_nacl_box *akkoo)
{
  kcg_size idx;
  /* @2/_L9/ */
  kcg_uint64 _L9_sel25519_it1_1_sel25519_8;
  /* @4/_L9/ */
  kcg_uint64 _L9_sel25519_it1_1_sel25519_7;
  gf_nacl_op tmp;
  gf_nacl_op tmp1;
  gf_nacl_op tmp2;
  gf_nacl_op tmp3;
  gf_nacl_op tmp4;
  gf_nacl_op tmp5;
  gf_nacl_op tmp6;
  gf_nacl_op tmp7;
  /* @6/_L9/ */
  kcg_uint64 _L9_sel25519_it1_1_sel25519_6;
  gf_nacl_op tmp8;
  gf_nacl_op tmp9;
  gf_nacl_op tmp10;
  /* @8/_L9/ */
  kcg_uint64 _L9_sel25519_it1_1_sel25519_5;
  /* @1/_L7/, @3/_L7/, @5/_L7/, @7/_L7/ */
  kcg_uint64 _L7_sel25519_5;
  /* IfBlock1:else:_L1/ */
  gf_nacl_op _L1_else_IfBlock1;
  /* IfBlock1:else:_L2/ */
  gf_nacl_op _L2_else_IfBlock1;
  /* IfBlock1:else:_L3/ */
  gf_nacl_op _L3_else_IfBlock1;
  /* IfBlock1:else:_L17/ */
  gf_nacl_op _L17_else_IfBlock1;
  /* IfBlock1:else:_L18/ */
  gf_nacl_op _L18_else_IfBlock1;
  /* @1/_L9/, @1/qo/, IfBlock1:else:_L20/ */
  gf_nacl_op _L20_else_IfBlock1;
  /* @1/_L8/, @1/po/, IfBlock1:else:_L19/ */
  gf_nacl_op _L19_else_IfBlock1;
  /* IfBlock1:else:_L21/ */
  gf_nacl_op _L21_else_IfBlock1;
  /* @3/_L9/, @3/qo/, IfBlock1:else:_L32/ */
  gf_nacl_op _L32_else_IfBlock1;
  /* @3/_L8/, @3/po/, IfBlock1:else:_L31/ */
  gf_nacl_op _L31_else_IfBlock1;
  /* IfBlock1:else:_L37/ */
  gf_nacl_op _L37_else_IfBlock1;
  /* @5/_L9/, @5/qo/, IfBlock1:else:_L40/ */
  gf_nacl_op _L40_else_IfBlock1;
  /* @7/_L9/, @7/qo/, IfBlock1:else:_L43/ */
  gf_nacl_op _L43_else_IfBlock1;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock;

  IfBlock1_clock = (*akku).i > kcg_lit_int32(32);
  /* IfBlock1: */
  if (IfBlock1_clock) {
    kcg_copy_scalMulAcc_nacl_box(akkoo, akku);
    (*akkoo).i = kcg_lit_int32(32);
  }
  else {
    (*akkoo).i = (*akku).i;
    _L7_sel25519_5 = kcg_lnot_uint64(
        /* @1/_L6=, @3/_L6=, @5/_L6=, @7/_L6= */(kcg_uint64)
          (/* IfBlock1:else:_L16=, IfBlock1:else:_L24= */(kcg_int32)
              ((b >> (kcg_lit_uint8(7) - i)) & kcg_lit_uint8(1)) -
            kcg_lit_int32(1)));
    /* @1/_L8=, @3/_L8= */
    for (idx = 0; idx < 16; idx++) {
      _L9_sel25519_it1_1_sel25519_8 = (/* @2/_L14= */(kcg_uint64)
            (*akku).a1[idx] ^ /* @2/_L15= */(kcg_uint64) (*akku).a2[idx]) &
        /* @2/_L16= */(kcg_uint64) /* @1/_L11= */(kcg_int64) _L7_sel25519_5;
      _L20_else_IfBlock1[idx] = /* @2/_L18= */(kcg_int64)
          (/* @2/_L15= */(kcg_uint64) (*akku).a2[idx] ^ _L9_sel25519_it1_1_sel25519_8);
      _L19_else_IfBlock1[idx] = /* @2/_L17= */(kcg_int64)
          (/* @2/_L14= */(kcg_uint64) (*akku).a1[idx] ^ _L9_sel25519_it1_1_sel25519_8);
      _L9_sel25519_it1_1_sel25519_7 = (/* @4/_L14= */(kcg_uint64)
            (*akku).a3[idx] ^ /* @4/_L15= */(kcg_uint64) (*akku).a4[idx]) &
        /* @4/_L16= */(kcg_uint64) /* @3/_L11= */(kcg_int64) _L7_sel25519_5;
      _L32_else_IfBlock1[idx] = /* @4/_L18= */(kcg_int64)
          (/* @4/_L15= */(kcg_uint64) (*akku).a4[idx] ^ _L9_sel25519_it1_1_sel25519_7);
      _L31_else_IfBlock1[idx] = /* @4/_L17= */(kcg_int64)
          (/* @4/_L14= */(kcg_uint64) (*akku).a3[idx] ^ _L9_sel25519_it1_1_sel25519_7);
    }
    /* IfBlock1:else:_L21=(nacl::op::A#6)/ */
    A_nacl_op(&_L19_else_IfBlock1, &_L31_else_IfBlock1, &_L21_else_IfBlock1);
    /* IfBlock1:else:_L17=(nacl::op::S#8)/ */
    S_nacl_op(&_L21_else_IfBlock1, &_L17_else_IfBlock1);
    /* IfBlock1:else:_L1=(nacl::op::Z#8)/ */
    Z_nacl_op(&_L19_else_IfBlock1, &_L31_else_IfBlock1, &_L1_else_IfBlock1);
    /* IfBlock1:else:_L37=(nacl::op::S#5)/ */
    S_nacl_op(&_L1_else_IfBlock1, &_L37_else_IfBlock1);
    /* IfBlock1:else:_L18=(nacl::op::Z#6)/ */
    Z_nacl_op(&_L17_else_IfBlock1, &_L37_else_IfBlock1, &_L18_else_IfBlock1);
    /* IfBlock1:else:_L13=(nacl::op::A#8)/ */
    A_nacl_op(&_L20_else_IfBlock1, &_L32_else_IfBlock1, &tmp);
    /* IfBlock1:else:_L2=(nacl::op::M#12)/ */
    M_nacl_op(&tmp, &_L1_else_IfBlock1, &_L2_else_IfBlock1);
    /* IfBlock1:else:_L12=(nacl::op::Z#7)/ */
    Z_nacl_op(&_L20_else_IfBlock1, &_L32_else_IfBlock1, &tmp1);
    /* IfBlock1:else:_L3=(nacl::op::M#11)/ */
    M_nacl_op(&tmp1, &_L21_else_IfBlock1, &_L3_else_IfBlock1);
    /* IfBlock1:else:_L44=(nacl::op::M#7)/ */
    M_nacl_op(&_L18_else_IfBlock1, (gf_nacl_op *) &_121665_nacl_op, &tmp4);
    /* IfBlock1:else:_L14=(nacl::op::A#7)/ */
    A_nacl_op(&_L17_else_IfBlock1, &tmp4, &tmp3);
    /* IfBlock1:else:_L33=(nacl::op::M#8)/ */
    M_nacl_op(&_L18_else_IfBlock1, &tmp3, &tmp2);
    /* IfBlock1:else:_L45=(nacl::op::Z#5)/ */
    Z_nacl_op(&_L2_else_IfBlock1, &_L3_else_IfBlock1, &tmp7);
    /* IfBlock1:else:_L23=(nacl::op::S#7)/ */ S_nacl_op(&tmp7, &tmp6);
    /* IfBlock1:else:_L25=(nacl::op::M#10)/ */ M_nacl_op(&tmp6, x, &tmp5);
    /* @5/_L8= */
    for (idx = 0; idx < 16; idx++) {
      _L9_sel25519_it1_1_sel25519_6 = (/* @6/_L14= */(kcg_uint64) tmp2[idx] ^
          /* @6/_L15= */(kcg_uint64) tmp5[idx]) & /* @6/_L16= */(kcg_uint64)
          /* @5/_L11= */(kcg_int64) _L7_sel25519_5;
      _L40_else_IfBlock1[idx] = /* @6/_L18= */(kcg_int64)
          (/* @6/_L15= */(kcg_uint64) tmp5[idx] ^ _L9_sel25519_it1_1_sel25519_6);
      (*akkoo).a3[idx] = /* @6/_L17= */(kcg_int64)
          (/* @6/_L14= */(kcg_uint64) tmp2[idx] ^ _L9_sel25519_it1_1_sel25519_6);
    }
    kcg_copy_gf_nacl_op(&(*akkoo).a4, &_L40_else_IfBlock1);
    /* IfBlock1:else:_L26=(nacl::op::M#9)/ */
    M_nacl_op(&_L17_else_IfBlock1, &_L37_else_IfBlock1, &tmp8);
    /* IfBlock1:else:_L35=(nacl::op::A#5)/ */
    A_nacl_op(&_L2_else_IfBlock1, &_L3_else_IfBlock1, &tmp10);
    /* IfBlock1:else:_L34=(nacl::op::S#6)/ */ S_nacl_op(&tmp10, &tmp9);
    /* @7/_L8= */
    for (idx = 0; idx < 16; idx++) {
      _L9_sel25519_it1_1_sel25519_5 = (/* @8/_L14= */(kcg_uint64) tmp8[idx] ^
          /* @8/_L15= */(kcg_uint64) tmp9[idx]) & /* @8/_L16= */(kcg_uint64)
          /* @7/_L11= */(kcg_int64) _L7_sel25519_5;
      _L43_else_IfBlock1[idx] = /* @8/_L18= */(kcg_int64)
          (/* @8/_L15= */(kcg_uint64) tmp9[idx] ^ _L9_sel25519_it1_1_sel25519_5);
      (*akkoo).a1[idx] = /* @8/_L17= */(kcg_int64)
          (/* @8/_L14= */(kcg_uint64) tmp8[idx] ^ _L9_sel25519_it1_1_sel25519_5);
    }
    kcg_copy_gf_nacl_op(&(*akkoo).a2, &_L43_else_IfBlock1);
  }
}

/*
  Expanded instances for: nacl::box::scalarmult_it2/
  @1: (nacl::op::sel25519#8)
  @2: @1/(nacl::op::sel25519_it1#1)
  @3: (nacl::op::sel25519#7)
  @4: @3/(nacl::op::sel25519_it1#1)
  @5: (nacl::op::sel25519#6)
  @6: @5/(nacl::op::sel25519_it1#1)
  @7: (nacl::op::sel25519#5)
  @8: @7/(nacl::op::sel25519_it1#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarmult_it2_nacl_box.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

