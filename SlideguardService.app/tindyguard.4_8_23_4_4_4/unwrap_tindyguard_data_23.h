/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _unwrap_tindyguard_data_23_H_
#define _unwrap_tindyguard_data_23_H_

#include "kcg_types.h"
#include "aeadOpen_nacl_box_23_1_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::data::unwrap/ */
extern void unwrap_tindyguard_data_23(
  /* _L131/, s/ */
  Session_tindyguardTypes *s_23,
  /* fail_in/ */
  kcg_bool fail_in_23,
  /* rlength/ */
  size_slideTypes rlength_23,
  /* datamsg/ */
  array_uint32_16_24 *datamsg_23,
  /* endpoint/ */
  peer_t_udp *endpoint_23,
  /* soo/ */
  Session_tindyguardTypes *soo_23,
  /* fail_flag/ */
  kcg_bool *fail_flag_23,
  /* len/ */
  length_t_udp *len_23,
  /* msg/ */
  array_uint32_16_23 *msg_23);



#endif /* _unwrap_tindyguard_data_23_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unwrap_tindyguard_data_23.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

