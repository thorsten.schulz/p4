/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:25
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "Xor_nacl_core_chacha_20.h"

/* nacl::core::chacha::Xor/ */
void Xor_nacl_core_chacha_20(
  /* _L61/, i/ */
  int_slideTypes i_20,
  /* @1/_L34/, @1/_L69/, @1/_L71/, @1/n/, _L34/, n/ */
  State_nacl_core_chacha *n_20,
  /* m/ */
  StreamChunk_slideTypes *m_20,
  /* _L62/, mlen/ */
  int_slideTypes mlen_20,
  /* IfBlock1:, _L67/, goOn/, more/ */
  kcg_bool *goOn_20,
  /* @1/_L68/, @1/noo/, _L68/, noo/ */
  State_nacl_core_chacha *noo_20,
  /* c/ */
  StreamChunk_slideTypes *c_20)
{
  array_uint32_16 tmp;
  kcg_size idx;
  /* @18/_L13/, @18/aoo/, @2/_L92/, @43/_L5/, @43/x/ */
  kcg_uint32 _L92_dual_1_generate_1;
  /* @13/_L11/, @13/doo/, @16/_L9/, @16/ret/, @2/_L99/ */
  kcg_uint32 _L99_dual_1_generate_1;
  /* @13/_L14/, @13/coo/, @17/_L11/, @17/y/, @2/_L98/ */
  kcg_uint32 _L98_dual_1_generate_1;
  /* @13/_L13/, @13/aoo/, @16/_L5/, @16/x/, @2/_L96/ */
  kcg_uint32 _L96_dual_1_generate_1;
  /* @11/_L9/, @11/ret/, @2/_L103/, @8/_L11/, @8/doo/ */
  kcg_uint32 _L103_dual_1_generate_1;
  /* @12/_L11/, @12/y/, @2/_L102/, @8/_L14/, @8/coo/ */
  kcg_uint32 _L102_dual_1_generate_1;
  /* @11/_L5/, @11/x/, @2/_L100/, @8/_L13/, @8/aoo/ */
  kcg_uint32 _L100_dual_1_generate_1;
  /* @2/_L107/, @3/_L11/, @3/doo/, @6/_L9/, @6/ret/ */
  kcg_uint32 _L107_dual_1_generate_1;
  /* @2/_L106/, @3/_L14/, @3/coo/, @7/_L11/, @7/y/ */
  kcg_uint32 _L106_dual_1_generate_1;
  /* @2/_L104/, @3/_L13/, @3/aoo/, @6/_L5/, @6/x/ */
  kcg_uint32 _L104_dual_1_generate_1;
  /* @2/_L91/,
     @21/_L11/,
     @21/doo/,
     @24/_L9/,
     @24/ret/,
     @3/_L5/,
     @3/d/,
     @4/_L11/,
     @4/y/ */
  kcg_uint32 _L91_dual_1_generate_1;
  /* @18/_L8/, @18/c/, @2/_L90/, @21/_L14/, @21/coo/, @25/_L11/, @25/y/ */
  kcg_uint32 _L90_dual_1_generate_1;
  /* @10/_L11/,
     @10/y/,
     @2/_L89/,
     @21/_L12/,
     @21/boo/,
     @25/_L9/,
     @25/ret/,
     @8/_L3/,
     @8/b/ */
  kcg_uint32 _L89_dual_1_generate_1;
  /* @13/_L2/, @13/a/, @2/_L88/, @21/_L13/, @21/aoo/, @24/_L5/, @24/x/ */
  kcg_uint32 _L88_dual_1_generate_1;
  /* @13/_L5/,
     @13/d/,
     @14/_L11/,
     @14/y/,
     @2/_L87/,
     @26/_L11/,
     @26/doo/,
     @29/_L9/,
     @29/ret/ */
  kcg_uint32 _L87_dual_1_generate_1;
  /* @2/_L86/, @26/_L14/, @26/coo/, @3/_L8/, @3/c/, @30/_L11/, @30/y/ */
  kcg_uint32 _L86_dual_1_generate_1;
  /* @18/_L3/,
     @18/b/,
     @2/_L85/,
     @20/_L11/,
     @20/y/,
     @26/_L12/,
     @26/boo/,
     @30/_L9/,
     @30/ret/ */
  kcg_uint32 _L85_dual_1_generate_1;
  /* @2/_L84/, @26/_L13/, @26/aoo/, @29/_L5/, @29/x/, @8/_L2/, @8/a/ */
  kcg_uint32 _L84_dual_1_generate_1;
  /* @2/_L83/,
     @31/_L11/,
     @31/doo/,
     @34/_L9/,
     @34/ret/,
     @8/_L5/,
     @8/d/,
     @9/_L11/,
     @9/y/ */
  kcg_uint32 _L83_dual_1_generate_1;
  /* @13/_L8/, @13/c/, @2/_L82/, @31/_L14/, @31/coo/, @35/_L11/, @35/y/ */
  kcg_uint32 _L82_dual_1_generate_1;
  /* @2/_L81/,
     @3/_L3/,
     @3/b/,
     @31/_L12/,
     @31/boo/,
     @35/_L9/,
     @35/ret/,
     @5/_L11/,
     @5/y/ */
  kcg_uint32 _L81_dual_1_generate_1;
  /* @18/_L2/, @18/a/, @2/_L80/, @31/_L13/, @31/aoo/, @34/_L5/, @34/x/ */
  kcg_uint32 _L80_dual_1_generate_1;
  /* @2/_L76/, @3/_L2/, @3/a/, @36/_L13/, @36/aoo/, @39/_L5/, @39/x/ */
  kcg_uint32 _L76_dual_1_generate_1;
  /* @13/_L3/,
     @13/b/,
     @15/_L11/,
     @15/y/,
     @2/_L77/,
     @36/_L12/,
     @36/boo/,
     @40/_L9/,
     @40/ret/ */
  kcg_uint32 _L77_dual_1_generate_1;
  /* @2/_L78/, @36/_L14/, @36/coo/, @40/_L11/, @40/y/, @8/_L8/, @8/c/ */
  kcg_uint32 _L78_dual_1_generate_1;
  /* @18/_L5/,
     @18/d/,
     @19/_L11/,
     @19/y/,
     @2/_L79/,
     @36/_L11/,
     @36/doo/,
     @39/_L9/,
     @39/ret/ */
  kcg_uint32 _L79_dual_1_generate_1;
  /* @4/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_8_dual_1_generate_1_16;
  /* @3/_L6/, @4/_L9/, @4/ret/, @6/_L11/, @6/y/ */
  kcg_uint32 _L9_XRol32_1_QR_8_dual_1_generate_1_16;
  /* @3/_L1/, @4/_L5/, @4/x/ */
  kcg_uint32 _L5_XRol32_1_QR_8_dual_1_generate_1_16;
  /* @5/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_8_dual_1_generate_1_12;
  /* @3/_L10/, @5/_L9/, @5/ret/, @7/_L5/, @7/x/ */
  kcg_uint32 _L9_XRol32_2_QR_8_dual_1_generate_1_12;
  /* @3/_L7/, @5/_L5/, @5/x/ */
  kcg_uint32 _L5_XRol32_2_QR_8_dual_1_generate_1_12;
  /* @6/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_8_dual_1_generate_1_8;
  /* @7/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_8_dual_1_generate_1_7;
  /* @9/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_7_dual_1_generate_1_16;
  /* @11/_L11/, @11/y/, @8/_L6/, @9/_L9/, @9/ret/ */
  kcg_uint32 _L9_XRol32_1_QR_7_dual_1_generate_1_16;
  /* @8/_L1/, @9/_L5/, @9/x/ */
  kcg_uint32 _L5_XRol32_1_QR_7_dual_1_generate_1_16;
  /* @10/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_7_dual_1_generate_1_12;
  /* @10/_L9/, @10/ret/, @12/_L5/, @12/x/, @8/_L10/ */
  kcg_uint32 _L9_XRol32_2_QR_7_dual_1_generate_1_12;
  /* @10/_L5/, @10/x/, @8/_L7/ */
  kcg_uint32 _L5_XRol32_2_QR_7_dual_1_generate_1_12;
  /* @11/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_7_dual_1_generate_1_8;
  /* @12/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_7_dual_1_generate_1_7;
  /* @14/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_6_dual_1_generate_1_16;
  /* @13/_L6/, @14/_L9/, @14/ret/, @16/_L11/, @16/y/ */
  kcg_uint32 _L9_XRol32_1_QR_6_dual_1_generate_1_16;
  /* @13/_L1/, @14/_L5/, @14/x/ */
  kcg_uint32 _L5_XRol32_1_QR_6_dual_1_generate_1_16;
  /* @15/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_6_dual_1_generate_1_12;
  /* @13/_L10/, @15/_L9/, @15/ret/, @17/_L5/, @17/x/ */
  kcg_uint32 _L9_XRol32_2_QR_6_dual_1_generate_1_12;
  /* @13/_L7/, @15/_L5/, @15/x/ */
  kcg_uint32 _L5_XRol32_2_QR_6_dual_1_generate_1_12;
  /* @16/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_6_dual_1_generate_1_8;
  /* @17/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_6_dual_1_generate_1_7;
  /* @19/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_5_dual_1_generate_1_16;
  /* @18/_L6/, @19/_L9/, @19/ret/, @43/_L11/, @43/y/ */
  kcg_uint32 _L9_XRol32_1_QR_5_dual_1_generate_1_16;
  /* @18/_L1/, @19/_L5/, @19/x/ */
  kcg_uint32 _L5_XRol32_1_QR_5_dual_1_generate_1_16;
  /* @20/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_5_dual_1_generate_1_12;
  /* @18/_L10/, @20/_L9/, @20/ret/, @41/_L5/, @41/x/ */
  kcg_uint32 _L9_XRol32_2_QR_5_dual_1_generate_1_12;
  /* @18/_L7/, @20/_L5/, @20/x/ */
  kcg_uint32 _L5_XRol32_2_QR_5_dual_1_generate_1_12;
  /* @43/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_5_dual_1_generate_1_8;
  /* @41/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_5_dual_1_generate_1_7;
  /* @22/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_4_dual_1_generate_1_16;
  /* @21/_L6/, @22/_L9/, @22/ret/, @24/_L11/, @24/y/ */
  kcg_uint32 _L9_XRol32_1_QR_4_dual_1_generate_1_16;
  /* @21/_L1/, @22/_L5/, @22/x/ */
  kcg_uint32 _L5_XRol32_1_QR_4_dual_1_generate_1_16;
  /* @23/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_4_dual_1_generate_1_12;
  /* @21/_L10/, @23/_L9/, @23/ret/, @25/_L5/, @25/x/ */
  kcg_uint32 _L9_XRol32_2_QR_4_dual_1_generate_1_12;
  /* @21/_L7/, @23/_L5/, @23/x/ */
  kcg_uint32 _L5_XRol32_2_QR_4_dual_1_generate_1_12;
  /* @24/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_4_dual_1_generate_1_8;
  /* @25/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_4_dual_1_generate_1_7;
  /* @27/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_3_dual_1_generate_1_16;
  /* @26/_L6/, @27/_L9/, @27/ret/, @29/_L11/, @29/y/ */
  kcg_uint32 _L9_XRol32_1_QR_3_dual_1_generate_1_16;
  /* @26/_L1/, @27/_L5/, @27/x/ */
  kcg_uint32 _L5_XRol32_1_QR_3_dual_1_generate_1_16;
  /* @28/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_3_dual_1_generate_1_12;
  /* @26/_L10/, @28/_L9/, @28/ret/, @30/_L5/, @30/x/ */
  kcg_uint32 _L9_XRol32_2_QR_3_dual_1_generate_1_12;
  /* @26/_L7/, @28/_L5/, @28/x/ */
  kcg_uint32 _L5_XRol32_2_QR_3_dual_1_generate_1_12;
  /* @29/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_3_dual_1_generate_1_8;
  /* @30/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_3_dual_1_generate_1_7;
  /* @32/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_2_dual_1_generate_1_16;
  /* @31/_L6/, @32/_L9/, @32/ret/, @34/_L11/, @34/y/ */
  kcg_uint32 _L9_XRol32_1_QR_2_dual_1_generate_1_16;
  /* @31/_L1/, @32/_L5/, @32/x/ */
  kcg_uint32 _L5_XRol32_1_QR_2_dual_1_generate_1_16;
  /* @33/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_2_dual_1_generate_1_12;
  /* @31/_L10/, @33/_L9/, @33/ret/, @35/_L5/, @35/x/ */
  kcg_uint32 _L9_XRol32_2_QR_2_dual_1_generate_1_12;
  /* @31/_L7/, @33/_L5/, @33/x/ */
  kcg_uint32 _L5_XRol32_2_QR_2_dual_1_generate_1_12;
  /* @34/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_2_dual_1_generate_1_8;
  /* @35/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_2_dual_1_generate_1_7;
  /* @37/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_1_dual_1_generate_1_16;
  /* @36/_L6/, @37/_L9/, @37/ret/, @39/_L11/, @39/y/ */
  kcg_uint32 _L9_XRol32_1_QR_1_dual_1_generate_1_16;
  /* @36/_L1/, @37/_L5/, @37/x/ */
  kcg_uint32 _L5_XRol32_1_QR_1_dual_1_generate_1_16;
  /* @38/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_1_dual_1_generate_1_12;
  /* @36/_L10/, @38/_L9/, @38/ret/, @40/_L5/, @40/x/ */
  kcg_uint32 _L9_XRol32_2_QR_1_dual_1_generate_1_12;
  /* @36/_L7/, @38/_L5/, @38/x/ */
  kcg_uint32 _L5_XRol32_2_QR_1_dual_1_generate_1_12;
  /* @39/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_1_dual_1_generate_1_8;
  /* @40/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_1_dual_1_generate_1_7;
  kcg_bool cond_iterw;
  /* @18/_L14/, @18/coo/, @2/_L94/, @41/_L11/, @41/y/, @44/_L8/ */
  kcg_uint32 _L8_zeroTrailing_it_1;
  /* IfBlock1:else:_L7/, c/ */
  StreamChunk_slideTypes c_partial_20;
  /* @42/_L2/, @42/o/, IfBlock1:else:_L8/ */
  kcg_int32 _L8_else_IfBlock1_20;
  /* IfBlock1:else:_L4/ */
  kcg_int32 _L4_else_IfBlock1_20;
  /* @18/_L11/, @18/doo/, @2/_L95/, @43/_L9/, @43/ret/, IfBlock1:else:_L13/ */
  kcg_uint32 _L13_else_IfBlock1_20;
  int_slideTypes noname;
  /* @1/_L74/, @1/cipher/, _L69/, cipher/ */
  StreamChunk_slideTypes cipher_20;
  /* _L65/, rest/ */
  kcg_int32 rest_20;

  kcg_copy_State_nacl_core_chacha(noo_20, n_20);
  (*noo_20)[12] = (*n_20)[12] + kcg_lit_uint32(1);
  kcg_copy_State_nacl_core_chacha(&tmp, n_20);
  /* @1/_L73= */
  for (idx = 0; idx < 10; idx++) {
    _L5_XRol32_1_QR_1_dual_1_generate_1_16 = tmp[0] + tmp[4];
    _L10_XRol32_1_QR_1_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_1_dual_1_generate_1_16 ^ tmp[12];
    _L9_XRol32_1_QR_1_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_1_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_1_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_1_dual_1_generate_1_12 = tmp[8] +
      _L9_XRol32_1_QR_1_dual_1_generate_1_16;
    _L10_XRol32_2_QR_1_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_1_dual_1_generate_1_12 ^ tmp[4];
    _L9_XRol32_2_QR_1_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_1_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_1_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L76_dual_1_generate_1 = _L5_XRol32_1_QR_1_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_1_dual_1_generate_1_12;
    _L10_XRol32_3_QR_1_dual_1_generate_1_8 = _L76_dual_1_generate_1 ^
      _L9_XRol32_1_QR_1_dual_1_generate_1_16;
    _L79_dual_1_generate_1 = (_L10_XRol32_3_QR_1_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_1_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    _L78_dual_1_generate_1 = _L5_XRol32_2_QR_1_dual_1_generate_1_12 +
      _L79_dual_1_generate_1;
    _L10_XRol32_4_QR_1_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_1_dual_1_generate_1_12 ^ _L78_dual_1_generate_1;
    _L77_dual_1_generate_1 = (_L10_XRol32_4_QR_1_dual_1_generate_1_7 >>
        kcg_lit_uint32(25)) | kcg_lsl_uint32(
        _L10_XRol32_4_QR_1_dual_1_generate_1_7,
        kcg_lit_uint32(7));
    _L5_XRol32_1_QR_2_dual_1_generate_1_16 = tmp[1] + tmp[5];
    _L10_XRol32_1_QR_2_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_2_dual_1_generate_1_16 ^ tmp[13];
    _L9_XRol32_1_QR_2_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_2_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_2_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_2_dual_1_generate_1_12 = tmp[9] +
      _L9_XRol32_1_QR_2_dual_1_generate_1_16;
    _L10_XRol32_2_QR_2_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_2_dual_1_generate_1_12 ^ tmp[5];
    _L9_XRol32_2_QR_2_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_2_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_2_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L80_dual_1_generate_1 = _L5_XRol32_1_QR_2_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_2_dual_1_generate_1_12;
    _L10_XRol32_3_QR_2_dual_1_generate_1_8 = _L80_dual_1_generate_1 ^
      _L9_XRol32_1_QR_2_dual_1_generate_1_16;
    _L83_dual_1_generate_1 = (_L10_XRol32_3_QR_2_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_2_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    _L82_dual_1_generate_1 = _L5_XRol32_2_QR_2_dual_1_generate_1_12 +
      _L83_dual_1_generate_1;
    _L10_XRol32_4_QR_2_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_2_dual_1_generate_1_12 ^ _L82_dual_1_generate_1;
    _L81_dual_1_generate_1 = (_L10_XRol32_4_QR_2_dual_1_generate_1_7 >>
        kcg_lit_uint32(25)) | kcg_lsl_uint32(
        _L10_XRol32_4_QR_2_dual_1_generate_1_7,
        kcg_lit_uint32(7));
    _L5_XRol32_1_QR_3_dual_1_generate_1_16 = tmp[2] + tmp[6];
    _L10_XRol32_1_QR_3_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_3_dual_1_generate_1_16 ^ tmp[14];
    _L9_XRol32_1_QR_3_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_3_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_3_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_3_dual_1_generate_1_12 = tmp[10] +
      _L9_XRol32_1_QR_3_dual_1_generate_1_16;
    _L10_XRol32_2_QR_3_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_3_dual_1_generate_1_12 ^ tmp[6];
    _L9_XRol32_2_QR_3_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_3_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_3_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L84_dual_1_generate_1 = _L5_XRol32_1_QR_3_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_3_dual_1_generate_1_12;
    _L10_XRol32_3_QR_3_dual_1_generate_1_8 = _L84_dual_1_generate_1 ^
      _L9_XRol32_1_QR_3_dual_1_generate_1_16;
    _L87_dual_1_generate_1 = (_L10_XRol32_3_QR_3_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_3_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    _L86_dual_1_generate_1 = _L5_XRol32_2_QR_3_dual_1_generate_1_12 +
      _L87_dual_1_generate_1;
    _L10_XRol32_4_QR_3_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_3_dual_1_generate_1_12 ^ _L86_dual_1_generate_1;
    _L85_dual_1_generate_1 = (_L10_XRol32_4_QR_3_dual_1_generate_1_7 >>
        kcg_lit_uint32(25)) | kcg_lsl_uint32(
        _L10_XRol32_4_QR_3_dual_1_generate_1_7,
        kcg_lit_uint32(7));
    _L5_XRol32_1_QR_4_dual_1_generate_1_16 = tmp[3] + tmp[7];
    _L10_XRol32_1_QR_4_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_4_dual_1_generate_1_16 ^ tmp[15];
    _L9_XRol32_1_QR_4_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_4_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_4_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_4_dual_1_generate_1_12 = tmp[11] +
      _L9_XRol32_1_QR_4_dual_1_generate_1_16;
    _L10_XRol32_2_QR_4_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_4_dual_1_generate_1_12 ^ tmp[7];
    _L9_XRol32_2_QR_4_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_4_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_4_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L88_dual_1_generate_1 = _L5_XRol32_1_QR_4_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_4_dual_1_generate_1_12;
    _L10_XRol32_3_QR_4_dual_1_generate_1_8 = _L88_dual_1_generate_1 ^
      _L9_XRol32_1_QR_4_dual_1_generate_1_16;
    _L91_dual_1_generate_1 = (_L10_XRol32_3_QR_4_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_4_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    _L90_dual_1_generate_1 = _L5_XRol32_2_QR_4_dual_1_generate_1_12 +
      _L91_dual_1_generate_1;
    _L10_XRol32_4_QR_4_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_4_dual_1_generate_1_12 ^ _L90_dual_1_generate_1;
    _L89_dual_1_generate_1 = (_L10_XRol32_4_QR_4_dual_1_generate_1_7 >>
        kcg_lit_uint32(25)) | kcg_lsl_uint32(
        _L10_XRol32_4_QR_4_dual_1_generate_1_7,
        kcg_lit_uint32(7));
    _L5_XRol32_1_QR_5_dual_1_generate_1_16 = _L80_dual_1_generate_1 +
      _L85_dual_1_generate_1;
    _L10_XRol32_1_QR_5_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_5_dual_1_generate_1_16 ^ _L79_dual_1_generate_1;
    _L9_XRol32_1_QR_5_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_5_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_5_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_5_dual_1_generate_1_12 = _L90_dual_1_generate_1 +
      _L9_XRol32_1_QR_5_dual_1_generate_1_16;
    _L10_XRol32_2_QR_5_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_5_dual_1_generate_1_12 ^ _L85_dual_1_generate_1;
    _L9_XRol32_2_QR_5_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_5_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_5_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L92_dual_1_generate_1 = _L5_XRol32_1_QR_5_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_5_dual_1_generate_1_12;
    tmp[1] = _L92_dual_1_generate_1;
    _L10_XRol32_3_QR_5_dual_1_generate_1_8 = _L92_dual_1_generate_1 ^
      _L9_XRol32_1_QR_5_dual_1_generate_1_16;
    _L13_else_IfBlock1_20 = (_L10_XRol32_3_QR_5_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_5_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    tmp[12] = _L13_else_IfBlock1_20;
    _L8_zeroTrailing_it_1 = _L5_XRol32_2_QR_5_dual_1_generate_1_12 +
      _L13_else_IfBlock1_20;
    tmp[11] = _L8_zeroTrailing_it_1;
    _L10_XRol32_4_QR_5_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_5_dual_1_generate_1_12 ^ _L8_zeroTrailing_it_1;
    tmp[6] = (_L10_XRol32_4_QR_5_dual_1_generate_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_5_dual_1_generate_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_6_dual_1_generate_1_16 = _L88_dual_1_generate_1 +
      _L77_dual_1_generate_1;
    _L10_XRol32_1_QR_6_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_6_dual_1_generate_1_16 ^ _L87_dual_1_generate_1;
    _L9_XRol32_1_QR_6_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_6_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_6_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_6_dual_1_generate_1_12 = _L82_dual_1_generate_1 +
      _L9_XRol32_1_QR_6_dual_1_generate_1_16;
    _L10_XRol32_2_QR_6_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_6_dual_1_generate_1_12 ^ _L77_dual_1_generate_1;
    _L9_XRol32_2_QR_6_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_6_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_6_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L96_dual_1_generate_1 = _L5_XRol32_1_QR_6_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_6_dual_1_generate_1_12;
    tmp[3] = _L96_dual_1_generate_1;
    _L10_XRol32_3_QR_6_dual_1_generate_1_8 = _L96_dual_1_generate_1 ^
      _L9_XRol32_1_QR_6_dual_1_generate_1_16;
    _L99_dual_1_generate_1 = (_L10_XRol32_3_QR_6_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_6_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    tmp[14] = _L99_dual_1_generate_1;
    _L98_dual_1_generate_1 = _L5_XRol32_2_QR_6_dual_1_generate_1_12 +
      _L99_dual_1_generate_1;
    tmp[9] = _L98_dual_1_generate_1;
    _L10_XRol32_4_QR_6_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_6_dual_1_generate_1_12 ^ _L98_dual_1_generate_1;
    tmp[4] = (_L10_XRol32_4_QR_6_dual_1_generate_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_6_dual_1_generate_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_7_dual_1_generate_1_16 = _L84_dual_1_generate_1 +
      _L89_dual_1_generate_1;
    _L10_XRol32_1_QR_7_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_7_dual_1_generate_1_16 ^ _L83_dual_1_generate_1;
    _L9_XRol32_1_QR_7_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_7_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_7_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_7_dual_1_generate_1_12 = _L78_dual_1_generate_1 +
      _L9_XRol32_1_QR_7_dual_1_generate_1_16;
    _L10_XRol32_2_QR_7_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_7_dual_1_generate_1_12 ^ _L89_dual_1_generate_1;
    _L9_XRol32_2_QR_7_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_7_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_7_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L100_dual_1_generate_1 = _L5_XRol32_1_QR_7_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_7_dual_1_generate_1_12;
    tmp[2] = _L100_dual_1_generate_1;
    _L10_XRol32_3_QR_7_dual_1_generate_1_8 = _L100_dual_1_generate_1 ^
      _L9_XRol32_1_QR_7_dual_1_generate_1_16;
    _L103_dual_1_generate_1 = (_L10_XRol32_3_QR_7_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_7_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    tmp[13] = _L103_dual_1_generate_1;
    _L102_dual_1_generate_1 = _L5_XRol32_2_QR_7_dual_1_generate_1_12 +
      _L103_dual_1_generate_1;
    tmp[8] = _L102_dual_1_generate_1;
    _L10_XRol32_4_QR_7_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_7_dual_1_generate_1_12 ^ _L102_dual_1_generate_1;
    tmp[7] = (_L10_XRol32_4_QR_7_dual_1_generate_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_7_dual_1_generate_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_8_dual_1_generate_1_16 = _L76_dual_1_generate_1 +
      _L81_dual_1_generate_1;
    _L10_XRol32_1_QR_8_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_8_dual_1_generate_1_16 ^ _L91_dual_1_generate_1;
    _L9_XRol32_1_QR_8_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_8_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_8_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_8_dual_1_generate_1_12 = _L86_dual_1_generate_1 +
      _L9_XRol32_1_QR_8_dual_1_generate_1_16;
    _L10_XRol32_2_QR_8_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_8_dual_1_generate_1_12 ^ _L81_dual_1_generate_1;
    _L9_XRol32_2_QR_8_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_8_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_8_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L104_dual_1_generate_1 = _L5_XRol32_1_QR_8_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_8_dual_1_generate_1_12;
    tmp[0] = _L104_dual_1_generate_1;
    _L10_XRol32_3_QR_8_dual_1_generate_1_8 = _L104_dual_1_generate_1 ^
      _L9_XRol32_1_QR_8_dual_1_generate_1_16;
    _L107_dual_1_generate_1 = (_L10_XRol32_3_QR_8_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_8_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    tmp[15] = _L107_dual_1_generate_1;
    _L106_dual_1_generate_1 = _L5_XRol32_2_QR_8_dual_1_generate_1_12 +
      _L107_dual_1_generate_1;
    tmp[10] = _L106_dual_1_generate_1;
    _L10_XRol32_4_QR_8_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_8_dual_1_generate_1_12 ^ _L106_dual_1_generate_1;
    tmp[5] = (_L10_XRol32_4_QR_8_dual_1_generate_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_8_dual_1_generate_1_7, kcg_lit_uint32(7));
  }
  /* @1/_L74= */
  for (idx = 0; idx < 16; idx++) {
    cipher_20[idx] = tmp[idx] + (*n_20)[idx];
  }
  rest_20 = mlen_20 - i_20 * kcg_lit_int32(64);
  *goOn_20 = rest_20 > kcg_lit_int32(64);
  /* IfBlock1: */
  if (*goOn_20) {
    /* IfBlock1:then:_L3= */
    for (idx = 0; idx < 16; idx++) {
      (*c_20)[idx] = cipher_20[idx] ^ (*m_20)[idx];
    }
  }
  else {
    _L8_else_IfBlock1_20 = rest_20 - kcg_lit_int32(1);
    _L4_else_IfBlock1_20 = _L8_else_IfBlock1_20 / kcg_lit_int32(4);
    /* IfBlock1:else:_L13= */
    switch (_L8_else_IfBlock1_20 % kcg_lit_int32(4)) {
      case kcg_lit_int32(2) :
        _L13_else_IfBlock1_20 = kcg_lit_uint32(16777215);
        break;
      case kcg_lit_int32(1) :
        _L13_else_IfBlock1_20 = kcg_lit_uint32(65535);
        break;
      case kcg_lit_int32(0) :
        _L13_else_IfBlock1_20 = kcg_lit_uint32(255);
        break;
      default :
        _L13_else_IfBlock1_20 = kcg_lit_uint32(4294967295);
        break;
    }
    /* IfBlock1:else:_L6= */
    for (idx = 0; idx < 16; idx++) {
      _L8_zeroTrailing_it_1 = (*m_20)[idx] ^ cipher_20[idx];
      cond_iterw = /* IfBlock1:else:_L6= */(kcg_int32) idx < _L4_else_IfBlock1_20;
      /* @44/_L10= */
      if (cond_iterw) {
        c_partial_20[idx] = _L8_zeroTrailing_it_1;
      }
      else {
        c_partial_20[idx] = _L8_zeroTrailing_it_1 & _L13_else_IfBlock1_20;
      }
      noname = /* IfBlock1:else:_L6= */(kcg_int32) (idx + 1);
      /* IfBlock1:else:_L6= */
      if (!cond_iterw) {
        break;
      }
    }
#ifdef KCG_MAPW_CPY

    /* IfBlock1:else:_L6= */
    for (idx = /* IfBlock1:else:_L6= */(kcg_size) noname; idx < 16; idx++) {
      c_partial_20[idx] = kcg_lit_uint32(0);
    }
#endif /* KCG_MAPW_CPY */

    kcg_copy_StreamChunk_slideTypes(c_20, &c_partial_20);
  }
}

/*
  Expanded instances for: nacl::core::chacha::Xor/
  @1: (nacl::core::chacha::generate#1)
  @2: @1/(nacl::core::chacha::dual#1)
  @3: @2/(nacl::core::chacha::QR#8)
  @4: @3/(nacl::op::XRol32#1)
  @5: @3/(nacl::op::XRol32#2)
  @6: @3/(nacl::op::XRol32#3)
  @7: @3/(nacl::op::XRol32#4)
  @8: @2/(nacl::core::chacha::QR#7)
  @9: @8/(nacl::op::XRol32#1)
  @10: @8/(nacl::op::XRol32#2)
  @11: @8/(nacl::op::XRol32#3)
  @12: @8/(nacl::op::XRol32#4)
  @13: @2/(nacl::core::chacha::QR#6)
  @14: @13/(nacl::op::XRol32#1)
  @15: @13/(nacl::op::XRol32#2)
  @16: @13/(nacl::op::XRol32#3)
  @17: @13/(nacl::op::XRol32#4)
  @18: @2/(nacl::core::chacha::QR#5)
  @19: @18/(nacl::op::XRol32#1)
  @20: @18/(nacl::op::XRol32#2)
  @21: @2/(nacl::core::chacha::QR#4)
  @22: @21/(nacl::op::XRol32#1)
  @23: @21/(nacl::op::XRol32#2)
  @24: @21/(nacl::op::XRol32#3)
  @25: @21/(nacl::op::XRol32#4)
  @26: @2/(nacl::core::chacha::QR#3)
  @27: @26/(nacl::op::XRol32#1)
  @28: @26/(nacl::op::XRol32#2)
  @29: @26/(nacl::op::XRol32#3)
  @30: @26/(nacl::op::XRol32#4)
  @31: @2/(nacl::core::chacha::QR#2)
  @32: @31/(nacl::op::XRol32#1)
  @33: @31/(nacl::op::XRol32#2)
  @34: @31/(nacl::op::XRol32#3)
  @35: @31/(nacl::op::XRol32#4)
  @36: @2/(nacl::core::chacha::QR#1)
  @37: @36/(nacl::op::XRol32#1)
  @38: @36/(nacl::op::XRol32#2)
  @39: @36/(nacl::op::XRol32#3)
  @40: @36/(nacl::op::XRol32#4)
  @41: @18/(nacl::op::XRol32#4)
  @42: (M::dec#1)
  @43: @18/(nacl::op::XRol32#3)
  @44: (nacl::op::zeroTrailing_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Xor_nacl_core_chacha_20.c
** Generation date: 2020-03-11T13:42:25
*************************************************************$ */

