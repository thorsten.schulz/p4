/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "chacha_IETF_nacl_box_stream_23_20.h"

/* nacl::box::stream::chacha_IETF/ */
void chacha_IETF_nacl_box_stream_23_20(
  /* _L20/, msg/ */
  array_uint32_16_23 *msg_23_20,
  /* _L2/, _L35/, mlen/ */
  int_slideTypes mlen_23_20,
  /* _L1/, n/ */
  State_nacl_core_chacha *n_23_20,
  /* _L31/, cm/ */
  array_uint32_16_23 *cm_23_20)
{
  kcg_bool cond_iterw;
  State_nacl_core_chacha acc;
  kcg_size idx;
  int_slideTypes noname;
  State_nacl_core_chacha _1_noname;

  kcg_copy_State_nacl_core_chacha(&_1_noname, n_23_20);
  /* _L29= */
  if (mlen_23_20 > kcg_lit_int32(0)) {
    /* _L29= */
    for (idx = 0; idx < 23; idx++) {
      kcg_copy_State_nacl_core_chacha(&acc, &_1_noname);
      /* _L29=(nacl::core::chacha::Xor#1)/ */
      Xor_nacl_core_chacha_20(
        /* _L29= */(kcg_int32) idx,
        &acc,
        &(*msg_23_20)[idx],
        mlen_23_20,
        &cond_iterw,
        &_1_noname,
        &(*cm_23_20)[idx]);
      noname = /* _L29= */(kcg_int32) (idx + 1);
      /* _L29= */
      if (!cond_iterw) {
        break;
      }
    }
  }
  else {
    noname = kcg_lit_int32(0);
  }
#ifdef KCG_MAPW_CPY

  /* _L29= */
  for (idx = /* _L29= */(kcg_size) noname; idx < 23; idx++) {
    kcg_copy_StreamChunk_slideTypes(
      &(*cm_23_20)[idx],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
#endif /* KCG_MAPW_CPY */

}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** chacha_IETF_nacl_box_stream_23_20.c
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

