/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "unpack25519_nacl_op.h"

/* nacl::op::unpack25519/ */
void unpack25519_nacl_op(
  /* _L2/, i/ */
  Key32_slideTypes *i,
  /* _L4/, o/ */
  gf_nacl_op *o)
{
  kcg_size idx;
  /* _L1/ */
  gf_nacl_op _L1;

  /* _L1= */
  for (idx = 0; idx < 16; idx++) {
    _L1[idx] = /* _L1=(nacl::op::unpack25519_it1#1)/ */
      unpack25519_it1_nacl_op(/* _L1= */(kcg_uint32) idx, i);
  }
  kcg_copy_gf_nacl_op(o, &_L1);
  (*o)[15] = /* _L7= */(kcg_int64)
      (/* _L8= */(kcg_uint64) _L1[15] & kcg_lit_uint64(0x7fff));
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unpack25519_nacl_op.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

