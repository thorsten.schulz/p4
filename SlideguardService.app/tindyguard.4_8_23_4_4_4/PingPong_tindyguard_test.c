/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "PingPong_tindyguard_test.h"

/* tindyguard::test::PingPong/ */
void PingPong_tindyguard_test(
  /* trigger/ */
  kcg_bool trigger,
  /* _L54/, shutdown/ */
  kcg_bool shutdown,
  /* Echos/, _L883/ */
  size_tindyguardTypes *Echos,
  /* Seqoo/ */
  kcg_uint32 *Seqoo,
  /* Rechos/, _L895/ */
  size_tindyguardTypes *Rechos,
  /* Bechos/, _L906/ */
  size_tindyguardTypes *Bechos,
  /* _L59/, fail/, failure/ */
  kcg_bool *failure,
  outC_PingPong_tindyguard_test *outC)
{
  array_int32_4 tmp;
  array_uint32_16_23_4 tmp1;
  _3_array tmp2;
  size_tindyguardTypes acc;
  kcg_size idx;
  length_t_udp tmp3;
  array_uint32_16_23 tmp4;
  /* IfBlock2:then:_L10/, SM1:peer_setup:_L10/ */
  kcg_bool _L10_peer_setup_SM1;
  /* IfBlock2:then:_L21/ */
  kcg_bool _L21_then_IfBlock2;
  /* IfBlock2:then:_L25/ */
  kcg_bool _L25_then_IfBlock2;
  kcg_uint32 noname;
  IpAddress_udp _5_noname;
  kcg_bool _6_noname;
  kcg_uint64 _7_noname;
  array_bool_4 _8_noname;
  /* IfBlock2: */
  kcg_bool IfBlock2_clock;
  /* SM1: */
  SSM_ST_SM1 SM1_state_sel;
  /* _L4/, _L898/, icmpidx/ */
  size_tindyguardTypes icmpidx;
  /* IfBlock1:, IfBlock2:then:_L9/, _L3/ */
  kcg_bool _L3;
  /* _L2/, _L900/, inlen/ */
  array_int32_4 _L2;
  /* In/, _L1/, _L902/ */
  array_uint32_16_23_4 _L1;
  /* _L860/, echoReqmsg/ */
  array_uint32_16_23 _L860;

  tmp[1] = outC->echoRsplen;
  tmp[2] = nil_tindyguardTypes;
  tmp[3] = nil_tindyguardTypes;
  _L3 = trigger != outC->mem_trigger;
  outC->mem_trigger = trigger;
  /* IfBlock1: */
  if (_L3) {
    /* IfBlock1:then:_L4=(icmp::echoRequest#2)/ */
    echoRequest_icmp_23(
      (IpAddress_udp *) &rpi_ip_tindyguard_test,
      (IpAddress_udp *) &Slider_ip_tindyguard_test,
      kcg_lit_uint32(921),
      &tmp[0],
      &_L860,
      &noname,
      &outC->Context_echoRequest_2);
  }
  else {
    for (idx = 0; idx < 23; idx++) {
      kcg_copy_StreamChunk_slideTypes(
        &_L860[idx],
        (StreamChunk_slideTypes *) &Zero_udp);
    }
    tmp[0] = kcg_lit_int32(-1);
  }
  kcg_copy_array_uint32_16_23(&tmp1[2], &_L860);
  kcg_copy_array_uint32_16_23(&tmp1[3], &tmp1[2]);
  kcg_copy_array_uint32_16_23(&tmp1[0], &_L860);
  kcg_copy_array_uint32_16_23(&tmp1[1], &outC->echoRspmsg);
  SM1_state_sel = outC->SM1_state_nxt;
  /* SM1: */
  switch (SM1_state_sel) {
    case SSM_st_peer_setup_done_SM1 :
      break;
    case SSM_st_peer_setup_SM1 :
      kcg_copy_Peer_tindyguardTypes(
        &outC->_L907[4],
        (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
      kcg_copy_Peer_tindyguardTypes(
        &outC->_L907[5],
        (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
      kcg_copy_Peer_tindyguardTypes(
        &outC->_L907[6],
        (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
      kcg_copy_Peer_tindyguardTypes(
        &outC->_L907[7],
        (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
      /* SM1:peer_setup:_L72=(tindyguard::initPeer#1)/ */
      initPeer_tindyguard(
        (pub_tindyguardTypes *) &BigZ_pub_tindyguard_test,
        (secret_tindyguardTypes *) &preshared_tindyguard_test,
        (peer_t_udp *) &BigZ_endpoint_tindyguard_test,
        (range_t_udp *) &BigZ_allowed_tindyguard_test,
        &outC->_L907[0]);
      /* SM1:peer_setup:_L73=(tindyguard::initPeer#2)/ */
      initPeer_tindyguard(
        (pub_tindyguardTypes *) &ChickenJoe_pub_tindyguard_test,
        (secret_tindyguardTypes *) &preshared_tindyguard_test,
        (peer_t_udp *) &ChickenJoeLocal_endpoint_tindyguard_test,
        (range_t_udp *) &ChickenJoe_allowed_tindyguard_test,
        &outC->_L907[1]);
      /* SM1:peer_setup:_L79=(tindyguard::initPeer#5)/ */
      initPeer_tindyguard(
        (pub_tindyguardTypes *) &rpi_pub_tindyguard_test,
        (secret_tindyguardTypes *) &preshared_tindyguard_test,
        (peer_t_udp *) &rpi_endpoint_tindyguard_test,
        (range_t_udp *) &rpi_allowed_tindyguard_test,
        &outC->_L907[2]);
      /* SM1:peer_setup:_L75=(tindyguard::initPeer#4)/ */
      initPeer_tindyguard(
        (pub_tindyguardTypes *) &Lani_pub_tindyguard_test,
        (secret_tindyguardTypes *) &preshared_tindyguard_test,
        (peer_t_udp *) &Lani_endpoint_tindyguard_test,
        (range_t_udp *) &net10_allowed_tindyguard_test,
        &outC->_L907[3]);
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* _L891= */
  if (outC->init) {
    outC->init = kcg_false;
    kcg_copy__3_array(&tmp2, &outC->_L907);
  }
  else {
    kcg_copy__3_array(&tmp2, &outC->_L884);
  }
  /* _L2=(tindyguard::bindings#1)/ */
  bindings_tindyguard_4_8_23_4_4_4(
    &tmp,
    &tmp1,
    &tmp2,
    &outC->ownKey,
    shutdown,
    Slider_endpoint_tindyguard_test.port,
    (range_t_udp *) &anyAddress_udp,
    &_L2,
    &_L1,
    &icmpidx,
    &acc,
    &outC->_L884,
    &_7_noname,
    &_L3,
    &outC->Context_bindings_1);
  icmpidx = nil_tindyguardTypes;
  /* _L896= */
  for (idx = 0; idx < 4; idx++) {
    acc = icmpidx;
    /* _L896=(tindyguard::data::filter1#3)/ */
    filter1_tindyguard_data_23(
      /* _L896= */(kcg_int32) idx,
      acc,
      _L2[idx],
      &_L1[idx],
      PROTOCOL_icmp_ip,
      &_L10_peer_setup_SM1,
      &icmpidx,
      &_8_noname[idx]);
    /* _L896= */
    if (!_L10_peer_setup_SM1) {
      break;
    }
  }
  /* SM1: */
  switch (SM1_state_sel) {
    case SSM_st_peer_setup_done_SM1 :
      IfBlock2_clock = kcg_false;
      break;
    case SSM_st_peer_setup_SM1 :
      /* SM1:peer_setup:_L10=(tindyguard::initOur#3)/ */
      initOur_tindyguard(
        (Key32_slideTypes *) &Slider_priv_tindyguard_test,
        &_L10_peer_setup_SM1,
        &outC->ownKey,
        &IfBlock2_clock,
        &outC->Context_initOur_3);
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  *failure = _L3 || IfBlock2_clock;
  /* SM1: */
  switch (SM1_state_sel) {
    case SSM_st_peer_setup_done_SM1 :
      outC->SM1_state_nxt = SSM_st_peer_setup_done_SM1;
      break;
    case SSM_st_peer_setup_SM1 :
      _L3 = !_L10_peer_setup_SM1;
      if (!*failure && _L3) {
        outC->SM1_state_nxt = SSM_st_peer_setup_done_SM1;
      }
      else {
        outC->SM1_state_nxt = SSM_st_peer_setup_SM1;
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  IfBlock2_clock = icmpidx != nil_tindyguardTypes;
  /* IfBlock2: */
  if (IfBlock2_clock) {
    if (kcg_lit_int32(0) <= icmpidx && icmpidx < kcg_lit_int32(4)) {
      tmp3 = _L2[icmpidx];
      kcg_copy_array_uint32_16_23(&tmp4, &_L1[icmpidx]);
    }
    else {
      tmp3 = nil_udp;
      for (idx = 0; idx < 23; idx++) {
        kcg_copy_chunk_t_udp(&tmp4[idx], (chunk_t_udp *) &Zero_udp);
      }
    }
    /* IfBlock2:then:_L3=(icmp::echoTake#2)/ */
    echoTake_icmp_23(
      tmp3,
      &tmp4,
      (IpAddress_udp *) &Slider_ip_tindyguard_test,
      &outC->echoRsplen,
      &outC->echoRspmsg,
      Seqoo,
      &_5_noname,
      &noname,
      &_6_noname,
      &_L3,
      &_L10_peer_setup_SM1);
    _L25_then_IfBlock2 = outC->echoRsplen < kcg_lit_int32(0);
    _L21_then_IfBlock2 = !_6_noname;
    /* IfBlock2:then:_L29= */
    if (noname == kcg_lit_uint32(921) && _L21_then_IfBlock2 &&
      !_L10_peer_setup_SM1 && _L25_then_IfBlock2) {
      *Echos = outC->goodEchos + kcg_lit_int32(1);
    }
    else {
      *Echos = outC->goodEchos;
    }
    /* IfBlock2:then:_L32= */
    if (_6_noname) {
      *Bechos = outC->badEchos + kcg_lit_int32(1);
    }
    else {
      *Bechos = outC->badEchos;
    }
    /* IfBlock2:then:_L35= */
    if (_L21_then_IfBlock2 && !_L25_then_IfBlock2) {
      *Rechos = outC->reqEchos + kcg_lit_int32(1);
    }
    else {
      *Rechos = outC->reqEchos;
    }
  }
  else {
    *Seqoo = kcg_lit_uint32(0);
    outC->echoRsplen = nil_udp;
    *Echos = outC->goodEchos;
    *Bechos = outC->badEchos;
    *Rechos = outC->reqEchos;
  }
  outC->reqEchos = *Rechos;
  outC->badEchos = *Bechos;
  outC->goodEchos = *Echos;
}

#ifndef KCG_USER_DEFINED_INIT
void PingPong_init_tindyguard_test(outC_PingPong_tindyguard_test *outC)
{
  kcg_size idx;
  kcg_size idx1;

  outC->mem_trigger = kcg_false;
  outC->init = kcg_true;
  for (idx = 0; idx < 8; idx++) {
    for (idx1 = 0; idx1 < 8; idx1++) {
      outC->_L907[idx].tpub[idx1] = kcg_lit_uint32(0);
    }
    for (idx1 = 0; idx1 < 8; idx1++) {
      outC->_L907[idx].preshared[idx1] = kcg_lit_uint32(0);
    }
    outC->_L907[idx].allowed.net.addr = kcg_lit_uint32(0);
    outC->_L907[idx].allowed.mask.addr = kcg_lit_uint32(0);
    for (idx1 = 0; idx1 < 16; idx1++) {
      outC->_L907[idx].hcache.salt[idx1] = kcg_lit_uint32(0);
    }
    for (idx1 = 0; idx1 < 8; idx1++) {
      outC->_L907[idx].hcache.hash1[idx1] = kcg_lit_uint32(0);
    }
    for (idx1 = 0; idx1 < 8; idx1++) {
      outC->_L907[idx].hcache.cookieHKey[idx1] = kcg_lit_uint32(0);
    }
    outC->_L907[idx].endpoint.addr.addr = kcg_lit_uint32(0);
    outC->_L907[idx].endpoint.port = kcg_lit_int32(0);
    outC->_L907[idx].endpoint.time = kcg_lit_int32(0);
    outC->_L907[idx].endpoint.mtime = kcg_lit_int64(0);
    for (idx1 = 0; idx1 < 3; idx1++) {
      outC->_L907[idx].tai[idx1] = kcg_lit_uint32(0);
    }
    outC->_L907[idx].timedOut = kcg_true;
    outC->_L907[idx].bestSession = kcg_lit_int32(0);
    outC->_L907[idx].inhibitInit = kcg_true;
    for (idx1 = 0; idx1 < 16; idx1++) {
      outC->_L907[idx].cookie.content[idx1] = kcg_lit_uint32(0);
    }
    outC->_L907[idx].cookie.opened = kcg_lit_int64(0);
    for (idx1 = 0; idx1 < 8; idx1++) {
      outC->_L884[idx].tpub[idx1] = kcg_lit_uint32(0);
    }
    for (idx1 = 0; idx1 < 8; idx1++) {
      outC->_L884[idx].preshared[idx1] = kcg_lit_uint32(0);
    }
    outC->_L884[idx].allowed.net.addr = kcg_lit_uint32(0);
    outC->_L884[idx].allowed.mask.addr = kcg_lit_uint32(0);
    for (idx1 = 0; idx1 < 16; idx1++) {
      outC->_L884[idx].hcache.salt[idx1] = kcg_lit_uint32(0);
    }
    for (idx1 = 0; idx1 < 8; idx1++) {
      outC->_L884[idx].hcache.hash1[idx1] = kcg_lit_uint32(0);
    }
    for (idx1 = 0; idx1 < 8; idx1++) {
      outC->_L884[idx].hcache.cookieHKey[idx1] = kcg_lit_uint32(0);
    }
    outC->_L884[idx].endpoint.addr.addr = kcg_lit_uint32(0);
    outC->_L884[idx].endpoint.port = kcg_lit_int32(0);
    outC->_L884[idx].endpoint.time = kcg_lit_int32(0);
    outC->_L884[idx].endpoint.mtime = kcg_lit_int64(0);
    for (idx1 = 0; idx1 < 3; idx1++) {
      outC->_L884[idx].tai[idx1] = kcg_lit_uint32(0);
    }
    outC->_L884[idx].timedOut = kcg_true;
    outC->_L884[idx].bestSession = kcg_lit_int32(0);
    outC->_L884[idx].inhibitInit = kcg_true;
    for (idx1 = 0; idx1 < 16; idx1++) {
      outC->_L884[idx].cookie.content[idx1] = kcg_lit_uint32(0);
    }
    outC->_L884[idx].cookie.opened = kcg_lit_int64(0);
  }
  /* SM1:peer_setup:_L10=(tindyguard::initOur#3)/ */
  initOur_init_tindyguard(&outC->Context_initOur_3);
  /* _L2=(tindyguard::bindings#1)/ */
  bindings_init_tindyguard_4_8_23_4_4_4(&outC->Context_bindings_1);
  /* IfBlock1:then:_L4=(icmp::echoRequest#2)/ */
  echoRequest_init_icmp_23(&outC->Context_echoRequest_2);
  outC->reqEchos = kcg_lit_int32(0);
  outC->badEchos = kcg_lit_int32(0);
  outC->goodEchos = kcg_lit_int32(0);
  kcg_copy_KeySalted_tindyguardTypes(
    &outC->ownKey,
    (KeySalted_tindyguardTypes *) &EmptyKey_tindyguardTypes);
  for (idx = 0; idx < 23; idx++) {
    kcg_copy_StreamChunk_slideTypes(
      &outC->echoRspmsg[idx],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  outC->echoRsplen = kcg_lit_int32(-1);
  outC->SM1_state_nxt = SSM_st_peer_setup_SM1;
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void PingPong_reset_tindyguard_test(outC_PingPong_tindyguard_test *outC)
{
  kcg_size idx;

  outC->mem_trigger = kcg_false;
  outC->init = kcg_true;
  /* SM1:peer_setup:_L10=(tindyguard::initOur#3)/ */
  initOur_reset_tindyguard(&outC->Context_initOur_3);
  /* _L2=(tindyguard::bindings#1)/ */
  bindings_reset_tindyguard_4_8_23_4_4_4(&outC->Context_bindings_1);
  /* IfBlock1:then:_L4=(icmp::echoRequest#2)/ */
  echoRequest_reset_icmp_23(&outC->Context_echoRequest_2);
  outC->reqEchos = kcg_lit_int32(0);
  outC->badEchos = kcg_lit_int32(0);
  outC->goodEchos = kcg_lit_int32(0);
  kcg_copy_KeySalted_tindyguardTypes(
    &outC->ownKey,
    (KeySalted_tindyguardTypes *) &EmptyKey_tindyguardTypes);
  for (idx = 0; idx < 23; idx++) {
    kcg_copy_StreamChunk_slideTypes(
      &outC->echoRspmsg[idx],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  outC->echoRsplen = kcg_lit_int32(-1);
  outC->SM1_state_nxt = SSM_st_peer_setup_SM1;
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** PingPong_tindyguard_test.c
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

