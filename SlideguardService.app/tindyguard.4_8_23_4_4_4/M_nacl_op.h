/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */
#ifndef _M_nacl_op_H_
#define _M_nacl_op_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::M/ */
extern void M_nacl_op(
  /* _L52/, a/ */
  gf_nacl_op *a,
  /* _L51/, _L54/, b/ */
  gf_nacl_op *b,
  /* @1/_L6/, @1/o/, _L53/, o/ */
  gf_nacl_op *o);

/*
  Expanded instances for: nacl::op::M/
  @1: (nacl::op::car25519#8)
*/

#endif /* _M_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** M_nacl_op.h
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

