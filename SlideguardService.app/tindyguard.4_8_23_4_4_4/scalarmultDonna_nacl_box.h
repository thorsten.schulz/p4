/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _scalarmultDonna_nacl_box_H_
#define _scalarmultDonna_nacl_box_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "assertGoodKey_nacl_box.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::box::scalarmultDonna/ */
extern void scalarmultDonna_nacl_box(
  /* _L3/, our/ */
  KeyPair32_slideTypes *our,
  /* _L2/, their/ */
  Key32_slideTypes *their,
  /* IfBlock1:, check/ */
  kcg_bool check,
  /* failed/ */
  kcg_bool *failed,
  /* _L56/, k/, key/ */
  Key32_slideTypes *key);



#endif /* _scalarmultDonna_nacl_box_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarmultDonna_nacl_box.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

