/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _wrap_tindyguard_data_23_H_
#define _wrap_tindyguard_data_23_H_

#include "kcg_types.h"
#include "aead_nacl_box_23_1_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::data::wrap/ */
extern void wrap_tindyguard_data_23(
  /* @1/_L1/, @1/i/, _L65/, len/ */
  size_slideTypes len_23,
  /* msg/ */
  array_uint32_16_23 *msg_23,
  /* s/ */
  Session_tindyguardTypes *s_23,
  /* slength/ */
  length_t_udp *slength_23,
  /* _L75/, soffs/ */
  length_t_udp *soffs_23,
  /* datamsg/ */
  array_uint32_16_24 *datamsg_23,
  /* soo/ */
  Session_tindyguardTypes *soo_23,
  /* _L73/, fail/, failed/ */
  kcg_bool *failed_23);

/*
  Expanded instances for: tindyguard::data::wrap/
  @1: (M::dec#2)
*/

#endif /* _wrap_tindyguard_data_23_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** wrap_tindyguard_data_23.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

