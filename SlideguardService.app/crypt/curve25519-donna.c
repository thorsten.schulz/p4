#include <string.h>

#include "kcg_types.h"

#ifndef __WORDSIZE
#if __SIZEOF_POINTER__ == 8
#define __WORDSIZE 64
#else
#define __WORDSIZE 32
#endif
#endif
/* Choose the version of curve25519-donna based on the word size */
#if __WORDSIZE == 64 && defined(__GNUC__)
#include "donna/curve25519-donna-c64.c"
#else
#include "donna/curve25519-donna.c"
#endif

void curve25519_donna_nacl_box( Key32_slideTypes *ourPriv, Key32_slideTypes *their, Key32_slideTypes *key);

void curve25519_donna_nacl_box( Key32_slideTypes *ourPriv, Key32_slideTypes *their, Key32_slideTypes *key) {
	curve25519_donna((uint8_t *)key, (uint8_t *)ourPriv, (uint8_t *)their);
}
