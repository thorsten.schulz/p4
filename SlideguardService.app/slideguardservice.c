/*
 ============================================================================
 Name        : slideguardservice.c aka tindyservice.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Version     :
 Copyright   : (c) 2019-2020 Universität Rostock
 Description : PikeOS-Stub for SlideGuard-Scade model
 SPDX-License-Identifier: EUPL-1.2
 ============================================================================
 */
#define _GNU_SOURCE
#include <string.h>
#include <ctype.h>

#include <stdio.h>
/* include the trial app's header for some useful definitions. The actual pingpong operator is not use though */
/* this also pulls the general binding to the tindyguard service */
#include "PingPong_tindyguard_test.h"
/* include some extra helpers for initialization */
#include "initOur_tindyguard.h"
#include "initPeer_tindyguard.h"
/* as the name suggests */
#include "kcg_consts.h"

#if !defined(PIKEOS_NATIVE)
#include <unistd.h> /* usleep() */
#include <stdlib.h>
#include <time.h>   /* clock_t + clock() */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#else
/* PSSW API. */
#include <vm.h>
/* PikeOS Personality Extensions API */
#include <p4ext/p4ext_assert.h>
#include <p4ext/p4ext_threads.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdarg.h>

/* ANIS socket API. */
#include <socket.h>

typedef vm_port_desc_t fdp_t;
typedef vm_file_desc_t fd_t;
#define clock() (p4_get_time()/1000)
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

#endif

/* try to generally abstract info/error printing */
#ifdef PIKEOS_NATIVE
#define dprint(...) vm_cprintf("tindy:: "__VA_ARGS__)
#define iprint(...) vm_cprintf("tindy:: "__VA_ARGS__)
#elif defined( _RM57Lx_ )
#define dprint(...) { char s[256]; snprintf(s, sizeof(s), __VA_ARGS__); sciDisplayText(s); }
#define iprint(...) { char s[256]; snprintf(s, sizeof(s), __VA_ARGS__); sciDisplayText(s); }
#else
#define dprint(...) fprintf(stderr, "tindy:: "__VA_ARGS__), fflush(stderr)
#define iprint(...) fprintf(stdout, "tindy:: "__VA_ARGS__), fflush(stdout)
#endif

#define IP(a,b,c,d)    ((a)<<24|(b)<<16|(c)<<8|(d))
#define DIM(a)         (  sizeof(a)/sizeof(a[0])  )
#define IP_SPLIT4(src) ((src.addr>>24)&0xFF), ((src.addr>>16)&0xFF), ((src.addr>>8)&0xFF), ((src.addr>>0)&0xFF)

/*
 * Ethernet frame strategy:
 * lwIP adds an Ethernet header, namely the Ethernet addresses and the Ethernet frame type. The addresses are
 * meaningless and can be discarded for outgoing and ~0 padded for incoming packets. However, lwIP will do useless ARP
 * requests and we must tailor foe replies for that.
 *
 */

#define ETH_HWADDR_LEN 6
#define ETH_TYPE_IP4_BE 0x0008U
#define ETH_TYPE_ARP_BE 0x0608U
#define ETH_TYPE_IP6_BE 0xDD86U

#define DEFAULT_DEST { .addr = {  ~0,  ~0,  ~0,  ~0,  ~0,  ~0 } }
#define DEFAULT_SRC  { .addr = {0x32,0x34,0x36,0x38,0x3A,0x3C } }

struct eth_addr {
	uint8_t addr[ETH_HWADDR_LEN];
};

struct eth_hdr {
	struct eth_addr dest;
	struct eth_addr src;
	uint16_t type;
}; /* 14 */

struct eth_frame {
	struct eth_hdr header; /* 14 */
	/* VLAN tags are forbidden. */
	uint8_t pdu[1522];     /* 1522 */
};

#define ARP_OPER_REQUEST_BE   0x0100
#define ARP_OPER_REPLY_BE     0x0200
#define ARP_HTYPE_ETHERNET_BE 0x0100
#define ARP_PTYPE_IP4         ETH_TYPE_IP4_BE
#define ARP_HLEN              6
#define ARP_PLEN_IP4          4

struct arp_frame {
	uint16_t htype;
	uint16_t ptype;
	uint8_t hlen;
	uint8_t plen;
	uint16_t oper;
	struct eth_addr sha;
/* avoid packed-attrib with work around for 16bit-access */
/*	IpAddress_udp spa; */
	uint16_t spa[2];
	struct eth_addr tha;
	IpAddress_udp tpa;
}; /* 28 */

#define DEBUG_Q_IO

/* Define default names for the qports to make life easier. Can be overwritten on the command line. */
#define QPORTSVCW_FILENAME "ethFrameOut" /* pipe is opened for WR, SOURCE */
#define QPORTSVCR_FILENAME "ethFrameIn" /* pipe is opened for RD, DESTINATION */

/* IMPORTANT currently, we block on packet transfer to the main app and only poke for available packets. */
#define QPORTSVCW_TIMEOUT P4_TIMEOUT_INFINITE
#define QPORTSVCR_TIMEOUT P4_TIMEOUT_NULL
/* define to send Ethernet frames across the QPort */
#define QPORTSVC_ETH

#define QSVCW 0 /* this is from Slideguard to the middleware */
#define QSVCR 1 /* this is from the middleware to Slideguard */

/* link some constants from kcg_consts */
#define dimKeyRetries retryKey_tindyguard_conf
#define dimPeer        dimPeer_tindyguard_test

/* adapt, if Scade dimensional parameters are changed */
/* _A_B_C_D_E_F := A=Out B=Peers C=Data D=Session E=RXDbuffer F=TXDbuffer
   A-Out   := dim of input latch, should be less or equal to F
   B-Peer  := dim of knownPeers, should match configuration
   C-Data  := max. length of pdu (ie. "MTU"), set to 23 for max size (=1472). This buffersize must also include IP and
              UDP or TCP header
   D-Sess  := Available worker sessions. This greatly affects performance: larger = more execution time, more memory.
              You will need at least the number of connected concurrent peers, then, every peer may initiate a new
              handshake in parallel to an existing connection, and there may also be erraneous connection requests
              that need to be handled before they can be discarded. NOTE: limited to D<=32
   E-RXDb  := dim of rx. This is also the dimension of maximum messages to be read from the underlying socket per
              cycle. Keep in mind, that internal buffer list is also used for session initiation/response messages.
   F-TXDb  := Message buffer for internal handling of outgoing messages. Should be at least DIMout, maybe larger to
              hold messages between cycles, when the session has to be initiated first.
*/
/* for the 8_8_23_12_16_16 config, the context and in/out buffers take roughly 70 pages / 278k. No idea about stack use. */
#define outC_bindings_tindyguard outC_bindings_tindyguard_8_8_23_12_16_16
#define bindings_init_tindyguard bindings_init_tindyguard_8_8_23_12_16_16
#define bindings_tindyguard           bindings_tindyguard_8_8_23_12_16_16
#define echoTake_icmp                           echoTake_icmp_23
#define THIS_HOST_IP { .addr =   IP(172,21,0,102), } /* only needed for PING, should derive IP from iface config. See
 the wg*.conf file for overall IP addresses in this demo. */

/* these structs wrap the data going in and out of the Scade root operator */
struct scade_in {
	kcg_int32 length_tx[dimLatch_tindyguard_test];
	array_uint32_16 pdu_tx[dimLatch_tindyguard_test][dimData_tindyguard_test];
	Peer_tindyguardTypes knownPeers[dimPeer];
	KeySalted_tindyguardTypes sks;
	kcg_bool shutdown;
	port_t_udp port;
	range_t_udp if_hint;
};

struct scade_out {
	kcg_int32 length_rx[dimRx_tindyguard_test];
	array_uint32_16 pdu_rx[dimRx_tindyguard_test][dimData_tindyguard_test];
	size_tindyguardTypes taken;
	size_tindyguardTypes tx_pdu_available;
	kcg_uint64 no_peer_mask;
	kcg_bool fd_failed;
};

struct sg_ctx {
	struct scade_in  in;
	struct scade_out out;
	fdp_t fdq[2];
	IpAddress_udp hostip;
	struct eth_addr target_addr;
	size_t idx_tx;
};

int sg_service_parseconf( const char *config, struct sg_ctx *sg );
int sg_service_ldconf(  int *argc, const char ***argv, struct sg_ctx *sg );
int sg_service_provide( int *argc, const char ***argv, struct sg_ctx *sg );

int isPingR(uint8_t *pdu, length_t_udp l);
int sg_service_handlePing(int idx_rx, struct sg_ctx *sg);
int sg_service_handleArp( struct arp_frame *af, struct sg_ctx *sg );
int sg_service_push(uint8_t *ipFrame, length_t_udp ipFrameLength, struct sg_ctx *sg);
int sg_service_pop(uint8_t *ipFrame, length_t_udp *ipFrameLength, struct sg_ctx *sg);

const char *strcasestr(const char *buf, const char *needle);
static int parseIPle(const char *str, IpAddress_udp *a, const char **end);
static int parsePeer(const char *val, peer_t_udp *peer);
static int parseRange(const char *val, range_t_udp *range);
static const char *getParam(const char *buf, const char *key, const char **end);
static int base64decode (const char *in, size_t inLen, char *out, size_t *outLen);


int sg_service_parseconf( const char *config, struct sg_ctx *sg ) {
	outC_initOur_tindyguard context;
	initOur_init_tindyguard( &context );
	const size_t peer_cap = DIM(sg->in.knownPeers);
	kcg_bool again=kcg_true, failed=kcg_false;
	Key32_slideTypes priv;
	const char *vtail=NULL;
	const char *vnose;
	size_t len=0;

	if ( (vnose = getParam(config, "ListenPort", &vtail)) ) {
		long int pt = strtoul(vnose, (char **)&vtail, 0);
		if (vtail > vnose && pt > 0 && pt < 0xffff ) sg->in.port = pt;
	}
	if ( sg->in.port <= 0 ) dprint("There is no \"ListenPort\" configured.\n");

	len = sizeof(Key32_slideTypes);
	if ( (vnose = getParam(config, "PrivateKey", &vtail)) && (0 == base64decode(vnose, vtail-vnose, (char *)priv, &len)) ) {
		do {
			initOur_tindyguard( &priv, &again, &sg->in.sks, &failed, &context );
			if (failed) return EXIT_FAILURE;
		} while (again); /* initOur will not run forever and after configured retries fail */
	} else
		dprint("There is no \"PrivateKey\" configured.\n");

	pub_tindyguardTypes pub;
	secret_tindyguardTypes psk;
	peer_t_udp endpoint = { .time =0, .mtime=0, };
	range_t_udp allowed;
	const char *section = config;
	const char *nextsection = NULL;

	P4_size_t i=0;
	for (; i<peer_cap; i++) {
		section = getParam(section, "[Peer]", &vtail);
		if (!section) break;
		nextsection = getParam(vtail, "[Peer]", NULL);
		if (!nextsection) nextsection = section + strlen(section);
		len = sizeof(secret_tindyguardTypes);

		if ( !(vnose = getParam(section, "PresharedKey", &vtail))
			|| vnose > nextsection
			|| ( 0 != base64decode(vnose, vtail-vnose, (char *)psk, &len)) )
			memset(psk, 0, sizeof(secret_tindyguardTypes));

		len = sizeof(pub_tindyguardTypes);
		if ( !(vnose = getParam(section, "PublicKey", &vtail))
			|| vnose > nextsection
			|| ( 0 != base64decode(vnose, vtail-vnose, (char *)pub, &len)) )
			continue;

		if ( !(vnose = getParam(section, "AllowedIPs", &vtail))
			|| vnose > nextsection
			|| ( EXIT_FAILURE == parseRange(vnose, &allowed) ) )
			continue;

		if ( !(vnose = getParam(section, "Endpoint", &vtail))
			|| vnose > nextsection
			|| ( EXIT_FAILURE == parsePeer(vnose, &endpoint) ) )
			continue;

		initPeer_tindyguard( &pub, &psk, &endpoint, &allowed, &sg->in.knownPeers[i] );
	}
	iprint("Latched peer configuration %ld/%ld.\n", i, peer_cap);

	return EXIT_SUCCESS;
}

int sg_service_ldconf( int *argc, const char ***argv, struct sg_ctx *sg ) {
#ifdef PIKEOS_NATIVE
	const char *fncfg;
	if ((*argc) >= 2) {
		fncfg = (*argv)[1];
		iprint("Loading: %s\n", fncfg);
		(*argc)--; /* parse args "ate" the first cmdline argument, so skip it */
		(*argv)++;
	} else {
		dprint("Could not start: %s had bad arguments (%d).", (*argv)[0], *argc);
		return EXIT_FAILURE;
	}
	char configdata[0x4000];
	vm_file_stat_t conf_file_properties;
	vm_file_desc_t fd_conf;
	P4_e_t err;

	err = vm_open( fncfg, VM_O_RD, &fd_conf);
	if ( err ) {
		dprint("open: %s\n", p4_strerror(err));
		return EXIT_FAILURE;
	}
	err = vm_fstat( &fd_conf, &conf_file_properties);
	if ( err ) { /* To obtain file size */
		dprint("fstat: %s\n", p4_strerror(err));
		return EXIT_FAILURE;
	}
	p4ext_assert( conf_file_properties.size <= (vm_off_t)sizeof(configdata) );
	P4_size_t todo=conf_file_properties.size;
	char *conf_buf_wr = configdata;
	while ( todo ) {
		P4_size_t read;
		err = vm_read( &fd_conf, conf_buf_wr, todo, &read);
		p4ext_assert( err == P4_E_OK );
		if ( !read ) break;
		conf_buf_wr += read;
		todo -= read;
	}

	vm_close( &fd_conf );
	return sg_service_parseconf( configdata, sg );

#else
	/* -/- */
#endif
	return EXIT_SUCCESS;
}

int sg_service_provide( int *argc, const char ***argv, struct sg_ctx *sg ) {
	P4_e_t err=0;
	const char* fnw = QPORTSVCW_FILENAME;
	const char* fnr = QPORTSVCR_FILENAME;
	if (*argc == 3) {
		fnr   = (*argv)[1];
		fnw   = (*argv)[2];
		(*argc)-=2; /* we ate some more cmdline arguments */
		(*argv)+=2;
	}

    err = vm_qport_open(fnr, VM_PORT_DESTINATION, &sg->fdq[QSVCR]);
    if (err != P4_E_OK) {
        dprint("Partition %d: Error in vm_qport_open(%s), %s\n", p4_my_respart(), fnr, p4_strerror(err));
        return 2;
    }

    err = vm_qport_open(fnw, VM_PORT_SOURCE, &sg->fdq[QSVCW]);
    if (err != P4_E_OK) {
        dprint("Partition %d: Error in vm_qport_open(%s), %s\n", p4_my_respart(), fnw, p4_strerror(err));
        return 2;
    }

	return EXIT_SUCCESS;
}

int main(int argc, const char *argv[]) {

	outC_bindings_tindyguard  context ;
	bindings_init_tindyguard(&context);
	struct sg_ctx sg = {
		.in = {
			.sks = EmptyKey_tindyguardTypes,
			.shutdown = kcg_false,
			.port     = nil_udp,
		/* interface selection is not available in PikeOS/ANIS */
		/*  .if_hint  = { .net  = { THIS_HOST_IP, }, .mask = { ~0, } },  */
		},
		.hostip      = THIS_HOST_IP,
		.target_addr = DEFAULT_DEST,
	};
	for (size_t i=0; i<DIM(sg.in.length_tx);  i++) sg.in.length_tx[i] = nil_udp;
	for (size_t i=0; i<DIM(sg.in.knownPeers); i++) sg.in.knownPeers[i] = EmptyPeer_tindyguardTypes;


	p4ext_assert( EXIT_SUCCESS == sg_service_ldconf(  &argc, &argv, &sg ) );

	p4ext_assert( EXIT_SUCCESS == sg_service_provide( &argc, &argv, &sg) );

	do {
		bindings_tindyguard(
			&sg.in.length_tx,
			&sg.in.pdu_tx,
			&sg.in.knownPeers,
			&sg.in.sks,
			 sg.in.shutdown,
			 sg.in.port,
			&sg.in.if_hint,

			&sg.out.length_rx,
			&sg.out.pdu_rx,
			&sg.out.taken,
			&sg.out.tx_pdu_available,
			&sg.in.knownPeers, /* feed-back required, so we pass an in-param. */
			&sg.out.no_peer_mask,
			&sg.out.fd_failed,
			&context
		);

	/* fd_failed is set, when one of the socket operations failed. The inner state machine would try to reestablish the
	   socket, but the wrapping loop will exit. */
		if (sg.out.fd_failed) dprint("[ FAILED ] tindyguard's fd failed.\n");

	/* This is important, the operator cannot change the inputs. */
		for (int i=0; i<sg.out.taken; i++) sg.in.length_tx[i] = nil_udp; /* discard sent packets */
		sg.idx_tx = 0;
	/* Packets, that were not sent, could be moved to the front, to get more priority next time.
	   Currently, this is not the case. */

	/* Now, try to handle incoming packets. length_rx and pdu_rx will be overwritten in the next run. */
		for ( size_t i=0; i<DIM(sg.out.length_rx); i++ ) {
			
			/* require at least IP packets and drop other rubbish. Ping request will be handled later. */
			if ( sg.out.length_rx[i] > headerBytes_ip 
				&& !isPingR( (uint8_t *)(sg.out.pdu_rx[i]), sg.out.length_rx[i]) ) {
				
				/* should restrict ICMP
				   to ICMP.Type=[3:unreachable,5:redirect,9,10:router-solicit,11:timeout,12:buggy-DG,13,14:timestamp] */
				/* if ( protocol == VM_PROTO_UDP && sg.out.length_rx[i] >= headerBytes_ip+8 ) { } */
				/* if ( protocol == VM_PROTO_TCP && sg.out.length_rx[i] >= headerBytes_ip+8 ) { } */

				/* Not differentiating here, hand it all over to the app */
				sg_service_push((uint8_t *)(sg.out.pdu_rx[i]), sg.out.length_rx[i], &sg);
			}
		}

	/* This could also just be a yield. It has no meaning other than throttling / let other partitions be scheduled. */
		p4ext_assert( P4_E_OK == p4_sleep(P4_TIMEOUT_REL(P4_USEC(500))) );

	/* pull new packets from Q */
		/* TODO What is the effective difference between tx_pdu_available and taken? */
		while (sg.idx_tx<DIM(sg.in.length_tx) && (kcg_int32)sg.idx_tx<sg.out.tx_pdu_available) {
			if (sg.in.length_tx[sg.idx_tx] == nil_udp) {
				sg_service_pop( (uint8_t *)(sg.in.pdu_tx[sg.idx_tx]), &sg.in.length_tx[sg.idx_tx], &sg );

				if (sg.in.length_tx[sg.idx_tx] == nil_udp) break; /* nothing filled? done. */
			}
			sg.idx_tx++;
		}

	/* Handle Pings, if space allows */
		for ( size_t idx_rx=0; idx_rx<DIM(sg.out.length_rx); idx_rx++ ) {
			if ( isPingR( (uint8_t *)(sg.out.pdu_rx[idx_rx]), sg.out.length_rx[idx_rx])
					&& sg.idx_tx < DIM(sg.in.length_tx) ) {   /* only if we can reply */
				sg_service_handlePing( idx_rx, &sg );
			}
		}
		
	} while (!sg.in.shutdown && !sg.out.fd_failed);
	return EXIT_SUCCESS;
}

/* some assisting functions to the main loop */

int isPingR(uint8_t *pdu, length_t_udp l) {
	return pdu[9] == PROTOCOL_icmp_ip 
		&& l >= headerBytes_ip+HeaderBytes_icmp
		&& !(pdu[20]&~8); /* and filter ICMP_Requests */
}

int sg_service_handlePing(int rx_idx, struct sg_ctx *sg) {
	kcg_uint32 seq, id;
	IpAddress_udp src;
	kcg_bool failed, badChks, badPL;

	/* this is a Scade operator from the unit tests */
	echoTake_icmp(
		sg->out.length_rx[rx_idx], &sg->out.pdu_rx[rx_idx],
		&sg->hostip,
		&sg->in.length_tx[sg->idx_tx], &sg->in.pdu_tx[sg->idx_tx],
		&seq, &src, &id, &failed, &badChks, &badPL );
				
	if (!failed) {
		dprint("[ PING ] from %u.%u.%u.%u seq=%u id=%u --> %d@%ld\n",
				IP_SPLIT4(src), seq, id, sg->in.length_tx[sg->idx_tx], sg->idx_tx);
		do { sg->idx_tx++; } while ( sg->idx_tx < DIM(sg->in.length_tx) && sg->in.length_tx[sg->idx_tx] != nil_udp );
	} else {
		dprint("[ Bad PING ] from %d.%d.%d.%d seq=%d id=%d %s %s\n",
				IP_SPLIT4(src), seq, id, badChks?"Checksum":"", badPL?"Payload":"");
	}
	return failed;
}

int sg_service_push(uint8_t *ipFrame, length_t_udp ipFrameLength, struct sg_ctx *sg) {
	P4_e_t err;
	static struct eth_frame ef = {
		.header = {
			.dest = DEFAULT_DEST,
			.src = DEFAULT_SRC,
			.type = ETH_TYPE_IP4_BE,
		}
	};
	if (ipFrameLength >= 0 && ipFrameLength <= (length_t_udp)sizeof(ef.pdu)) {
#ifdef QPORTSVC_ETH
		/* this is a least-effort guess, expecting only IP-packets */
		ef.header.type = (ipFrame[0] & 0xF) == 6 ? ETH_TYPE_IP6_BE : ETH_TYPE_IP4_BE;
		ef.header.dest = sg->target_addr;
		memcpy(ef.pdu, ipFrame, ipFrameLength);
		length_t_udp ethFrameLength = ipFrameLength + sizeof(struct eth_hdr);
		err = vm_qport_write(&sg->fdq[QSVCW], &ef, ethFrameLength, QPORTSVCW_TIMEOUT);
#else
		err = vm_qport_write(&sg->fdq[QSVCW], ipFrame, ipFrameLength, QPORTSVCW_TIMEOUT);
#endif
#ifdef DEBUG_Q_IO
		/* poke directly into the IPv4 header */
		iprint("[ RX ] %4d<%d> -> %d \n", ipFrameLength, ipFrame[9] /* proto */, ethFrameLength);
#endif
		if ( err ) {
	        dprint("Error in vm_qport_write(%u), %s\n", sg->fdq[QSVCW].id, p4_strerror(err));
		} else {
			return EXIT_SUCCESS;
		}
	}
	dprint("Failed Frame, length=%d\n", ipFrameLength);
	return EXIT_FAILURE;
}

int sg_service_handleArp( struct arp_frame *af, struct sg_ctx *sg ) {
	if ( af->htype != ARP_HTYPE_ETHERNET_BE || af->ptype != ARP_PTYPE_IP4
			|| af->hlen != ARP_HLEN || af->plen != ARP_PLEN_IP4) {
		dprint("[ARP] ignoring invalid request.\n");
		return 0; /* invalid ARP */
	}
	if ( !af->spa[0] && !af->spa[1] ) {/* ARP probe */
		dprint("[ARP] ignoring probe <spa == 0>\n");
		return 0; /* just ignore */
	}
	int tha_zero = !af->tha.addr[0] && !af->tha.addr[1] && !af->tha.addr[2]
				&& !af->tha.addr[3] && !af->tha.addr[4] && !af->tha.addr[5];
	IpAddress_udp spa;
	spa.addr = af->spa[0] | (af->spa[1] << 16);
	if ( af->oper == ARP_OPER_REPLY_BE || (spa.addr == af->tpa.addr && tha_zero)) { /* ARP announce */
		sg->target_addr = af->sha;
		dprint("[ARP] ignoring announce <reply, spa==tpa, tha==0>.\n");
		return 0;
	}
	if ( af->oper == ARP_OPER_REQUEST_BE /* && tha_zero */) {
#ifdef DEBUG_Q_IO
		iprint("[ARP] request\n");
#endif
		af->oper = ARP_OPER_REPLY_BE;
		af->tha = af->sha;
		af->sha = (struct eth_addr)DEFAULT_SRC;
		af->spa[0] = (uint16_t)af->tpa.addr;
		af->spa[1] = (uint16_t)(af->tpa.addr >> 16);
		af->tpa = spa;
		return 1;
	}
	return 0;
}

int sg_service_pop(uint8_t *ipFrame, length_t_udp *ipFrameLength, struct sg_ctx *sg) {
	static struct eth_frame ef;
	*ipFrameLength = nil_udp;
	P4_e_t err;
#ifdef QPORTSVC_ETH
	size_t ethFrameLength;
	err = vm_qport_read(&sg->fdq[QSVCR], &ef, sizeof(ef), QPORTSVCR_TIMEOUT, &ethFrameLength);
#else
	err = vm_qport_read(&sg->fdq[QSVCR], ipFrame, sizeof(sg->in.pdu_tx[0]), QPORTSVCR_TIMEOUT, ipFrameLength);
#endif
	if ( !(err == P4_E_OK || err == P4_E_TIMEOUT) ) {
		dprint("Error in vm_qport_read(%u), %s\n", sg->fdq[QSVCR].id, p4_strerror(err));
		return EXIT_FAILURE;
	}

#ifndef QPORTSVC_ETH
	if ( err == P4_E_TIMEOUT ) *ipFrameLength = nil_udp;
#else
	if ( err == P4_E_TIMEOUT ) ethFrameLength = 0;

	if (ethFrameLength == sizeof( struct arp_frame )+sizeof( struct eth_hdr )
			&& ef.header.type == ETH_TYPE_ARP_BE) {

		if ( sg_service_handleArp( (struct arp_frame *)ef.pdu, sg )) {
			ef.header.dest = ef.header.src;
			ef.header.src = (struct eth_addr)DEFAULT_SRC;

#ifdef DEBUG_Q_IO
			iprint("[ARP] response.\n");
#endif
			err = vm_qport_write(&sg->fdq[QSVCW], &ef, ethFrameLength, P4_TIMEOUT_INFINITE);
			if ( err ) {
				dprint("Error in vm_qport_write(%u), %s\n", sg->fdq[QSVCW].id, p4_strerror(err));
				return EXIT_FAILURE;
			}
		}
	} else
	if (ethFrameLength > (sizeof(struct eth_hdr)+headerBytes_ip) && ethFrameLength <= sizeof(struct eth_frame)
			&& (ef.header.type == ETH_TYPE_IP6_BE || ef.header.type == ETH_TYPE_IP4_BE )) {
		*ipFrameLength = ethFrameLength-sizeof(struct eth_hdr);
		memcpy(ipFrame, ef.pdu, *ipFrameLength);
	} else
	if (ethFrameLength > 0)
		dprint("Odd packet from App. type=0x%X, ethLength=%ld\n", ef.header.type, ethFrameLength);
#endif
	return EXIT_SUCCESS;
}


/* parse the wg config in a very cheap and slippery way */
/* This is NOT a robust config parser. */

static int parseIPle(const char *str, IpAddress_udp *a, const char **end) {
	const char *endptr = str;
	kcg_uint32 addr = 0;
	for (int i=0; i<4 && *str != '\0' && *str != ':' && *str != '/' && !isspace(*str); i++) {
		long int d = strtoul(str, (char **)&endptr, 0);
		if (str == endptr && *str) return EXIT_FAILURE;
		if (*endptr == '.') {
			endptr++;
			if (d > 255) return EXIT_FAILURE;
			d <<= 8*(3-i);
		}
		addr |= d;
		str = endptr;
	}
	a->addr = addr;
	if (end) *end = endptr;
	return EXIT_SUCCESS;
}

static int parsePeer(const char *val, peer_t_udp *peer) {
	const char *sep;
	if ( EXIT_SUCCESS == parseIPle(val, &peer->addr, &sep) && (*sep == ':') ) {
		sep++; /* skip the separator */
		char *endptr;
		peer->port = strtoul(sep, &endptr, 0);
		return (peer->port > 0 && peer->port < 0xFFFF && endptr > sep && (!*endptr || isspace(*endptr))) ? EXIT_SUCCESS : EXIT_FAILURE;
	}
	return EXIT_FAILURE;
}

static int parseRange(const char *val, range_t_udp *range) {
	const char *sep;
	if ( EXIT_SUCCESS == parseIPle(val, &range->net, &sep) && *sep == '/' ) {
		sep++; /* skip the separator */
		long int m = strtoul(sep, NULL, 0);
		if (m <= 32) {
			m = 32-m;
			range->mask.addr = ((kcg_uint32)~0) << m;
			return EXIT_SUCCESS;
		}
	}
	return EXIT_FAILURE;
}


const char *strcasestr(const char *buf, const char *needle) {
	const char *start = NULL;
	const char *n = needle;
	while ( *buf && *n ) {
		if ( tolower(*buf) == tolower(*n) ) {
			n++;
			if ( !start ) start = buf;
		} else {
			n = needle;
			if ( tolower(*buf) == tolower(*needle) ) /* re-check if it is yet a new start */
				n++;
			else
				start = NULL;
		}
		buf++;
	}
	if (*n) start = NULL;
	return start;
}

static const char *getParam(const char *buf, const char *key, const char **end) {
	if (!buf || !*buf || !key || !*key) return NULL;
	char needle[32];
	const char *val;
	int l = snprintf(needle, sizeof(needle), "\n%s", key);
	val = strcasestr(buf, needle);
	if (!val) return NULL;
	val += l;
	while (*val == ' ' || *val == '=') val++;
	if (end) {
		*end = strchr(val, '\n');
		if (!*end) *end = val+strlen(val);
	}
	return val;
}
/* un-base64
 * taken from https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64#C_2
 *  Creative Commons Attribution-ShareAlike License
 */

#define WHITESPACE 64
#define EQUALS     65
#define INVALID    66

static const unsigned char d[] = {
    66,66,66,66,66,66,66,66,66,66,64,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,62,66,66,66,63,52,53,
    54,55,56,57,58,59,60,61,66,66,66,65,66,66,66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,66,66,66,66,66,66,26,27,28,
    29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66
};

static int base64decode (const char *in, size_t inLen, char *out, size_t *outLen) {
	unsigned char *_out = (unsigned char *)out;
    const char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end) {
        unsigned char c = d[(uint8_t)*in++];

        switch (c) {
        case WHITESPACE: continue;   /* skip whitespace */
        case INVALID:    return 1;   /* invalid input, return error */
        case EQUALS:                 /* pad character, end of data */
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; // increment the number of iteration
            /* If the buffer is full, split it into bytes */
            if (iter == 4) {
                if ((len += 3) > *outLen) return 1; /* buffer overflow */
                *(_out++) = (buf >> 16) & 255;
                *(_out++) = (buf >> 8) & 255;
                *(_out++) = buf & 255;
                buf = 0; iter = 0;

            }
        }
    }

    if (iter == 3) {
        if ((len += 2) > *outLen) return 1; /* buffer overflow */
        *(_out++) = (buf >> 10) & 255;
        *(_out++) = (buf >> 2) & 255;
    }
    else if (iter == 2) {
        if (++len > *outLen) return 1; /* buffer overflow */
        *(_out++) = (buf >> 4) & 255;
    }

    *outLen = len; /* modify to reflect the actual output size */
    return 0;
}


