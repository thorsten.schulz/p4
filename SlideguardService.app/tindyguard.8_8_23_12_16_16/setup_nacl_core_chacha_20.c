/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "setup_nacl_core_chacha_20.h"

/* nacl::core::chacha::setup/ */
void setup_nacl_core_chacha_20(
  /* _L5/, nonce/ */
  Nonce_nacl_core_chacha *nonce_20,
  /* _L3/, key/ */
  Key_nacl_core *key_20,
  /* @1/_L68/, @1/noo/, _L8/, noo/ */
  State_nacl_core_chacha *noo_20,
  /* @1/_L74/, @1/cipher/, _L9/, mackeystream/ */
  StreamChunk_slideTypes *mackeystream_20)
{
  array_uint32_16 tmp;
  kcg_size idx;
  /* @18/_L11/, @18/doo/, @2/_L95/, @21/_L9/, @21/ret/ */
  kcg_uint32 _L95_dual_1_generate_1;
  /* @18/_L14/, @18/coo/, @2/_L94/, @22/_L11/, @22/y/ */
  kcg_uint32 _L94_dual_1_generate_1;
  /* @18/_L13/, @18/aoo/, @2/_L92/, @21/_L5/, @21/x/ */
  kcg_uint32 _L92_dual_1_generate_1;
  /* @13/_L11/, @13/doo/, @16/_L9/, @16/ret/, @2/_L99/ */
  kcg_uint32 _L99_dual_1_generate_1;
  /* @13/_L14/, @13/coo/, @17/_L11/, @17/y/, @2/_L98/ */
  kcg_uint32 _L98_dual_1_generate_1;
  /* @13/_L13/, @13/aoo/, @16/_L5/, @16/x/, @2/_L96/ */
  kcg_uint32 _L96_dual_1_generate_1;
  /* @11/_L9/, @11/ret/, @2/_L103/, @8/_L11/, @8/doo/ */
  kcg_uint32 _L103_dual_1_generate_1;
  /* @12/_L11/, @12/y/, @2/_L102/, @8/_L14/, @8/coo/ */
  kcg_uint32 _L102_dual_1_generate_1;
  /* @11/_L5/, @11/x/, @2/_L100/, @8/_L13/, @8/aoo/ */
  kcg_uint32 _L100_dual_1_generate_1;
  /* @2/_L107/, @3/_L11/, @3/doo/, @6/_L9/, @6/ret/ */
  kcg_uint32 _L107_dual_1_generate_1;
  /* @2/_L106/, @3/_L14/, @3/coo/, @7/_L11/, @7/y/ */
  kcg_uint32 _L106_dual_1_generate_1;
  /* @2/_L104/, @3/_L13/, @3/aoo/, @6/_L5/, @6/x/ */
  kcg_uint32 _L104_dual_1_generate_1;
  /* @2/_L91/,
     @23/_L11/,
     @23/doo/,
     @26/_L9/,
     @26/ret/,
     @3/_L5/,
     @3/d/,
     @4/_L11/,
     @4/y/ */
  kcg_uint32 _L91_dual_1_generate_1;
  /* @18/_L8/, @18/c/, @2/_L90/, @23/_L14/, @23/coo/, @27/_L11/, @27/y/ */
  kcg_uint32 _L90_dual_1_generate_1;
  /* @10/_L11/,
     @10/y/,
     @2/_L89/,
     @23/_L12/,
     @23/boo/,
     @27/_L9/,
     @27/ret/,
     @8/_L3/,
     @8/b/ */
  kcg_uint32 _L89_dual_1_generate_1;
  /* @13/_L2/, @13/a/, @2/_L88/, @23/_L13/, @23/aoo/, @26/_L5/, @26/x/ */
  kcg_uint32 _L88_dual_1_generate_1;
  /* @13/_L5/,
     @13/d/,
     @14/_L11/,
     @14/y/,
     @2/_L87/,
     @28/_L11/,
     @28/doo/,
     @31/_L9/,
     @31/ret/ */
  kcg_uint32 _L87_dual_1_generate_1;
  /* @2/_L86/, @28/_L14/, @28/coo/, @3/_L8/, @3/c/, @32/_L11/, @32/y/ */
  kcg_uint32 _L86_dual_1_generate_1;
  /* @18/_L3/,
     @18/b/,
     @2/_L85/,
     @20/_L11/,
     @20/y/,
     @28/_L12/,
     @28/boo/,
     @32/_L9/,
     @32/ret/ */
  kcg_uint32 _L85_dual_1_generate_1;
  /* @2/_L84/, @28/_L13/, @28/aoo/, @31/_L5/, @31/x/, @8/_L2/, @8/a/ */
  kcg_uint32 _L84_dual_1_generate_1;
  /* @2/_L83/,
     @33/_L11/,
     @33/doo/,
     @36/_L9/,
     @36/ret/,
     @8/_L5/,
     @8/d/,
     @9/_L11/,
     @9/y/ */
  kcg_uint32 _L83_dual_1_generate_1;
  /* @13/_L8/, @13/c/, @2/_L82/, @33/_L14/, @33/coo/, @37/_L11/, @37/y/ */
  kcg_uint32 _L82_dual_1_generate_1;
  /* @2/_L81/,
     @3/_L3/,
     @3/b/,
     @33/_L12/,
     @33/boo/,
     @37/_L9/,
     @37/ret/,
     @5/_L11/,
     @5/y/ */
  kcg_uint32 _L81_dual_1_generate_1;
  /* @18/_L2/, @18/a/, @2/_L80/, @33/_L13/, @33/aoo/, @36/_L5/, @36/x/ */
  kcg_uint32 _L80_dual_1_generate_1;
  /* @2/_L76/, @3/_L2/, @3/a/, @38/_L13/, @38/aoo/, @41/_L5/, @41/x/ */
  kcg_uint32 _L76_dual_1_generate_1;
  /* @13/_L3/,
     @13/b/,
     @15/_L11/,
     @15/y/,
     @2/_L77/,
     @38/_L12/,
     @38/boo/,
     @42/_L9/,
     @42/ret/ */
  kcg_uint32 _L77_dual_1_generate_1;
  /* @2/_L78/, @38/_L14/, @38/coo/, @42/_L11/, @42/y/, @8/_L8/, @8/c/ */
  kcg_uint32 _L78_dual_1_generate_1;
  /* @18/_L5/,
     @18/d/,
     @19/_L11/,
     @19/y/,
     @2/_L79/,
     @38/_L11/,
     @38/doo/,
     @41/_L9/,
     @41/ret/ */
  kcg_uint32 _L79_dual_1_generate_1;
  /* @4/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_8_dual_1_generate_1_16;
  /* @3/_L6/, @4/_L9/, @4/ret/, @6/_L11/, @6/y/ */
  kcg_uint32 _L9_XRol32_1_QR_8_dual_1_generate_1_16;
  /* @3/_L1/, @4/_L5/, @4/x/ */
  kcg_uint32 _L5_XRol32_1_QR_8_dual_1_generate_1_16;
  /* @5/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_8_dual_1_generate_1_12;
  /* @3/_L10/, @5/_L9/, @5/ret/, @7/_L5/, @7/x/ */
  kcg_uint32 _L9_XRol32_2_QR_8_dual_1_generate_1_12;
  /* @3/_L7/, @5/_L5/, @5/x/ */
  kcg_uint32 _L5_XRol32_2_QR_8_dual_1_generate_1_12;
  /* @6/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_8_dual_1_generate_1_8;
  /* @7/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_8_dual_1_generate_1_7;
  /* @9/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_7_dual_1_generate_1_16;
  /* @11/_L11/, @11/y/, @8/_L6/, @9/_L9/, @9/ret/ */
  kcg_uint32 _L9_XRol32_1_QR_7_dual_1_generate_1_16;
  /* @8/_L1/, @9/_L5/, @9/x/ */
  kcg_uint32 _L5_XRol32_1_QR_7_dual_1_generate_1_16;
  /* @10/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_7_dual_1_generate_1_12;
  /* @10/_L9/, @10/ret/, @12/_L5/, @12/x/, @8/_L10/ */
  kcg_uint32 _L9_XRol32_2_QR_7_dual_1_generate_1_12;
  /* @10/_L5/, @10/x/, @8/_L7/ */
  kcg_uint32 _L5_XRol32_2_QR_7_dual_1_generate_1_12;
  /* @11/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_7_dual_1_generate_1_8;
  /* @12/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_7_dual_1_generate_1_7;
  /* @14/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_6_dual_1_generate_1_16;
  /* @13/_L6/, @14/_L9/, @14/ret/, @16/_L11/, @16/y/ */
  kcg_uint32 _L9_XRol32_1_QR_6_dual_1_generate_1_16;
  /* @13/_L1/, @14/_L5/, @14/x/ */
  kcg_uint32 _L5_XRol32_1_QR_6_dual_1_generate_1_16;
  /* @15/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_6_dual_1_generate_1_12;
  /* @13/_L10/, @15/_L9/, @15/ret/, @17/_L5/, @17/x/ */
  kcg_uint32 _L9_XRol32_2_QR_6_dual_1_generate_1_12;
  /* @13/_L7/, @15/_L5/, @15/x/ */
  kcg_uint32 _L5_XRol32_2_QR_6_dual_1_generate_1_12;
  /* @16/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_6_dual_1_generate_1_8;
  /* @17/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_6_dual_1_generate_1_7;
  /* @19/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_5_dual_1_generate_1_16;
  /* @18/_L6/, @19/_L9/, @19/ret/, @21/_L11/, @21/y/ */
  kcg_uint32 _L9_XRol32_1_QR_5_dual_1_generate_1_16;
  /* @18/_L1/, @19/_L5/, @19/x/ */
  kcg_uint32 _L5_XRol32_1_QR_5_dual_1_generate_1_16;
  /* @20/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_5_dual_1_generate_1_12;
  /* @18/_L10/, @20/_L9/, @20/ret/, @22/_L5/, @22/x/ */
  kcg_uint32 _L9_XRol32_2_QR_5_dual_1_generate_1_12;
  /* @18/_L7/, @20/_L5/, @20/x/ */
  kcg_uint32 _L5_XRol32_2_QR_5_dual_1_generate_1_12;
  /* @21/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_5_dual_1_generate_1_8;
  /* @22/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_5_dual_1_generate_1_7;
  /* @24/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_4_dual_1_generate_1_16;
  /* @23/_L6/, @24/_L9/, @24/ret/, @26/_L11/, @26/y/ */
  kcg_uint32 _L9_XRol32_1_QR_4_dual_1_generate_1_16;
  /* @23/_L1/, @24/_L5/, @24/x/ */
  kcg_uint32 _L5_XRol32_1_QR_4_dual_1_generate_1_16;
  /* @25/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_4_dual_1_generate_1_12;
  /* @23/_L10/, @25/_L9/, @25/ret/, @27/_L5/, @27/x/ */
  kcg_uint32 _L9_XRol32_2_QR_4_dual_1_generate_1_12;
  /* @23/_L7/, @25/_L5/, @25/x/ */
  kcg_uint32 _L5_XRol32_2_QR_4_dual_1_generate_1_12;
  /* @26/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_4_dual_1_generate_1_8;
  /* @27/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_4_dual_1_generate_1_7;
  /* @29/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_3_dual_1_generate_1_16;
  /* @28/_L6/, @29/_L9/, @29/ret/, @31/_L11/, @31/y/ */
  kcg_uint32 _L9_XRol32_1_QR_3_dual_1_generate_1_16;
  /* @28/_L1/, @29/_L5/, @29/x/ */
  kcg_uint32 _L5_XRol32_1_QR_3_dual_1_generate_1_16;
  /* @30/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_3_dual_1_generate_1_12;
  /* @28/_L10/, @30/_L9/, @30/ret/, @32/_L5/, @32/x/ */
  kcg_uint32 _L9_XRol32_2_QR_3_dual_1_generate_1_12;
  /* @28/_L7/, @30/_L5/, @30/x/ */
  kcg_uint32 _L5_XRol32_2_QR_3_dual_1_generate_1_12;
  /* @31/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_3_dual_1_generate_1_8;
  /* @32/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_3_dual_1_generate_1_7;
  /* @34/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_2_dual_1_generate_1_16;
  /* @33/_L6/, @34/_L9/, @34/ret/, @36/_L11/, @36/y/ */
  kcg_uint32 _L9_XRol32_1_QR_2_dual_1_generate_1_16;
  /* @33/_L1/, @34/_L5/, @34/x/ */
  kcg_uint32 _L5_XRol32_1_QR_2_dual_1_generate_1_16;
  /* @35/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_2_dual_1_generate_1_12;
  /* @33/_L10/, @35/_L9/, @35/ret/, @37/_L5/, @37/x/ */
  kcg_uint32 _L9_XRol32_2_QR_2_dual_1_generate_1_12;
  /* @33/_L7/, @35/_L5/, @35/x/ */
  kcg_uint32 _L5_XRol32_2_QR_2_dual_1_generate_1_12;
  /* @36/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_2_dual_1_generate_1_8;
  /* @37/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_2_dual_1_generate_1_7;
  /* @39/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_1_dual_1_generate_1_16;
  /* @38/_L6/, @39/_L9/, @39/ret/, @41/_L11/, @41/y/ */
  kcg_uint32 _L9_XRol32_1_QR_1_dual_1_generate_1_16;
  /* @38/_L1/, @39/_L5/, @39/x/ */
  kcg_uint32 _L5_XRol32_1_QR_1_dual_1_generate_1_16;
  /* @40/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_1_dual_1_generate_1_12;
  /* @38/_L10/, @40/_L9/, @40/ret/, @42/_L5/, @42/x/ */
  kcg_uint32 _L9_XRol32_2_QR_1_dual_1_generate_1_12;
  /* @38/_L7/, @40/_L5/, @40/x/ */
  kcg_uint32 _L5_XRol32_2_QR_1_dual_1_generate_1_12;
  /* @41/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_1_dual_1_generate_1_8;
  /* @42/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_1_dual_1_generate_1_7;
  /* @1/_L34/, @1/_L69/, @1/_L71/, @1/n/, _L4/ */
  array_uint32_16 _L4_20;

  kcg_copy_array_uint32_4(&_L4_20[0], (array_uint32_4 *) &sigma_nacl_core);
  kcg_copy_Key_nacl_core(&_L4_20[4], key_20);
  _L4_20[12] = kcg_lit_uint32(0);
  kcg_copy_Nonce_nacl_core_chacha(&_L4_20[13], nonce_20);
  kcg_copy_State_nacl_core_chacha(noo_20, &_L4_20);
  (*noo_20)[12] = _L4_20[12] + kcg_lit_uint32(1);
  kcg_copy_array_uint32_16(&tmp, &_L4_20);
  /* @1/_L73= */
  for (idx = 0; idx < 10; idx++) {
    _L5_XRol32_1_QR_1_dual_1_generate_1_16 = tmp[0] + tmp[4];
    _L10_XRol32_1_QR_1_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_1_dual_1_generate_1_16 ^ tmp[12];
    _L9_XRol32_1_QR_1_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_1_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_1_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_1_dual_1_generate_1_12 = tmp[8] +
      _L9_XRol32_1_QR_1_dual_1_generate_1_16;
    _L10_XRol32_2_QR_1_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_1_dual_1_generate_1_12 ^ tmp[4];
    _L9_XRol32_2_QR_1_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_1_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_1_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L76_dual_1_generate_1 = _L5_XRol32_1_QR_1_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_1_dual_1_generate_1_12;
    _L10_XRol32_3_QR_1_dual_1_generate_1_8 = _L76_dual_1_generate_1 ^
      _L9_XRol32_1_QR_1_dual_1_generate_1_16;
    _L79_dual_1_generate_1 = (_L10_XRol32_3_QR_1_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_1_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    _L78_dual_1_generate_1 = _L5_XRol32_2_QR_1_dual_1_generate_1_12 +
      _L79_dual_1_generate_1;
    _L10_XRol32_4_QR_1_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_1_dual_1_generate_1_12 ^ _L78_dual_1_generate_1;
    _L77_dual_1_generate_1 = (_L10_XRol32_4_QR_1_dual_1_generate_1_7 >>
        kcg_lit_uint32(25)) | kcg_lsl_uint32(
        _L10_XRol32_4_QR_1_dual_1_generate_1_7,
        kcg_lit_uint32(7));
    _L5_XRol32_1_QR_2_dual_1_generate_1_16 = tmp[1] + tmp[5];
    _L10_XRol32_1_QR_2_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_2_dual_1_generate_1_16 ^ tmp[13];
    _L9_XRol32_1_QR_2_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_2_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_2_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_2_dual_1_generate_1_12 = tmp[9] +
      _L9_XRol32_1_QR_2_dual_1_generate_1_16;
    _L10_XRol32_2_QR_2_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_2_dual_1_generate_1_12 ^ tmp[5];
    _L9_XRol32_2_QR_2_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_2_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_2_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L80_dual_1_generate_1 = _L5_XRol32_1_QR_2_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_2_dual_1_generate_1_12;
    _L10_XRol32_3_QR_2_dual_1_generate_1_8 = _L80_dual_1_generate_1 ^
      _L9_XRol32_1_QR_2_dual_1_generate_1_16;
    _L83_dual_1_generate_1 = (_L10_XRol32_3_QR_2_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_2_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    _L82_dual_1_generate_1 = _L5_XRol32_2_QR_2_dual_1_generate_1_12 +
      _L83_dual_1_generate_1;
    _L10_XRol32_4_QR_2_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_2_dual_1_generate_1_12 ^ _L82_dual_1_generate_1;
    _L81_dual_1_generate_1 = (_L10_XRol32_4_QR_2_dual_1_generate_1_7 >>
        kcg_lit_uint32(25)) | kcg_lsl_uint32(
        _L10_XRol32_4_QR_2_dual_1_generate_1_7,
        kcg_lit_uint32(7));
    _L5_XRol32_1_QR_3_dual_1_generate_1_16 = tmp[2] + tmp[6];
    _L10_XRol32_1_QR_3_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_3_dual_1_generate_1_16 ^ tmp[14];
    _L9_XRol32_1_QR_3_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_3_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_3_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_3_dual_1_generate_1_12 = tmp[10] +
      _L9_XRol32_1_QR_3_dual_1_generate_1_16;
    _L10_XRol32_2_QR_3_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_3_dual_1_generate_1_12 ^ tmp[6];
    _L9_XRol32_2_QR_3_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_3_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_3_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L84_dual_1_generate_1 = _L5_XRol32_1_QR_3_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_3_dual_1_generate_1_12;
    _L10_XRol32_3_QR_3_dual_1_generate_1_8 = _L84_dual_1_generate_1 ^
      _L9_XRol32_1_QR_3_dual_1_generate_1_16;
    _L87_dual_1_generate_1 = (_L10_XRol32_3_QR_3_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_3_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    _L86_dual_1_generate_1 = _L5_XRol32_2_QR_3_dual_1_generate_1_12 +
      _L87_dual_1_generate_1;
    _L10_XRol32_4_QR_3_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_3_dual_1_generate_1_12 ^ _L86_dual_1_generate_1;
    _L85_dual_1_generate_1 = (_L10_XRol32_4_QR_3_dual_1_generate_1_7 >>
        kcg_lit_uint32(25)) | kcg_lsl_uint32(
        _L10_XRol32_4_QR_3_dual_1_generate_1_7,
        kcg_lit_uint32(7));
    _L5_XRol32_1_QR_4_dual_1_generate_1_16 = tmp[3] + tmp[7];
    _L10_XRol32_1_QR_4_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_4_dual_1_generate_1_16 ^ tmp[15];
    _L9_XRol32_1_QR_4_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_4_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_4_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_4_dual_1_generate_1_12 = tmp[11] +
      _L9_XRol32_1_QR_4_dual_1_generate_1_16;
    _L10_XRol32_2_QR_4_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_4_dual_1_generate_1_12 ^ tmp[7];
    _L9_XRol32_2_QR_4_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_4_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_4_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L88_dual_1_generate_1 = _L5_XRol32_1_QR_4_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_4_dual_1_generate_1_12;
    _L10_XRol32_3_QR_4_dual_1_generate_1_8 = _L88_dual_1_generate_1 ^
      _L9_XRol32_1_QR_4_dual_1_generate_1_16;
    _L91_dual_1_generate_1 = (_L10_XRol32_3_QR_4_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_4_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    _L90_dual_1_generate_1 = _L5_XRol32_2_QR_4_dual_1_generate_1_12 +
      _L91_dual_1_generate_1;
    _L10_XRol32_4_QR_4_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_4_dual_1_generate_1_12 ^ _L90_dual_1_generate_1;
    _L89_dual_1_generate_1 = (_L10_XRol32_4_QR_4_dual_1_generate_1_7 >>
        kcg_lit_uint32(25)) | kcg_lsl_uint32(
        _L10_XRol32_4_QR_4_dual_1_generate_1_7,
        kcg_lit_uint32(7));
    _L5_XRol32_1_QR_5_dual_1_generate_1_16 = _L80_dual_1_generate_1 +
      _L85_dual_1_generate_1;
    _L10_XRol32_1_QR_5_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_5_dual_1_generate_1_16 ^ _L79_dual_1_generate_1;
    _L9_XRol32_1_QR_5_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_5_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_5_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_5_dual_1_generate_1_12 = _L90_dual_1_generate_1 +
      _L9_XRol32_1_QR_5_dual_1_generate_1_16;
    _L10_XRol32_2_QR_5_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_5_dual_1_generate_1_12 ^ _L85_dual_1_generate_1;
    _L9_XRol32_2_QR_5_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_5_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_5_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L92_dual_1_generate_1 = _L5_XRol32_1_QR_5_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_5_dual_1_generate_1_12;
    tmp[1] = _L92_dual_1_generate_1;
    _L10_XRol32_3_QR_5_dual_1_generate_1_8 = _L92_dual_1_generate_1 ^
      _L9_XRol32_1_QR_5_dual_1_generate_1_16;
    _L95_dual_1_generate_1 = (_L10_XRol32_3_QR_5_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_5_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    tmp[12] = _L95_dual_1_generate_1;
    _L94_dual_1_generate_1 = _L5_XRol32_2_QR_5_dual_1_generate_1_12 +
      _L95_dual_1_generate_1;
    tmp[11] = _L94_dual_1_generate_1;
    _L10_XRol32_4_QR_5_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_5_dual_1_generate_1_12 ^ _L94_dual_1_generate_1;
    tmp[6] = (_L10_XRol32_4_QR_5_dual_1_generate_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_5_dual_1_generate_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_6_dual_1_generate_1_16 = _L88_dual_1_generate_1 +
      _L77_dual_1_generate_1;
    _L10_XRol32_1_QR_6_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_6_dual_1_generate_1_16 ^ _L87_dual_1_generate_1;
    _L9_XRol32_1_QR_6_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_6_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_6_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_6_dual_1_generate_1_12 = _L82_dual_1_generate_1 +
      _L9_XRol32_1_QR_6_dual_1_generate_1_16;
    _L10_XRol32_2_QR_6_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_6_dual_1_generate_1_12 ^ _L77_dual_1_generate_1;
    _L9_XRol32_2_QR_6_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_6_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_6_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L96_dual_1_generate_1 = _L5_XRol32_1_QR_6_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_6_dual_1_generate_1_12;
    tmp[3] = _L96_dual_1_generate_1;
    _L10_XRol32_3_QR_6_dual_1_generate_1_8 = _L96_dual_1_generate_1 ^
      _L9_XRol32_1_QR_6_dual_1_generate_1_16;
    _L99_dual_1_generate_1 = (_L10_XRol32_3_QR_6_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_6_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    tmp[14] = _L99_dual_1_generate_1;
    _L98_dual_1_generate_1 = _L5_XRol32_2_QR_6_dual_1_generate_1_12 +
      _L99_dual_1_generate_1;
    tmp[9] = _L98_dual_1_generate_1;
    _L10_XRol32_4_QR_6_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_6_dual_1_generate_1_12 ^ _L98_dual_1_generate_1;
    tmp[4] = (_L10_XRol32_4_QR_6_dual_1_generate_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_6_dual_1_generate_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_7_dual_1_generate_1_16 = _L84_dual_1_generate_1 +
      _L89_dual_1_generate_1;
    _L10_XRol32_1_QR_7_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_7_dual_1_generate_1_16 ^ _L83_dual_1_generate_1;
    _L9_XRol32_1_QR_7_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_7_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_7_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_7_dual_1_generate_1_12 = _L78_dual_1_generate_1 +
      _L9_XRol32_1_QR_7_dual_1_generate_1_16;
    _L10_XRol32_2_QR_7_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_7_dual_1_generate_1_12 ^ _L89_dual_1_generate_1;
    _L9_XRol32_2_QR_7_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_7_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_7_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L100_dual_1_generate_1 = _L5_XRol32_1_QR_7_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_7_dual_1_generate_1_12;
    tmp[2] = _L100_dual_1_generate_1;
    _L10_XRol32_3_QR_7_dual_1_generate_1_8 = _L100_dual_1_generate_1 ^
      _L9_XRol32_1_QR_7_dual_1_generate_1_16;
    _L103_dual_1_generate_1 = (_L10_XRol32_3_QR_7_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_7_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    tmp[13] = _L103_dual_1_generate_1;
    _L102_dual_1_generate_1 = _L5_XRol32_2_QR_7_dual_1_generate_1_12 +
      _L103_dual_1_generate_1;
    tmp[8] = _L102_dual_1_generate_1;
    _L10_XRol32_4_QR_7_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_7_dual_1_generate_1_12 ^ _L102_dual_1_generate_1;
    tmp[7] = (_L10_XRol32_4_QR_7_dual_1_generate_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_7_dual_1_generate_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_8_dual_1_generate_1_16 = _L76_dual_1_generate_1 +
      _L81_dual_1_generate_1;
    _L10_XRol32_1_QR_8_dual_1_generate_1_16 =
      _L5_XRol32_1_QR_8_dual_1_generate_1_16 ^ _L91_dual_1_generate_1;
    _L9_XRol32_1_QR_8_dual_1_generate_1_16 =
      (_L10_XRol32_1_QR_8_dual_1_generate_1_16 >> kcg_lit_uint32(16)) |
      kcg_lsl_uint32(_L10_XRol32_1_QR_8_dual_1_generate_1_16, kcg_lit_uint32(16));
    _L5_XRol32_2_QR_8_dual_1_generate_1_12 = _L86_dual_1_generate_1 +
      _L9_XRol32_1_QR_8_dual_1_generate_1_16;
    _L10_XRol32_2_QR_8_dual_1_generate_1_12 =
      _L5_XRol32_2_QR_8_dual_1_generate_1_12 ^ _L81_dual_1_generate_1;
    _L9_XRol32_2_QR_8_dual_1_generate_1_12 =
      (_L10_XRol32_2_QR_8_dual_1_generate_1_12 >> kcg_lit_uint32(20)) |
      kcg_lsl_uint32(_L10_XRol32_2_QR_8_dual_1_generate_1_12, kcg_lit_uint32(12));
    _L104_dual_1_generate_1 = _L5_XRol32_1_QR_8_dual_1_generate_1_16 +
      _L9_XRol32_2_QR_8_dual_1_generate_1_12;
    tmp[0] = _L104_dual_1_generate_1;
    _L10_XRol32_3_QR_8_dual_1_generate_1_8 = _L104_dual_1_generate_1 ^
      _L9_XRol32_1_QR_8_dual_1_generate_1_16;
    _L107_dual_1_generate_1 = (_L10_XRol32_3_QR_8_dual_1_generate_1_8 >>
        kcg_lit_uint32(24)) | kcg_lsl_uint32(
        _L10_XRol32_3_QR_8_dual_1_generate_1_8,
        kcg_lit_uint32(8));
    tmp[15] = _L107_dual_1_generate_1;
    _L106_dual_1_generate_1 = _L5_XRol32_2_QR_8_dual_1_generate_1_12 +
      _L107_dual_1_generate_1;
    tmp[10] = _L106_dual_1_generate_1;
    _L10_XRol32_4_QR_8_dual_1_generate_1_7 =
      _L9_XRol32_2_QR_8_dual_1_generate_1_12 ^ _L106_dual_1_generate_1;
    tmp[5] = (_L10_XRol32_4_QR_8_dual_1_generate_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_8_dual_1_generate_1_7, kcg_lit_uint32(7));
  }
  /* @1/_L74= */
  for (idx = 0; idx < 16; idx++) {
    (*mackeystream_20)[idx] = tmp[idx] + _L4_20[idx];
  }
}

/*
  Expanded instances for: nacl::core::chacha::setup/
  @1: (nacl::core::chacha::generate#1)
  @2: @1/(nacl::core::chacha::dual#1)
  @3: @2/(nacl::core::chacha::QR#8)
  @4: @3/(nacl::op::XRol32#1)
  @5: @3/(nacl::op::XRol32#2)
  @6: @3/(nacl::op::XRol32#3)
  @7: @3/(nacl::op::XRol32#4)
  @8: @2/(nacl::core::chacha::QR#7)
  @9: @8/(nacl::op::XRol32#1)
  @10: @8/(nacl::op::XRol32#2)
  @11: @8/(nacl::op::XRol32#3)
  @12: @8/(nacl::op::XRol32#4)
  @13: @2/(nacl::core::chacha::QR#6)
  @14: @13/(nacl::op::XRol32#1)
  @15: @13/(nacl::op::XRol32#2)
  @16: @13/(nacl::op::XRol32#3)
  @17: @13/(nacl::op::XRol32#4)
  @18: @2/(nacl::core::chacha::QR#5)
  @19: @18/(nacl::op::XRol32#1)
  @20: @18/(nacl::op::XRol32#2)
  @21: @18/(nacl::op::XRol32#3)
  @22: @18/(nacl::op::XRol32#4)
  @23: @2/(nacl::core::chacha::QR#4)
  @24: @23/(nacl::op::XRol32#1)
  @25: @23/(nacl::op::XRol32#2)
  @26: @23/(nacl::op::XRol32#3)
  @27: @23/(nacl::op::XRol32#4)
  @28: @2/(nacl::core::chacha::QR#3)
  @29: @28/(nacl::op::XRol32#1)
  @30: @28/(nacl::op::XRol32#2)
  @31: @28/(nacl::op::XRol32#3)
  @32: @28/(nacl::op::XRol32#4)
  @33: @2/(nacl::core::chacha::QR#2)
  @34: @33/(nacl::op::XRol32#1)
  @35: @33/(nacl::op::XRol32#2)
  @36: @33/(nacl::op::XRol32#3)
  @37: @33/(nacl::op::XRol32#4)
  @38: @2/(nacl::core::chacha::QR#1)
  @39: @38/(nacl::op::XRol32#1)
  @40: @38/(nacl::op::XRol32#2)
  @41: @38/(nacl::op::XRol32#3)
  @42: @38/(nacl::op::XRol32#4)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** setup_nacl_core_chacha_20.c
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

