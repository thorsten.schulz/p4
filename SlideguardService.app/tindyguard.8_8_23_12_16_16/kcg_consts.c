/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

#include "kcg_consts.h"

/* udp::ipBroadcast/ */
const IpAddress_udp ipBroadcast_udp = { kcg_lit_uint32(0xFFFFFFFF) };

/* icmp::payload/ */
const array_uint32_5 payload_icmp = { kcg_lit_uint32(0x646e6974),
  kcg_lit_uint32(0x61754779), kcg_lit_uint32(0x61206472), kcg_lit_uint32(
    0x6576696c), kcg_lit_uint32(0x0021) };

/* ip::HEADER_icmp/ */
const header_ip HEADER_icmp_ip = { kcg_lit_uint8(0x45), kcg_lit_uint8(0),
  kcg_lit_int32(20) + kcg_lit_int32(16) + kcg_lit_int32(8), kcg_lit_uint16(0),
  kcg_lit_uint16(0x4000), kcg_lit_uint8(64), kcg_lit_uint8(1), kcg_lit_uint16(
    0), kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* udp::anyAddress/ */
const range_t_udp anyAddress_udp = { { kcg_lit_uint32(0) }, { kcg_lit_uint32(
      0) } };

/* tindyguard::test::Slider_endpoint/ */
const peer_t_udp Slider_endpoint_tindyguard_test = { { kcg_lit_uint32(
      0x8b1ec9a0) }, kcg_lit_int32(35378), kcg_lit_int32(0), kcg_lit_int64(
    -1) };

/* tindyguard::test::rpi_pub/ */
const pub_tindyguardTypes rpi_pub_tindyguard_test = { kcg_lit_uint32(
    0x7B53FA7B), kcg_lit_uint32(0x8B35EC41), kcg_lit_uint32(0x9456920A),
  kcg_lit_uint32(0x73718C9F), kcg_lit_uint32(0x45CED2A8), kcg_lit_uint32(
    0x1ACA4B82), kcg_lit_uint32(0x744DF071), kcg_lit_uint32(0x4CEF624F) };

/* tindyguard::test::rpi_allowed/ */
const range_t_udp rpi_allowed_tindyguard_test = { { kcg_lit_uint32(
      0x0a000011) }, { kcg_lit_uint32(0xFFFFFFFF) } };

/* tindyguard::test::rpi_endpoint/ */
const peer_t_udp rpi_endpoint_tindyguard_test = { { kcg_lit_uint32(0) },
  kcg_lit_int32(0), kcg_lit_int32(0), kcg_lit_int64(0) };

/* tindyguard::test::Lani_endpoint/ */
const peer_t_udp Lani_endpoint_tindyguard_test = { { kcg_lit_uint32(
      0x8b1ec9a0) }, kcg_lit_int32(35863), kcg_lit_int32(0), kcg_lit_int64(
    -1) };

/* tindyguard::test::Slider_priv/ */
const secret_tindyguardTypes Slider_priv_tindyguard_test = { kcg_lit_uint32(
    0x87A62A00), kcg_lit_uint32(0x4B4F88FC), kcg_lit_uint32(0x0F164F94),
  kcg_lit_uint32(0xB87A3D08), kcg_lit_uint32(0xE02F4AE6), kcg_lit_uint32(
    0x1C2D0B03), kcg_lit_uint32(0x9C5A7C3D), kcg_lit_uint32(0x6F81F0DB) };

/* tindyguard::test::BigZ_endpoint/ */
const peer_t_udp BigZ_endpoint_tindyguard_test = { { kcg_lit_uint32(0) },
  kcg_lit_int32(0), kcg_lit_int32(0), kcg_lit_int64(0) };

/* tindyguard::test::Lani_pub/ */
const pub_tindyguardTypes Lani_pub_tindyguard_test = { kcg_lit_uint32(
    0x6DDBC87E), kcg_lit_uint32(0xFF0FCCE9), kcg_lit_uint32(0x412D7526),
  kcg_lit_uint32(0xB39D2BB5), kcg_lit_uint32(0x74BE3186), kcg_lit_uint32(
    0x94F04406), kcg_lit_uint32(0x4F8CC83F), kcg_lit_uint32(0x429F1E4D) };

/* tindyguard::test::net10_allowed/ */
const range_t_udp net10_allowed_tindyguard_test = { { kcg_lit_uint32(
      0x0a000000) }, { kcg_lit_uint32(0xFFFFFF00) } };

/* tindyguard::test::preshared/ */
const secret_tindyguardTypes preshared_tindyguard_test = { kcg_lit_uint32(
    0x87B29016), kcg_lit_uint32(0x1C733D0B), kcg_lit_uint32(0x315EA116),
  kcg_lit_uint32(0x265FBB10), kcg_lit_uint32(0xEC37C9F8), kcg_lit_uint32(
    0xC81355D0), kcg_lit_uint32(0x5A51594A), kcg_lit_uint32(0x51A5A807) };

/* tindyguard::test::ChickenJoeLocal_endpoint/ */
const peer_t_udp ChickenJoeLocal_endpoint_tindyguard_test = { { kcg_lit_uint32(
      0xc0a80001) }, kcg_lit_int32(35361), kcg_lit_int32(0), kcg_lit_int64(
    -1) };

/* tindyguard::test::BigZ_pub/ */
const pub_tindyguardTypes BigZ_pub_tindyguard_test = { kcg_lit_uint32(
    0xE3E8BA7D), kcg_lit_uint32(0xB5512B3D), kcg_lit_uint32(0x20E47063),
  kcg_lit_uint32(0xC24C282D), kcg_lit_uint32(0x2A7FEE0A), kcg_lit_uint32(
    0x28B075E2), kcg_lit_uint32(0x502D916C), kcg_lit_uint32(0x29F5915D) };

/* tindyguard::test::ChickenJoe_allowed/ */
const range_t_udp ChickenJoe_allowed_tindyguard_test = { { kcg_lit_uint32(
      0x0a000001) }, { kcg_lit_uint32(0xFFFFFFFF) } };

/* tindyguard::test::ChickenJoe_ip/ */
const IpAddress_udp ChickenJoe_ip_tindyguard_test = { kcg_lit_uint32(
    0x0a000001) };

/* tindyguard::test::ChickenJoe_pub/ */
const pub_tindyguardTypes ChickenJoe_pub_tindyguard_test = { kcg_lit_uint32(
    0xF2FEC77C), kcg_lit_uint32(0x1CB02902), kcg_lit_uint32(0xC00E2537),
  kcg_lit_uint32(0x4BD66C62), kcg_lit_uint32(0x139FFDFD), kcg_lit_uint32(
    0xADDCD3B6), kcg_lit_uint32(0xD93748DF), kcg_lit_uint32(0x25CCC50F) };

/* tindyguard::test::BigZ_allowed/ */
const range_t_udp BigZ_allowed_tindyguard_test = { { kcg_lit_uint32(
      0x0a000003) }, { kcg_lit_uint32(0xFFFFFFFF) } };

/* tindyguard::test::BigZ_ip/ */
const IpAddress_udp BigZ_ip_tindyguard_test = { kcg_lit_uint32(0x0a000003) };

/* tindyguard::test::rpi_ip/ */
const IpAddress_udp rpi_ip_tindyguard_test = { kcg_lit_uint32(0x0a000011) };

/* tindyguard::test::Slider_ip/ */
const IpAddress_udp Slider_ip_tindyguard_test = { kcg_lit_uint32(0x0a000012) };

/* tindyguardTypes::EmptyKey/ */
const KeySalted_tindyguardTypes EmptyKey_tindyguardTypes = { { { kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0) } }, { { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, kcg_lit_int64(0) }, { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, kcg_lit_int64(0) } } };

/* tindyguard::MAC1_label/ */
const array_uint32_2 MAC1_label_tindyguard = { kcg_lit_uint32(0x3163616d),
  kcg_lit_uint32(0x2d2d2d2d) };

/* tindyguard::CONSTRUCTION_IDENTIFIER_hash/ */
const array_uint32_8 CONSTRUCTION_IDENTIFIER_hash_tindyguard = { kcg_lit_uint32(
    0x61b31122), kcg_lit_uint32(0x66c51a08), kcg_lit_uint32(0xdb431269),
  kcg_lit_uint32(0x32d58a45), kcg_lit_uint32(0x666c9c2d), kcg_lit_uint32(
    0xb7e89322), kcg_lit_uint32(0x659ce10e), kcg_lit_uint32(0xf39e07ba) };

/* tindyguard::COOKIE_label/ */
const array_uint32_2 COOKIE_label_tindyguard = { kcg_lit_uint32(0x6b6f6f63),
  kcg_lit_uint32(0x2d2d6569) };

/* tindyguard::CONSTRUCTION_chunk/ */
const StreamChunk_slideTypes CONSTRUCTION_chunk_tindyguard = { kcg_lit_uint32(
    0xae6de260), kcg_lit_uint32(0xc0ef27f3), kcg_lit_uint32(0xe235c32e),
  kcg_lit_uint32(0xd0d225a0), kcg_lit_uint32(0x0642eb16), kcg_lit_uint32(
    0xf57772f8), kcg_lit_uint32(0x98d1382d), kcg_lit_uint32(0x36cd788b),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* nacl::onetime::kMask32/ */
const array_uint32_4 kMask32_nacl_onetime = { kcg_lit_uint32(0x0FFFFFFF),
  kcg_lit_uint32(0x0FFFFFFC), kcg_lit_uint32(0x0FFFFFFC), kcg_lit_uint32(
    0x0FFFFFFC) };

/* nacl::onetime::minusp/ */
const array_uint8_17 minusp_nacl_onetime = { kcg_lit_uint8(5), kcg_lit_uint8(0),
  kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0),
  kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0),
  kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0),
  kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(252) };

/* nacl::core::sigma/ */
const array_uint32_4 sigma_nacl_core = { kcg_lit_uint32(0x61707865),
  kcg_lit_uint32(0x3320646e), kcg_lit_uint32(0x79622d32), kcg_lit_uint32(
    0x6b206574) };

/* tindyguardTypes::EmptySession/ */
const Session_tindyguardTypes EmptySession_tindyguardTypes = { { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0) }, kcg_lit_uint64(0), kcg_lit_uint64(0), kcg_lit_uint64(
    0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_int64(0), kcg_lit_int32(
    -1), kcg_lit_int32(0), kcg_lit_int64(-1), kcg_lit_int64(0), kcg_lit_int32(
    -1), { { { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { { kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0xFFFFFFFF) } }, { { kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { kcg_lit_uint32(0) },
      kcg_lit_int32(0), kcg_lit_int32(0), kcg_lit_int64(0) }, { kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, kcg_false, kcg_lit_int32(
      -1), kcg_false, { { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) },
      kcg_lit_int64(0) } }, kcg_false, kcg_false, kcg_false };

/* hash::blake2s::sigma/ */
const array_uint8_16_10 sigma_hash_blake2s = { { kcg_lit_uint8(0),
    kcg_lit_uint8(1), kcg_lit_uint8(2), kcg_lit_uint8(3), kcg_lit_uint8(4),
    kcg_lit_uint8(5), kcg_lit_uint8(6), kcg_lit_uint8(7), kcg_lit_uint8(8),
    kcg_lit_uint8(9), kcg_lit_uint8(10), kcg_lit_uint8(11), kcg_lit_uint8(12),
    kcg_lit_uint8(13), kcg_lit_uint8(14), kcg_lit_uint8(15) }, { kcg_lit_uint8(
      14), kcg_lit_uint8(10), kcg_lit_uint8(4), kcg_lit_uint8(8), kcg_lit_uint8(
      9), kcg_lit_uint8(15), kcg_lit_uint8(13), kcg_lit_uint8(6), kcg_lit_uint8(
      1), kcg_lit_uint8(12), kcg_lit_uint8(0), kcg_lit_uint8(2), kcg_lit_uint8(
      11), kcg_lit_uint8(7), kcg_lit_uint8(5), kcg_lit_uint8(3) }, {
    kcg_lit_uint8(11), kcg_lit_uint8(8), kcg_lit_uint8(12), kcg_lit_uint8(0),
    kcg_lit_uint8(5), kcg_lit_uint8(2), kcg_lit_uint8(15), kcg_lit_uint8(13),
    kcg_lit_uint8(10), kcg_lit_uint8(14), kcg_lit_uint8(3), kcg_lit_uint8(6),
    kcg_lit_uint8(7), kcg_lit_uint8(1), kcg_lit_uint8(9), kcg_lit_uint8(4) }, {
    kcg_lit_uint8(7), kcg_lit_uint8(9), kcg_lit_uint8(3), kcg_lit_uint8(1),
    kcg_lit_uint8(13), kcg_lit_uint8(12), kcg_lit_uint8(11), kcg_lit_uint8(14),
    kcg_lit_uint8(2), kcg_lit_uint8(6), kcg_lit_uint8(5), kcg_lit_uint8(10),
    kcg_lit_uint8(4), kcg_lit_uint8(0), kcg_lit_uint8(15), kcg_lit_uint8(8) }, {
    kcg_lit_uint8(9), kcg_lit_uint8(0), kcg_lit_uint8(5), kcg_lit_uint8(7),
    kcg_lit_uint8(2), kcg_lit_uint8(4), kcg_lit_uint8(10), kcg_lit_uint8(15),
    kcg_lit_uint8(14), kcg_lit_uint8(1), kcg_lit_uint8(11), kcg_lit_uint8(12),
    kcg_lit_uint8(6), kcg_lit_uint8(8), kcg_lit_uint8(3), kcg_lit_uint8(13) }, {
    kcg_lit_uint8(2), kcg_lit_uint8(12), kcg_lit_uint8(6), kcg_lit_uint8(10),
    kcg_lit_uint8(0), kcg_lit_uint8(11), kcg_lit_uint8(8), kcg_lit_uint8(3),
    kcg_lit_uint8(4), kcg_lit_uint8(13), kcg_lit_uint8(7), kcg_lit_uint8(5),
    kcg_lit_uint8(15), kcg_lit_uint8(14), kcg_lit_uint8(1), kcg_lit_uint8(9) },
  { kcg_lit_uint8(12), kcg_lit_uint8(5), kcg_lit_uint8(1), kcg_lit_uint8(15),
    kcg_lit_uint8(14), kcg_lit_uint8(13), kcg_lit_uint8(4), kcg_lit_uint8(10),
    kcg_lit_uint8(0), kcg_lit_uint8(7), kcg_lit_uint8(6), kcg_lit_uint8(3),
    kcg_lit_uint8(9), kcg_lit_uint8(2), kcg_lit_uint8(8), kcg_lit_uint8(11) }, {
    kcg_lit_uint8(13), kcg_lit_uint8(11), kcg_lit_uint8(7), kcg_lit_uint8(14),
    kcg_lit_uint8(12), kcg_lit_uint8(1), kcg_lit_uint8(3), kcg_lit_uint8(9),
    kcg_lit_uint8(5), kcg_lit_uint8(0), kcg_lit_uint8(15), kcg_lit_uint8(4),
    kcg_lit_uint8(8), kcg_lit_uint8(6), kcg_lit_uint8(2), kcg_lit_uint8(10) }, {
    kcg_lit_uint8(6), kcg_lit_uint8(15), kcg_lit_uint8(14), kcg_lit_uint8(9),
    kcg_lit_uint8(11), kcg_lit_uint8(3), kcg_lit_uint8(0), kcg_lit_uint8(8),
    kcg_lit_uint8(12), kcg_lit_uint8(2), kcg_lit_uint8(13), kcg_lit_uint8(7),
    kcg_lit_uint8(1), kcg_lit_uint8(4), kcg_lit_uint8(10), kcg_lit_uint8(5) }, {
    kcg_lit_uint8(10), kcg_lit_uint8(2), kcg_lit_uint8(8), kcg_lit_uint8(4),
    kcg_lit_uint8(7), kcg_lit_uint8(6), kcg_lit_uint8(1), kcg_lit_uint8(5),
    kcg_lit_uint8(15), kcg_lit_uint8(11), kcg_lit_uint8(9), kcg_lit_uint8(14),
    kcg_lit_uint8(3), kcg_lit_uint8(12), kcg_lit_uint8(13), kcg_lit_uint8(
      0) } };

/* hash::blake2s::IV/ */
const array_uint32_8 IV_hash_blake2s = { kcg_lit_uint32(0x6A09E667),
  kcg_lit_uint32(0xBB67AE85), kcg_lit_uint32(0x3C6EF372), kcg_lit_uint32(
    0xA54FF53A), kcg_lit_uint32(0x510E527F), kcg_lit_uint32(0x9B05688C),
  kcg_lit_uint32(0x1F83D9AB), kcg_lit_uint32(0x5BE0CD19) };

/* slideTypes::ZeroChunk/ */
const StreamChunk_slideTypes ZeroChunk_slideTypes = { kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* slideTypes::ZeroKeyPair/ */
const KeyPair32_slideTypes ZeroKeyPair_slideTypes = { { kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) } };

/* nacl::box::badKey/ */
const array_uint32_8_5 badKey_nacl_box = { { kcg_lit_uint32(0), kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(
      1), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, { kcg_lit_uint32(0xffffffEC), kcg_lit_uint32(0xffffffff),
    kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0xffffffff), kcg_lit_uint32(
      0xffffffff), kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0xffffffff),
    kcg_lit_uint32(0x7Fffffff) }, { kcg_lit_uint32(0x7c7aebe0), kcg_lit_uint32(
      0xaeb8413b), kcg_lit_uint32(0xfae35616), kcg_lit_uint32(0x6ac49ff1),
    kcg_lit_uint32(0xeb8d09da), kcg_lit_uint32(0xfdb1329c), kcg_lit_uint32(
      0x16056286), kcg_lit_uint32(0x00b8495f) }, { kcg_lit_uint32(0xbc959c5f),
    kcg_lit_uint32(0x248c50a3), kcg_lit_uint32(0x55b1d0b1), kcg_lit_uint32(
      0x5bef839c), kcg_lit_uint32(0xc45c4404), kcg_lit_uint32(0x868e1c58),
    kcg_lit_uint32(0xdd4e22d8), kcg_lit_uint32(0x57119fd0) } };

/* nacl::box::bad2/ */
const Key32_slideTypes bad2_nacl_box = { kcg_lit_uint32(0xbc959c5f),
  kcg_lit_uint32(0x248c50a3), kcg_lit_uint32(0x55b1d0b1), kcg_lit_uint32(
    0x5bef839c), kcg_lit_uint32(0xc45c4404), kcg_lit_uint32(0x868e1c58),
  kcg_lit_uint32(0xdd4e22d8), kcg_lit_uint32(0x57119fd0) };

/* nacl::box::bad1/ */
const Key32_slideTypes bad1_nacl_box = { kcg_lit_uint32(0x7c7aebe0),
  kcg_lit_uint32(0xaeb8413b), kcg_lit_uint32(0xfae35616), kcg_lit_uint32(
    0x6ac49ff1), kcg_lit_uint32(0xeb8d09da), kcg_lit_uint32(0xfdb1329c),
  kcg_lit_uint32(0x16056286), kcg_lit_uint32(0x00b8495f) };

/* nacl::box::invKey/ */
const Key32_slideTypes invKey_nacl_box = { kcg_lit_uint32(0xffffffEC),
  kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0xffffffff), kcg_lit_uint32(
    0xffffffff), kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0xffffffff),
  kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0x7Fffffff) };

/* nacl::box::oneKey/ */
const Key32_slideTypes oneKey_nacl_box = { kcg_lit_uint32(1), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* nacl::box::zeroKey/ */
const Key32_slideTypes zeroKey_nacl_box = { kcg_lit_uint32(0), kcg_lit_uint32(
    0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
    0), kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* nacl::op::_121665/ */
const gf_nacl_op _121665_nacl_op = { kcg_lit_int64(0xDB41), kcg_lit_int64(1),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0) };

/* nacl::box::initialScalMulAcc/ */
const scalMulAcc_nacl_box initialScalMulAcc_nacl_box = { kcg_lit_int32(32) +
  kcg_lit_int32(1), { kcg_lit_int64(1), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0) }, { kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0) }, { kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0) }, { kcg_lit_int64(1), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0) } };

/* nacl::op::gf0/ */
const gf_nacl_op gf0_nacl_op = { kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0) };

/* nacl::op::gf1/ */
const gf_nacl_op gf1_nacl_op = { kcg_lit_int64(1), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0) };

/* nacl::box::_9/ */
const array_uint32_8 _9_nacl_box = { kcg_lit_uint32(9), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* tindyguard::conf::fixedEphemeral/ */
const secret_tindyguardTypes fixedEphemeral_tindyguard_conf = { kcg_lit_uint32(
    0x01010100), kcg_lit_uint32(0x01010101), kcg_lit_uint32(0x01010101),
  kcg_lit_uint32(0x01010101), kcg_lit_uint32(0x01010101), kcg_lit_uint32(
    0x01010101), kcg_lit_uint32(0x01010101), kcg_lit_uint32(0x41010101) };

/* tindyguardTypes::EmptyPeer/ */
const Peer_tindyguardTypes EmptyPeer_tindyguardTypes = { { kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, { { kcg_lit_uint32(0) }, { kcg_lit_uint32(0xFFFFFFFF) } }, { {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) } }, { { kcg_lit_uint32(0) }, kcg_lit_int32(0),
    kcg_lit_int32(0), kcg_lit_int64(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(
      0), kcg_lit_uint32(0) }, kcg_false, kcg_lit_int32(-1), kcg_false, { {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0) }, kcg_lit_int64(0) } };

/* udp::emptyPeer/ */
const peer_t_udp emptyPeer_udp = { { kcg_lit_uint32(0) }, kcg_lit_int32(0),
  kcg_lit_int32(0), kcg_lit_int64(0) };

/* udp::ipAny/ */
const IpAddress_udp ipAny_udp = { kcg_lit_uint32(0) };

/* tindyguardTypes::EmptyJar/ */
const CookieJar_tindyguardTypes EmptyJar_tindyguardTypes = { { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, kcg_lit_int64(0) };

/* tindyguard::handshake::EmptyState/ */
const State_tindyguard_handshake EmptyState_tindyguard_handshake = { { {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0) } };

/* udp::Zero/ */
const chunk_t_udp Zero_udp = { kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_consts.c
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

