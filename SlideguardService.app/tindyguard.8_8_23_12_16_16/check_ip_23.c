/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "check_ip_23.h"

/* ip::check/ */
void check_ip_23(
  /* len/ */
  length_t_udp len_23,
  /* msg/ */
  array_uint32_16_23 *msg_23,
  /* expect/ */
  range_t_udp *expect_23,
  /* in/ */
  header_ip *in_23,
  /* checksum/ */
  kcg_bool checksum_23,
  /* payloadlen/ */
  length_t_udp *payloadlen_23,
  /* src/ */
  IpAddress_udp *src_23,
  /* failed/ */
  kcg_bool *failed_23)
{
  kcg_uint32 tmp;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_23;
  /* @1/_L8/, @1/dst/, IfBlock1:then:IfBlock2:then:_L24/ */
  IpAddress_udp _L24_then_IfBlock2_then_IfBlock1_23;
  /* IfBlock1:then:IfBlock2:then:_L12/ */
  kcg_bool _L12_then_IfBlock2_then_IfBlock1_23;
  /* @1/_L10/, @1/o/, IfBlock1:then:IfBlock2:then:_L5/ */
  kcg_bool _L5_then_IfBlock2_then_IfBlock1_23;
  /* IfBlock1:then:IfBlock2: */
  kcg_bool IfBlock2_clock_then_IfBlock1_23;
  /* IfBlock1:then:_L8/ */
  kcg_uint16 _L8_then_IfBlock1_23;

  IfBlock1_clock_23 = len_23 >= kcg_lit_int32(20);
  /* IfBlock1: */
  if (IfBlock1_clock_23) {
    _L8_then_IfBlock1_23 = /* IfBlock1:then:_L8=(sys::hton#3)/ */
      htons_sys_specialization(
        /* IfBlock1:then:_L15= */(kcg_uint16) ((*msg_23)[0][0] >> kcg_lit_uint32(16)));
    IfBlock2_clock_then_IfBlock1_23 = !(/* IfBlock1:then:_L2= */(kcg_uint8)
          (*msg_23)[0][0] != (*in_23).ihl_version ||
        /* IfBlock1:then:_L16= */(kcg_int32) _L8_then_IfBlock1_23 < (*in_23).tot_len ||
        /* IfBlock1:then:_L16= */(kcg_int32) _L8_then_IfBlock1_23 > len_23 ||
        /* IfBlock1:then:_L18= */(kcg_uint8) ((*msg_23)[0][2] >> kcg_lit_uint32(8)) !=
        (*in_23).protocol);
    /* IfBlock1:then:IfBlock2: */
    if (IfBlock2_clock_then_IfBlock1_23) {
      _L24_then_IfBlock2_then_IfBlock1_23.addr =
        /* IfBlock1:then:IfBlock2:then:_L4=(sys::hton#2)/ */
        htonl_sys_specialization((*msg_23)[0][4]);
      _L5_then_IfBlock2_then_IfBlock1_23 =
        _L24_then_IfBlock2_then_IfBlock1_23.addr != kcg_lit_uint32(0) &&
        kcg_lit_uint32(0) == ((_L24_then_IfBlock2_then_IfBlock1_23.addr ^
            (*expect_23).net.addr) & (*expect_23).mask.addr) && kcg_lit_uint32(
          0) != (*expect_23).net.addr;
      _L12_then_IfBlock2_then_IfBlock1_23 =
        _L5_then_IfBlock2_then_IfBlock1_23 && checksum_23;
      if (_L12_then_IfBlock2_then_IfBlock1_23) {
        tmp = /* IfBlock1:then:IfBlock2:then:_L7=(ip::ipsum_BEH#1)/ */
          ipsum_BEH_ip_23(msg_23, headerBytes_ip, kcg_lit_int32(0));
      }
      else {
        tmp = kcg_lit_uint32(4294901760);
      }
      *failed_23 = !_L5_then_IfBlock2_then_IfBlock1_23 || tmp != kcg_lit_uint32(
          4294901760);
      (*src_23).addr = /* IfBlock1:then:IfBlock2:then:_L2=(sys::hton#1)/ */
        htonl_sys_specialization((*msg_23)[0][3]);
      /* IfBlock1:then:IfBlock2:then:_L20= */
      if (*failed_23) {
        *payloadlen_23 = kcg_lit_int32(-1);
      }
      else {
        *payloadlen_23 = /* IfBlock1:then:_L16= */(kcg_int32)
            _L8_then_IfBlock1_23 - (*in_23).tot_len;
      }
    }
    else {
      *failed_23 = kcg_true;
      *payloadlen_23 = kcg_lit_int32(-1);
      kcg_copy_IpAddress_udp(src_23, (IpAddress_udp *) &ipAny_udp);
    }
  }
  else {
    *failed_23 = kcg_true;
    *payloadlen_23 = kcg_lit_int32(-1);
    kcg_copy_IpAddress_udp(src_23, (IpAddress_udp *) &ipAny_udp);
  }
}

/*
  Expanded instances for: ip::check/
  @1: (udp::within#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** check_ip_23.c
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

