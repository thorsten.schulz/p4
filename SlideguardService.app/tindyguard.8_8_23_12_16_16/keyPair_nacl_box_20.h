/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _keyPair_nacl_box_20_H_
#define _keyPair_nacl_box_20_H_

#include "kcg_types.h"
#include "scalarmultBase_nacl_box_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_scalarmultBase_nacl_box_20 /* _L1=(nacl::box::scalarmultBase#1)/ */ Context_scalarmultBase_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_keyPair_nacl_box_20;

/* ===========  node initialization and cycle functions  =========== */
/* nacl::box::keyPair/ */
extern void keyPair_nacl_box_20(
  /* _L2/, randomness/ */
  array_uint32_8 *randomness_20,
  /* _L6/, again/ */
  kcg_bool *again_20,
  /* _L11/, key/ */
  KeyPair32_slideTypes *key_20,
  /* _L14/, failed/ */
  kcg_bool *failed_20,
  outC_keyPair_nacl_box_20 *outC);

extern void keyPair_reset_nacl_box_20(outC_keyPair_nacl_box_20 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void keyPair_init_nacl_box_20(outC_keyPair_nacl_box_20 *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _keyPair_nacl_box_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** keyPair_nacl_box_20.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

