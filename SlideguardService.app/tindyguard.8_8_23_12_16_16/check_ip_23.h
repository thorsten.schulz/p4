/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _check_ip_23_H_
#define _check_ip_23_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "ipsum_BEH_ip_23.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ip::check/ */
extern void check_ip_23(
  /* len/ */
  length_t_udp len_23,
  /* msg/ */
  array_uint32_16_23 *msg_23,
  /* expect/ */
  range_t_udp *expect_23,
  /* in/ */
  header_ip *in_23,
  /* checksum/ */
  kcg_bool checksum_23,
  /* payloadlen/ */
  length_t_udp *payloadlen_23,
  /* src/ */
  IpAddress_udp *src_23,
  /* failed/ */
  kcg_bool *failed_23);



#endif /* _check_ip_23_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** check_ip_23.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

