/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "TAI_validate_tindyguardTypes.h"

/* tindyguardTypes::TAI_validate/ */
void TAI_validate_tindyguardTypes(
  /* _L1/, x_n/ */
  TAI_tindyguardTypes *x_n,
  /* _L15/, ok/ */
  kcg_bool *ok,
  /* _L52/, x_h/ */
  TAI_tindyguardTypes *x_h)
{
  kcg_uint32 noname;
  /* _L33/ */
  kcg_uint32 _L33;
  /* _L32/ */
  kcg_uint32 _L32;
  /* _L46/ */
  kcg_uint32 _L46;
  /* _L47/ */
  kcg_uint32 _L47;

  _L46 = /* _L46=(sys::hton#1)/ */ htonl_sys_specialization((*x_n)[0]);
  (*x_h)[0] = _L46;
  _L47 = /* _L47=(sys::hton#2)/ */ htonl_sys_specialization((*x_n)[1]);
  (*x_h)[1] = _L47;
  (*x_h)[2] = /* _L48=(sys::hton#3)/ */ htonl_sys_specialization((*x_n)[2]);
  /* _L32=(sys::tai#1)/ */ tai_sys(kcg_false, &_L32, &_L33, &noname);
  *ok = (kcg_lsl_uint64(/* _L35= */(kcg_uint64) _L46, kcg_lit_uint64(32)) |
      /* _L36= */(kcg_uint64) _L47) + FatalTAIage_tindyguardTypes >
    (kcg_lsl_uint64(/* _L38= */(kcg_uint64) _L32, kcg_lit_uint64(32)) |
      /* _L37= */(kcg_uint64) _L33);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** TAI_validate_tindyguardTypes.c
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

