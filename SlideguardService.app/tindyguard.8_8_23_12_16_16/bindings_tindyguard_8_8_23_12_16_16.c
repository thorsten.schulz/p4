/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bindings_tindyguard_8_8_23_12_16_16.h"

/* tindyguard::bindings/ */
void bindings_tindyguard_8_8_23_12_16_16(
  /* length/ */
  array_int32_8 *length_8_8_23_12_16_16,
  /* out/ */
  array_uint32_16_23_8 *out_8_8_23_12_16_16,
  /* knownPeers/ */
  _4_array *knownPeers_8_8_23_12_16_16,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8_8_23_12_16_16,
  /* shutdown/ */
  kcg_bool shutdown_8_8_23_12_16_16,
  /* port/ */
  port_t_udp port_8_8_23_12_16_16,
  /* if_hint/ */
  range_t_udp *if_hint_8_8_23_12_16_16,
  /* length_in/ */
  array_int32_16 *length_in_8_8_23_12_16_16,
  /* in/ */
  array_uint32_16_23_16 *in_8_8_23_12_16_16,
  /* taken/ */
  size_tindyguardTypes *taken_8_8_23_12_16_16,
  /* st_buffer_avail/ */
  size_tindyguardTypes *st_buffer_avail_8_8_23_12_16_16,
  /* updatedPeers/ */
  _4_array *updatedPeers_8_8_23_12_16_16,
  /* no_peer_mask/ */
  kcg_uint64 *no_peer_mask_8_8_23_12_16_16,
  /* _L237/, fd_failed/, fdfail/ */
  kcg_bool *fd_failed_8_8_23_12_16_16,
  outC_bindings_tindyguard_8_8_23_12_16_16 *outC)
{
  array_int32_16 tmp;
  kcg_uint64 acc;
  kcg_size idx;
  array_int32_16 tmp1;
  kcg_bool acc2;
  kcg_size idx_mergeSocket_5;
  /* @1/IfBlock1:then:_L11/, wipe_machine:main:ride_machine:setup:_L2/ */
  length_t_udp _L11_mergeSocket_5_then_IfBlock1_23_12;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_mergeSocket_5_23_12;
  array_int32_16 acc3;
  array_int32_16 acc4;
  array_int32_12 acc5;
  array_int32_16 acc6;
  kcg_bool acc7;
  array_bool_12_16 tmp8;
  kcg_int32 acc9;
  array_int32_12_16 tmp10;
  array_uint32_16_23_12_16 tmp11;
  array_uint32_16_23 pow;
  fd_t_udp op_call;
  kcg_bool _12_op_call;
  /* wipe_machine:main:ride_machine:setup:_L13/ */
  kcg_bool _L13_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:setup:_L11/ */
  IpAddress_udp _L11_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:,
     wipe_machine:main:ride_machine:cleanup:<1>,
     wipe_machine:main:ride_machine:cleanup:_L1/,
     wipe_machine:main:ride_machine:ride:_L65/,
     wipe_machine:main:ride_machine:setup:_L9/ */
  kcg_bool _L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:setup:_L3/ */
  array _L3_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L103/,
     wipe_machine:main:ride_machine:ride:_L84/,
     wipe_machine:main:ride_machine:ride:_L85/,
     wipe_machine:main:ride_machine:ride:sInfo/ */
  _3_array _L103_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L101/ */
  array_int32_12 _L101_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L95/ */
  kcg_int64 _L95_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L5/ */
  array_int32_16_12 _L5_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L14/ */
  array_int32_16 _L14_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L11/ */
  array_int32_16 _L11_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* no_peer_mask/, wipe_machine:main:ride_machine:ride:_L10/ */
  kcg_uint64 _L10_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* taken/, wipe_machine:main:ride_machine:ride:_L9/ */
  size_tindyguardTypes _L9_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L24/ */
  array_int32_12 _L24_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* in/, wipe_machine:main:ride_machine:ride:_L27/ */
  array_uint32_16_23_16 _L27_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* length_in/,
     wipe_machine:main:ride_machine:ride:_L26/,
     wipe_machine:main:ride_machine:ride:_L44/ */
  array_int32_16 _L26_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L45/ */
  array_int32_16_12 _L45_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L53/ */
  array_bool_12 _L53_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L52/ */
  array_bool_12 _L52_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L49/ */
  array_bool_16_12 _L49_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L48/ */
  array_uint32_16_23_16_12 _L48_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L47/ */
  array_int32_16_12 _L47_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L69/ */
  array_uint32_16 _L69_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L13/,
     wipe_machine:main:ride_machine:ride:_L89/,
     wipe_machine:main:ride_machine:ride:_L90/,
     wipe_machine:main:ride_machine:ride:txmlength/ */
  array_int32_16 txmlength_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L12/,
     wipe_machine:main:ride_machine:ride:_L88/,
     wipe_machine:main:ride_machine:ride:txmbuffer/ */
  array_uint32_16_23_16 txmbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L67/,
     wipe_machine:main:ride_machine:ride:_L83/,
     wipe_machine:main:ride_machine:ride:rxdendpoint/ */
  _5_array rxdendpoint_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine:ride:_L66/,
     wipe_machine:main:ride_machine:ride:_L80/,
     wipe_machine:main:ride_machine:ride:rxdbuffer/ */
  array_uint32_16_24_16 rxdbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  array_int32_12 noname;
  time_t_sys _13_noname;
  array_int32_16 _14_noname;
  array_bool_12 _15_noname;
  /* wipe_machine:main:ride_machine: */
  SSM_ST_ride_machine_main_wipe_machine ride_machine_clock_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine: */
  SSM_ST_ride_machine_main_wipe_machine _16_ride_machine_clock_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:ride_machine: */
  SSM_TR_ride_machine_main_wipe_machine ride_machine_fired_strong_main_wipe_machine_8_8_23_12_16_16;
  /* wipe_machine:main:<1> */
  kcg_bool tr_1_guard_main_wipe_machine_8_8_23_12_16_16;

  tr_1_guard_main_wipe_machine_8_8_23_12_16_16 =
    !outC->mem_shutdown_8_8_23_12_16_16 && shutdown_8_8_23_12_16_16;
  outC->mem_shutdown_8_8_23_12_16_16 = shutdown_8_8_23_12_16_16;
  if (tr_1_guard_main_wipe_machine_8_8_23_12_16_16) {
    outC->init1 = kcg_true;
    outC->init = kcg_true;
  }
  /* wipe_machine:main:ride_machine: */
  if (outC->init1) {
    outC->init1 = kcg_false;
    _16_ride_machine_clock_main_wipe_machine_8_8_23_12_16_16 =
      SSM_st_setup_ride_machine_main_wipe_machine;
  }
  else {
    _16_ride_machine_clock_main_wipe_machine_8_8_23_12_16_16 =
      outC->ride_machine_state_nxt_main_wipe_machine_8_8_23_12_16_16;
  }
  /* wipe_machine:main:ride_machine: */
  switch (_16_ride_machine_clock_main_wipe_machine_8_8_23_12_16_16) {
    case SSM_st_ride_ride_machine_main_wipe_machine :
      if (shutdown_8_8_23_12_16_16) {
        ride_machine_clock_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_cleanup_ride_machine_main_wipe_machine;
        ride_machine_fired_strong_main_wipe_machine_8_8_23_12_16_16 =
          SSM_TR_ride_cleanup_1_ride_ride_machine_main_wipe_machine;
      }
      else {
        ride_machine_clock_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_ride_ride_machine_main_wipe_machine;
        ride_machine_fired_strong_main_wipe_machine_8_8_23_12_16_16 =
          SSM_TR_no_trans_ride_machine_main_wipe_machine;
      }
      break;
    case SSM_st_cleanup_ride_machine_main_wipe_machine :
      _L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16 =
        !shutdown_8_8_23_12_16_16;
      if (_L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16) {
        ride_machine_clock_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_setup_ride_machine_main_wipe_machine;
        ride_machine_fired_strong_main_wipe_machine_8_8_23_12_16_16 =
          SSM_TR_cleanup_setup_1_cleanup_ride_machine_main_wipe_machine;
      }
      else {
        ride_machine_clock_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_cleanup_ride_machine_main_wipe_machine;
        ride_machine_fired_strong_main_wipe_machine_8_8_23_12_16_16 =
          SSM_TR_no_trans_ride_machine_main_wipe_machine;
      }
      break;
    case SSM_st_setup_ride_machine_main_wipe_machine :
      if (shutdown_8_8_23_12_16_16) {
        ride_machine_clock_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_cleanup_ride_machine_main_wipe_machine;
        ride_machine_fired_strong_main_wipe_machine_8_8_23_12_16_16 =
          SSM_TR_setup_cleanup_1_setup_ride_machine_main_wipe_machine;
      }
      else {
        ride_machine_clock_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_setup_ride_machine_main_wipe_machine;
        ride_machine_fired_strong_main_wipe_machine_8_8_23_12_16_16 =
          SSM_TR_no_trans_ride_machine_main_wipe_machine;
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  switch (ride_machine_clock_main_wipe_machine_8_8_23_12_16_16) {
    case SSM_st_ride_ride_machine_main_wipe_machine :
      /* wipe_machine:main:ride_machine:ride:sInfo/ */
      if (outC->init) {
        for (idx = 0; idx < 12; idx++) {
          kcg_copy_Session_tindyguardTypes(
            &_L103_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
            (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
        }
      }
      else {
        kcg_copy__3_array(
          &_L103_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      }
      break;
    case SSM_st_setup_ride_machine_main_wipe_machine :
      /* wipe_machine:main:ride_machine:setup:_L2=(udp::addrs#2)/ */
      addrs_udp(
        kcg_true,
        kcg_false,
        kcg_true,
        &_L11_mergeSocket_5_then_IfBlock1_23_12,
        &_L3_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      /* wipe_machine:main:ride_machine:setup:_L9=(udp::addrsFind#2)/ */
      addrsFind_udp(
        &_L3_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16,
        if_hint_8_8_23_12_16_16,
        &_L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16,
        &_L11_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      acc2 = /* wipe_machine:main:ride_machine:setup:_L12=(udp::close#3)/ */
        close_udp(outC->socket_8_8_23_12_16_16);
      _L13_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16 = acc2 &&
        _L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16;
      if (_L13_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16) {
        /* wipe_machine:main:ride_machine:setup:_L5=(udp::bind#2)/ */
        bind_udp(
          &_L11_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          port_8_8_23_12_16_16,
          &_12_op_call,
          &op_call);
      }
      break;
    default :
      /* this branch is empty */
      break;
  }
  if (tr_1_guard_main_wipe_machine_8_8_23_12_16_16) {
    /* wipe_machine:main:ride_machine:ride:_L9=(tindyguard::data::mergeApplicationBuffer#5)/ */
    for (idx = 0; idx < 16; idx++) {
      /* wipe_machine:main:ride_machine:ride:_L9=(tindyguard::data::mergeApplicationBuffer#5)/ */
      mergeApplicationBuffer_reset_tindyguard_data_23_8_8(
        &outC->Context_mergeApplicationBuffer_5[idx]);
    }
  }
  switch (ride_machine_clock_main_wipe_machine_8_8_23_12_16_16) {
    case SSM_st_ride_ride_machine_main_wipe_machine :
      /* wipe_machine:main:ride_machine:ride:txmlength_kept/ */
      if (outC->init) {
        for (idx = 0; idx < 16; idx++) {
          tmp[idx] = kcg_lit_int32(-1);
        }
      }
      else {
        kcg_copy_array_int32_16(
          &tmp,
          &outC->txmlength_kept_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      }
      _L9_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16 = kcg_lit_int32(0);
      _L10_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16 = kcg_lit_uint64(0);
      /* wipe_machine:main:ride_machine:ride:_L9= */
      for (idx = 0; idx < 16; idx++) {
        acc9 = _L9_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
        acc = _L10_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
        /* wipe_machine:main:ride_machine:ride:_L9=(tindyguard::data::mergeApplicationBuffer#5)/ */
        mergeApplicationBuffer_tindyguard_data_23_8_8(
          acc9,
          acc,
          tmp[idx],
          length_8_8_23_12_16_16,
          out_8_8_23_12_16_16,
          knownPeers_8_8_23_12_16_16,
          &_L9_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &_L10_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &_L11_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &txmbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &txmlength_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &_L14_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &outC->Context_mergeApplicationBuffer_5[idx]);
      }
      break;
    default :
      /* this branch is empty */
      break;
  }
  if (tr_1_guard_main_wipe_machine_8_8_23_12_16_16) {
    /* @1/IfBlock1:then:_L8=(tindyguard::handshake::check#2)/ */
    for (idx = 0; idx < 16; idx++) {
      outC->init_mergeSocket_5[idx] = kcg_true;
      /* @1/IfBlock1:then:_L8=(tindyguard::handshake::check#2)/ */
      check_reset_tindyguard_handshake_23(
        &outC->Context_check_2_mergeSocket_5[idx]);
    }
  }
  switch (ride_machine_clock_main_wipe_machine_8_8_23_12_16_16) {
    case SSM_st_ride_ride_machine_main_wipe_machine :
      _L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16 = kcg_true;
      /* wipe_machine:main:ride_machine:ride:rxdlength_or_sid/ */
      if (outC->init) {
        for (idx = 0; idx < 16; idx++) {
          tmp1[idx] = nil_udp;
        }
      }
      else {
        kcg_copy_array_int32_16(
          &tmp1,
          &outC->rxdlength_or_sid_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      }
      /* wipe_machine:main:ride_machine:ride:_L65= */
      for (idx = 0; idx < 16; idx++) {
        acc2 = _L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16;
        IfBlock1_clock_mergeSocket_5_23_12 = tmp1[idx] < kcg_lit_int32(12) && acc2;
        /* @1/IfBlock1: */
        if (IfBlock1_clock_mergeSocket_5_23_12) {
          /* @1/IfBlock1:then:_L10=(udp::recvChunksFrom#2)/ */
          recvChunks24_udp_specialization(
            outC->socket_8_8_23_12_16_16,
            kcg_false,
            RXoffs_tindyguard_data,
            &acc7,
            &_L11_mergeSocket_5_then_IfBlock1_23_12,
            &rxdbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
            &rxdendpoint_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx]);
          /* @1/IfBlock1:then:_L8=(tindyguard::handshake::check#2)/ */
          check_tindyguard_handshake_23(
            _L11_mergeSocket_5_then_IfBlock1_23_12,
            &rxdbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
            &rxdendpoint_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
            sks_8_8_23_12_16_16,
            kcg_false,
            &outC->rxdlength_or_sid_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
            &_L69_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
            &outC->Context_check_2_mergeSocket_5[idx]);
          _L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16 =
            _L11_mergeSocket_5_then_IfBlock1_23_12 >= kcg_lit_int32(0);
        }
        else {
          /* @1/bufferoo/, @1/endpointoo/, @1/ouroo/ */
          if (outC->init_mergeSocket_5[idx]) {
            _L69_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx] =
              kcg_lit_uint32(0);
            kcg_copy_peer_t_udp(
              &rxdendpoint_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
              (peer_t_udp *) &emptyPeer_udp);
            for (
              idx_mergeSocket_5 = 0;
              idx_mergeSocket_5 < 24;
              idx_mergeSocket_5++) {
              kcg_copy_StreamChunk_slideTypes(
                &rxdbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx][idx_mergeSocket_5],
                (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
            }
          }
          else {
            _L69_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx] =
              outC->mem_ouroo_mergeSocket_5_23_12[idx];
            kcg_copy_peer_t_udp(
              &rxdendpoint_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
              &outC->mem_endpointoo_mergeSocket_5_23_12[idx]);
            kcg_copy_array_uint32_16_24(
              &rxdbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
              &outC->mem_bufferoo_mergeSocket_5_23_12[idx]);
          }
          _L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16 = acc2;
          outC->rxdlength_or_sid_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx] =
            tmp1[idx];
        }
        outC->mem_ouroo_mergeSocket_5_23_12[idx] =
          _L69_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx];
        kcg_copy_peer_t_udp(
          &outC->mem_endpointoo_mergeSocket_5_23_12[idx],
          &rxdendpoint_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx]);
        kcg_copy_array_uint32_16_24(
          &outC->mem_bufferoo_mergeSocket_5_23_12[idx],
          &rxdbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx]);
        outC->init_mergeSocket_5[idx] = kcg_false;
      }
      /* wipe_machine:main:ride_machine:ride:_L4= */
      for (idx = 0; idx < 12; idx++) {
        kcg_copy_array_int32_16(
          &acc3,
          &outC->rxdlength_or_sid_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
        /* wipe_machine:main:ride_machine:ride:_L4=(tindyguard::session::mapReceived#5)/ */
        mapReceived_tindyguard_session_16_12(
          /* wipe_machine:main:ride_machine:ride:_L4= */(kcg_int32) idx,
          &acc3,
          &_L69_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &_L103_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &outC->rxdlength_or_sid_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &_L5_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &_15_noname[idx]);
      }
      kcg_copy_array_int32_16(
        &_14_noname,
        &_L11_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      /* wipe_machine:main:ride_machine:ride:_L23= */
      for (idx = 0; idx < 12; idx++) {
        kcg_copy_array_int32_16(&acc4, &_14_noname);
        /* wipe_machine:main:ride_machine:ride:_L23=(tindyguard::session::mapInitPeer#2)/ */
        mapInitPeer_tindyguard_session_16(
          &acc4,
          &_L103_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          _15_noname[idx],
          &_14_noname,
          &_L24_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx]);
      }
      /* wipe_machine:main:ride_machine:ride:reKeyList/ */
      if (outC->init) {
        for (idx = 0; idx < 12; idx++) {
          noname[idx] = InvalidPeer_tindyguardTypes;
        }
      }
      else {
        kcg_copy_array_int32_12(
          &noname,
          &outC->reKeyList_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      }
      kcg_copy_array_int32_16(
        &_L26_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
        &txmlength_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      /* wipe_machine:main:ride_machine:ride:_L100=,
         wipe_machine:main:ride_machine:ride:_L44= */
      for (idx = 0; idx < 12; idx++) {
        kcg_copy_array_int32_12(&acc5, &noname);
        /* wipe_machine:main:ride_machine:ride:_L100=(tindyguard::session::mapRekey#1)/ */
        mapRekey_tindyguard_session_12(
          &acc5,
          &_L103_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          _L24_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &noname,
          &_L101_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx]);
        kcg_copy_array_int32_16(
          &acc6,
          &_L26_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
        /* wipe_machine:main:ride_machine:ride:_L44=(tindyguard::session::mapOutgoing#5)/ */
        mapOutgoing_tindyguard_session_16(
          /* wipe_machine:main:ride_machine:ride:_L44= */(kcg_int32) idx,
          &acc6,
          &_L14_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &_L26_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &_L45_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx]);
      }
      /* wipe_machine:main:ride_machine:ride:_L94=(sys::now#2)/ */
      now_sys(
        &_13_noname,
        &_L95_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      break;
    default :
      /* this branch is empty */
      break;
  }
  if (tr_1_guard_main_wipe_machine_8_8_23_12_16_16) {
    /* wipe_machine:main:ride_machine:ride:_L46=(tindyguard::session::player#6)/ */
    for (idx = 0; idx < 12; idx++) {
      /* wipe_machine:main:ride_machine:ride:_L46=(tindyguard::session::player#6)/ */
      player_reset_tindyguard_session_8_23_16_16(&outC->Context_player_6[idx]);
    }
  }
  /* wipe_machine:main:ride_machine: */
  switch (ride_machine_clock_main_wipe_machine_8_8_23_12_16_16) {
    case SSM_st_ride_ride_machine_main_wipe_machine :
      *fd_failed_8_8_23_12_16_16 = kcg_false;
      /* wipe_machine:main:ride_machine:ride:_L46= */
      for (idx = 0; idx < 12; idx++) {
        acc7 = *fd_failed_8_8_23_12_16_16;
        /* wipe_machine:main:ride_machine:ride:_L46=(tindyguard::session::player#6)/ */
        player_tindyguard_session_8_23_16_16(
          acc7,
          &_L5_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &rxdbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &rxdendpoint_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          _L101_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &txmbuffer_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &_L45_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          sks_8_8_23_12_16_16,
          knownPeers_8_8_23_12_16_16,
          outC->socket_8_8_23_12_16_16,
          _L95_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          fd_failed_8_8_23_12_16_16,
          &_L47_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &_L48_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &_L49_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &_15_noname[idx],
          &_L52_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &_L53_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &outC->reKeyList_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &outC->Context_player_6[idx]);
      }
      if (*fd_failed_8_8_23_12_16_16) {
        outC->ride_machine_state_nxt_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_setup_ride_machine_main_wipe_machine;
      }
      else {
        outC->ride_machine_state_nxt_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_ride_ride_machine_main_wipe_machine;
      }
      *no_peer_mask_8_8_23_12_16_16 =
        _L10_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
      /* wipe_machine:main:ride_machine:ride:_L15= */
      for (idx = 0; idx < 8; idx++) {
        /* wipe_machine:main:ride_machine:ride:_L15=(tindyguard::session::updatePeer#2)/ */
        updatePeer_tindyguard_session_12(
          /* wipe_machine:main:ride_machine:ride:_L15= */(kcg_int32) idx,
          &(*knownPeers_8_8_23_12_16_16)[idx],
          &_L52_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &_L53_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16,
          &(*updatedPeers_8_8_23_12_16_16)[idx]);
      }
      for (idx = 0; idx < 12; idx++) {
        for (idx_mergeSocket_5 = 0; idx_mergeSocket_5 < 16; idx_mergeSocket_5++)
        {
          tmp8[idx_mergeSocket_5][idx] =
            _L49_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx][idx_mergeSocket_5];
        }
      }
      *st_buffer_avail_8_8_23_12_16_16 = kcg_lit_int32(16);
      /* wipe_machine:main:ride_machine:ride:_L17= */
      for (idx = 0; idx < 16; idx++) {
        acc9 = *st_buffer_avail_8_8_23_12_16_16;
        /* wipe_machine:main:ride_machine:ride:_L17=(tindyguard::session::discardSent#5)/ */
        discardSent_tindyguard_session_12(
          acc9,
          txmlength_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &tmp8[idx],
          st_buffer_avail_8_8_23_12_16_16,
          &outC->txmlength_kept_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx]);
      }
      *taken_8_8_23_12_16_16 =
        _L9_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
      for (idx = 0; idx < 12; idx++) {
        for (idx_mergeSocket_5 = 0; idx_mergeSocket_5 < 16; idx_mergeSocket_5++)
        {
          tmp10[idx_mergeSocket_5][idx] =
            _L47_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx][idx_mergeSocket_5];
          kcg_copy_array_uint32_16_23(
            &tmp11[idx_mergeSocket_5][idx],
            &_L48_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx][idx_mergeSocket_5]);
        }
      }
      /* wipe_machine:main:ride_machine:ride:_L26= */
      for (idx = 0; idx < 16; idx++) {
        /* wipe_machine:main:ride_machine:ride:_L26=(tindyguard::session::collectCrops#5)/ */
        collectCrops_tindyguard_session_23_12(
          outC->rxdlength_or_sid_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &tmp10[idx],
          &tmp11[idx],
          &_L26_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx],
          &_L27_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx]);
      }
      break;
    case SSM_st_cleanup_ride_machine_main_wipe_machine :
      *fd_failed_8_8_23_12_16_16 = kcg_false;
      outC->ride_machine_state_nxt_main_wipe_machine_8_8_23_12_16_16 =
        SSM_st_cleanup_ride_machine_main_wipe_machine;
      _L9_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16 =
        /* wipe_machine:main:ride_machine:cleanup:_L1=(udp::close#2)/ */
        close_udp(outC->socket_8_8_23_12_16_16);
      outC->socket_8_8_23_12_16_16 = invalidSocket_udp;
      *no_peer_mask_8_8_23_12_16_16 = kcg_lit_uint64(0);
      kcg_copy__4_array(updatedPeers_8_8_23_12_16_16, knownPeers_8_8_23_12_16_16);
      *st_buffer_avail_8_8_23_12_16_16 = nil_tindyguardTypes;
      *taken_8_8_23_12_16_16 = kcg_lit_int32(0);
      break;
    case SSM_st_setup_ride_machine_main_wipe_machine :
      acc2 = ride_machine_fired_strong_main_wipe_machine_8_8_23_12_16_16 !=
        SSM_TR_no_trans_ride_machine_main_wipe_machine;
      if (_L13_setup_ride_machine_main_wipe_machine_8_8_23_12_16_16) {
        *fd_failed_8_8_23_12_16_16 = _12_op_call;
        outC->socket_8_8_23_12_16_16 = op_call;
      }
      else {
        *fd_failed_8_8_23_12_16_16 = kcg_true;
        outC->socket_8_8_23_12_16_16 = invalidSocket_udp;
      }
      /* wipe_machine:main:ride_machine:setup: */
      if (acc2) {
        outC->ride_machine_state_nxt_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_setup_ride_machine_main_wipe_machine;
      }
      else if (*fd_failed_8_8_23_12_16_16) {
        outC->ride_machine_state_nxt_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_setup_ride_machine_main_wipe_machine;
      }
      else {
        outC->ride_machine_state_nxt_main_wipe_machine_8_8_23_12_16_16 =
          SSM_st_ride_ride_machine_main_wipe_machine;
      }
      *no_peer_mask_8_8_23_12_16_16 = kcg_lit_uint64(0);
      kcg_copy__4_array(updatedPeers_8_8_23_12_16_16, knownPeers_8_8_23_12_16_16);
      *st_buffer_avail_8_8_23_12_16_16 = nil_tindyguardTypes;
      *taken_8_8_23_12_16_16 = kcg_lit_int32(0);
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  for (idx = 0; idx < 23; idx++) {
    kcg_copy_chunk_t_udp(&pow[idx], (chunk_t_udp *) &Zero_udp);
  }
  /* wipe_machine:main:ride_machine: */
  switch (ride_machine_clock_main_wipe_machine_8_8_23_12_16_16) {
    case SSM_st_ride_ride_machine_main_wipe_machine :
      kcg_copy_array_uint32_16_23_16(
        in_8_8_23_12_16_16,
        &_L27_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      kcg_copy_array_int32_16(
        length_in_8_8_23_12_16_16,
        &_L26_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16);
      outC->init = kcg_false;
      break;
    case SSM_st_cleanup_ride_machine_main_wipe_machine :
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_array_uint32_16_23(&(*in_8_8_23_12_16_16)[idx], &pow);
        (*length_in_8_8_23_12_16_16)[idx] = nil_udp;
      }
      break;
    case SSM_st_setup_ride_machine_main_wipe_machine :
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_array_uint32_16_23(&(*in_8_8_23_12_16_16)[idx], &pow);
        (*length_in_8_8_23_12_16_16)[idx] = nil_udp;
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
}

#ifndef KCG_USER_DEFINED_INIT
void bindings_init_tindyguard_8_8_23_12_16_16(
  outC_bindings_tindyguard_8_8_23_12_16_16 *outC)
{
  kcg_size idx;
  kcg_size idx1;
  kcg_size idx2;

  outC->mem_shutdown_8_8_23_12_16_16 = kcg_true;
  outC->init = kcg_true;
  outC->init1 = kcg_true;
  outC->ride_machine_state_nxt_main_wipe_machine_8_8_23_12_16_16 =
    SSM_st_setup_ride_machine_main_wipe_machine;
  for (idx = 0; idx < 16; idx++) {
    outC->rxdlength_or_sid_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx] =
      kcg_lit_int32(0);
  }
  for (idx = 0; idx < 12; idx++) {
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].ot[idx2] =
        kcg_lit_uint32(0);
    }
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].to[idx2] =
        kcg_lit_uint32(0);
    }
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].ot_cnt =
      kcg_lit_uint64(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].to_cnt =
      kcg_lit_uint64(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].to_cnt_cache =
      kcg_lit_uint64(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].our =
      kcg_lit_uint32(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].their =
      kcg_lit_uint32(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].rx_bytes =
      kcg_lit_int64(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].rx_cnt =
      kcg_lit_int32(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].tx_cnt =
      kcg_lit_int32(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].txTime =
      kcg_lit_int64(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].sTime =
      kcg_lit_int64(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].pid =
      kcg_lit_int32(0);
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].handshake.ephemeral.sk_x[idx2] =
        kcg_lit_uint32(0);
    }
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].handshake.ephemeral.pk_y[idx2] =
        kcg_lit_uint32(0);
    }
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].handshake.their[idx2] =
        kcg_lit_uint32(0);
    }
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].handshake.ihash[idx2] =
        kcg_lit_uint32(0);
    }
    for (idx2 = 0; idx2 < 16; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].handshake.chainingKey[idx2] =
        kcg_lit_uint32(0);
    }
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.tpub[idx2] =
        kcg_lit_uint32(0);
    }
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.preshared[idx2] =
        kcg_lit_uint32(0);
    }
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.allowed.net.addr =
      kcg_lit_uint32(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.allowed.mask.addr =
      kcg_lit_uint32(0);
    for (idx2 = 0; idx2 < 16; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.hcache.salt[idx2] =
        kcg_lit_uint32(0);
    }
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.hcache.hash1[idx2] =
        kcg_lit_uint32(0);
    }
    for (idx2 = 0; idx2 < 8; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.hcache.cookieHKey[idx2] =
        kcg_lit_uint32(0);
    }
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.endpoint.addr.addr =
      kcg_lit_uint32(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.endpoint.port =
      kcg_lit_int32(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.endpoint.time =
      kcg_lit_int32(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.endpoint.mtime =
      kcg_lit_int64(0);
    for (idx2 = 0; idx2 < 3; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.tai[idx2] =
        kcg_lit_uint32(0);
    }
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.timedOut =
      kcg_true;
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.bestSession =
      kcg_lit_int32(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.inhibitInit =
      kcg_true;
    for (idx2 = 0; idx2 < 16; idx2++) {
      outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.cookie.content[idx2] =
        kcg_lit_uint32(0);
    }
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].peer.cookie.opened =
      kcg_lit_int64(0);
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].transmissive =
      kcg_true;
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].gotKeepAlive =
      kcg_true;
    outC->sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx].sentKeepAlive =
      kcg_true;
  }
  for (idx = 0; idx < 16; idx++) {
    outC->txmlength_kept_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx] =
      kcg_lit_int32(0);
  }
  for (idx = 0; idx < 12; idx++) {
    outC->reKeyList_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16[idx] =
      kcg_lit_int32(0);
  }
  for (idx = 0; idx < 16; idx++) {
    outC->mem_ouroo_mergeSocket_5_23_12[idx] = kcg_lit_uint32(0);
    outC->mem_endpointoo_mergeSocket_5_23_12[idx].addr.addr = kcg_lit_uint32(0);
    outC->mem_endpointoo_mergeSocket_5_23_12[idx].port = kcg_lit_int32(0);
    outC->mem_endpointoo_mergeSocket_5_23_12[idx].time = kcg_lit_int32(0);
    outC->mem_endpointoo_mergeSocket_5_23_12[idx].mtime = kcg_lit_int64(0);
    for (idx2 = 0; idx2 < 24; idx2++) {
      for (idx1 = 0; idx1 < 16; idx1++) {
        outC->mem_bufferoo_mergeSocket_5_23_12[idx][idx2][idx1] = kcg_lit_uint32(0);
      }
    }
    outC->init_mergeSocket_5[idx] = kcg_true;
  }
  for (idx = 0; idx < 12; idx++) {
    /* wipe_machine:main:ride_machine:ride:_L46=(tindyguard::session::player#6)/ */
    player_init_tindyguard_session_8_23_16_16(&outC->Context_player_6[idx]);
  }
  for (idx = 0; idx < 16; idx++) {
    /* @1/IfBlock1:then:_L8=(tindyguard::handshake::check#2)/ */
    check_init_tindyguard_handshake_23(
      &outC->Context_check_2_mergeSocket_5[idx]);
    /* wipe_machine:main:ride_machine:ride:_L9=(tindyguard::data::mergeApplicationBuffer#5)/ */
    mergeApplicationBuffer_init_tindyguard_data_23_8_8(
      &outC->Context_mergeApplicationBuffer_5[idx]);
  }
  outC->socket_8_8_23_12_16_16 = nil_udp;
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void bindings_reset_tindyguard_8_8_23_12_16_16(
  outC_bindings_tindyguard_8_8_23_12_16_16 *outC)
{
  kcg_size idx;

  outC->mem_shutdown_8_8_23_12_16_16 = kcg_true;
  outC->init = kcg_true;
  outC->init1 = kcg_true;
  for (idx = 0; idx < 16; idx++) {
    outC->init_mergeSocket_5[idx] = kcg_true;
  }
  for (idx = 0; idx < 12; idx++) {
    /* wipe_machine:main:ride_machine:ride:_L46=(tindyguard::session::player#6)/ */
    player_reset_tindyguard_session_8_23_16_16(&outC->Context_player_6[idx]);
  }
  for (idx = 0; idx < 16; idx++) {
    /* @1/IfBlock1:then:_L8=(tindyguard::handshake::check#2)/ */
    check_reset_tindyguard_handshake_23(
      &outC->Context_check_2_mergeSocket_5[idx]);
    /* wipe_machine:main:ride_machine:ride:_L9=(tindyguard::data::mergeApplicationBuffer#5)/ */
    mergeApplicationBuffer_reset_tindyguard_data_23_8_8(
      &outC->Context_mergeApplicationBuffer_5[idx]);
  }
  outC->socket_8_8_23_12_16_16 = nil_udp;
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

/*
  Expanded instances for: tindyguard::bindings/
  @1: (tindyguard::data::mergeSocket#5)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bindings_tindyguard_8_8_23_12_16_16.c
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

