/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "Z_nacl_op.h"

/* nacl::op::Z/ */
void Z_nacl_op(
  /* _L1/, a/ */
  gf_nacl_op *a,
  /* _L2/, b/ */
  gf_nacl_op *b,
  /* _L3/, d/ */
  gf_nacl_op *d)
{
  kcg_size idx;

  /* _L3= */
  for (idx = 0; idx < 16; idx++) {
    (*d)[idx] = (*a)[idx] - (*b)[idx];
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Z_nacl_op.c
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

