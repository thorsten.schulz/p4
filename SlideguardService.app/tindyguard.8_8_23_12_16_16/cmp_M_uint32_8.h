/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */
#ifndef _cmp_M_uint32_8_H_
#define _cmp_M_uint32_8_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* M::cmp/ */
extern int_slideTypes cmp_M_uint32_8(
  /* _L2/, x/ */
  array_uint32_8 *x_uint32_8,
  /* _L3/, y/ */
  array_uint32_8 *y_uint32_8);



#endif /* _cmp_M_uint32_8_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** cmp_M_uint32_8.h
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

