/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mergeApplicationBuffer_tindyguard_data_23_8_8.h"

/* tindyguard::data::mergeApplicationBuffer/ */
void mergeApplicationBuffer_tindyguard_data_23_8_8(
  /* ii/ */
  size_tindyguardTypes ii_23_8_8,
  /* no_peer_acc/ */
  kcg_uint64 no_peer_acc_23_8_8,
  /* txm_residual_length/ */
  size_slideTypes txm_residual_length_23_8_8,
  /* length/ */
  array_int32_8 *length_23_8_8,
  /* msg/ */
  array_uint32_16_23_8 *msg_23_8_8,
  /* knownPeer/ */
  _4_array *knownPeer_23_8_8,
  /* ioo/ */
  size_tindyguardTypes *ioo_23_8_8,
  /* no_peer_mask/ */
  kcg_uint64 *no_peer_mask_23_8_8,
  /* pid_req/ */
  size_tindyguardTypes *pid_req_23_8_8,
  /* txmbuffer/ */
  array_uint32_16_23 *txmbuffer_23_8_8,
  /* txmlength/ */
  size_slideTypes *txmlength_23_8_8,
  /* sid/ */
  size_tindyguardTypes *sid_23_8_8,
  outC_mergeApplicationBuffer_tindyguard_data_23_8_8 *outC)
{
  size_tindyguardTypes acc;
  kcg_size idx;
  /* @2/_L20/, @3/_L10/, @3/o/ */
  kcg_bool _L20_selectPeer_it_1_selectPeer_4;
  size_tindyguardTypes acc1;
  /* @5/_L20/, @6/_L10/, @6/o/ */
  kcg_bool _L20_selectPeer_it_1_selectPeer_3;
  kcg_uint64 tmp;
  /* @1/_L13/, @1/dst/, @7/_L3/, @7/addr/, IfBlock1:then:IfBlock2:then:_L12/ */
  IpAddress_udp addr_D_4;
  /* @7/_L2/ */
  kcg_uint32 _L2_D_4;
  /* @4/_L13/, @4/dst/, @8/_L3/, @8/addr/, IfBlock1:else:_L25/ */
  IpAddress_udp addr_D_3;
  /* @8/_L2/ */
  kcg_uint32 _L2_D_3;
  /* IfBlock1:then:_L48/, IfBlock1:then:ilength/ */
  length_t_udp ilength_then_IfBlock1_23_8_8;
  /* IfBlock1:then:IfBlock2: */
  kcg_bool IfBlock2_clock_then_IfBlock1_23_8_8;
  /* @1/_L4/, @1/pid/, IfBlock1:then:IfBlock2:then:_L8/ */
  size_tindyguardTypes _L8_then_IfBlock2_then_IfBlock1_23_8_8;
  /* @1/_L10/, @1/fail/, IfBlock1:then:IfBlock2:then:_L9/ */
  kcg_bool _L9_then_IfBlock2_then_IfBlock1_23_8_8;
  /* IfBlock1:then:IfBlock2:then:_L5/ */
  Peer_tindyguardTypes _L5_then_IfBlock2_then_IfBlock1_23_8_8;
  /* IfBlock1:then:IfBlock2:then:_L1/, txmbuffer/ */
  array_uint32_16_23 txmbuffer_partial_23_8_8;
  /* @4/_L4/, @4/pid/, IfBlock1:else:_L15/ */
  size_tindyguardTypes _L15_else_IfBlock1_23_8_8;
  /* IfBlock1:else:_L23/ */
  Peer_tindyguardTypes _L23_else_IfBlock1_23_8_8;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_23_8_8;

  IfBlock1_clock_23_8_8 = txm_residual_length_23_8_8 == nil_udp;
  /* IfBlock1: */
  if (IfBlock1_clock_23_8_8) {
    if (kcg_lit_int32(0) <= ii_23_8_8 && ii_23_8_8 < kcg_lit_int32(8)) {
      ilength_then_IfBlock1_23_8_8 = (*length_23_8_8)[ii_23_8_8];
    }
    else {
      ilength_then_IfBlock1_23_8_8 = nil_udp;
    }
    IfBlock2_clock_then_IfBlock1_23_8_8 = ilength_then_IfBlock1_23_8_8 != nil_udp;
    *ioo_23_8_8 = ii_23_8_8 + kcg_lit_int32(1);
    /* IfBlock1:then:IfBlock2: */
    if (IfBlock2_clock_then_IfBlock1_23_8_8) {
      if (kcg_lit_int32(0) <= ii_23_8_8 && ii_23_8_8 < kcg_lit_int32(8)) {
        kcg_copy_array_uint32_16_23(
          &txmbuffer_partial_23_8_8,
          &(*msg_23_8_8)[ii_23_8_8]);
      }
      else {
        for (idx = 0; idx < 23; idx++) {
          kcg_copy_chunk_t_udp(&txmbuffer_partial_23_8_8[idx], (chunk_t_udp *) &Zero_udp);
        }
      }
      kcg_copy_array_uint32_16_23(txmbuffer_23_8_8, &txmbuffer_partial_23_8_8);
      _L2_D_4 = /* @7/_L2=(sys::hton#1)/ */
        htonl_sys_specialization(txmbuffer_partial_23_8_8[0][4]);
      /* @7/_L3= */
      if (kcg_lit_uint32(64) == (kcg_lit_uint32(240) &
          txmbuffer_partial_23_8_8[0][0])) {
        addr_D_4.addr = _L2_D_4;
      }
      else {
        kcg_copy_IpAddress_udp(&addr_D_4, (IpAddress_udp *) &ipAny_udp);
      }
      _L8_then_IfBlock2_then_IfBlock1_23_8_8 = InvalidPeer_tindyguardTypes;
      /* @1/_L3= */
      for (idx = 0; idx < 8; idx++) {
        acc = _L8_then_IfBlock2_then_IfBlock1_23_8_8;
        _L20_selectPeer_it_1_selectPeer_4 = addr_D_4.addr != kcg_lit_uint32(
            0) && kcg_lit_uint32(0) == ((addr_D_4.addr ^
              (*knownPeer_23_8_8)[idx].allowed.net.addr) &
            (*knownPeer_23_8_8)[idx].allowed.mask.addr) && kcg_lit_uint32(0) !=
          (*knownPeer_23_8_8)[idx].allowed.net.addr;
        /* @2/_L9= */
        if (_L20_selectPeer_it_1_selectPeer_4) {
          _L8_then_IfBlock2_then_IfBlock1_23_8_8 = /* @1/_L3= */(kcg_int32) idx;
        }
        else {
          _L8_then_IfBlock2_then_IfBlock1_23_8_8 = acc;
        }
        /* @1/_L3= */
        if (!!_L20_selectPeer_it_1_selectPeer_4) {
          break;
        }
      }
      _L9_then_IfBlock2_then_IfBlock1_23_8_8 = InvalidPeer_tindyguardTypes ==
        _L8_then_IfBlock2_then_IfBlock1_23_8_8;
      if (kcg_lit_int32(0) <= _L8_then_IfBlock2_then_IfBlock1_23_8_8 &&
        _L8_then_IfBlock2_then_IfBlock1_23_8_8 < kcg_lit_int32(8)) {
        kcg_copy_Peer_tindyguardTypes(
          &_L5_then_IfBlock2_then_IfBlock1_23_8_8,
          &(*knownPeer_23_8_8)[_L8_then_IfBlock2_then_IfBlock1_23_8_8]);
      }
      else {
        kcg_copy_Peer_tindyguardTypes(
          &_L5_then_IfBlock2_then_IfBlock1_23_8_8,
          (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
      }
      /* IfBlock1:then:IfBlock2:then:_L6= */
      if (_L9_then_IfBlock2_then_IfBlock1_23_8_8) {
        tmp = kcg_lsl_uint64(kcg_lit_uint64(1), ii_23_8_8);
        *txmlength_23_8_8 = nil_udp;
      }
      else {
        tmp = kcg_lit_uint64(0);
        *txmlength_23_8_8 = ilength_then_IfBlock1_23_8_8;
      }
      *no_peer_mask_23_8_8 = tmp | no_peer_acc_23_8_8;
      /* IfBlock1:then:IfBlock2:then:_L13= */
      if (_L5_then_IfBlock2_then_IfBlock1_23_8_8.inhibitInit) {
        *pid_req_23_8_8 = InvalidPeer_tindyguardTypes;
      }
      else {
        *pid_req_23_8_8 = _L8_then_IfBlock2_then_IfBlock1_23_8_8;
      }
      *sid_23_8_8 = _L5_then_IfBlock2_then_IfBlock1_23_8_8.bestSession;
    }
    else {
      kcg_copy_array_uint32_16_23(txmbuffer_23_8_8, &outC->mem_txmbuffer_23_8_8);
      *no_peer_mask_23_8_8 = no_peer_acc_23_8_8;
      *pid_req_23_8_8 = InvalidPeer_tindyguardTypes;
      *txmlength_23_8_8 = nil_udp;
      *sid_23_8_8 = nil_tindyguard_session;
    }
  }
  else {
    kcg_copy_array_uint32_16_23(txmbuffer_23_8_8, &outC->mem_txmbuffer_23_8_8);
    _L2_D_3 = /* @8/_L2=(sys::hton#1)/ */
      htonl_sys_specialization(outC->mem_txmbuffer_23_8_8[0][4]);
    /* @8/_L3= */
    if (kcg_lit_uint32(64) == (kcg_lit_uint32(240) &
        outC->mem_txmbuffer_23_8_8[0][0])) {
      addr_D_3.addr = _L2_D_3;
    }
    else {
      kcg_copy_IpAddress_udp(&addr_D_3, (IpAddress_udp *) &ipAny_udp);
    }
    _L15_else_IfBlock1_23_8_8 = InvalidPeer_tindyguardTypes;
    /* @4/_L3= */
    for (idx = 0; idx < 8; idx++) {
      acc1 = _L15_else_IfBlock1_23_8_8;
      _L20_selectPeer_it_1_selectPeer_3 = addr_D_3.addr != kcg_lit_uint32(0) &&
        kcg_lit_uint32(0) == ((addr_D_3.addr ^
            (*knownPeer_23_8_8)[idx].allowed.net.addr) &
          (*knownPeer_23_8_8)[idx].allowed.mask.addr) && kcg_lit_uint32(0) !=
        (*knownPeer_23_8_8)[idx].allowed.net.addr;
      /* @5/_L9= */
      if (_L20_selectPeer_it_1_selectPeer_3) {
        _L15_else_IfBlock1_23_8_8 = /* @4/_L3= */(kcg_int32) idx;
      }
      else {
        _L15_else_IfBlock1_23_8_8 = acc1;
      }
      /* @4/_L3= */
      if (!!_L20_selectPeer_it_1_selectPeer_3) {
        break;
      }
    }
    if (kcg_lit_int32(0) <= _L15_else_IfBlock1_23_8_8 &&
      _L15_else_IfBlock1_23_8_8 < kcg_lit_int32(8)) {
      kcg_copy_Peer_tindyguardTypes(
        &_L23_else_IfBlock1_23_8_8,
        &(*knownPeer_23_8_8)[_L15_else_IfBlock1_23_8_8]);
    }
    else {
      kcg_copy_Peer_tindyguardTypes(
        &_L23_else_IfBlock1_23_8_8,
        (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
    }
    *ioo_23_8_8 = ii_23_8_8;
    *no_peer_mask_23_8_8 = no_peer_acc_23_8_8;
    /* IfBlock1:else:_L14= */
    if (_L23_else_IfBlock1_23_8_8.inhibitInit) {
      *pid_req_23_8_8 = InvalidPeer_tindyguardTypes;
    }
    else {
      *pid_req_23_8_8 = _L15_else_IfBlock1_23_8_8;
    }
    /* IfBlock1:else:_L30= */
    if (InvalidPeer_tindyguardTypes == _L15_else_IfBlock1_23_8_8) {
      *txmlength_23_8_8 = nil_udp;
    }
    else {
      *txmlength_23_8_8 = txm_residual_length_23_8_8;
    }
    *sid_23_8_8 = _L23_else_IfBlock1_23_8_8.bestSession;
  }
  kcg_copy_array_uint32_16_23(&outC->mem_txmbuffer_23_8_8, txmbuffer_23_8_8);
}

#ifndef KCG_USER_DEFINED_INIT
void mergeApplicationBuffer_init_tindyguard_data_23_8_8(
  outC_mergeApplicationBuffer_tindyguard_data_23_8_8 *outC)
{
  kcg_size idx;

  for (idx = 0; idx < 23; idx++) {
    kcg_copy_chunk_t_udp(
      &outC->mem_txmbuffer_23_8_8[idx],
      (chunk_t_udp *) &Zero_udp);
  }
}
#endif /* KCG_USER_DEFINED_INIT */


void mergeApplicationBuffer_reset_tindyguard_data_23_8_8(
  outC_mergeApplicationBuffer_tindyguard_data_23_8_8 *outC)
{
  kcg_size idx;

  for (idx = 0; idx < 23; idx++) {
    kcg_copy_chunk_t_udp(
      &outC->mem_txmbuffer_23_8_8[idx],
      (chunk_t_udp *) &Zero_udp);
  }
}

/*
  Expanded instances for: tindyguard::data::mergeApplicationBuffer/
  @1: (tindyguard::data::selectPeer#4)
  @2: @1/(tindyguard::data::selectPeer_it#1)
  @3: @2/(udp::within#1)
  @4: (tindyguard::data::selectPeer#3)
  @5: @4/(tindyguard::data::selectPeer_it#1)
  @6: @5/(udp::within#1)
  @7: (ip::D#4)
  @8: (ip::D#3)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mergeApplicationBuffer_tindyguard_data_23_8_8.c
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

