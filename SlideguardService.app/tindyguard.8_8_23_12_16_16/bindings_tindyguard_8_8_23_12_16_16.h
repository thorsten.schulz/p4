/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _bindings_tindyguard_8_8_23_12_16_16_H_
#define _bindings_tindyguard_8_8_23_12_16_16_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "mapRekey_tindyguard_session_12.h"
#include "mapOutgoing_tindyguard_session_16.h"
#include "collectCrops_tindyguard_session_23_12.h"
#include "mapInitPeer_tindyguard_session_16.h"
#include "discardSent_tindyguard_session_12.h"
#include "updatePeer_tindyguard_session_12.h"
#include "mapReceived_tindyguard_session_16_12.h"
#include "addrsFind_udp.h"
#include "check_tindyguard_handshake_23.h"
#include "player_tindyguard_session_8_23_16_16.h"
#include "mergeApplicationBuffer_tindyguard_data_23_8_8.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  array_bool_16 init_mergeSocket_5;
  array_uint32_16_24_16 /* @1/bufferoo/ */ mem_bufferoo_mergeSocket_5_23_12;
  _5_array /* @1/endpointoo/ */ mem_endpointoo_mergeSocket_5_23_12;
  array_uint32_16 /* @1/ouroo/ */ mem_ouroo_mergeSocket_5_23_12;
  kcg_bool /* shutdown/ */ mem_shutdown_8_8_23_12_16_16;
  kcg_bool init;
  kcg_bool init1;
  array_int32_12 /* wipe_machine:main:ride_machine:ride:_L97/,
     wipe_machine:main:ride_machine:ride:reKeyList/ */ reKeyList_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  array_int32_16 /* wipe_machine:main:ride_machine:ride:_L18/,
     wipe_machine:main:ride_machine:ride:txmlength_kept/ */ txmlength_kept_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  _3_array /* wipe_machine:main:ride_machine:ride:_L50/,
     wipe_machine:main:ride_machine:ride:_L86/,
     wipe_machine:main:ride_machine:ride:sInfo/ */ sInfo_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  array_int32_16 /* wipe_machine:main:ride_machine:ride:_L4/,
     wipe_machine:main:ride_machine:ride:_L81/,
     wipe_machine:main:ride_machine:ride:rxdlength_or_sid/ */ rxdlength_or_sid_ride_ride_machine_main_wipe_machine_8_8_23_12_16_16;
  SSM_ST_ride_machine_main_wipe_machine /* wipe_machine:main:ride_machine: */ ride_machine_state_nxt_main_wipe_machine_8_8_23_12_16_16;
  fd_t_udp /* socket/ */ socket_8_8_23_12_16_16;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_player_tindyguard_session_8_23_16_16 /* wipe_machine:main:ride_machine:ride:_L46=(tindyguard::session::player#6)/ */ Context_player_6[12];
  outC_check_tindyguard_handshake_23 /* @1/IfBlock1:then:_L8=(tindyguard::handshake::check#2)/ */ Context_check_2_mergeSocket_5[16];
  outC_mergeApplicationBuffer_tindyguard_data_23_8_8 /* wipe_machine:main:ride_machine:ride:_L9=(tindyguard::data::mergeApplicationBuffer#5)/ */ Context_mergeApplicationBuffer_5[16];
  /* ----------------- no clocks of observable data ------------------ */
} outC_bindings_tindyguard_8_8_23_12_16_16;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::bindings/ */
extern void bindings_tindyguard_8_8_23_12_16_16(
  /* length/ */
  array_int32_8 *length_8_8_23_12_16_16,
  /* out/ */
  array_uint32_16_23_8 *out_8_8_23_12_16_16,
  /* knownPeers/ */
  _4_array *knownPeers_8_8_23_12_16_16,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8_8_23_12_16_16,
  /* shutdown/ */
  kcg_bool shutdown_8_8_23_12_16_16,
  /* port/ */
  port_t_udp port_8_8_23_12_16_16,
  /* if_hint/ */
  range_t_udp *if_hint_8_8_23_12_16_16,
  /* length_in/ */
  array_int32_16 *length_in_8_8_23_12_16_16,
  /* in/ */
  array_uint32_16_23_16 *in_8_8_23_12_16_16,
  /* taken/ */
  size_tindyguardTypes *taken_8_8_23_12_16_16,
  /* st_buffer_avail/ */
  size_tindyguardTypes *st_buffer_avail_8_8_23_12_16_16,
  /* updatedPeers/ */
  _4_array *updatedPeers_8_8_23_12_16_16,
  /* no_peer_mask/ */
  kcg_uint64 *no_peer_mask_8_8_23_12_16_16,
  /* _L237/, fd_failed/, fdfail/ */
  kcg_bool *fd_failed_8_8_23_12_16_16,
  outC_bindings_tindyguard_8_8_23_12_16_16 *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void bindings_reset_tindyguard_8_8_23_12_16_16(
  outC_bindings_tindyguard_8_8_23_12_16_16 *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void bindings_init_tindyguard_8_8_23_12_16_16(
  outC_bindings_tindyguard_8_8_23_12_16_16 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::bindings/
  @1: (tindyguard::data::mergeSocket#5)
*/

#endif /* _bindings_tindyguard_8_8_23_12_16_16_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bindings_tindyguard_8_8_23_12_16_16.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

