/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _stream_it_hash_blake2s_H_
#define _stream_it_hash_blake2s_H_

#include "kcg_types.h"
#include "block_refine_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::stream_it/ */
extern void stream_it_hash_blake2s(
  /* _L9/, i/ */
  size_slideTypes i,
  /* _L3/, hin/ */
  array_uint32_8 *hin,
  /* m/ */
  StreamChunk_slideTypes *m,
  /* _L5/, len/ */
  size_slideTypes len,
  /* _L10/, goOn/ */
  kcg_bool *goOn,
  /* _L2/, hout/ */
  array_uint32_8 *hout);



#endif /* _stream_it_hash_blake2s_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** stream_it_hash_blake2s.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

