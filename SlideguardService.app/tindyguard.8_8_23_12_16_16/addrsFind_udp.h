/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */
#ifndef _addrsFind_udp_H_
#define _addrsFind_udp_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* udp::addrsFind/ */
extern void addrsFind_udp(
  /* _L18/, addrList/ */
  array *addrList,
  /* _L15/, _L21/, net/ */
  range_t_udp *net,
  /* _L24/, found/ */
  kcg_bool *found,
  /* _L14/, addr/ */
  IpAddress_udp *addr);



#endif /* _addrsFind_udp_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** addrsFind_udp.h
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

