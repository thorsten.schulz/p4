/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _single_hash_blake2s_H_
#define _single_hash_blake2s_H_

#include "kcg_types.h"
#include "stream_it_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::single/ */
extern void single_hash_blake2s(
  /* _L64/, msg/ */
  StreamChunk_slideTypes *msg,
  /* _L66/, len/ */
  size_slideTypes len,
  /* _L72/, hash/ */
  Hash_hash_blake2s *hash);



#endif /* _single_hash_blake2s_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** single_hash_blake2s.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

