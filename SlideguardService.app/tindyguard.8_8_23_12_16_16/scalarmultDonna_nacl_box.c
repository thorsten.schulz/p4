/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "scalarmultDonna_nacl_box.h"

/* nacl::box::scalarmultDonna/ */
void scalarmultDonna_nacl_box(
  /* _L3/, our/ */
  KeyPair32_slideTypes *our,
  /* _L2/, their/ */
  Key32_slideTypes *their,
  /* IfBlock1:, check/ */
  kcg_bool check,
  /* failed/ */
  kcg_bool *failed,
  /* _L56/, k/, key/ */
  Key32_slideTypes *key)
{
  /* _L56=(nacl::box::curve25519_donna#1)/ */
  curve25519_donna_nacl_box(&(*our).sk_x, their, key);
  /* IfBlock1: */
  if (check) {
    *failed = /* IfBlock1:then:_L10=(nacl::box::assertGoodKey#2)/ */
      assertGoodKey_nacl_box(their) ||
      /* IfBlock1:then:_L8=(nacl::box::assertGoodKey#1)/ */
      assertGoodKey_nacl_box(key);
  }
  else {
    *failed = kcg_false;
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarmultDonna_nacl_box.c
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

