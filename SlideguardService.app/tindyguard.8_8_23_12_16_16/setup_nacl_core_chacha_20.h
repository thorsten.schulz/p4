/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _setup_nacl_core_chacha_20_H_
#define _setup_nacl_core_chacha_20_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::core::chacha::setup/ */
extern void setup_nacl_core_chacha_20(
  /* _L5/, nonce/ */
  Nonce_nacl_core_chacha *nonce_20,
  /* _L3/, key/ */
  Key_nacl_core *key_20,
  /* @1/_L68/, @1/noo/, _L8/, noo/ */
  State_nacl_core_chacha *noo_20,
  /* @1/_L74/, @1/cipher/, _L9/, mackeystream/ */
  StreamChunk_slideTypes *mackeystream_20);

/*
  Expanded instances for: nacl::core::chacha::setup/
  @1: (nacl::core::chacha::generate#1)
*/

#endif /* _setup_nacl_core_chacha_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** setup_nacl_core_chacha_20.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

