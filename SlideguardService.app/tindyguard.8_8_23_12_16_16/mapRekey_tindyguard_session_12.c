/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapRekey_tindyguard_session_12.h"

/* tindyguard::session::mapRekey/ */
void mapRekey_tindyguard_session_12(
  /* rekey/ */
  array_int32_12 *rekey_12,
  /* s/ */
  Session_tindyguardTypes *s_12,
  /* pid_cmd_in/ */
  size_tindyguardTypes pid_cmd_in_12,
  /* pid_not_taken/ */
  array_int32_12 *pid_not_taken_12,
  /* pid_cmd/ */
  size_tindyguardTypes *pid_cmd_12)
{
  size_tindyguardTypes acc;
  kcg_size idx;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_findNonNil_it_1;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_12;

  IfBlock1_clock_12 = pid_cmd_in_12 != nil_tindyguard_session || (*s_12).pid !=
    nil_tindyguard_session;
  /* IfBlock1: */
  if (IfBlock1_clock_12) {
    kcg_copy_array_int32_12(pid_not_taken_12, rekey_12);
    *pid_cmd_12 = pid_cmd_in_12;
  }
  else {
    *pid_cmd_12 = InvalidPeer_tindyguardTypes;
    /* IfBlock1:else:_L7= */
    for (idx = 0; idx < 12; idx++) {
      acc = *pid_cmd_12;
      IfBlock1_clock_findNonNil_it_1 = acc == InvalidPeer_tindyguardTypes;
      /* @1/IfBlock1: */
      if (IfBlock1_clock_findNonNil_it_1) {
        /* @1/IfBlock1:then:_L1= */
        if ((*rekey_12)[idx] != InvalidPeer_tindyguardTypes) {
          *pid_cmd_12 = (*rekey_12)[idx];
        }
        else {
          *pid_cmd_12 = InvalidPeer_tindyguardTypes;
        }
        (*pid_not_taken_12)[idx] = InvalidPeer_tindyguardTypes;
      }
      else {
        *pid_cmd_12 = acc;
        (*pid_not_taken_12)[idx] = (*rekey_12)[idx];
      }
    }
  }
}

/*
  Expanded instances for: tindyguard::session::mapRekey/
  @1: (tindyguard::session::findNonNil_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapRekey_tindyguard_session_12.c
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

