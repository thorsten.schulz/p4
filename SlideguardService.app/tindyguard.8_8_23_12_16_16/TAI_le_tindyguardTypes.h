/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _TAI_le_tindyguardTypes_H_
#define _TAI_le_tindyguardTypes_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguardTypes::TAI_le/ */
extern kcg_bool TAI_le_tindyguardTypes(
  /* _L1/, x_le/ */
  TAI_tindyguardTypes *x_le,
  /* _L2/, y_le/ */
  TAI_tindyguardTypes *y_le);



#endif /* _TAI_le_tindyguardTypes_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** TAI_le_tindyguardTypes.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

