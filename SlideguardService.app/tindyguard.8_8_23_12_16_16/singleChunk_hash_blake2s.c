/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "singleChunk_hash_blake2s.h"

/* hash::blake2s::singleChunk/ */
void singleChunk_hash_blake2s(
  /* _L64/, msg/ */
  StreamChunk_slideTypes *msg,
  /* _L66/, len/ */
  size_slideTypes len,
  /* _L65/, chunk/ */
  HashChunk_hash_blake2s *chunk)
{
  array_uint32_8 tmp;
  kcg_bool noname;
  kcg_size idx;

  tmp[0] = kcg_lit_uint32(1795745351);
  kcg_copy_array_uint32_7(&tmp[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
  /* _L61=(hash::blake2s::stream_it#1)/ */
  stream_it_hash_blake2s(
    kcg_lit_int32(0),
    &tmp,
    msg,
    len,
    &noname,
    (array_uint32_8 *) &(*chunk)[0]);
  for (idx = 0; idx < 8; idx++) {
    (*chunk)[idx + 8] = kcg_lit_uint32(0);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** singleChunk_hash_blake2s.c
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

