/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "updatePeer_tindyguard_session_12.h"

/* tindyguard::session::updatePeer/ */
void updatePeer_tindyguard_session_12(
  /* _L23/, pid/ */
  size_tindyguardTypes pid_12,
  /* _L21/, knownPeer/ */
  Peer_tindyguardTypes *knownPeer_12,
  /* _L18/, isNewTAI/ */
  array_bool_12 *isNewTAI_12,
  /* _L19/, s/ */
  _3_array *s_12,
  /* _L25/, t_flag/ */
  array_bool_12 *t_flag_12,
  /* _L22/, updatedPeer/ */
  Peer_tindyguardTypes *updatedPeer_12)
{
  Peer_tindyguardTypes acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_updatePeer_it_1;
  /* @1/IfBlock1:then:_L13/ */
  Peer_tindyguardTypes _L13_updatePeer_it_1_then_IfBlock1;
  /* @1/IfBlock1:then:_L17/ */
  Peer_tindyguardTypes _L17_updatePeer_it_1_then_IfBlock1;

  kcg_copy_Peer_tindyguardTypes(updatedPeer_12, knownPeer_12);
  (*updatedPeer_12).inhibitInit = kcg_false;
  /* _L12= */
  for (idx = 0; idx < 12; idx++) {
    kcg_copy_Peer_tindyguardTypes(&acc, updatedPeer_12);
    IfBlock1_clock_updatePeer_it_1 = pid_12 == (*s_12)[idx].pid;
    /* @1/IfBlock1: */
    if (IfBlock1_clock_updatePeer_it_1) {
      kcg_copy_Peer_tindyguardTypes(&_L17_updatePeer_it_1_then_IfBlock1, &acc);
      _L17_updatePeer_it_1_then_IfBlock1.inhibitInit = kcg_true;
      _L17_updatePeer_it_1_then_IfBlock1.timedOut = (*t_flag_12)[idx];
      /* @1/IfBlock1:then:_L13= */
      if ((*isNewTAI_12)[idx] || (*s_12)[idx].peer.endpoint.mtime >
        acc.endpoint.mtime || (*s_12)[idx].peer.endpoint.mtime <= kcg_lit_int64(
          0)) {
        kcg_copy_Peer_tindyguardTypes(
          &_L13_updatePeer_it_1_then_IfBlock1,
          &_L17_updatePeer_it_1_then_IfBlock1);
        _L13_updatePeer_it_1_then_IfBlock1.bestSession = /* _L12= */(kcg_int32) idx;
        kcg_copy_peer_t_udp(
          &_L13_updatePeer_it_1_then_IfBlock1.endpoint,
          &(*s_12)[idx].peer.endpoint);
      }
      else {
        kcg_copy_Peer_tindyguardTypes(
          &_L13_updatePeer_it_1_then_IfBlock1,
          &_L17_updatePeer_it_1_then_IfBlock1);
      }
      cond_iterw = !(*isNewTAI_12)[idx];
      /* @1/IfBlock1:then:_L9= */
      if ((*isNewTAI_12)[idx]) {
        kcg_copy_Peer_tindyguardTypes(
          updatedPeer_12,
          &_L13_updatePeer_it_1_then_IfBlock1);
        kcg_copy_TAI_tindyguardTypes(&(*updatedPeer_12).tai, &(*s_12)[idx].peer.tai);
      }
      else {
        kcg_copy_Peer_tindyguardTypes(
          updatedPeer_12,
          &_L13_updatePeer_it_1_then_IfBlock1);
      }
    }
    else {
      cond_iterw = kcg_true;
      kcg_copy_Peer_tindyguardTypes(updatedPeer_12, &acc);
    }
    /* _L12= */
    if (!cond_iterw) {
      break;
    }
  }
}

/*
  Expanded instances for: tindyguard::session::updatePeer/
  @1: (tindyguard::session::updatePeer_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** updatePeer_tindyguard_session_12.c
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

