/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _Xor_nacl_core_chacha_20_H_
#define _Xor_nacl_core_chacha_20_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::core::chacha::Xor/ */
extern void Xor_nacl_core_chacha_20(
  /* _L61/, i/ */
  int_slideTypes i_20,
  /* @1/_L34/, @1/_L69/, @1/_L71/, @1/n/, _L34/, n/ */
  State_nacl_core_chacha *n_20,
  /* m/ */
  StreamChunk_slideTypes *m_20,
  /* _L62/, mlen/ */
  int_slideTypes mlen_20,
  /* IfBlock1:, _L67/, goOn/, more/ */
  kcg_bool *goOn_20,
  /* @1/_L68/, @1/noo/, _L68/, noo/ */
  State_nacl_core_chacha *noo_20,
  /* c/ */
  StreamChunk_slideTypes *c_20);

/*
  Expanded instances for: nacl::core::chacha::Xor/
  @1: (nacl::core::chacha::generate#1)
*/

#endif /* _Xor_nacl_core_chacha_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Xor_nacl_core_chacha_20.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

