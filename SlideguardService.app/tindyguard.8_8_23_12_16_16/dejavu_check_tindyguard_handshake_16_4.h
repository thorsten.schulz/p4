/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _dejavu_check_tindyguard_handshake_16_4_H_
#define _dejavu_check_tindyguard_handshake_16_4_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  array_uint32_4_4_16 /* @1/IfBlock1:then:list/ */ list_dejavu_bucket_it_1_then_IfBlock1_4;
  array_uint32_16 /* @1/IfBlock1:then:cnt/ */ cnt_dejavu_bucket_it_1_then_IfBlock1_4;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_dejavu_check_tindyguard_handshake_16_4;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::handshake::dejavu_check/ */
extern void dejavu_check_tindyguard_handshake_16_4(
  /* _L1/, _L33/, mac/ */
  Mac_hash_blake2s *mac_16_4,
  /* _L32/, dejavu/ */
  kcg_bool *dejavu_16_4,
  outC_dejavu_check_tindyguard_handshake_16_4 *outC);

extern void dejavu_check_reset_tindyguard_handshake_16_4(
  outC_dejavu_check_tindyguard_handshake_16_4 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void dejavu_check_init_tindyguard_handshake_16_4(
  outC_dejavu_check_tindyguard_handshake_16_4 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::handshake::dejavu_check/
  @1: (tindyguard::handshake::dejavu_bucket_it#1)
*/

#endif /* _dejavu_check_tindyguard_handshake_16_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** dejavu_check_tindyguard_handshake_16_4.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

