/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */
#ifndef _inv25519_nacl_op_H_
#define _inv25519_nacl_op_H_

#include "kcg_types.h"
#include "inv25519_it1_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::inv25519/ */
extern void inv25519_nacl_op(
  /* _L1/, i/ */
  gf_nacl_op *i,
  /* _L2/, o/ */
  gf_nacl_op *o);



#endif /* _inv25519_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** inv25519_nacl_op.h
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

