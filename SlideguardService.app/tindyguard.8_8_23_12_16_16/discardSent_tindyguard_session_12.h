/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */
#ifndef _discardSent_tindyguard_session_12_H_
#define _discardSent_tindyguard_session_12_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::discardSent/ */
extern void discardSent_tindyguard_session_12(
  /* @1/_L1/, @1/i/, _L8/, slots/ */
  size_tindyguardTypes slots_12,
  /* _L1/, lenbuf/ */
  size_tindyguardTypes lenbuf_12,
  /* _L2/, taken/ */
  array_bool_12 *taken_12,
  /* _L11/, avail/ */
  size_tindyguardTypes *avail_12,
  /* _L7/, lenbufoo/ */
  size_tindyguardTypes *lenbufoo_12);

/*
  Expanded instances for: tindyguard::session::discardSent/
  @1: (M::dec#1)
*/

#endif /* _discardSent_tindyguard_session_12_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** discardSent_tindyguard_session_12.h
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

