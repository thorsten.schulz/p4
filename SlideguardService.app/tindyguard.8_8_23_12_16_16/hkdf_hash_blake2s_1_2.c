/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hkdf_hash_blake2s_1_2.h"

/* hash::blake2s::hkdf/ */
void hkdf_hash_blake2s_1_2(
  /* _L4/, ikm/ */
  array_uint32_16_1 *ikm_1_2,
  /* _L5/, ikmlen/ */
  size_slideTypes ikmlen_1_2,
  /* _L3/, chainingKey/ */
  HashChunk_hash_blake2s *chainingKey_1_2,
  /* _L14/, okm/ */
  array_uint32_8_2 *okm_1_2)
{
  StreamChunk_slideTypes acc;
  kcg_size idx;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_hkdf_expand_it2_1;
  /* @1/IfBlock1:else:_L9/ */
  array_uint32_16_1 _L9_hkdf_expand_it2_1_else_IfBlock1;
  /* @1/IfBlock1:else:_L5/, @1/hashoo/ */
  HashChunk_hash_blake2s hashoo_partial_hkdf_expand_it2_1;
  /* @1/IfBlock1:then:_L10/ */
  array_uint32_16_1 _L10_hkdf_expand_it2_1_then_IfBlock1;
  /* @1/IfBlock1:then:_L5/, @1/hashoo/ */
  HashChunk_hash_blake2s _1_hashoo_partial_hkdf_expand_it2_1;
  /* _L18/ */
  HashChunk_hash_blake2s _L18_1_2;
  HashChunk_hash_blake2s noname;

  /* _L18=(hash::blake2s::hmacChunk#1)/ */
  hmacChunk_hash_blake2s_1(ikm_1_2, ikmlen_1_2, chainingKey_1_2, &_L18_1_2);
  kcg_copy_HashChunk_hash_blake2s(
    &noname,
    (HashChunk_hash_blake2s *) &ZeroChunk_slideTypes);
  /* _L16= */
  for (idx = 0; idx < 2; idx++) {
    kcg_copy_StreamChunk_slideTypes(&acc, &noname);
    IfBlock1_clock_hkdf_expand_it2_1 = /* _L16= */(kcg_uint32) idx ==
      kcg_lit_uint32(0);
    /* @1/IfBlock1: */
    if (IfBlock1_clock_hkdf_expand_it2_1) {
      kcg_copy_StreamChunk_slideTypes(
        &_L10_hkdf_expand_it2_1_then_IfBlock1[0],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
      _L10_hkdf_expand_it2_1_then_IfBlock1[0][0] = kcg_lit_uint32(1);
      /* @1/IfBlock1:then:_L5=(hash::blake2s::hmacChunk#2)/ */
      hmacChunk_hash_blake2s_1(
        &_L10_hkdf_expand_it2_1_then_IfBlock1,
        kcg_lit_int32(1),
        &_L18_1_2,
        &_1_hashoo_partial_hkdf_expand_it2_1);
      kcg_copy_HashChunk_hash_blake2s(&noname, &_1_hashoo_partial_hkdf_expand_it2_1);
      kcg_copy_Hash_hash_blake2s(
        &(*okm_1_2)[idx],
        (Hash_hash_blake2s *) &_1_hashoo_partial_hkdf_expand_it2_1[0]);
    }
    else {
      kcg_copy_StreamChunk_slideTypes(&_L9_hkdf_expand_it2_1_else_IfBlock1[0], &acc);
      _L9_hkdf_expand_it2_1_else_IfBlock1[0][8] = /* _L16= */(kcg_uint32) idx +
        kcg_lit_uint32(1);
      /* @1/IfBlock1:else:_L5=(hash::blake2s::hmacChunk#1)/ */
      hmacChunk_hash_blake2s_1(
        &_L9_hkdf_expand_it2_1_else_IfBlock1,
        kcg_lit_int32(33),
        &_L18_1_2,
        &hashoo_partial_hkdf_expand_it2_1);
      kcg_copy_HashChunk_hash_blake2s(&noname, &hashoo_partial_hkdf_expand_it2_1);
      kcg_copy_Hash_hash_blake2s(
        &(*okm_1_2)[idx],
        (Hash_hash_blake2s *) &hashoo_partial_hkdf_expand_it2_1[0]);
    }
  }
}

/*
  Expanded instances for: hash::blake2s::hkdf/
  @1: (hash::blake2s::hkdf_expand_it2#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_hash_blake2s_1_2.c
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

