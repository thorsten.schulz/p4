/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _ipsum_BEH_ip_1_H_
#define _ipsum_BEH_ip_1_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ip::ipsum_BEH/ */
extern kcg_uint32 ipsum_BEH_ip_1(
  /* _L1/, d/ */
  array_uint32_16_1 *d_1,
  /* _L2/, size/ */
  length_t_udp size_1,
  /* _L3/, offs/ */
  length_t_udp offs_1);



#endif /* _ipsum_BEH_ip_1_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** ipsum_BEH_ip_1.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

