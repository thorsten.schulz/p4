/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "unwrap_tindyguard_data_23.h"

/* tindyguard::data::unwrap/ */
void unwrap_tindyguard_data_23(
  /* _L131/, s/ */
  Session_tindyguardTypes *s_23,
  /* fail_in/ */
  kcg_bool fail_in_23,
  /* rlength/ */
  size_slideTypes rlength_23,
  /* datamsg/ */
  array_uint32_16_24 *datamsg_23,
  /* endpoint/ */
  peer_t_udp *endpoint_23,
  /* soo/ */
  Session_tindyguardTypes *soo_23,
  /* fail_flag/ */
  kcg_bool *fail_flag_23,
  /* len/ */
  length_t_udp *len_23,
  /* msg/ */
  array_uint32_16_23 *msg_23)
{
  array_uint8_16 tmp;
  array_uint32_3 tmp1;
  /* _L126/ */
  array_uint32_2 _L126_23;
  /* _L134/ */
  kcg_bool _L134_23;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_23;
  /* @1/_L2/, @1/o/, IfBlock1:then:_L28/ */
  kcg_int32 _L28_then_IfBlock1_23;
  /* IfBlock1:then:_L16/ */
  kcg_bool _L16_then_IfBlock1_23;
  /* @2/_L25/, @2/u32/, IfBlock1:then:_L13/ */
  array_uint32_4 _L13_then_IfBlock1_23;
  /* IfBlock1:then:_L11/ */
  StreamChunk_slideTypes _L11_then_IfBlock1_23;
  /* IfBlock1:then:_L10/ */
  array_uint32_16_1 _L10_then_IfBlock1_23;
  /* IfBlock1:then:_L19/, IfBlock1:then:_L7/ */
  kcg_int32 _L7_then_IfBlock1_23;
  /* IfBlock1:then:_L51/ */
  kcg_bool _L51_then_IfBlock1_23;
  /* @3/IfBlock1:else: */
  kcg_bool else_clock_accept_nonce_3_IfBlock1;
  /* @3/IfBlock1:else:else:_L5/ */
  kcg_uint64 _L5_accept_nonce_3_else_else_IfBlock1;
  /* @3/IfBlock1:else:else:_L6/ */
  kcg_uint64 _L6_accept_nonce_3_else_else_IfBlock1;
  /* @3/IfBlock1:else:else:_L14/, @3/failed/ */
  kcg_bool _L14_accept_nonce_3_else_else_IfBlock1;
  /* @3/IfBlock1:else:then:_L5/ */
  kcg_uint64 _L5_accept_nonce_3_then_else_IfBlock1;
  /* @3/IfBlock1: */
  kcg_bool IfBlock1_clock_accept_nonce_3;
  /* @3/_L10/, @3/new_n/ */
  kcg_uint64 new_n_accept_nonce_3;
  /* @2/_L15/ */
  kcg_uint32 _L15_stAuth_4;
  /* @2/_L14/ */
  kcg_uint32 _L14_stAuth_4;
  /* @2/_L32/ */
  kcg_uint32 _L32_stAuth_4;
  /* @2/_L30/ */
  kcg_uint32 _L30_stAuth_4;
  /* @2/_L38/ */
  kcg_uint32 _L38_stAuth_4;
  /* @2/_L37/ */
  kcg_uint32 _L37_stAuth_4;
  /* @2/_L46/ */
  kcg_uint32 _L46_stAuth_4;
  /* @2/_L42/ */
  kcg_uint32 _L42_stAuth_4;
  kcg_size idx;

  _L134_23 = MsgType_Data_tindyguard == (*datamsg_23)[0][12];
  kcg_copy_array_uint32_2(&_L126_23, (array_uint32_2 *) &(*datamsg_23)[0][14]);
  if (_L134_23) {
    IfBlock1_clock_accept_nonce_3 = (*s_23).pid == InvalidPeer_tindyguardTypes;
    new_n_accept_nonce_3 = /* @3/_L8= */(kcg_uint64) _L126_23[0] |
      kcg_lsl_uint64(/* @3/_L9= */(kcg_uint64) _L126_23[1], kcg_lit_uint64(32));
    /* @3/IfBlock1: */
    if (IfBlock1_clock_accept_nonce_3) {
      IfBlock1_clock_23 = kcg_false;
    }
    else {
      else_clock_accept_nonce_3_IfBlock1 = new_n_accept_nonce_3 > (*s_23).to_cnt;
      /* @3/IfBlock1:else: */
      if (else_clock_accept_nonce_3_IfBlock1) {
        IfBlock1_clock_23 = kcg_true;
        _L5_accept_nonce_3_then_else_IfBlock1 = new_n_accept_nonce_3 - (*s_23).to_cnt;
      }
      else {
        _L5_accept_nonce_3_else_else_IfBlock1 = (*s_23).to_cnt - new_n_accept_nonce_3;
        _L6_accept_nonce_3_else_else_IfBlock1 = kcg_lsl_uint64(
            kcg_lit_uint64(1),
            _L5_accept_nonce_3_else_else_IfBlock1);
        _L14_accept_nonce_3_else_else_IfBlock1 = ((*s_23).to_cnt_cache &
            _L6_accept_nonce_3_else_else_IfBlock1) != kcg_lit_uint64(0) ||
          _L5_accept_nonce_3_else_else_IfBlock1 >= kcg_lit_uint64(64);
        IfBlock1_clock_23 = !_L14_accept_nonce_3_else_else_IfBlock1;
      }
    }
  }
  else {
    IfBlock1_clock_23 = kcg_false;
  }
  /* IfBlock1: */
  if (IfBlock1_clock_23) {
    _L7_then_IfBlock1_23 = rlength_23 - kcg_lit_int32(32);
    _L28_then_IfBlock1_23 = _L7_then_IfBlock1_23 / StreamChunkBytes_slideTypes +
      kcg_lit_int32(1);
    if (kcg_lit_int32(0) <= _L28_then_IfBlock1_23 && _L28_then_IfBlock1_23 <
      kcg_lit_int32(24)) {
      kcg_copy_StreamChunk_slideTypes(
        &_L11_then_IfBlock1_23,
        &(*datamsg_23)[_L28_then_IfBlock1_23]);
    }
    else {
      kcg_copy_StreamChunk_slideTypes(
        &_L11_then_IfBlock1_23,
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
    /* IfBlock1:then:_L13= */
    switch (_L7_then_IfBlock1_23 % StreamChunkBytes_slideTypes / kcg_lit_int32(
        16)) {
      case kcg_lit_int32(0) :
        kcg_copy_array_uint32_4(
          &_L13_then_IfBlock1_23,
          (array_uint32_4 *) &_L11_then_IfBlock1_23[0]);
        break;
      case kcg_lit_int32(1) :
        kcg_copy_array_uint32_4(
          &_L13_then_IfBlock1_23,
          (array_uint32_4 *) &_L11_then_IfBlock1_23[4]);
        break;
      case kcg_lit_int32(2) :
        kcg_copy_array_uint32_4(
          &_L13_then_IfBlock1_23,
          (array_uint32_4 *) &_L11_then_IfBlock1_23[8]);
        break;
      default :
        kcg_copy_array_uint32_4(
          &_L13_then_IfBlock1_23,
          (array_uint32_4 *) &_L11_then_IfBlock1_23[12]);
        break;
    }
    _L14_stAuth_4 = _L13_then_IfBlock1_23[0] >> kcg_lit_uint32(8);
    _L15_stAuth_4 = _L14_stAuth_4 >> kcg_lit_uint32(8);
    _L32_stAuth_4 = _L13_then_IfBlock1_23[1] >> kcg_lit_uint32(8);
    _L30_stAuth_4 = _L32_stAuth_4 >> kcg_lit_uint32(8);
    _L37_stAuth_4 = _L13_then_IfBlock1_23[2] >> kcg_lit_uint32(8);
    _L38_stAuth_4 = _L37_stAuth_4 >> kcg_lit_uint32(8);
    _L46_stAuth_4 = _L13_then_IfBlock1_23[3] >> kcg_lit_uint32(8);
    _L42_stAuth_4 = _L46_stAuth_4 >> kcg_lit_uint32(8);
    tmp[0] = /* @2/_L8= */(kcg_uint8) _L13_then_IfBlock1_23[0];
    tmp[1] = /* @2/_L7= */(kcg_uint8) _L14_stAuth_4;
    tmp[2] = /* @2/_L6= */(kcg_uint8) _L15_stAuth_4;
    tmp[3] = /* @2/_L5= */(kcg_uint8) (_L15_stAuth_4 >> kcg_lit_uint32(8));
    tmp[4] = /* @2/_L26= */(kcg_uint8) _L13_then_IfBlock1_23[1];
    tmp[5] = /* @2/_L29= */(kcg_uint8) _L32_stAuth_4;
    tmp[6] = /* @2/_L27= */(kcg_uint8) _L30_stAuth_4;
    tmp[7] = /* @2/_L31= */(kcg_uint8) (_L30_stAuth_4 >> kcg_lit_uint32(8));
    tmp[8] = /* @2/_L34= */(kcg_uint8) _L13_then_IfBlock1_23[2];
    tmp[9] = /* @2/_L36= */(kcg_uint8) _L37_stAuth_4;
    tmp[10] = /* @2/_L33= */(kcg_uint8) _L38_stAuth_4;
    tmp[11] = /* @2/_L35= */(kcg_uint8) (_L38_stAuth_4 >> kcg_lit_uint32(8));
    tmp[12] = /* @2/_L41= */(kcg_uint8) _L13_then_IfBlock1_23[3];
    tmp[13] = /* @2/_L44= */(kcg_uint8) _L46_stAuth_4;
    tmp[14] = /* @2/_L43= */(kcg_uint8) _L42_stAuth_4;
    tmp[15] = /* @2/_L45= */(kcg_uint8) (_L42_stAuth_4 >> kcg_lit_uint32(8));
    _L51_then_IfBlock1_23 = _L7_then_IfBlock1_23 == kcg_lit_int32(0);
    if (_L134_23) {
      tmp1[0] = kcg_lit_uint32(0);
      tmp1[1] = _L126_23[0];
      tmp1[2] = _L126_23[1];
    }
    else {
      for (idx = 0; idx < 3; idx++) {
        tmp1[idx] = kcg_lit_uint32(0);
      }
    }
    kcg_copy_StreamChunk_slideTypes(
      &_L10_then_IfBlock1_23[0],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    /* IfBlock1:then:_L15=(nacl::box::aeadOpen#4)/ */
    aeadOpen_nacl_box_23_1_20(
      &tmp,
      (array_uint32_16_23 *) &(*datamsg_23)[1],
      _L7_then_IfBlock1_23,
      &_L10_then_IfBlock1_23,
      kcg_lit_int32(0),
      &tmp1,
      &(*s_23).to,
      msg_23,
      &_L16_then_IfBlock1_23);
    *fail_flag_23 = fail_in_23 && _L16_then_IfBlock1_23;
    /* IfBlock1:then:_L47= */
    if (_L16_then_IfBlock1_23) {
      kcg_copy_Session_tindyguardTypes(soo_23, s_23);
      *len_23 = nil_udp;
    }
    else {
      if (_L134_23) {
        /* @3/IfBlock1: */
        if (IfBlock1_clock_accept_nonce_3) {
          kcg_copy_Session_tindyguardTypes(soo_23, s_23);
        }
        else /* @3/IfBlock1:else: */
        if (else_clock_accept_nonce_3_IfBlock1) {
          kcg_copy_Session_tindyguardTypes(soo_23, s_23);
          (*soo_23).to_cnt = new_n_accept_nonce_3;
          /* @3/IfBlock1:else:then:_L4= */
          if (_L5_accept_nonce_3_then_else_IfBlock1 >= kcg_lit_uint64(64)) {
            (*soo_23).to_cnt_cache = kcg_lit_uint64(1);
          }
          else {
            (*soo_23).to_cnt_cache = kcg_lit_uint64(1) | kcg_lsl_uint64(
                (*s_23).to_cnt_cache,
                _L5_accept_nonce_3_then_else_IfBlock1);
          }
        }
        else {
          kcg_copy_Session_tindyguardTypes(soo_23, s_23);
          /* @3/IfBlock1:else:else:_L15= */
          if (_L14_accept_nonce_3_else_else_IfBlock1) {
            (*soo_23).to_cnt_cache = (*s_23).to_cnt_cache;
          }
          else {
            (*soo_23).to_cnt_cache = _L6_accept_nonce_3_else_else_IfBlock1 |
              (*s_23).to_cnt_cache;
          }
        }
      }
      else {
        kcg_copy_Session_tindyguardTypes(soo_23, s_23);
      }
      (*soo_23).rx_cnt = (*s_23).rx_cnt + kcg_lit_int32(1);
      kcg_copy_peer_t_udp(&(*soo_23).peer.endpoint, endpoint_23);
      (*soo_23).gotKeepAlive = _L51_then_IfBlock1_23;
      (*soo_23).sentKeepAlive = !_L51_then_IfBlock1_23;
      (*soo_23).rx_bytes = /* IfBlock1:then:_L64= */(kcg_int64)
          _L7_then_IfBlock1_23 + (*s_23).rx_bytes;
      *len_23 = _L7_then_IfBlock1_23;
    }
  }
  else {
    kcg_copy_Session_tindyguardTypes(soo_23, s_23);
    *fail_flag_23 = fail_in_23;
    *len_23 = kcg_lit_int32(-1);
    for (idx = 0; idx < 23; idx++) {
      kcg_copy_StreamChunk_slideTypes(
        &(*msg_23)[idx],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
  }
}

/*
  Expanded instances for: tindyguard::data::unwrap/
  @1: (M::inc#7)
  @2: (slideTypes::stAuth#4)
  @3: (tindyguard::session::accept_nonce#3)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unwrap_tindyguard_data_23.c
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

