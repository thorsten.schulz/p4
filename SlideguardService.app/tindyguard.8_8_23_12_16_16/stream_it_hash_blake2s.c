/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "stream_it_hash_blake2s.h"

/* hash::blake2s::stream_it/ */
void stream_it_hash_blake2s(
  /* _L9/, i/ */
  size_slideTypes i,
  /* _L3/, hin/ */
  array_uint32_8 *hin,
  /* m/ */
  StreamChunk_slideTypes *m,
  /* _L5/, len/ */
  size_slideTypes len,
  /* _L10/, goOn/ */
  kcg_bool *goOn,
  /* _L2/, hout/ */
  array_uint32_8 *hout)
{
  kcg_int32 tmp;
  /* _L8/ */
  kcg_int32 _L8;

  _L8 = i * hBlockBytes_hash_blake2s + hBlockBytes_hash_blake2s;
  *goOn = len > _L8;
  /* _L14= */
  if (*goOn) {
    tmp = _L8;
  }
  else {
    tmp = len;
  }
  /* _L2=(hash::blake2s::block_refine#1)/ */
  block_refine_hash_blake2s(hin, tmp, m, *goOn, hout);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** stream_it_hash_blake2s.c
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

