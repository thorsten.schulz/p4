/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _hmacChunk_hash_blake2s_1_H_
#define _hmacChunk_hash_blake2s_1_H_

#include "kcg_types.h"
#include "hashChunk_hash_blake2s_2_8.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hmacChunk/ */
extern void hmacChunk_hash_blake2s_1(
  /* _L8/, msg/ */
  array_uint32_16_1 *msg_1,
  /* _L9/, mlen/ */
  int_slideTypes mlen_1,
  /* @1/_L1/, @1/chunk/, @2/_L1/, @2/chunk/, _L32/, key/ */
  HashChunk_hash_blake2s *key_1,
  /* _L20/, chunk/ */
  HashChunk_hash_blake2s *chunk_1);

/*
  Expanded instances for: hash::blake2s::hmacChunk/
  @1: (slideTypes::xorChunkConst#1)
  @2: (slideTypes::xorChunkConst#2)
*/

#endif /* _hmacChunk_hash_blake2s_1_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hmacChunk_hash_blake2s_1.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

