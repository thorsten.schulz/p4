/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "initSending_tindyguard_handshake_8.h"

/* tindyguard::handshake::initSending/ */
void initSending_tindyguard_handshake_8(
  /* s/ */
  Session_tindyguardTypes *s_8,
  /* _L8/, pid/ */
  size_tindyguardTypes pid_8,
  /* _L15/, knownPeers/ */
  _4_array *knownPeers_8,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8,
  /* socket/ */
  fd_t_udp socket_8,
  /* @1/now/, now/ */
  kcg_int64 now_8,
  /* @1/soo/, _L12/, soo/ */
  Session_tindyguardTypes *soo_8,
  /* @1/IfBlock1:, @1/failed/, _L10/, fdfail/ */
  kcg_bool *fdfail_8,
  /* _L6/, again/ */
  kcg_bool *again_8,
  outC_initSending_tindyguard_handshake_8 *outC)
{
  Peer_tindyguardTypes tmp;
  /* @1/IfBlock1:else:then:_L9/ */
  kcg_bool _L9_updateTx_1_then_else_IfBlock1;
  /* @1/IfBlock1:else: */
  kcg_bool else_clock_updateTx_1_IfBlock1;
  /* _L7/ */
  size_tindyguardTypes _L7_8;
  /* @1/length/, _L3/ */
  length_t_udp _L3_8;
  /* _L4/ */
  array_uint32_16_4 _L4_8;
  /* @1/s/, _L17/ */
  Session_tindyguardTypes _L17_8;

  /* _L7= */
  if ((*s_8).pid != InvalidPeer_tindyguardTypes) {
    _L7_8 = (*s_8).pid;
  }
  else {
    _L7_8 = pid_8;
  }
  if (kcg_lit_int32(0) <= _L7_8 && _L7_8 < kcg_lit_int32(8)) {
    kcg_copy_Peer_tindyguardTypes(&tmp, &(*knownPeers_8)[_L7_8]);
  }
  else {
    kcg_copy_Peer_tindyguardTypes(
      &tmp,
      (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  }
  /* _L3=(tindyguard::handshake::init#1)/ */
  init_tindyguard_handshake(
    &tmp,
    _L7_8,
    sks_8,
    now_8,
    &_L3_8,
    &_L4_8,
    &_L17_8,
    again_8,
    &outC->Context_init_1);
  *fdfail_8 = /* _L10=(udp::sendChunksTo#1)/ */
    sendChunks04_udp_specialization(
      _L3_8,
      kcg_lit_int32(0),
      &_L4_8,
      &_L17_8.peer.endpoint,
      socket_8);
  _L17_8.tx_cnt = (*s_8).tx_cnt;
  /* @1/IfBlock1: */
  if (*fdfail_8) {
    kcg_copy_Session_tindyguardTypes(soo_8, &_L17_8);
    (*soo_8).pid = InvalidPeer_tindyguardTypes;
    kcg_copy_Peer_tindyguardTypes(
      &(*soo_8).peer,
      (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
    kcg_copy_State_tindyguard_handshake(
      &(*soo_8).handshake,
      (State_tindyguard_handshake *) &EmptyState_tindyguard_handshake);
  }
  else {
    else_clock_updateTx_1_IfBlock1 = _L3_8 > nil_tindyguard_session;
    /* @1/IfBlock1:else: */
    if (else_clock_updateTx_1_IfBlock1) {
      _L9_updateTx_1_then_else_IfBlock1 = _L3_8 == kcg_lit_int32(32);
      kcg_copy_Session_tindyguardTypes(soo_8, &_L17_8);
      (*soo_8).tx_cnt = _L17_8.tx_cnt + kcg_lit_int32(1);
      (*soo_8).sentKeepAlive = _L9_updateTx_1_then_else_IfBlock1;
      (*soo_8).gotKeepAlive = !_L9_updateTx_1_then_else_IfBlock1;
      (*soo_8).txTime = now_8;
    }
    else {
      kcg_copy_Session_tindyguardTypes(soo_8, &_L17_8);
    }
  }
}

#ifndef KCG_USER_DEFINED_INIT
void initSending_init_tindyguard_handshake_8(
  outC_initSending_tindyguard_handshake_8 *outC)
{
  /* _L3=(tindyguard::handshake::init#1)/ */
  init_init_tindyguard_handshake(&outC->Context_init_1);
}
#endif /* KCG_USER_DEFINED_INIT */


void initSending_reset_tindyguard_handshake_8(
  outC_initSending_tindyguard_handshake_8 *outC)
{
  /* _L3=(tindyguard::handshake::init#1)/ */
  init_reset_tindyguard_handshake(&outC->Context_init_1);
}

/*
  Expanded instances for: tindyguard::handshake::initSending/
  @1: (tindyguard::session::updateTx#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** initSending_tindyguard_handshake_8.c
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

