/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */
#ifndef _updatePeer_tindyguard_session_12_H_
#define _updatePeer_tindyguard_session_12_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::updatePeer/ */
extern void updatePeer_tindyguard_session_12(
  /* _L23/, pid/ */
  size_tindyguardTypes pid_12,
  /* _L21/, knownPeer/ */
  Peer_tindyguardTypes *knownPeer_12,
  /* _L18/, isNewTAI/ */
  array_bool_12 *isNewTAI_12,
  /* _L19/, s/ */
  _3_array *s_12,
  /* _L25/, t_flag/ */
  array_bool_12 *t_flag_12,
  /* _L22/, updatedPeer/ */
  Peer_tindyguardTypes *updatedPeer_12);



#endif /* _updatePeer_tindyguard_session_12_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** updatePeer_tindyguard_session_12.h
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

