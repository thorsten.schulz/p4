/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _filter1_tindyguard_data_23_H_
#define _filter1_tindyguard_data_23_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::data::filter1/ */
extern void filter1_tindyguard_data_23(
  /* _L19/, i/ */
  size_tindyguardTypes i_23,
  /* _L21/, acc/ */
  size_tindyguardTypes acc_23,
  /* _L1/, len/ */
  size_tindyguardTypes len_23,
  /* _L2/, msg/ */
  array_uint32_16_23 *msg_23,
  /* _L3/, protocol/ */
  kcg_uint8 protocol_23,
  /* _L4/, goOn/ */
  kcg_bool *goOn_23,
  /* _L20/, ioo/ */
  size_tindyguardTypes *ioo_23,
  /* _L18/, matching/ */
  kcg_bool *matching_23);



#endif /* _filter1_tindyguard_data_23_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** filter1_tindyguard_data_23.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

