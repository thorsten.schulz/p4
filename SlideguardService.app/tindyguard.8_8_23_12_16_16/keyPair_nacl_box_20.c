/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "keyPair_nacl_box_20.h"

/* nacl::box::keyPair/ */
void keyPair_nacl_box_20(
  /* _L2/, randomness/ */
  array_uint32_8 *randomness_20,
  /* _L6/, again/ */
  kcg_bool *again_20,
  /* _L11/, key/ */
  KeyPair32_slideTypes *key_20,
  /* _L14/, failed/ */
  kcg_bool *failed_20,
  outC_keyPair_nacl_box_20 *outC)
{
  /* _L1/ */
  array_uint32_8 _L1_20;
  /* _L16/ */
  KeyPair32_slideTypes _L16_20;
  kcg_size idx;

  kcg_copy_Key32_slideTypes(&_L16_20.sk_x, randomness_20);
  for (idx = 0; idx < 8; idx++) {
    _L16_20.pk_y[idx] = kcg_lit_uint32(0);
  }
  /* _L1=(nacl::box::scalarmultBase#1)/ */
  scalarmultBase_nacl_box_20(
    &_L16_20,
    &_L1_20,
    again_20,
    failed_20,
    &outC->Context_scalarmultBase_1);
  /* _L11= */
  if (*failed_20) {
    kcg_copy_KeyPair32_slideTypes(
      key_20,
      (KeyPair32_slideTypes *) &ZeroKeyPair_slideTypes);
  }
  else {
    kcg_copy_KeyPair32_slideTypes(key_20, &_L16_20);
    kcg_copy_Key32_slideTypes(&(*key_20).pk_y, &_L1_20);
  }
}

#ifndef KCG_USER_DEFINED_INIT
void keyPair_init_nacl_box_20(outC_keyPair_nacl_box_20 *outC)
{
  /* _L1=(nacl::box::scalarmultBase#1)/ */
  scalarmultBase_init_nacl_box_20(&outC->Context_scalarmultBase_1);
}
#endif /* KCG_USER_DEFINED_INIT */


void keyPair_reset_nacl_box_20(outC_keyPair_nacl_box_20 *outC)
{
  /* _L1=(nacl::box::scalarmultBase#1)/ */
  scalarmultBase_reset_nacl_box_20(&outC->Context_scalarmultBase_1);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** keyPair_nacl_box_20.c
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

