/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "echoTake_icmp_23.h"

/* icmp::echoTake/ */
void echoTake_icmp_23(
  /* _L1/, len/ */
  length_t_udp len_23,
  /* _L2/, msg/ */
  array_uint32_16_23 *msg_23,
  /* _L50/, host/ */
  IpAddress_udp *host_23,
  /* lenoo/ */
  length_t_udp *lenoo_23,
  /* msgoo/ */
  array_uint32_16_23 *msgoo_23,
  /* seqoo/ */
  kcg_uint32 *seqoo_23,
  /* _L33/, src/ */
  IpAddress_udp *src_23,
  /* id/ */
  kcg_uint32 *id_23,
  /* _L60/, fail1/, failed/ */
  kcg_bool *failed_23,
  /* badChecksum/ */
  kcg_bool *badChecksum_23,
  /* badPayload/ */
  kcg_bool *badPayload_23)
{
  range_t_udp tmp_str;
  /* IfBlock1:else: */
  kcg_bool else_clock_IfBlock1_23;
  /* IfBlock1:else:then:_L22/ */
  kcg_uint32 _L22_then_else_IfBlock1_23;
  /* IfBlock1:else:then:_L32/, IfBlock2:then:_L3/ */
  kcg_uint32 _L32_then_else_IfBlock1_23;
  /* IfBlock1:else:then:_L36/, IfBlock2:then:_L9/ */
  kcg_uint32 _L36_then_else_IfBlock1_23;
  /* IfBlock2:, _L34/ */
  kcg_bool IfBlock2_clock_23;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_23;
  /* _L32/, pll/ */
  length_t_udp _L32_23;

  kcg_copy_IpAddress_udp(&tmp_str.net, host_23);
  kcg_copy_IpAddress_udp(&tmp_str.mask, (IpAddress_udp *) &ipBroadcast_udp);
  /* _L32=(ip::check#1)/ */
  check_ip_23(
    len_23,
    msg_23,
    &tmp_str,
    (header_ip *) &HEADER_icmp_ip,
    kcg_false,
    &_L32_23,
    src_23,
    &IfBlock2_clock_23);
  *failed_23 = ((*msg_23)[0][5] & kcg_lit_uint32(65280)) != kcg_lit_uint32(0) ||
    IfBlock2_clock_23;
  IfBlock2_clock_23 = !*failed_23;
  /* IfBlock2: */
  if (IfBlock2_clock_23) {
    _L36_then_else_IfBlock1_23 = /* IfBlock2:then:_L9=(sys::hton#1)/ */
      htonl_sys_specialization((*msg_23)[0][6]);
    _L32_then_else_IfBlock1_23 = /* IfBlock2:then:_L3=(ip::ipsum_BEH#5)/ */
      ipsum_BEH_ip_23(
        msg_23,
        HEADER_icmp_ip.tot_len + _L32_23 - headerBytes_ip,
        headerBytes_ip);
    *seqoo_23 = kcg_lit_uint32(65535) & _L36_then_else_IfBlock1_23;
    *id_23 = _L36_then_else_IfBlock1_23 >> kcg_lit_uint32(16);
    *badChecksum_23 = _L32_then_else_IfBlock1_23 != kcg_lit_uint32(4294901760);
  }
  else {
    *badChecksum_23 = kcg_false;
    *seqoo_23 = kcg_lit_uint32(0);
    *id_23 = kcg_lit_uint32(0);
  }
  IfBlock1_clock_23 = IfBlock2_clock_23 && /* _L55= */(kcg_uint8)
      (*msg_23)[0][5] == ECHOREPLY_icmp;
  /* IfBlock1: */
  if (IfBlock1_clock_23) {
    *lenoo_23 = nil_udp;
    kcg_copy_array_uint32_16_23(msgoo_23, msg_23);
    *badPayload_23 = _L32_23 != payloadlen_icmp || !kcg_comp_array_uint32_5(
        (array_uint32_5 *) &(*msg_23)[0][11],
        (array_uint32_5 *) &payload_icmp);
  }
  else {
    *badPayload_23 = kcg_false;
    else_clock_IfBlock1_23 = IfBlock2_clock_23 && /* _L55= */(kcg_uint8)
        (*msg_23)[0][5] == ECHO_icmp;
    /* IfBlock1:else: */
    if (else_clock_IfBlock1_23) {
      _L32_then_else_IfBlock1_23 = ((*msg_23)[0][2] & kcg_lit_uint32(0xFFFF)) -
        kcg_lit_uint32(1);
      _L22_then_else_IfBlock1_23 =
        /* IfBlock1:else:then:_L22=(ip::ipsum_BEH#3)/ */
        ipsum_BEH_ip_23(
          msg_23,
          kcg_lit_int32(24) + _L32_23 - kcg_lit_int32(4),
          kcg_lit_int32(24));
      kcg_copy_array_uint32_16_23(msgoo_23, msg_23);
      (*msgoo_23)[0][5] = _L22_then_else_IfBlock1_23;
      (*msgoo_23)[0][4] = (*msg_23)[0][3];
      (*msgoo_23)[0][3] = (*msg_23)[0][4];
      (*msgoo_23)[0][2] = _L32_then_else_IfBlock1_23;
      _L36_then_else_IfBlock1_23 =
        /* IfBlock1:else:then:_L36=(ip::ipsum_BEH#4)/ */
        ipsum_BEH_ip_23(msgoo_23, headerBytes_ip, kcg_lit_int32(0));
      *lenoo_23 = len_23;
      (*msgoo_23)[0][2] = _L36_then_else_IfBlock1_23 | _L32_then_else_IfBlock1_23;
    }
    else {
      *lenoo_23 = nil_udp;
      kcg_copy_array_uint32_16_23(msgoo_23, msg_23);
    }
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** echoTake_icmp_23.c
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

