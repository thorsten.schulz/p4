/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */
#ifndef _mapReceived_tindyguard_session_16_12_H_
#define _mapReceived_tindyguard_session_16_12_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::mapReceived/ */
extern void mapReceived_tindyguard_session_16_12(
  /* _L12/, sid/ */
  size_tindyguardTypes sid_16_12,
  /* _L1/, length/ */
  array_int32_16 *length_16_12,
  /* _L2/, our/ */
  array_uint32_16 *our_16_12,
  /* _L11/, s/ */
  Session_tindyguardTypes *s_16_12,
  /* _L6/, residual_length_or_sid/ */
  array_int32_16 *residual_length_or_sid_16_12,
  /* _L10/, length_for_session/ */
  array_int32_16 *length_for_session_16_12,
  /* _L8/, for_init_response/ */
  kcg_bool *for_init_response_16_12);



#endif /* _mapReceived_tindyguard_session_16_12_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapReceived_tindyguard_session_16_12.h
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

