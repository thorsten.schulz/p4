/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapReceived_tindyguard_session_16_12.h"

/* tindyguard::session::mapReceived/ */
void mapReceived_tindyguard_session_16_12(
  /* _L12/, sid/ */
  size_tindyguardTypes sid_16_12,
  /* _L1/, length/ */
  array_int32_16 *length_16_12,
  /* _L2/, our/ */
  array_uint32_16 *our_16_12,
  /* _L11/, s/ */
  Session_tindyguardTypes *s_16_12,
  /* _L6/, residual_length_or_sid/ */
  array_int32_16 *residual_length_or_sid_16_12,
  /* _L10/, length_for_session/ */
  array_int32_16 *length_for_session_16_12,
  /* _L8/, for_init_response/ */
  kcg_bool *for_init_response_16_12)
{
  kcg_bool acc;
  kcg_size idx;
  /* @1/IfBlock1:then:_L24/ */
  kcg_bool _L24_mapRxd_it_1_then_IfBlock1_12;
  /* @1/IfBlock1:then:_L19/,
     @1/IfBlock1:then:_L22/,
     @1/IfBlock1:then:take_packet/ */
  kcg_bool _L22_mapRxd_it_1_then_IfBlock1_12;
  /* @1/IfBlock1:then:_L1/ */
  kcg_bool _L1_mapRxd_it_1_then_IfBlock1_12;
  /* @1/IfBlock1:then:_L5/ */
  kcg_bool _L5_mapRxd_it_1_then_IfBlock1_12;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_mapRxd_it_1_12;

  *for_init_response_16_12 = kcg_false;
  /* _L8= */
  for (idx = 0; idx < 16; idx++) {
    acc = *for_init_response_16_12;
    IfBlock1_clock_mapRxd_it_1_12 = (*length_16_12)[idx] >= kcg_lit_int32(12);
    /* @1/IfBlock1: */
    if (IfBlock1_clock_mapRxd_it_1_12) {
      _L24_mapRxd_it_1_then_IfBlock1_12 = (*length_16_12)[idx] == kcg_lit_int32(148);
      _L1_mapRxd_it_1_then_IfBlock1_12 = (*s_16_12).pid ==
        InvalidPeer_tindyguardTypes && _L24_mapRxd_it_1_then_IfBlock1_12 &&
        !acc;
      _L5_mapRxd_it_1_then_IfBlock1_12 = (*our_16_12)[idx] == (*s_16_12).our;
      _L22_mapRxd_it_1_then_IfBlock1_12 = _L5_mapRxd_it_1_then_IfBlock1_12 ||
        _L1_mapRxd_it_1_then_IfBlock1_12;
      *for_init_response_16_12 = _L1_mapRxd_it_1_then_IfBlock1_12 || acc;
      /* @1/IfBlock1:then:_L8= */
      if (_L22_mapRxd_it_1_then_IfBlock1_12) {
        (*residual_length_or_sid_16_12)[idx] = sid_16_12;
        (*length_for_session_16_12)[idx] = (*length_16_12)[idx];
      }
      else {
        /* @1/IfBlock1:then:_L30= */
        if (!(_L5_mapRxd_it_1_then_IfBlock1_12 ||
            _L24_mapRxd_it_1_then_IfBlock1_12) && sid_16_12 == kcg_lit_int32(
            11)) {
          (*residual_length_or_sid_16_12)[idx] = nil_udp;
        }
        else {
          (*residual_length_or_sid_16_12)[idx] = (*length_16_12)[idx];
        }
        (*length_for_session_16_12)[idx] = nil_udp;
      }
    }
    else {
      *for_init_response_16_12 = acc;
      (*residual_length_or_sid_16_12)[idx] = (*length_16_12)[idx];
      (*length_for_session_16_12)[idx] = nil_udp;
    }
  }
}

/*
  Expanded instances for: tindyguard::session::mapReceived/
  @1: (tindyguard::session::mapRxd_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapReceived_tindyguard_session_16_12.c
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

