/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _eval_init_tindyguard_handshake_8_H_
#define _eval_init_tindyguard_handshake_8_H_

#include "kcg_types.h"
#include "hash_hash_blake2s_2.h"
#include "TAI_le_tindyguardTypes.h"
#include "TAI_validate_tindyguardTypes.h"
#include "aeadOpen_nacl_box_1_1_20.h"
#include "hkdf_hash_blake2s_1_2.h"
#include "scalarmultDonna_nacl_box.h"
#include "single_hash_blake2s.h"
#include "hkdf_cKonly_hash_blake2s_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::handshake::eval_init/ */
extern void eval_init_tindyguard_handshake_8(
  /* initmsg/ */
  array_uint32_16_4 *initmsg_8,
  /* rlength/ */
  length_t_udp rlength_8,
  /* endpoint/ */
  peer_t_udp *endpoint_8,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8,
  /* knownPeer/ */
  _4_array *knownPeer_8,
  /* session/ */
  Session_tindyguardTypes *session_8,
  /* soo/ */
  Session_tindyguardTypes *soo_8,
  /* failed/ */
  kcg_bool *failed_8,
  /* noPeer/ */
  kcg_bool *noPeer_8);



#endif /* _eval_init_tindyguard_handshake_8_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** eval_init_tindyguard_handshake_8.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

