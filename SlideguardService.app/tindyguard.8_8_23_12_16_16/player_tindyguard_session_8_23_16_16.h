/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _player_tindyguard_session_8_23_16_16_H_
#define _player_tindyguard_session_8_23_16_16_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "wrap_tindyguard_data_23.h"
#include "unwrap_tindyguard_data_23.h"
#include "eval_response_tindyguard_handshake_23.h"
#include "initSending_tindyguard_handshake_8.h"
#include "respond_tindyguard_handshake_8.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  kcg_bool init;
  kcg_bool init1;
  kcg_bool init2;
  kcg_bool init3;
  kcg_size /* @1/_/v3/ */ v3_times_1_size;
  kcg_size /* @2/_/v3/ */ v3_times_7_size;
  kcg_size /* @3/_/v3/ */ v3_times_2_size;
  kcg_size /* @4/_/v3/ */ v3_times_3_size;
  kcg_size /* @5/_/v3/ */ v3_times_4_size;
  kcg_bool /* fdfail/ */ fdfail_8_23_16_16;
  kcg_bool /* fail/ */ fail_8_23_16_16;
  _6_SSM_ST_SM1 /* SM1: */ SM1_state_nxt_8_23_16_16;
  SSM_ST_SM2 /* SM2: */ SM2_state_nxt_8_23_16_16;
  Session_tindyguardTypes /* s/ */ s_8_23_16_16;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_respond_tindyguard_handshake_8 /* @6/_L4=(tindyguard::handshake::respond#1)/ */ Context_respond_1_respondSending_1[16];
  outC_initSending_tindyguard_handshake_8 /* SM1:initiate:_L25=(tindyguard::handshake::initSending#1)/ */ Context_initSending_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_player_tindyguard_session_8_23_16_16;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::session::player/ */
extern void player_tindyguard_session_8_23_16_16(
  /* _L19/, fd_bad/ */
  kcg_bool fd_bad_8_23_16_16,
  /* rxdlength/ */
  array_int32_16 *rxdlength_8_23_16_16,
  /* rxdbuffer/ */
  array_uint32_16_24_16 *rxdbuffer_8_23_16_16,
  /* rxdendpoint/ */
  _5_array *rxdendpoint_8_23_16_16,
  /* pid/ */
  size_tindyguardTypes pid_8_23_16_16,
  /* txm/ */
  array_uint32_16_23_16 *txm_8_23_16_16,
  /* _L10/, txmlen/ */
  array_int32_16 *txmlen_8_23_16_16,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8_23_16_16,
  /* knownPeers/ */
  _4_array *knownPeers_8_23_16_16,
  /* socket/ */
  fd_t_udp socket_8_23_16_16,
  /* now/ */
  kcg_int64 now_8_23_16_16,
  /* _L1/, fd_failing/ */
  kcg_bool *fd_failing_8_23_16_16,
  /* rxmlength/ */
  array_int32_16 *rxmlength_8_23_16_16,
  /* rxmbuffer/ */
  array_uint32_16_23_16 *rxmbuffer_8_23_16_16,
  /* taken/ */
  array_bool_16 *taken_8_23_16_16,
  /* _L15/, soo/ */
  Session_tindyguardTypes *soo_8_23_16_16,
  /* _L5/, weakKey/, weakKeyFail/ */
  kcg_bool *weakKeyFail_8_23_16_16,
  /* latchTAI/ */
  kcg_bool *latchTAI_8_23_16_16,
  /* _L14/, timedOut/, timeout/ */
  kcg_bool *timeout_8_23_16_16,
  /* _L27/, reInitPid/ */
  size_tindyguardTypes *reInitPid_8_23_16_16,
  outC_player_tindyguard_session_8_23_16_16 *outC);

extern void player_reset_tindyguard_session_8_23_16_16(
  outC_player_tindyguard_session_8_23_16_16 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void player_init_tindyguard_session_8_23_16_16(
  outC_player_tindyguard_session_8_23_16_16 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::session::player/
  @1: (times#1)
  @2: (times#7)
  @3: (times#2)
  @4: (times#3)
  @5: (times#4)
  @6: (tindyguard::handshake::respondSending#1)
*/

#endif /* _player_tindyguard_session_8_23_16_16_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** player_tindyguard_session_8_23_16_16.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

