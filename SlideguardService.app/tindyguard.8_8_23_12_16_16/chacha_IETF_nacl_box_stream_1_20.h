/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _chacha_IETF_nacl_box_stream_1_20_H_
#define _chacha_IETF_nacl_box_stream_1_20_H_

#include "kcg_types.h"
#include "Xor_nacl_core_chacha_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::box::stream::chacha_IETF/ */
extern void chacha_IETF_nacl_box_stream_1_20(
  /* _L20/, msg/ */
  array_uint32_16_1 *msg_1_20,
  /* _L2/, _L35/, mlen/ */
  int_slideTypes mlen_1_20,
  /* _L1/, n/ */
  State_nacl_core_chacha *n_1_20,
  /* _L31/, cm/ */
  array_uint32_16_1 *cm_1_20);



#endif /* _chacha_IETF_nacl_box_stream_1_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** chacha_IETF_nacl_box_stream_1_20.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

