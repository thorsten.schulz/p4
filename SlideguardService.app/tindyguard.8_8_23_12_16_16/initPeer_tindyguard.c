/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "initPeer_tindyguard.h"

/* tindyguard::initPeer/ */
void initPeer_tindyguard(
  /* @1/_L24/, @1/pub/, _L14/, _L26/, pub/ */
  pub_tindyguardTypes *pub,
  /* _L15/, preshared/ */
  secret_tindyguardTypes *preshared,
  /* _L16/, endpoint/ */
  peer_t_udp *endpoint,
  /* _L17/, allowed/ */
  range_t_udp *allowed,
  /* _L12/, p/ */
  Peer_tindyguardTypes *p)
{
  array_uint32_16 tmp;
  /* @1/_L19/ */
  array_uint32_6 _L19_initHashes_2;
  kcg_size idx;

  kcg_copy_pub_tindyguardTypes(&(*p).tpub, pub);
  kcg_copy_psk_tindyguardTypes(&(*p).preshared, preshared);
  kcg_copy_range_t_udp(&(*p).allowed, allowed);
  kcg_copy_peer_t_udp(&(*p).endpoint, endpoint);
  (*p).timedOut = kcg_false;
  (*p).bestSession = nil_tindyguard_session;
  (*p).inhibitInit = kcg_false;
  kcg_copy_CookieJar_tindyguardTypes(
    &(*p).cookie,
    (CookieJar_tindyguardTypes *) &EmptyJar_tindyguardTypes);
  for (idx = 0; idx < 6; idx++) {
    _L19_initHashes_2[idx] = kcg_lit_uint32(0);
  }
  kcg_copy_array_uint32_2(&tmp[0], (array_uint32_2 *) &MAC1_label_tindyguard);
  kcg_copy_pub_tindyguardTypes(&tmp[2], pub);
  kcg_copy_array_uint32_6(&tmp[10], &_L19_initHashes_2);
  /* @1/_L18=(hash::blake2s::singleChunk#2)/ */
  singleChunk_hash_blake2s(&tmp, kcg_lit_int32(40), &(*p).hcache.salt);
  kcg_copy_array_uint32_8(
    &tmp[0],
    (array_uint32_8 *) &CONSTRUCTION_IDENTIFIER_hash_tindyguard);
  kcg_copy_pub_tindyguardTypes(&tmp[8], pub);
  /* @1/_L12=(hash::blake2s::single#2)/ */
  single_hash_blake2s(&tmp, kcg_lit_int32(64), &(*p).hcache.hash1);
  kcg_copy_array_uint32_2(&tmp[0], (array_uint32_2 *) &COOKIE_label_tindyguard);
  kcg_copy_pub_tindyguardTypes(&tmp[2], pub);
  kcg_copy_array_uint32_6(&tmp[10], &_L19_initHashes_2);
  /* @1/_L17=(hash::blake2s::single#3)/ */
  single_hash_blake2s(&tmp, kcg_lit_int32(40), &(*p).hcache.cookieHKey);
  for (idx = 0; idx < 3; idx++) {
    (*p).tai[idx] = kcg_lit_uint32(0);
  }
}

/*
  Expanded instances for: tindyguard::initPeer/
  @1: (tindyguard::initHashes#2)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** initPeer_tindyguard.c
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

