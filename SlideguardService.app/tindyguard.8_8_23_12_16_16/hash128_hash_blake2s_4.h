/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _hash128_hash_blake2s_4_H_
#define _hash128_hash_blake2s_4_H_

#include "kcg_types.h"
#include "stream_it_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hash128/ */
extern void hash128_hash_blake2s_4(
  /* _L64/, msg/ */
  array_uint32_16_4 *msg_4,
  /* _L66/, len/ */
  size_slideTypes len_4,
  /* _L70/, keybytes/ */
  size_slideTypes keybytes_4,
  /* _L75/, mac/ */
  Mac_hash_blake2s *mac_4);



#endif /* _hash128_hash_blake2s_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hash128_hash_blake2s_4.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

