/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _echoRequest_icmp_23_H_
#define _echoRequest_icmp_23_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "ipsum_BEH_ip_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  kcg_uint32 /* seq/ */ seq_23;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_echoRequest_icmp_23;

/* ===========  node initialization and cycle functions  =========== */
/* icmp::echoRequest/ */
extern void echoRequest_icmp_23(
  /* @1/dst/, _L32/, dst/ */
  IpAddress_udp *dst_23,
  /* @1/src/, _L33/, src/ */
  IpAddress_udp *src_23,
  /* _L35/, id/ */
  kcg_uint32 id_23,
  /* _L81/, len/ */
  length_t_udp *len_23,
  /* _L71/, msg/ */
  array_uint32_16_23 *msg_23,
  /* _L26/, seqoo/ */
  kcg_uint32 *seqoo_23,
  outC_echoRequest_icmp_23 *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void echoRequest_reset_icmp_23(outC_echoRequest_icmp_23 *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void echoRequest_init_icmp_23(outC_echoRequest_icmp_23 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: icmp::echoRequest/
  @1: (ip::hdr#1)
*/

#endif /* _echoRequest_icmp_23_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** echoRequest_icmp_23.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

