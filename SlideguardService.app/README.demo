SPDX-License-Identifier: EUPL-1.2

Project Name:
Tindyguard / Slideguard: A Wireguard encryption filter for PikeOS/ANIS

Member Project(s):
  inetd-internal.app(*)
  random.dev
  rtc.dev
  rfusion.kernel
  nanoStw_crypto-trial.int
  
  (*) inetd-internal is a virgin demo app, thus not included. Go File->New->PikeOS, create a new POSIX-Project,
      then choose Templates: Demo-projects->inetd-internal. Create, build and install it, before wrapping the
      integration project.
  

Description:
Provides a two-way filter to push eth-packets from lwIP / or nay other app into a Wireguard pipe and vice-versa.
Currently, the key-details must be provided using an *original wg-style* ini-file. Most notable difference, do not 
start valid lines in that ini with white-space, DNS / hostnames are not supported at all in the implementation.
AllowedIPs only supports a single IP with netmask. IPv6 is not implemented at all.
ANIS cannot list its interfaces, so binding to a certain interface in slideguard does not work at run-time. For Ping to
work, you may need to adjust the host-ip-address in the source.

Note on the name:
The original encryption trial started out as SLIDE. The WireGuard filter started out as Tindyguard as a new project.
However, it tends to merge names with its library unexpectedly like a shape-shifter, be afraid, be very afraid. 
Nevertheless, both names apply and stand for the same thing.

Howto run:
 1. install stock WireGuard driver and utilities in your host Linux - no need for further setup.
 1a. once, add the line "allow br-up" to /etc/qemu/bridge  
  
 2. setup network:
#tip: type 'sudo ls' first to unlock sudo, then copy the whole block)
sudo ip link del br-up
sudo ip link del ve1-up
#to force br-up to be up
sudo ip link add ve1-up type veth peer name ve2-up
sudo ip addr add 172.22.0.10/32 dev ve2-up
sudo ip link set ve1-up up
sudo ip link set ve2-up up
sudo ip link add br-up type bridge
sudo ip addr add 172.22.0.1/24 dev br-up
sudo ip link set ve1-up master br-up
sudo ip link set br-up up

sudo ip link del wg-up
sudo ip link add wg-up type wireguard
sudo wg setconf wg-up ../SlideguardService.app/wg+gg.conf
sudo ip addr add 172.21.0.10/24 dev wg-up
sudo ip link set wg-up up
 
 3. run QEMU
sudo qemu-system-x86_64 -kernel ../nanoStw_crypto-trial.int/boot/anis-pikeos-qemu-x86-64-qemu -nographic -serial mon:stdio -enable-kvm -netdev bridge,id=eth0,br=br-up -device e1000,netdev=eth0 -m 256

 4. open another terminal and run
#this will ping ANIS. Pinging ANSI first will help the virtual bridge to get acquainted with the new situation.
ping 172.22.0.102

#this will ping Tindyguard
ping 172.21.0.102

#then use demo's services, e.g.
telnet 172.21.0.102 daytime
#use ctrl+']' to close connection and ctrl+d to exit telnet ;)
telnet 172.21.0.102 echo
