/*
 * SCADE simple UDP library, a work in progress library
 *
 * Used w/ Windows-API, Linux and lwip/TI-Hercules
 *
 * (c) Thorsten Schulz, University of Rostock, thorsten.schulz@uni-rostock.de, 2017-May-23
 * Available under the terms of the EUPL.
 */

/* SCADE sets this for simulation compilation,
 * otherwise, **to use SCADE-generated code**, define this macro
 * on gcc's command-line, Makefile or Eclipse's settings.
 */
/* to avoid inclusion of the default empty user_macros.h, force-include it here */
#include "../user_macros.h"

#include "kcg_types.h"
#include "kcg_consts.h"
#include "kcg_imported_functions.h"
#include "kcg_imported_types.h"

#ifndef invalidSocket_udp
#define invalidSocket_udp (-1)
#endif
typedef kcg_int32 fd_t_udp;
typedef kcg_int32 length_t_udp;

#ifndef __USE_MISC
#define __USE_MISC
#endif

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#ifndef PIKEOS_NATIVE
#include <stdio.h>
#include <errno.h>
#include <time.h>
#else
#include <vm.h>                   /* PSSW API */
#include <stand/string.h>         /* memset() */
#include <time_devrtc.h>
#include <net/util/inet.h>
#endif

#define MAX_PACKS 16           /* for the lwip-hercules-variant, select the depth of the incoming queue. */
#ifdef kcg_use_IpAddress_udp
static const int optvalML = 0; /* select here whether MCAST loopback is needed. */
#endif

#if (defined(__WIN32__) || defined(WIN32) || defined(__CYGWIN32__))
#if !defined(WINDOWS)
#define WINDOWS
#endif
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__APPLE__)
#ifndef LINUX
#define LINUX
#endif
#elif PIKEOS_NATIVE
/* no adjusts */
#else /* _RM57Lx_ */
#define LWIP
#endif

#ifdef PIKEOS_NATIVE
#define dprint(...) vm_cprintf("net:: "__VA_ARGS__)
#define iprint(...) vm_cprintf("net:: "__VA_ARGS__)
#elif defined( _RM57Lx_ )
#define dprint(...) { char s[256]; snprintf(s, sizeof(s), __VA_ARGS__); sciDisplayText(s); }
#define iprint(...) { char s[256]; snprintf(s, sizeof(s), __VA_ARGS__); sciDisplayText(s); }
#else
#define dprint(...)	fprintf(stderr, "net:: "__VA_ARGS__), fflush(stderr)
#define iprint(...)	fprintf(stdout, "net:: "__VA_ARGS__), fflush(stdout)
#endif

/* this is for windows */
# ifndef __socklen_t_defined
typedef int socklen_t;
#  define __socklen_t_defined
# endif

#ifndef __unused
#define __unused __attribute__((unused))
#endif

/* from sys.c */
void now_sys( /* millis */ time_t_sys *millis, /* nanos */ kcg_int64 *nanos);
void nowfd(fd_t_udp fd, /* millis */ time_t_sys *millis, /* nanos */ kcg_int64 *nanos);

#ifdef PIKEOS_NATIVE
int errno;
#endif

#ifdef LWIP
#include "utils/lwiplib.h"
#include "lwip/inet.h"
#include "lwip/igmp.h"
#include "lwip/sys.h"

#ifndef _SSIZE_T
#define _SSIZE_T
typedef int ssize_t;
#endif

typedef struct pitem {
	struct pbuf *p;
	u16_t pport;
	IpAddress_udp paddr;
} pitem;

typedef struct plist {
	pitem pi[MAX_PACKS];
	int wr;
	int rd;
	SYS_ARCH_DECL_PROTECT(lvl);
} plist;

plist pl;

void capture(void *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, u16_t port) {

	struct pbuf *plast = 0;
    SYS_ARCH_PROTECT(pl.lvl);
    	int next = pl.wr+1;
    	next %= MAX_PACKS;

    	plast = pl.pi[pl.wr].p; /* old buffers will be overwritten */
		pl.pi[pl.wr].p = p;
		pl.pi[pl.wr].pport = port;
		pl.pi[pl.wr].paddr.addr = addr->addr;
		pl.wr = next;

    SYS_ARCH_UNPROTECT(pl.lvl);
	if (plast) pbuf_free(plast);
}

#else

#if PIKEOS_NATIVE

#include <vm.h>                   /* PSSW API */
#include <stand/string.h>         /* memset() */
#include <p4ext/p4ext_threads.h>  /* Extended thread API for anis_init */
#include <instr.h>                /* ANIS Instrumentation API. */
#include <socket.h>               /* ANIS socket API. */

#define SOCK_NONBLOCK 0
#define FPNAME  "anisfp"          /* this is the default/demo name of the ANIS file provider. But it is configurable. */
static int anis_initialized;     /* = false; singleton for anis_init */

#else /* ! PIKEOS_NATIVE */

#include <sys/time.h>
#include <unistd.h>

#ifdef WINDOWS
#undef  _WIN32_WINNT
#define _WIN32_WINNT 0x0600

#include <Winsock2.h>
#include <Ws2tcpip.h>
#include <winioctl.h>

#define SOCK_NONBLOCK 0
#define MSG_DONTWAIT  0           /* works different on Windows via ioctlsocket */

static int socket_count = 0; /* to decide when to tidy up with wsa-cleanup */
#else /* !WINDOWS */
#include <arpa/inet.h>
#include <sys/socket.h>
#include <asm/sockios.h>

#include <netdb.h>
#include <ifaddrs.h>
#include <linux/if_link.h>
#include <net/if.h>

#endif /* WINDOWS */
#endif /* PIKEOS_NATIVE */
#endif /* LWIP */

#define MAX(a,b) (((a)>(b))?(a):(b))
#define MIN(a,b) (((a)>(b))?(b):(a))

kcg_bool close_udp(/* udp::close::fd */ fd_t_udp fd) {
	if (fd == invalidSocket_udp) return kcg_true;
#ifdef LWIP
	if (fd != 0) {
		udp_remove((struct udp_pcb *)fd);

		SYS_ARCH_PROTECT(pl.lvl);
		while (pl.wr != pl.rd) {
			struct pbuf *p = pl.pi[pl.rd].p;
			pl.pi[pl.rd].p = 0;
			pl.rd++;
			pl.rd %= MAX_PACKS;
			pbuf_free(p);
		}
		SYS_ARCH_UNPROTECT(pl.lvl);
	}
#else
#ifdef WINDOWS
	int rc = closesocket(fd);
	socket_count--;
	if (!socket_count) WSACleanup();
	if ( rc )
		dprint("[FAILED (%d)] close_udp(fd=%d) \n", WSAGetLastError(), fd);
#elif PIKEOS_NATIVE
	P4_e_t rc;
	rc = socket_close(fd);
	if ( rc )
		dprint("[FAILED (%d)] close_udp(fd=%d) \n", rc, fd);
#else
	errno = 0;
	if ( close(fd) )
		dprint("[FAILED (%d)] close_udp(fd=%d) = %s\n", errno, fd, strerror(errno));
#endif
#ifdef DEBUG
	else
		dprint("[OK] close_udp(%d).\n", fd);
#endif
#endif

	return kcg_true;
}

#ifdef kcg_use_IpAddress_udp
void bind_udp(IpAddress_udp *ipAddress, port_t_udp udpPort, kcg_bool *fail, fd_t_udp *fd) {

	*fail = kcg_true;
#ifdef LWIP
	struct udp_pcb *up = udp_new();
	if (up) {
		udp_recv(up, capture, NULL);
		udp_bind(up, (ip_addr_t*)&ipAddress->addr, udpPort);
		*fail = kcg_false;
		*fd = (fd_t_udp)up;

		if (optvalML) {
			udp_setflags(up, udp_flags(up) | UDP_FLAGS_MULTICAST_LOOP);
		} else {
			udp_setflags(up, udp_flags(up) & ~UDP_FLAGS_MULTICAST_LOOP);
		}
	} else {
		*fail = kcg_true;
		*fd = (fd_t_udp)invalidSocket_udp;
	}
#else
#ifdef WINDOWS
	// Initialize Winsock
	WSADATA wsaData;
	if (!socket_count) {
		int iResult = WSAStartup(WINSOCK_VERSION, &wsaData);
		if (iResult != 0) return;
	}
#elif defined (PIKEOS_NATIVE)
	P4_e_t p4e;
	/* on PikeOS you can bind to port 0, but this conflicts with the usual Any-Free-Port handling on other OS */
	/* So, to avoid confusion, I will fail here, because I cannot find out free port reliably. */
	if (!udpPort) {
		dprint("[FAILED] Bind param-check: Port=0 does *not* mean \"any free port\" on PikeOS. Please select a fixed predefined port!\n");
		return;
	}
	if (!anis_initialized) {
		P4_thr_t thrno = -1;
		p4e = p4ext_thr_num_alloc(&thrno);
		if (p4e != P4_E_OK) {
			dprint("[FAILED (%d)] to allocate thread number for ANIS: %s\n", p4e, p4_strerror(p4e));
			return;
		}

		int ret = anis_init(FPNAME, thrno);
		if (ret != ANIS_E_OK) {
			dprint("[FAILED (%d)] ANIS init error.\n", ret);
			return;
		}
		anis_initialized = 1;
	}
#endif
	*fd = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0);
	if (*fd < 0) {
		dprint("[FAILED (%d)] to create a socket.\n", *fd);
		return;
	}
	int e;
#ifdef WINDOWS
#ifdef DEBUG
	/* in most occasions on Windows, we will run in the SCADE's debugger,
	 * which has no std-(err)-console, so we need to redirect to a file.
	 * This fixed location should work on most default setups.
	 */
	freopen("C:\\Users\\Public\\Documents\\bind_recv_send_stderr.txt", "a", stderr);
#endif
	socket_count++;
	unsigned long cmd = 1;
	e = ioctlsocket(*fd, FIONBIO, &cmd);
	if (e < 0) {
		dprint("[FAILED (%d)] %d => ioctlsocket[FIONBIO]\n", e, cmd);
		return; /* set non-blocking, so select is not needed */
	}
#endif
	e = setsockopt(*fd, IPPROTO_IP, IP_MULTICAST_LOOP, (const void *)&optvalML,sizeof(int));
	if (e < 0) {
		dprint("[FAILED (%d)] %d => setsockopt[IP_MULTICAST_LOOP]\n", e, optvalML);
		close_udp(*fd);
		*fd = (fd_t_udp)invalidSocket_udp;
		return;
	}
#ifndef PIKEOS_NATIVE
	int optval1 = 1;
	e = setsockopt(*fd, SOL_SOCKET, SO_BROADCAST,      (const void *)&optval1, sizeof(int)) < 0;
	if (e < 0) {
		dprint("[FAILED (%d)] %d => setsockopt[SO_BROADCAST]\n", e, optval1);
		close_udp(*fd);
		*fd = (fd_t_udp)invalidSocket_udp;
		return;
	}
#endif

	struct sockaddr_in InAddr = {
		.sin_family = AF_INET,
		.sin_port = htons(udpPort),
		.sin_addr = { .s_addr = htonl(ipAddress->addr) }
	};
	socklen_t addrlen = sizeof(InAddr);

	int bret = bind(*fd, (struct sockaddr *) &InAddr, addrlen);
	if (bret < 0) {
		dprint("[FAILED (%d)] to assign socket address (%d.%d.%d.%d:%d): %m\n", bret, ((InAddr.sin_addr.s_addr>>0)&0xFF), ((InAddr.sin_addr.s_addr>>8)&0xFF), ((InAddr.sin_addr.s_addr>>16)&0xFF), ((InAddr.sin_addr.s_addr>>24)&0xFF), ntohs(InAddr.sin_port));
		*fd = (fd_t_udp)invalidSocket_udp;
	} else {
#if !defined(_RM57Lx_)
		if (getsockname(*fd, (struct sockaddr *) &InAddr, &addrlen) == 0) {
			dprint("[OK] Bound socket [%d] to %d.%d.%d.%d:%d\n", *fd, ((InAddr.sin_addr.s_addr>>0)&0xFF), ((InAddr.sin_addr.s_addr>>8)&0xFF), ((InAddr.sin_addr.s_addr>>16)&0xFF), ((InAddr.sin_addr.s_addr>>24)&0xFF), ntohs(InAddr.sin_port));
		}
#endif
		*fail = kcg_false;
	}
#endif
}
#endif

#ifdef kcg_use_IpAddress_udp
kcg_bool mcadd_udp(fd_t_udp fd, IpAddress_udp *ipAddress);
kcg_bool mcadd_udp(fd_t_udp fd, IpAddress_udp *ipAddress) {

	if ((fd != (fd_t_udp)invalidSocket_udp) && ((ipAddress->addr & (224U<<24)) == (224U<<24))) {
		/* Join the broadcast group: */
#ifdef LWIP
		if (igmp_joingroup(NULL, (ip_addr_t*)&ipAddress->addr) == ERR_OK) return kcg_false;
#else
		struct ip_mreq mcc = { .imr_multiaddr = { .s_addr = 0 } };
		mcc.imr_multiaddr.s_addr = htonl(ipAddress->addr);
		if (setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (const void *)&mcc, sizeof(mcc)) == 0) return kcg_false;
#endif
	}
	return kcg_true;
}

kcg_bool mcdrop_udp(fd_t_udp fd, IpAddress_udp *ipAddress);
kcg_bool mcdrop_udp(fd_t_udp fd, IpAddress_udp *ipAddress) {

	if ((fd != (fd_t_udp)invalidSocket_udp) && ((ipAddress->addr & (224U<<24)) == (224U<<24))) {
		/* Drop the broadcast group: */
#ifdef LWIP
		if (igmp_leavegroup(NULL, (ip_addr_t*)&ipAddress->addr) == ERR_OK) return kcg_false;
#else
		struct ip_mreq mcc = { .imr_multiaddr = { .s_addr = 0 } };
		mcc.imr_multiaddr.s_addr = htonl(ipAddress->addr);
		if (setsockopt(fd, IPPROTO_IP, IP_DROP_MEMBERSHIP, (const void *)&mcc, sizeof(mcc))== 0) return kcg_false;
#endif
	}
	return kcg_true;
}

kcg_bool mcttl_udp(fd_t_udp fd, int ttl);
kcg_bool mcttl_udp(fd_t_udp fd, int ttl) {

	if ((fd != (fd_t_udp)invalidSocket_udp) && (ttl > 0) && (ttl <= 255)) {
#ifdef LWIP
		((struct udp_pcb *)fd)->ttl = *ttl;
		return kcg_false;
#else
		if (setsockopt(fd, IPPROTO_IP, IP_MULTICAST_TTL, (const void *)&ttl, sizeof(ttl)) == 0) return kcg_false;
#endif
	}
	return kcg_true;
}
#endif

#ifdef kcg_use_IpAddress_udp
void recvChunksFrom_udp(fd_t_udp fd, kcg_bool latest, length_t_udp offs, length_t_udp cap, kcg_bool *fail, length_t_udp *length, char *data, peer_t_udp *peer);
void recvChunksFrom_udp(fd_t_udp fd, kcg_bool latest, length_t_udp offs, length_t_udp cap, kcg_bool *fail, length_t_udp *length, char *data, peer_t_udp *peer) {

	peer->addr.addr = 0;
	peer->port = 0;
	*length = (length_t_udp)nil_udp;
	*fail = kcg_true;
	if (fd != (fd_t_udp)invalidSocket_udp) {
#ifdef LWIP
		*fail = kcg_false;

		SYS_ARCH_PROTECT(pl.lvl);

		if (pl.wr != pl.rd) {
			if (latest) {
				pl.rd = pl.wr-1+MAX_PACKS;
				pl.rd %= MAX_PACKS;
			}
			struct pbuf *p = pl.pi[pl.rd].p;
			pl.pi[pl.rd].p = 0;
			peer->port = pl.pi[pl.rd].pport;
			peer->addr.addr = pl.pi[pl.rd].paddr;
			pl.rd++;
			pl.rd %= MAX_PACKS;

			SYS_ARCH_UNPROTECT(pl.lvl);

			*length = MIN(p->tot_len, cap-offs);
			if (offs>0) memset(data, 0, offs);
			pbuf_copy_partial(p, data+offs, *length, 0);

			pbuf_free(p);
		} else {
			SYS_ARCH_UNPROTECT(pl.lvl);
		}

#else /* !LWIP */
		struct sockaddr_in s_in;
		length_t_udp recvlen = nil_udp;
		socklen_t lAddr;

		do {
			lAddr = sizeof(s_in);
			errno = 0;
			if (offs > 0) memset(data, 0, offs);
			recvlen = recvfrom(fd, data+offs, cap-offs, MSG_DONTWAIT, (struct sockaddr *)&s_in, &lAddr);
			if (recvlen >= 0) {
				peer->addr.addr = ntohl(s_in.sin_addr.s_addr);
				peer->port = ntohs(s_in.sin_port);
				*fail = kcg_false; /* only true on non-recoverable problems */
				*length = recvlen;
				nowfd(fd, &peer->time, (kcg_int64 *)&peer->mtime); /* explicit cast necessary when used in Scade-6.4-lang-mode */
#ifdef DEBUG
				dprint("Receivd [%d@%d/%d] from %d.%d.%d.%d:%d\n", recvlen, offs, cap-offs, ((s_in.sin_addr.s_addr>>0)&0xFF), ((s_in.sin_addr.s_addr>>8)&0xFF), ((s_in.sin_addr.s_addr>>16)&0xFF), ((s_in.sin_addr.s_addr>>24)&0xFF), peer->port);
#endif

			} else {
				now_sys(&peer->time, (kcg_int64 *)&peer->mtime);
				peer->mtime /= 1000; /* now-sys return nanos */
#ifdef WINDOWS
				int error = WSAGetLastError();
				*fail = (!error || error == WSAEWOULDBLOCK                ) ? kcg_false : kcg_true;
				if (*fail) dprint("[FAILED (%d)] recvChunksFrom(fd=%d, latest=%d, offs=%d, cap=%d) \n", error, fd, latest, offs, cap);
#elif PIKEOS_NATIVE
				*fail = (        recvlen == ANIS_E_AGAIN                  ) ? kcg_false : kcg_true;
				if (*fail) dprint("[FAILED (%d)] recvChunksFrom(fd=%d, latest=%d, offs=%d, cap=%d) \n", recvlen, fd, latest, offs, cap);
#else /* !WINDOWS !PIKEOS_NATIVE */
				*fail = (!errno || errno == EWOULDBLOCK || errno == EAGAIN) ? kcg_false : kcg_true;
				if (*fail) dprint("[FAILED (%d)] recvChunksFrom(fd=%d, latest=%d, offs=%d, cap=%d) = %s\n", errno, fd, latest, offs, cap, strerror(errno));
#endif /* WINDOWS */
			}
		} while ((!*fail) && (recvlen >= 0) && latest);
#endif /* LWIP */
	}
}

kcg_bool sendChunksTo_udp(
	/* udp::sendto::length */ length_t_udp length,
	/* udp::sendto::offs */  length_t_udp offs,
	/* udp::sendto::data */ char *data,
	/* udp::sendto::peer */ peer_t_udp *peer,
	/* udp::sendto::fd */ fd_t_udp fd);
kcg_bool sendChunksTo_udp(length_t_udp length, length_t_udp offs, char *data, peer_t_udp *peer, fd_t_udp fd) {

	/* nothing to send on nil length, no error */
	if (length <= nil_udp) return kcg_false;

	/* param sanity check */
	if (fd == invalidSocket_udp || !peer->port || (peer->port & ~0xFFFF)) return kcg_true;

	/* convert the any-address to the broadcast address */
	if (!peer->addr.addr)  peer->addr.addr = ~0;

#ifdef LWIP
	struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT, length, PBUF_RAM);
	if (!p) return kcg_true;
	memcpy(p->payload, ((char *)data)+offs, length);
	err_t ret = udp_sendto((struct udp_pcb *)fd, p, (ip_addr_t *)&peer->ipAddress, peer->port);
	pbuf_free(p); // release from user-space
	return ret == ERR_OK ? kcg_false : kcg_true;
#else
	struct sockaddr_in s_in = { .sin_family = AF_INET };
	s_in.sin_addr.s_addr = htonl(peer->addr.addr);
	s_in.sin_port =        htons(peer->port);
	ssize_t ret = sendto(fd, ((char *)data)+offs, length, MSG_DONTWAIT, (struct sockaddr *) &s_in, sizeof(s_in));
	kcg_bool fail = (ret>=0) ? kcg_false : kcg_true;
#ifdef DEBUG
	if (!fail) dprint("Sending [%d] to %d.%d.%d.%d:%d\n", length, ((s_in.sin_addr.s_addr>>0)&0xFF), ((s_in.sin_addr.s_addr>>8)&0xFF), ((s_in.sin_addr.s_addr>>16)&0xFF), ((s_in.sin_addr.s_addr>>24)&0xFF), peer->port);
#endif
#ifdef WINDOWS
	ret = WSAGetLastError();
	if (fail) dprint("[FAILED (%d)] sendChunksTo(length=%d, offs=%d, fd=%d) \n", ret, length, offs, fd);
#elif PIKEOS_NATIVE
	if (fail) dprint("[FAILED (%ld)] sendChunksTo(length=%d, offs=%d, fd=%d) \n", ret, length, offs, fd);
#else
	if (fail) dprint("[FAILED (%d)] sendChunksTo(length=%d, offs=%d, fd=%d) = %s\n", errno, length, offs, fd, strerror(errno));
#endif /* WINDOWS */

	return fail;
#endif /*LWIP */
}

#ifdef kcg_copy_array_uint32_16_2
void recvChunks02_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_2 *data, peer_t_udp *peer);
void recvChunks02_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_2 *data, peer_t_udp *peer) {
	recvChunksFrom_udp(fd, latest, offs, 2*64, fail, length, (char *)data, peer);
}

kcg_bool sendChunks02_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_2 *data, peer_t_udp *peer, fd_t_udp fd);
kcg_bool sendChunks02_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_2 *data, peer_t_udp *peer, fd_t_udp fd) {
	return (((2*64)<(offs+length)) || sendChunksTo_udp( length, offs, (char *)data, peer, fd));
}
#endif

#ifdef kcg_copy_array_uint32_16_4
void recvChunks04_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_4 *data, peer_t_udp *peer);
void recvChunks04_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_4 *data, peer_t_udp *peer) {
	recvChunksFrom_udp(fd, latest, offs, 4*64, fail, length, (char *)data, peer);
}

kcg_bool sendChunks04_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_4 *data, peer_t_udp *peer, fd_t_udp fd);
kcg_bool sendChunks04_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_4 *data, peer_t_udp *peer, fd_t_udp fd) {
	return (((4*64)<(offs+length)) || sendChunksTo_udp( length, offs, (char *)data, peer, fd));
}
#endif

#ifdef kcg_copy_array_uint32_16_6
void recvChunks06_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_6 *data, peer_t_udp *peer);
void recvChunks06_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_6 *data, peer_t_udp *peer) {
	recvChunksFrom_udp(fd, latest, offs, 6*64, fail, length, (char *)data, peer);
}

kcg_bool sendChunks06_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_6 *data, peer_t_udp *peer, fd_t_udp fd);
kcg_bool sendChunks06_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_6 *data, peer_t_udp *peer, fd_t_udp fd) {
	return (((6*64)<(offs+length)) || sendChunksTo_udp( length, offs, (char *)data, peer, fd));
}
#endif

#ifdef kcg_copy_array_uint32_16_8
void recvChunks08_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_8 *data, peer_t_udp *peer);
void recvChunks08_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_8 *data, peer_t_udp *peer) {
	recvChunksFrom_udp(fd, latest, offs, 8*64, fail, length, (char *)data, peer);
}

kcg_bool sendChunks08_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_8 *data, peer_t_udp *peer, fd_t_udp fd);
kcg_bool sendChunks08_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_8 *data, peer_t_udp *peer, fd_t_udp fd) {
	return (((8*64)<(offs+length)) || sendChunksTo_udp( length, offs, (char *)data, peer, fd));
}
#endif

#ifdef kcg_copy_array_uint32_16_10
void recvChunks10_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_10 *data, peer_t_udp *peer);
void recvChunks10_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_10 *data, peer_t_udp *peer) {
	recvChunksFrom_udp(fd, latest, offs,10*64, fail, length, (char *)data, peer);
}

kcg_bool sendChunks10_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_10 *data, peer_t_udp *peer, fd_t_udp fd);
kcg_bool sendChunks10_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_10 *data, peer_t_udp *peer, fd_t_udp fd) {
	return (((10*64)<(offs+length)) || sendChunksTo_udp( length, offs, (char *)data, peer, fd));
}
#endif

#ifdef kcg_copy_array_uint32_16_12
void recvChunks12_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_12 *data, peer_t_udp *peer);
void recvChunks12_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_12 *data, peer_t_udp *peer) {
	recvChunksFrom_udp(fd, latest, offs,12*64, fail, length, (char *)data, peer);
}

kcg_bool sendChunks12_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_12 *data, peer_t_udp *peer, fd_t_udp fd);
kcg_bool sendChunks12_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_12 *data, peer_t_udp *peer, fd_t_udp fd) {
	return (((12*64)<(offs+length)) || sendChunksTo_udp( length, offs, (char *)data, peer, fd));
}
#endif

#ifdef kcg_copy_array_uint32_16_16
void recvChunks16_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_16 *data, peer_t_udp *peer);
void recvChunks16_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_16 *data, peer_t_udp *peer) {
	recvChunksFrom_udp(fd, latest, offs,16*64, fail, length, (char *)data, peer);
}

kcg_bool sendChunks16_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_16 *data, peer_t_udp *peer, fd_t_udp fd);
kcg_bool sendChunks16_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_16 *data, peer_t_udp *peer, fd_t_udp fd) {
	return (((16*64)<(offs+length)) || sendChunksTo_udp( length, offs, (char *)data, peer, fd));
}
#endif

#ifdef kcg_copy_array_uint32_16_20
void recvChunks20_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_20 *data, peer_t_udp *peer);
void recvChunks20_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_20 *data, peer_t_udp *peer) {
	recvChunksFrom_udp(fd, latest, offs,20*64, fail, length, (char *)data, peer);
}

kcg_bool sendChunks20_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_20 *data, peer_t_udp *peer, fd_t_udp fd);
kcg_bool sendChunks20_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_20 *data, peer_t_udp *peer, fd_t_udp fd) {
	return (((20*64)<(offs+length)) || sendChunksTo_udp( length, offs, (char *)data, peer, fd));
}
#endif

#ifdef kcg_copy_array_uint32_16_24
void recvChunks24_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_24 *data, peer_t_udp *peer);
void recvChunks24_udp_specialization(fd_t_udp fd, kcg_bool latest, length_t_udp offs,kcg_bool *fail, length_t_udp *length, array_uint32_16_24 *data, peer_t_udp *peer) {
	recvChunksFrom_udp(fd, latest, offs,24*64, fail, length, (char *)data, peer);
}

kcg_bool sendChunks24_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_24 *data, peer_t_udp *peer, fd_t_udp fd);
kcg_bool sendChunks24_udp_specialization(length_t_udp length, length_t_udp offs, array_uint32_16_24 *data, peer_t_udp *peer, fd_t_udp fd) {
	return (((24*64)<(offs+length)) || sendChunksTo_udp( length, offs, (char *)data, peer, fd));
}
#endif

#endif /* kcg_use_IpAddress_udp */

typedef addrs_t_udp array_addrs_t_udp[16];

void addrs_udp( /* this is basically the example from the Linux-manual */
  /* udp::addrs::onlyUp */ kcg_bool onlyUp __unused, /* mark as unused so PikeOS-CC does not complain */
  /* udp::addrs::needMC */ kcg_bool needMC __unused,
  /* udp::addrs::needBC */ kcg_bool needBC __unused,
  /* udp::addrs::length */ length_t_udp *available,
  /* udp::addrs::addrList */ array_addrs_t_udp *addrList) {

	*available = 0;
	memset(addrList, 0, addrsLength_udp * sizeof(addrs_t_udp) );

#if !defined(LINUX)
	/* getifaddrs replacement is really messy on Windows, and not designed on PikeOS */
	*available = 1; /* set up a fake address entry */
	(*addrList)[0].broadcast = kcg_true;
	(*addrList)[0].mc = kcg_true;
	(*addrList)[0].up = kcg_true;
	(*addrList)[0].ip.addr = 0;
#else
	struct ifaddrs *ifaddr, *ifa;

	if (getifaddrs(&ifaddr) == -1) { /* ANY wont work either */
		dprint("[FAILED (%d)] getifaddrs() = %s\n", errno, strerror(errno));
		return;
	}

	/* Walk through linked list, maintaining head pointer so we can free list later */

	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {

		if (ifa->ifa_addr && ifa->ifa_addr->sa_family == AF_INET /*|| family == AF_INET6*/) { /* so far, this lib is only concerned with IPv4 */
			if (
				   (!onlyUp || (ifa->ifa_flags & (IFF_RUNNING|IFF_UP)) == (IFF_RUNNING|IFF_UP))
				&& (!needMC || (ifa->ifa_flags &  IFF_MULTICAST))
				&& (!needBC || (ifa->ifa_flags &  IFF_BROADCAST))) {

				addrList->ip.addr = ntohl(((struct sockaddr_in *)ifa->ifa_addr)->sin_addr.s_addr);
				if ( ifa->ifa_flags &  IFF_BROADCAST) addrList->broadcast = kcg_true;
				if ((ifa->ifa_flags & (IFF_RUNNING|IFF_UP)) == (IFF_RUNNING|IFF_UP)) addrList->up = kcg_true;
				if ( ifa->ifa_flags &  IFF_MULTICAST) addrList->mc = kcg_true;
				if ( ifa->ifa_flags &  IFF_POINTOPOINT) addrList->p2p = kcg_true;
				addrList++;
				if (++*available == addrsLength_udp) break;
			}
		}
	}

	freeifaddrs(ifaddr);
#endif
}
