/*-------------------------- FILE PROLOGUE ---------------------------*/

/**
 ***********************************************************************
 * @copyright
 *  (C) Copyright SYSGO AG.
 *  Ulm, Germany
 *  All rights reserved.
 *
 * @file
 *  mirror.c
 *
 * @purpose
 *  Mirror application
 *
 * @cfg_management
 *  $Id: mirror.c,v 1.1.2.5 2017/09/29 11:00:45 gja Exp $
 *  $Author: gja $
 *  $Date: 2017/09/29 11:00:45 $
 *  $Revision: 1.1.2.5 $
 *  $State: Exp $
 *
 ***********************************************************************
 */

/* ---------------------------- FILE INCLUSION ----------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <stdint.h>

#define _UINT32_T_DECLARED
#include <instr.h>

/* --------------------------- MACRO DEFINITIONS --------------------------- */

#define MAX_SEND_LENGTH    10000
#define RATE_NUMBER         1000
#define MIRROR_PORT         2000
#define TARGET_PORT         2000
#define INSTR_PORT          5000

/* CONFIG: */
#define USE_SELECT          1   /* Use select() to detect disappearing peer. */
#define SELECT_TO_SEC       5   /* Timeout for select() in seconds. */
#define INTERFACES          1   /* Number of configured Ethernet interfaces
                                   without localhost loopback */

/* -------------------------- OBJECT DECLARATIONS -------------------------- */

static char prog[] = "-\\|/";

static const char *imon_strs[] = {
    "Send to unbound socket    ",
    "UDP message size          ",
    "UDP checksum error        ",
    "Out of memory             ",
};

static const char *intf_imon_strs[] = {
    "Interface enabled         ",
    "Fragmentation timeout     ",
    "Fragmentation space       ",
    "Fragmentation error       ",
    "Fragmentation offset      ",
    "Receive buffer overflow   ",
    "Unsupported ICMP type     ",
    "Fragmented ICMP received  ",
    "ICMP checksum error       ",
    "Unsupported IGMP type     ",
    "Fragmented IGMP received  ",
    "IGMP checksum error       ",
    "IP checksum error         ",
    "IP_HDR error              ",
    "Invalid total length      ",
    "Unsupported IP type       ",
    "Unbound UDP socket        ",
    "Bad IP address            ",
    "Destination Unreachable   ",
    "ARP_HDR error             "
};

/* ---------------------- LOCAL FUNCTION DEFINITIONS ----------------------- */

static void
show_instrumon_data(anis_instr_stat_t *buffer, int len)
{
    int i;
    int cnt;
    uint32_t *instrval = (uint32_t *)buffer;

    puts("Global Instrumentation counters:");
    for (i = 0; i < sizeof(imon_strs)/sizeof(imon_strs[0]); i++) {
        printf("%s: %u\n", imon_strs[i], ntohl(instrval[i]));
    }
    instrval = (uint32_t *)&buffer->intf[0];
    puts("Interface specific instrumentation counters:");

    printf("Interface                        ");
    for (i = 0; i < INTERFACES; i++) {
        printf("%d      ", i);
    }

    printf("lo\n");
    for (i = 0; i < sizeof(intf_imon_strs) / sizeof(intf_imon_strs[0]); i++) {
        printf("%s: ", intf_imon_strs[i]);
        for (cnt = 0; cnt < (INTERFACES + 1); cnt++) {
            printf("%6u ",
                   ntohl(instrval[(cnt * sizeof(anis_instr_intf_stat_t)) + i]));
        }
        printf("\n");
    }
    printf("\n");
}

/* --------------------- GLOBAL FUNCTION DEFINITIONS ----------------------- */

/**
 *  @purpose
 *    Read packets and send them back to the target on same port.
 *    (MIRROR_PORT = TARGET_PORT)
 *    After RATE_NUMBER of packets, ask for instrumentation data,
 *    receive and display them.
 */
int main(void)
{
    int i;
    int rc;
    int pr = 0;
    int fd, monfd;
    char buffer[MAX_SEND_LENGTH];
    socklen_t len;
    struct sockaddr_in dst;
    struct timeval start, now;
#if USE_SELECT
    fd_set rfds;
    struct timeval tv;
#endif

    monfd = socket(PF_INET, SOCK_DGRAM, 0);
    if (monfd < 0) {
        perror("socket(Monitor)");
        exit(EXIT_FAILURE);
    } else {
        puts("Monitor Socket created.");
    }

    dst.sin_family = AF_INET;
    dst.sin_port = htons(INSTR_PORT);
    dst.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(monfd, (struct sockaddr *)&dst, sizeof(dst)) < 0) {
        perror("bind(Monitor)");
        exit(EXIT_FAILURE);
    }

    fd = socket(PF_INET, SOCK_DGRAM, 0);
    if (fd < 0) {
        perror("socket(data)");
        exit(EXIT_FAILURE);
    } else {
        puts("Data socket created.");
    }

    dst.sin_family = AF_INET;
    dst.sin_port = htons(MIRROR_PORT);
    dst.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(fd, (struct sockaddr *)&dst, sizeof(dst)) < 0) {
        perror("bind(data)");
        exit(EXIT_FAILURE);
    }

    i = 0;
    gettimeofday(&start, NULL);
    putchar('\n');

    for (;;) {
        printf("\r%c", prog[pr]);
        pr = (pr + 1) % 4;
        fflush(stdout);
#if USE_SELECT
        tv.tv_sec = SELECT_TO_SEC;
        tv.tv_usec = 0;
        FD_ZERO(&rfds);
        FD_SET(fd, &rfds);
        rc = select(fd + 1, &rfds, NULL, NULL, &tv);
        if (rc == 0) {
           fputs("Timeout waiting for packet - please start the target.\n", stderr);
        } else if (rc == -1) {
           perror("select");
        } else
#endif
        {
            len = sizeof(dst);
            rc = recvfrom(fd, buffer, sizeof(buffer), 0,
                          (struct sockaddr *)&dst, &len);
            if (rc < 0) {
                perror("Error reading");
            } else {
                dst.sin_port = htons(TARGET_PORT);
                rc = sendto(fd, buffer, rc, 0,
                            (struct sockaddr *)&dst, sizeof(dst));
                if (rc < 0) {
                    perror("Error sending back");
                } else {
                    i++;
                }
            }

            if (i == RATE_NUMBER) {
                gettimeofday(&now, NULL);
                printf("\nNetwork rate: %.2f bps\n",
                       ((float)i * rc) /
                       ((float)(now.tv_sec - start.tv_sec) * 1000000 +
                        (float)(now.tv_usec - start.tv_usec)) * 1000000);
                i = 0;
                gettimeofday(&start, NULL);

                /* send a request to instrumon */
                len = sizeof(dst);
                dst.sin_port = htons(INSTR_PORT);
                rc = sendto(monfd, buffer, 1, 0,
                            (struct sockaddr *)&dst, sizeof(dst));
                if (rc < 0) {
                    perror("Error sending to instrumon");
                } else {
                    rc = recvfrom(monfd, buffer, sizeof(buffer), 0,
                                  (struct sockaddr *)&dst, &len);
                    if (rc < 0) {
                        perror("Error receiving from instrumon");
                    } else {
                        show_instrumon_data((anis_instr_stat_t *)buffer, rc);
                    }
                }
            }
        }
    }

    close(fd);
    return 0;
}
