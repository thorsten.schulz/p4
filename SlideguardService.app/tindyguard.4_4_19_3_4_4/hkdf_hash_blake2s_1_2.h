/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _hkdf_hash_blake2s_1_2_H_
#define _hkdf_hash_blake2s_1_2_H_

#include "kcg_types.h"
#include "hmacChunk_hash_blake2s_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hkdf/ */
extern void hkdf_hash_blake2s_1_2(
  /* _L4/, ikm/ */
  array_uint32_16_1 *ikm_1_2,
  /* _L5/, ikmlen/ */
  size_slideTypes ikmlen_1_2,
  /* _L3/, chainingKey/ */
  HashChunk_hash_blake2s *chainingKey_1_2,
  /* _L14/, okm/ */
  array_uint32_8_2 *okm_1_2);



#endif /* _hkdf_hash_blake2s_1_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_hash_blake2s_1_2.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

