/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _echoTake_icmp_19_H_
#define _echoTake_icmp_19_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "check_ip_19.h"
#include "ipsum_BEH_ip_19.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* icmp::echoTake/ */
extern void echoTake_icmp_19(
  /* _L1/, len/ */
  length_t_udp len_19,
  /* _L2/, msg/ */
  array_uint32_16_19 *msg_19,
  /* _L50/, host/ */
  IpAddress_udp *host_19,
  /* lenoo/ */
  length_t_udp *lenoo_19,
  /* msgoo/ */
  array_uint32_16_19 *msgoo_19,
  /* seqoo/ */
  kcg_uint32 *seqoo_19,
  /* _L33/, src/ */
  IpAddress_udp *src_19,
  /* id/ */
  kcg_uint32 *id_19,
  /* _L60/, fail1/, failed/ */
  kcg_bool *failed_19,
  /* badChecksum/ */
  kcg_bool *badChecksum_19,
  /* badPayload/ */
  kcg_bool *badPayload_19);



#endif /* _echoTake_icmp_19_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** echoTake_icmp_19.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

