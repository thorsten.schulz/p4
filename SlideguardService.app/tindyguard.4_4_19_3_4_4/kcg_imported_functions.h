/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _KCG_IMPORTED_FUNCTIONS_H_
#define _KCG_IMPORTED_FUNCTIONS_H_

#include "kcg_types.h"

#ifndef htons_sys_specialization
/* sys::specialization::htons/ */
extern kcg_uint16 htons_sys_specialization(/* h/ */ kcg_uint16 h);
#endif /* htons_sys_specialization */

#ifndef now_sys
/* sys::now/ */
extern void now_sys(
  /* millis/ */
  time_t_sys *millis,
  /* nanos/ */
  kcg_int64 *nanos);
#endif /* now_sys */

#ifndef recvChunks20_udp_specialization
/* udp::specialization::recvChunks20/ */
extern void recvChunks20_udp_specialization(
  /* fd/ */
  fd_t_udp fd,
  /* latest/ */
  kcg_bool latest,
  /* offs/ */
  length_t_udp offs,
  /* fail/ */
  kcg_bool *fail,
  /* length/ */
  length_t_udp *length,
  /* data/ */
  array_uint32_16_20 *data,
  /* peer/ */
  peer_t_udp *peer);
#endif /* recvChunks20_udp_specialization */

#ifndef sendChunks20_udp_specialization
/* udp::specialization::sendChunks20/ */
extern kcg_bool sendChunks20_udp_specialization(
  /* length/ */
  length_t_udp length,
  /* offs/ */
  length_t_udp offs,
  /* data/ */
  array_uint32_16_20 *data,
  /* peer/ */
  peer_t_udp *peer,
  /* fd/ */
  fd_t_udp fd);
#endif /* sendChunks20_udp_specialization */

#ifndef tai_sys
/* sys::tai/ */
extern void tai_sys(
  /* bigEndian/ */
  kcg_bool bigEndian,
  /* xsec/ */
  kcg_uint32 *xsec,
  /* sec/ */
  kcg_uint32 *sec,
  /* nano/ */
  kcg_uint32 *nano);
#endif /* tai_sys */

#ifndef random01_sys_specialization
/* sys::specialization::random01/ */
extern void random01_sys_specialization(
  /* fail/ */
  kcg_bool *fail,
  /* data/ */
  array_uint32_1 *data);
#endif /* random01_sys_specialization */

#ifndef curve25519_donna_nacl_box
/* nacl::box::curve25519_donna/ */
extern void curve25519_donna_nacl_box(
  /* ourPriv/ */
  Key32_slideTypes *ourPriv,
  /* their/ */
  Key32_slideTypes *their,
  /* key/ */
  Key32_slideTypes *key);
#endif /* curve25519_donna_nacl_box */

#ifndef random08_sys_specialization
/* sys::specialization::random08/ */
extern void random08_sys_specialization(
  /* fail/ */
  kcg_bool *fail,
  /* data/ */
  array_uint32_8 *data);
#endif /* random08_sys_specialization */

#ifndef sendChunks04_udp_specialization
/* udp::specialization::sendChunks04/ */
extern kcg_bool sendChunks04_udp_specialization(
  /* length/ */
  length_t_udp length,
  /* offs/ */
  length_t_udp offs,
  /* data/ */
  array_uint32_16_4 *data,
  /* peer/ */
  peer_t_udp *peer,
  /* fd/ */
  fd_t_udp fd);
#endif /* sendChunks04_udp_specialization */

#ifndef htonl_sys_specialization
/* sys::specialization::htonl/ */
extern kcg_uint32 htonl_sys_specialization(/* h/ */ kcg_uint32 h);
#endif /* htonl_sys_specialization */

#ifndef close_udp
/* udp::close/ */
extern kcg_bool close_udp(/* fd/ */ fd_t_udp fd);
#endif /* close_udp */

#ifndef bind_udp
/* udp::bind/ */
extern void bind_udp(
  /* ipAddress/ */
  IpAddress_udp *ipAddress,
  /* udpPort/ */
  port_t_udp udpPort,
  /* fail/ */
  kcg_bool *fail,
  /* fd/ */
  fd_t_udp *fd);
#endif /* bind_udp */

#ifndef addrs_udp
/* udp::addrs/ */
extern void addrs_udp(
  /* onlyUp/ */
  kcg_bool onlyUp,
  /* needMC/ */
  kcg_bool needMC,
  /* needBC/ */
  kcg_bool needBC,
  /* available/ */
  length_t_udp *available,
  /* addrList/ */
  _2_array *addrList);
#endif /* addrs_udp */

#endif /* _KCG_IMPORTED_FUNCTIONS_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_imported_functions.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

