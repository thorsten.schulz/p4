/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "pack25519_it2_nacl_op.h"

/* nacl::op::pack25519_it2/ */
kcg_uint32 pack25519_it2_nacl_op(
  /* i/ */
  kcg_uint32 i,
  /* _L28/, t/ */
  gf_nacl_op *t)
{
  kcg_int64 tmp;
  kcg_int64 tmp1;
  kcg_uint32 idx;
  kcg_uint32 idx2;
  /* _L33/, o/ */
  kcg_uint32 o;

  idx = i * kcg_lit_uint32(2);
  idx2 = idx + kcg_lit_uint32(1);
  if (idx < kcg_lit_uint32(16)) {
    tmp = (*t)[idx];
  }
  else {
    tmp = kcg_lit_int64(0);
  }
  if (idx2 < kcg_lit_uint32(16)) {
    tmp1 = (*t)[idx2];
  }
  else {
    tmp1 = kcg_lit_int64(0);
  }
  o = /* _L24= */(kcg_uint32) /* _L27= */(kcg_uint16) tmp | kcg_lsl_uint32(
      /* _L31= */(kcg_uint32) /* _L30= */(kcg_uint16) tmp1,
      kcg_lit_uint32(16));
  return o;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack25519_it2_nacl_op.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

