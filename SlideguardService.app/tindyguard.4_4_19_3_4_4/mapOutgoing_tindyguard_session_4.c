/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapOutgoing_tindyguard_session_4.h"

/* tindyguard::session::mapOutgoing/ */
void mapOutgoing_tindyguard_session_4(
  /* _L7/, sid/ */
  size_tindyguardTypes sid_4,
  /* _L5/, length/ */
  array_int32_4 *length_4,
  /* _L8/, txm_sid/ */
  array_int32_4 *txm_sid_4,
  /* _L2/, length_not_taken/ */
  array_int32_4 *length_not_taken_4,
  /* _L3/, length_picked/ */
  array_int32_4 *length_picked_4)
{
  kcg_size idx;
  /* @1/_L1/ */
  kcg_bool _L1_mapOutgoing_it_3;

  /* _L2= */
  for (idx = 0; idx < 4; idx++) {
    _L1_mapOutgoing_it_3 = (*length_4)[idx] > nil_udp && (*txm_sid_4)[idx] == sid_4;
    /* @1/_L2= */
    if (_L1_mapOutgoing_it_3) {
      (*length_not_taken_4)[idx] = nil_udp;
      (*length_picked_4)[idx] = (*length_4)[idx];
    }
    else {
      (*length_not_taken_4)[idx] = (*length_4)[idx];
      (*length_picked_4)[idx] = nil_udp;
    }
  }
}

/*
  Expanded instances for: tindyguard::session::mapOutgoing/
  @1: (tindyguard::session::mapOutgoing_it#3)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapOutgoing_tindyguard_session_4.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

