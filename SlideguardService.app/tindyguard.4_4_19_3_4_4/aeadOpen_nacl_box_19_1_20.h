/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _aeadOpen_nacl_box_19_1_20_H_
#define _aeadOpen_nacl_box_19_1_20_H_

#include "kcg_types.h"
#include "chacha_IETF_nacl_box_stream_19_20.h"
#include "auth2_nacl_onetime_1_19.h"
#include "cmp_M_uint8_16.h"
#include "setup_nacl_core_chacha_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::box::aeadOpen/ */
extern void aeadOpen_nacl_box_19_1_20(
  /* @1/_L43/, @1/a/, _L5/, a/ */
  Mac_nacl_onetime *a_19_1_20,
  /* @1/_L44/, @1/msg2/, _L13/, _L7/, cm/ */
  array_uint32_16_19 *cm_19_1_20,
  /* @1/_L45/, @1/mlen2/, _L12/, _L6/, mlen/ */
  int_slideTypes mlen_19_1_20,
  /* @1/_L49/, @1/msg1/, _L18/, ad/ */
  array_uint32_16_1 *ad_19_1_20,
  /* @1/_L50/, @1/mlen1/, _L19/, adlen/ */
  int_slideTypes adlen_19_1_20,
  /* _L2/, nonce/ */
  Nonce_nacl_core_chacha *nonce_19_1_20,
  /* _L3/, key/ */
  Key_nacl_core *key_19_1_20,
  /* _L8/, msg/ */
  array_uint32_16_19 *msg_19_1_20,
  /* @1/_L47/, @1/failed/, _L4/, failed/ */
  kcg_bool *failed_19_1_20);

/*
  Expanded instances for: nacl::box::aeadOpen/
  @1: (nacl::onetime::verify2#1)
*/

#endif /* _aeadOpen_nacl_box_19_1_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** aeadOpen_nacl_box_19_1_20.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

