/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "pack25519_nacl_op.h"

/* nacl::op::pack25519/ */
void pack25519_nacl_op(
  /* @1/_L1/, @1/a/, _L2/, i/ */
  gf_nacl_op *i,
  /* _L5/, o/ */
  array_uint32_8 *o)
{
  kcg_size idx;
  /* @11/_L4/, @3/_L4/, @7/_L4/ */
  kcg_uint64 _L4_srs16_1_car25519_it1_1_car25519_2;
  kcg_bool every_srs16_1_car25519_it1_1_car25519_2;
  /* @2/_L11/, @6/_L4/, @7/_L1/, @7/i/, @9/_L4/ */
  kcg_int64 _L4_car25519_it1_1_car25519_2;
  /* @10/_L4/, @11/_L1/, @11/i/, @6/_L11/ */
  kcg_int64 _L11_car25519_it1_1_car25519_2;
  kcg_int64 tmp_car25519_it1_1_car25519_2;
  gf_nacl_op tmp;
  gf_nacl_op tmp1;
  /* @2/_L4/, @3/_L1/, @3/i/, @5/_L4/ */
  kcg_int64 _L4_car25519_2;
  /* @1/_L3/, @5/_L3/, @9/_L3/ */
  gf_nacl_op _L3_car25519_2;
  /* _L7/ */
  gf_nacl_op _L7;

  tmp_car25519_it1_1_car25519_2 = kcg_lit_int64(0);
  /* @1/_L4= */
  for (idx = 0; idx < 16; idx++) {
    _L4_car25519_2 = tmp_car25519_it1_1_car25519_2 + (*i)[idx] + kcg_lit_int64(
        0x10000);
    _L4_srs16_1_car25519_it1_1_car25519_2 = /* @3/_L2= */(kcg_uint64)
        _L4_car25519_2 >> kcg_lit_uint64(16);
    every_srs16_1_car25519_it1_1_car25519_2 = _L4_car25519_2 < kcg_lit_int64(0);
    _L3_car25519_2[idx] = _L4_car25519_2 - /* @2/_L19= */(kcg_int64)
        (/* @2/_L20= */(kcg_uint64) _L4_car25519_2 & kcg_lit_uint64(
            0xFFFFFFFFFFFF0000));
    if (every_srs16_1_car25519_it1_1_car25519_2) {
      _L4_car25519_it1_1_car25519_2 = /* @3/_L3= */(kcg_int64)
          (kcg_lit_uint64(0xFFFF000000000000) | _L4_srs16_1_car25519_it1_1_car25519_2) -
        kcg_lit_int64(1);
    }
    else {
      _L4_car25519_it1_1_car25519_2 = /* @3/_L3= */(kcg_int64)
          _L4_srs16_1_car25519_it1_1_car25519_2 - kcg_lit_int64(1);
    }
    /* @4/_L2= */
    if (kcg_lit_int32(15) == /* @1/_L4= */(kcg_int32) idx) {
      _L11_car25519_it1_1_car25519_2 = kcg_lit_int64(1);
    }
    else {
      _L11_car25519_it1_1_car25519_2 = kcg_lit_int64(0);
    }
    tmp_car25519_it1_1_car25519_2 = _L4_car25519_it1_1_car25519_2 +
      _L4_car25519_it1_1_car25519_2 * kcg_lit_int64(37) *
      _L11_car25519_it1_1_car25519_2;
  }
  kcg_copy_gf_nacl_op(&tmp, &_L3_car25519_2);
  tmp[0] = tmp_car25519_it1_1_car25519_2 + _L3_car25519_2[0];
  _L4_car25519_2 = kcg_lit_int64(0);
  /* @5/_L4= */
  for (idx = 0; idx < 16; idx++) {
    _L4_car25519_it1_1_car25519_2 = _L4_car25519_2 + tmp[idx] + kcg_lit_int64(
        0x10000);
    _L4_srs16_1_car25519_it1_1_car25519_2 = /* @7/_L2= */(kcg_uint64)
        _L4_car25519_it1_1_car25519_2 >> kcg_lit_uint64(16);
    every_srs16_1_car25519_it1_1_car25519_2 = _L4_car25519_it1_1_car25519_2 <
      kcg_lit_int64(0);
    _L3_car25519_2[idx] = _L4_car25519_it1_1_car25519_2 -
      /* @6/_L19= */(kcg_int64)
        (/* @6/_L20= */(kcg_uint64) _L4_car25519_it1_1_car25519_2 &
          kcg_lit_uint64(0xFFFFFFFFFFFF0000));
    if (every_srs16_1_car25519_it1_1_car25519_2) {
      _L11_car25519_it1_1_car25519_2 = /* @7/_L3= */(kcg_int64)
          (kcg_lit_uint64(0xFFFF000000000000) | _L4_srs16_1_car25519_it1_1_car25519_2) -
        kcg_lit_int64(1);
    }
    else {
      _L11_car25519_it1_1_car25519_2 = /* @7/_L3= */(kcg_int64)
          _L4_srs16_1_car25519_it1_1_car25519_2 - kcg_lit_int64(1);
    }
    /* @8/_L2= */
    if (kcg_lit_int32(15) == /* @5/_L4= */(kcg_int32) idx) {
      tmp_car25519_it1_1_car25519_2 = kcg_lit_int64(1);
    }
    else {
      tmp_car25519_it1_1_car25519_2 = kcg_lit_int64(0);
    }
    _L4_car25519_2 = _L11_car25519_it1_1_car25519_2 +
      _L11_car25519_it1_1_car25519_2 * kcg_lit_int64(37) *
      tmp_car25519_it1_1_car25519_2;
  }
  kcg_copy_gf_nacl_op(&tmp, &_L3_car25519_2);
  tmp[0] = _L4_car25519_2 + _L3_car25519_2[0];
  _L4_car25519_it1_1_car25519_2 = kcg_lit_int64(0);
  /* @9/_L4= */
  for (idx = 0; idx < 16; idx++) {
    _L11_car25519_it1_1_car25519_2 = _L4_car25519_it1_1_car25519_2 + tmp[idx] +
      kcg_lit_int64(0x10000);
    _L4_srs16_1_car25519_it1_1_car25519_2 = /* @11/_L2= */(kcg_uint64)
        _L11_car25519_it1_1_car25519_2 >> kcg_lit_uint64(16);
    every_srs16_1_car25519_it1_1_car25519_2 = _L11_car25519_it1_1_car25519_2 <
      kcg_lit_int64(0);
    _L3_car25519_2[idx] = _L11_car25519_it1_1_car25519_2 -
      /* @10/_L19= */(kcg_int64)
        (/* @10/_L20= */(kcg_uint64) _L11_car25519_it1_1_car25519_2 &
          kcg_lit_uint64(0xFFFFFFFFFFFF0000));
    if (every_srs16_1_car25519_it1_1_car25519_2) {
      tmp_car25519_it1_1_car25519_2 = /* @11/_L3= */(kcg_int64)
          (kcg_lit_uint64(0xFFFF000000000000) | _L4_srs16_1_car25519_it1_1_car25519_2) -
        kcg_lit_int64(1);
    }
    else {
      tmp_car25519_it1_1_car25519_2 = /* @11/_L3= */(kcg_int64)
          _L4_srs16_1_car25519_it1_1_car25519_2 - kcg_lit_int64(1);
    }
    /* @12/_L2= */
    if (kcg_lit_int32(15) == /* @9/_L4= */(kcg_int32) idx) {
      _L4_car25519_2 = kcg_lit_int64(1);
    }
    else {
      _L4_car25519_2 = kcg_lit_int64(0);
    }
    _L4_car25519_it1_1_car25519_2 = tmp_car25519_it1_1_car25519_2 +
      tmp_car25519_it1_1_car25519_2 * kcg_lit_int64(37) * _L4_car25519_2;
  }
  kcg_copy_gf_nacl_op(&tmp, &_L3_car25519_2);
  tmp[0] = _L4_car25519_it1_1_car25519_2 + _L3_car25519_2[0];
  /* _L6=(nacl::op::pack25519_it1#1)/ */ pack25519_it1_nacl_op(&tmp, &tmp1);
  /* _L7=(nacl::op::pack25519_it1#2)/ */ pack25519_it1_nacl_op(&tmp1, &_L7);
  /* _L5= */
  for (idx = 0; idx < 8; idx++) {
    (*o)[idx] = /* _L5=(nacl::op::pack25519_it2#1)/ */
      pack25519_it2_nacl_op(/* _L5= */(kcg_uint32) idx, &_L7);
  }
}

/*
  Expanded instances for: nacl::op::pack25519/
  @1: (nacl::op::car25519#1)
  @2: @1/(nacl::op::car25519_it1#1)
  @3: @2/(nacl::op::srs16#1)
  @4: @2/(math::BoolToInt64#1)
  @5: (nacl::op::car25519#2)
  @6: @5/(nacl::op::car25519_it1#1)
  @7: @6/(nacl::op::srs16#1)
  @8: @6/(math::BoolToInt64#1)
  @9: (nacl::op::car25519#3)
  @10: @9/(nacl::op::car25519_it1#1)
  @11: @10/(nacl::op::srs16#1)
  @12: @10/(math::BoolToInt64#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack25519_nacl_op.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

