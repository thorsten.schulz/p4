/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:49
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "filter1_tindyguard_data_19.h"

/* tindyguard::data::filter1/ */
void filter1_tindyguard_data_19(
  /* _L19/, i/ */
  size_tindyguardTypes i_19,
  /* _L21/, acc/ */
  size_tindyguardTypes acc_19,
  /* _L1/, len/ */
  size_tindyguardTypes len_19,
  /* _L2/, msg/ */
  array_uint32_16_19 *msg_19,
  /* _L3/, protocol/ */
  kcg_uint8 protocol_19,
  /* _L4/, goOn/ */
  kcg_bool *goOn_19,
  /* _L20/, ioo/ */
  size_tindyguardTypes *ioo_19,
  /* _L18/, matching/ */
  kcg_bool *matching_19)
{
  kcg_int32 tmp;

  /* _L7= */
  switch (protocol_19) {
    case kcg_lit_uint8(1) :
      tmp = kcg_lit_int32(36);
      break;
    case kcg_lit_uint8(6) :
      tmp = kcg_lit_int32(40);
      break;
    case kcg_lit_uint8(17) :
      tmp = kcg_lit_int32(28);
      break;
    default :
      tmp = len_19 + kcg_lit_int32(1);
      break;
  }
  *matching_19 = /* _L15= */(kcg_uint8)
      ((*msg_19)[0][2] >> kcg_lit_uint32(8)) == protocol_19 && len_19 >= tmp;
  /* _L20= */
  if (*matching_19) {
    *ioo_19 = i_19;
  }
  else {
    *ioo_19 = acc_19;
  }
  *goOn_19 = !*matching_19;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** filter1_tindyguard_data_19.c
** Generation date: 2019-10-22T14:01:49
*************************************************************$ */

