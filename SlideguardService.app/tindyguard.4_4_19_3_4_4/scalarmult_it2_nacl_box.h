/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _scalarmult_it2_nacl_box_H_
#define _scalarmult_it2_nacl_box_H_

#include "kcg_types.h"
#include "S_nacl_op.h"
#include "A_nacl_op.h"
#include "M_nacl_op.h"
#include "Z_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::box::scalarmult_it2/ */
extern void scalarmult_it2_nacl_box(
  /* i/ */
  kcg_uint8 i,
  /* akku/ */
  scalMulAcc_nacl_box *akku,
  /* b/ */
  kcg_uint8 b,
  /* x/ */
  gf_nacl_op *x,
  /* akkoo/ */
  scalMulAcc_nacl_box *akkoo);



#endif /* _scalarmult_it2_nacl_box_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarmult_it2_nacl_box.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

