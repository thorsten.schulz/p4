/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _check_tindyguard_handshake_19_H_
#define _check_tindyguard_handshake_19_H_

#include "kcg_types.h"
#include "cmp_M_uint32_4.h"
#include "hash128_hash_blake2s_4.h"
#include "hash128_hash_blake2s_3.h"
#include "hash128_hash_blake2s_2.h"
#include "dejavu_check_tindyguard_handshake_16_4.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_dejavu_check_tindyguard_handshake_16_4 /* IfBlock2:else:else:then:_L3=(tindyguard::handshake::dejavu_check#6)/ */ Context_dejavu_check_6;
  outC_dejavu_check_tindyguard_handshake_16_4 /* IfBlock2:else:then:_L3=(tindyguard::handshake::dejavu_check#7)/ */ Context_dejavu_check_7;
  /* ----------------- no clocks of observable data ------------------ */
} outC_check_tindyguard_handshake_19;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::handshake::check/ */
extern void check_tindyguard_handshake_19(
  /* length/ */
  length_t_udp length_19,
  /* hdrmsg/ */
  array_uint32_16_20 *hdrmsg_19,
  /* endpoint/ */
  peer_t_udp *endpoint_19,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_19,
  /* requireMac2/ */
  kcg_bool requireMac2_19,
  /* lengthoo/ */
  length_t_udp *lengthoo_19,
  /* our/ */
  kcg_uint32 *our_19,
  outC_check_tindyguard_handshake_19 *outC);

extern void check_reset_tindyguard_handshake_19(
  outC_check_tindyguard_handshake_19 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void check_init_tindyguard_handshake_19(
  outC_check_tindyguard_handshake_19 *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _check_tindyguard_handshake_19_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** check_tindyguard_handshake_19.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

