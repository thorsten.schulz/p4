/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _check_ip_19_H_
#define _check_ip_19_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "ipsum_BEH_ip_19.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ip::check/ */
extern void check_ip_19(
  /* len/ */
  length_t_udp len_19,
  /* msg/ */
  array_uint32_16_19 *msg_19,
  /* expect/ */
  range_t_udp *expect_19,
  /* in/ */
  header_ip *in_19,
  /* checksum/ */
  kcg_bool checksum_19,
  /* payloadlen/ */
  length_t_udp *payloadlen_19,
  /* src/ */
  IpAddress_udp *src_19,
  /* failed/ */
  kcg_bool *failed_19);



#endif /* _check_ip_19_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** check_ip_19.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

