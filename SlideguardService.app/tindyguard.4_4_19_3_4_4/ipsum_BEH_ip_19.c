/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "ipsum_BEH_ip_19.h"

/* ip::ipsum_BEH/ */
kcg_uint32 ipsum_BEH_ip_19(
  /* _L1/, d/ */
  array_uint32_16_19 *d_19,
  /* _L2/, size/ */
  length_t_udp size_19,
  /* _L3/, offs/ */
  length_t_udp offs_19)
{
  kcg_size idx;
  /* @2/IfBlock2: */
  kcg_bool IfBlock2_clock_sum_chunk_it_1_sum_data_it_1;
  /* @2/IfBlock1: */
  kcg_bool IfBlock1_clock_sum_chunk_it_1_sum_data_it_1;
  /* @2/_L22/, @2/d1/ */
  kcg_uint32 _L22_sum_chunk_it_1_sum_data_it_1;
  kcg_size idx_sum_data_it_1;
  kcg_bool cond_iterw_sum_data_it_1;
  kcg_uint32 acc_sum_data_it_1;
  kcg_uint32 tmp;
  /* _L5/ */
  kcg_uint32 _L5_19;
  /* _L14/ */
  kcg_uint32 _L14_19;
  /* @2/IfBlock1:then:_L40/, @2/IfBlock1:then:_L41/, _L22/ */
  kcg_uint32 _L22_19;
  /* _L28/, sum/ */
  kcg_uint32 sum_19;

  _L5_19 = kcg_lit_uint32(0);
  /* _L6= */
  for (idx = 0; idx < 19; idx++) {
    /* @1/_L8= */
    if (offs_19 < (/* _L6= */(kcg_int32) idx + kcg_lit_int32(1)) *
      chunkBytes_udp) {
      /* @1/_L8= */
      for (idx_sum_data_it_1 = 0; idx_sum_data_it_1 < 16; idx_sum_data_it_1++) {
        acc_sum_data_it_1 = _L5_19;
        IfBlock2_clock_sum_chunk_it_1_sum_data_it_1 = (offs_19 + size_19 -
            kcg_lit_int32(1)) / kcg_lit_int32(4) == /* @1/_L8= */(kcg_int32)
            idx_sum_data_it_1 + /* _L6= */(kcg_int32) idx * chunkLength_udp;
        /* @2/_L22= */
        if (offs_19 != /* @1/_L8= */(kcg_int32) idx_sum_data_it_1 *
          kcg_lit_int32(4) + /* _L6= */(kcg_int32) idx * chunkBytes_udp +
          kcg_lit_int32(2)) {
          _L22_sum_chunk_it_1_sum_data_it_1 = (*d_19)[idx][idx_sum_data_it_1];
        }
        else {
          _L22_sum_chunk_it_1_sum_data_it_1 = (*d_19)[idx][idx_sum_data_it_1] &
            kcg_lit_uint32(4294901760);
        }
        /* @2/IfBlock2: */
        if (IfBlock2_clock_sum_chunk_it_1_sum_data_it_1) {
          cond_iterw_sum_data_it_1 = kcg_false;
          /* @2/IfBlock2:then:_L2= */
          switch ((offs_19 + size_19) % kcg_lit_int32(4)) {
            case kcg_lit_int32(1) :
              _L14_19 = kcg_lit_uint32(255);
              break;
            case kcg_lit_int32(2) :
              _L14_19 = kcg_lit_uint32(65535);
              break;
            case kcg_lit_int32(3) :
              _L14_19 = kcg_lit_uint32(16777215);
              break;
            default :
              _L14_19 = kcg_lit_uint32(4294967295);
              break;
          }
          tmp = _L22_sum_chunk_it_1_sum_data_it_1 & _L14_19;
        }
        else {
          cond_iterw_sum_data_it_1 = kcg_true;
          tmp = _L22_sum_chunk_it_1_sum_data_it_1;
        }
        IfBlock1_clock_sum_chunk_it_1_sum_data_it_1 = offs_19 <
          (/* @1/_L8= */(kcg_int32) idx_sum_data_it_1 + kcg_lit_int32(1)) *
          kcg_lit_int32(4) + /* _L6= */(kcg_int32) idx * chunkBytes_udp;
        /* @2/IfBlock1: */
        if (IfBlock1_clock_sum_chunk_it_1_sum_data_it_1) {
          _L22_19 = tmp >> kcg_lit_uint32(8);
          _L5_19 = ((kcg_lsl_uint32(tmp, kcg_lit_uint32(8)) & kcg_lit_uint32(
                  0xFF00)) | (_L22_19 & kcg_lit_uint32(0xFF))) +
            acc_sum_data_it_1 + ((_L22_19 & kcg_lit_uint32(0xFF00)) | ((tmp >>
                  kcg_lit_uint32(24)) & kcg_lit_uint32(0xFF)));
        }
        else {
          _L5_19 = acc_sum_data_it_1;
        }
        /* @1/_L8= */
        if (!cond_iterw_sum_data_it_1) {
          break;
        }
      }
    }
    /* _L6= */
    if (!(size_19 + offs_19 > (/* _L6= */(kcg_int32) idx + kcg_lit_int32(1)) *
        chunkBytes_udp)) {
      break;
    }
  }
  _L14_19 = (kcg_lit_uint32(65535) & _L5_19) + (_L5_19 >> kcg_lit_uint32(16));
  /* _L22= */
  if (_L14_19 > kcg_lit_uint32(65535)) {
    _L22_19 = _L14_19 - kcg_lit_uint32(65535);
  }
  else {
    _L22_19 = _L14_19;
  }
  /* _L18= */
  if (kcg_lit_uint32(65535) == _L22_19) {
    tmp = _L22_19;
  }
  else {
    tmp = _L22_19 ^ kcg_lit_uint32(65535);
  }
  sum_19 = /* _L28=(sys::hton#1)/ */ htonl_sys_specialization(tmp);
  return sum_19;
}

/*
  Expanded instances for: ip::ipsum_BEH/
  @1: (ip::sum_data_it#1)
  @2: @1/(ip::sum_chunk_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** ipsum_BEH_ip_19.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

