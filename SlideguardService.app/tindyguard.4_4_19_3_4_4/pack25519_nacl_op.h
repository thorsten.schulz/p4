/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _pack25519_nacl_op_H_
#define _pack25519_nacl_op_H_

#include "kcg_types.h"
#include "pack25519_it1_nacl_op.h"
#include "pack25519_it2_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::pack25519/ */
extern void pack25519_nacl_op(
  /* @1/_L1/, @1/a/, _L2/, i/ */
  gf_nacl_op *i,
  /* _L5/, o/ */
  array_uint32_8 *o);

/*
  Expanded instances for: nacl::op::pack25519/
  @1: (nacl::op::car25519#1)
*/

#endif /* _pack25519_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack25519_nacl_op.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

