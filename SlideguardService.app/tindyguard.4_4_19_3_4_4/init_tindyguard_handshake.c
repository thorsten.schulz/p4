/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "init_tindyguard_handshake.h"

/* tindyguard::handshake::init/ */
void init_tindyguard_handshake(
  /* peer/ */
  Peer_tindyguardTypes *peer,
  /* pid/ */
  size_tindyguardTypes pid,
  /* sks/ */
  KeySalted_tindyguardTypes *sks,
  /* now/ */
  kcg_int64 now,
  /* slength/ */
  length_t_udp *slength,
  /* initmsg/ */
  array_uint32_16_4 *initmsg,
  /* soo/ */
  Session_tindyguardTypes *soo,
  /* again/ */
  kcg_bool *again,
  outC_init_tindyguard_handshake *outC)
{
  kcg_bool tmp;
  array_uint32_16 tmp1;
  HashChunk_hash_blake2s tmp2;
  array_uint32_16_1 tmp3;
  array_uint32_16 tmp4;
  array_uint32_16_1 tmp5;
  array_uint32_16 tmp6;
  array_uint32_16_1 tmp7;
  array_uint32_16 tmp8;
  array_uint32_16_2 tmp9;
  array_uint32_16 tmp10;
  array_uint32_16 tmp11;
  array_uint8_4_4 tmp12;
  kcg_size idx;
  array_uint32_16 tmp13;
  array_uint32_16_1 tmp14;
  array_uint32_16 tmp15;
  array_uint32_16_1 tmp16;
  array_uint32_16 tmp17;
  array_uint8_4_4 tmp18;
  array_uint8_4_4 tmp19;
  array_uint32_16_3 tmp20;
  array_uint32_16_4 tmp21;
  array_uint32_16 tmp22;
  array_uint8_4_4 tmp23;
  array_uint32_16 tmp24;
  /* @1/_L155/, @1/_L316/, @1/_L320/, @1/m_mac1/ */
  Mac_hash_blake2s _L316_build_init_2;
  /* @1/_L152/, @1/_L305/, @1/_L308/, @1/_L312/, @1/_L321/, @1/m_pt1/ */
  array_uint32_32 _L321_build_init_2;
  /* @1/_L323/, @1/m_pt2/ */
  array_uint32_16_4 _L323_build_init_2;
  /* @1/_L281/ */
  kcg_uint32 _L281_build_init_2;
  /* @1/_L282/ */
  kcg_uint32 _L282_build_init_2;
  /* @1/_L199/ */
  array_uint32_3 _L199_build_init_2;
  /* @1/_L74/ */
  array_uint32_16_1 _L74_build_init_2;
  /* @1/_L56/ */
  array_uint32_3 _L56_build_init_2;
  /* @1/_L48/ */
  array_uint32_16_1 _L48_build_init_2;
  /* @1/_L247/, @1/_L264/, @1/_L57/, @1/c_static/ */
  array_uint32_8 c_static_build_init_2;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_build_init_2;
  /* @1/IfBlock1:then:_L6/ */
  Mac_hash_blake2s _L6_build_init_2_then_IfBlock1;
  kcg_bool op_call_ephemeral_1_build_init_2;
  /* @2/IfBlock1:else:_L6/ */
  kcg_bool _L6_ephemeral_1_build_init_2_else_IfBlock1;
  /* @2/IfBlock1:else:_L1/ */
  kcg_bool _L1_ephemeral_1_build_init_2_else_IfBlock1;
  /* @2/IfBlock1:else:_L2/ */
  array_uint32_8 _L2_ephemeral_1_build_init_2_else_IfBlock1;
  /* @1/_L253/,
     @1/_L263/,
     @1/_L47/,
     @1/a_static/,
     @4/_L1/,
     @4/a/,
     @7/_L1/,
     @7/a/ */
  Mac_nacl_onetime _L1_ldAuth_2_build_init_2;
  /* @1/_L255/,
     @1/_L266/,
     @1/_L73/,
     @1/a_TAI/,
     @11/_L1/,
     @11/a/,
     @9/_L1/,
     @9/a/ */
  Mac_nacl_onetime _L1_ldAuth_1_build_init_2;
  /* @6/_L6/ */
  array_uint32_8_2 _L6_DHDerive_2_build_init_2;
  /* @6/_L5/ */
  array_uint32_16 _L5_DHDerive_2_build_init_2;
  /* @1/_L169/, @6/_L3/, @6/fail/ */
  kcg_bool _L3_DHDerive_2_build_init_2;
  /* @6/_L14/ */
  array_uint32_16_1 _L14_DHDerive_2_build_init_2;
  /* @1/_L243/,
     @1/_L268/,
     @1/ek/,
     @2/IfBlock1:else:_L4/,
     @2/kp/,
     @3/_L9/,
     @3/sk/ */
  KeyPair32_slideTypes _L9_DHDerive_1_build_init_2;
  /* @3/_L6/ */
  array_uint32_8_2 _L6_DHDerive_1_build_init_2;
  /* @3/_L5/ */
  array_uint32_16 _L5_DHDerive_1_build_init_2;
  /* @1/_L164/, @3/_L3/, @3/fail/, _L2/, fail/ */
  kcg_bool _L3_DHDerive_1_build_init_2;
  /* @3/_L14/ */
  array_uint32_16_1 _L14_DHDerive_1_build_init_2;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock;
  /* _L3/ */
  array_uint32_1 _L3;

  /* _L2=(sys::random#1)/ */
  random01_sys_specialization(&_L3_DHDerive_1_build_init_2, &_L3);
  IfBlock1_clock = !_L3_DHDerive_1_build_init_2 && pid >
    InvalidPeer_tindyguardTypes;
  /* IfBlock1: */
  if (IfBlock1_clock) {
    kcg_copy_StreamChunk_slideTypes(
      &_L323_build_init_2[3],
      (array_uint32_16 *) &ZeroChunk_slideTypes);
    kcg_copy_StreamChunk_slideTypes(&tmp20[0], &(*peer).hcache.salt);
    _L321_build_init_2[0] = MsgType_Initiation_tindyguard;
    _L321_build_init_2[1] = _L3[0];
    /* @2/IfBlock1:else:_L1=(sys::random#1)/ */
    random08_sys_specialization(
      &_L1_ephemeral_1_build_init_2_else_IfBlock1,
      &_L2_ephemeral_1_build_init_2_else_IfBlock1);
    _L6_ephemeral_1_build_init_2_else_IfBlock1 =
      !_L1_ephemeral_1_build_init_2_else_IfBlock1;
    /* @6/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &(*sks).key,
      &(*peer).tpub,
      kcg_true,
      &_L3_DHDerive_2_build_init_2,
      (Key32_slideTypes *) &_L5_DHDerive_2_build_init_2[0]);
    if (_L6_ephemeral_1_build_init_2_else_IfBlock1) {
      /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
      keyPair_nacl_box_20(
        &_L2_ephemeral_1_build_init_2_else_IfBlock1,
        &op_call_ephemeral_1_build_init_2,
        &_L9_DHDerive_1_build_init_2,
        &tmp,
        &outC->Context_keyPair_2_ephemeral_1_build_init_2);
    }
    else {
      kcg_copy_KeyPair32_slideTypes(
        &_L9_DHDerive_1_build_init_2,
        (KeyPair32_slideTypes *) &ZeroKeyPair_slideTypes);
      tmp = kcg_true;
    }
    /* @3/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &_L9_DHDerive_1_build_init_2,
      &(*peer).tpub,
      kcg_true,
      &_L3_DHDerive_1_build_init_2,
      (Key32_slideTypes *) &_L5_DHDerive_1_build_init_2[0]);
    *again = _L3_DHDerive_1_build_init_2 || _L3_DHDerive_2_build_init_2 || tmp;
    kcg_copy_Hash_hash_blake2s(&tmp1[0], &(*peer).hcache.hash1);
    kcg_copy_Key32_slideTypes(&tmp1[8], &_L9_DHDerive_1_build_init_2.pk_y);
    /* @1/_L20=(hash::blake2s::single#2)/ */
    single_hash_blake2s(
      &tmp1,
      kcg_lit_int32(64),
      (Hash_hash_blake2s *) &tmp10[0]);
    kcg_copy_Key32_slideTypes(&tmp4[0], &_L9_DHDerive_1_build_init_2.pk_y);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_1_build_init_2[idx + 8] = kcg_lit_uint32(0);
      tmp4[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp3[0], &tmp4);
    /* @1/_L16=(hash::blake2s::hkdf_cKonly#1)/ */
    hkdf_cKonly_hash_blake2s_1(
      &tmp3,
      kcg_lit_int32(32),
      (HashChunk_hash_blake2s *) &CONSTRUCTION_chunk_tindyguard,
      &tmp2);
    kcg_copy_array_uint32_16(
      &_L14_DHDerive_1_build_init_2[0],
      &_L5_DHDerive_1_build_init_2);
    /* @3/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_1_build_init_2,
      kcg_lit_int32(32),
      &tmp2,
      &_L6_DHDerive_1_build_init_2);
    kcg_copy_Key32_slideTypes(&tmp6[0], &(*sks).key.pk_y);
    for (idx = 0; idx < 8; idx++) {
      tmp6[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp5[0], &tmp6);
    kcg_copy_array_uint32_8(&tmp8[0], (array_uint32_8 *) &tmp10[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp8[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp7[0], &tmp8);
    for (idx = 0; idx < 3; idx++) {
      _L56_build_init_2[idx] = kcg_lit_uint32(0);
    }
    /* @1/_L47=(nacl::box::aead#1)/ */
    aead_nacl_box_1_1_20(
      &tmp5,
      kcg_lit_int32(32),
      &tmp7,
      kcg_lit_int32(32),
      &_L56_build_init_2,
      &_L6_DHDerive_1_build_init_2[1],
      &_L1_ldAuth_2_build_init_2,
      &_L48_build_init_2);
    kcg_copy_array_uint32_8(
      &c_static_build_init_2,
      (array_uint32_8 *) &_L48_build_init_2[0][0]);
    kcg_copy_array_uint8_4(
      &tmp12[0],
      (array_uint8_4 *) &_L1_ldAuth_2_build_init_2[0]);
    kcg_copy_array_uint8_4(&tmp18[0], &tmp12[0]);
    kcg_copy_array_uint8_4(
      &tmp12[1],
      (array_uint8_4 *) &_L1_ldAuth_2_build_init_2[4]);
    kcg_copy_array_uint8_4(&tmp18[1], &tmp12[1]);
    kcg_copy_array_uint8_4(
      &tmp12[2],
      (array_uint8_4 *) &_L1_ldAuth_2_build_init_2[8]);
    kcg_copy_array_uint8_4(&tmp18[2], &tmp12[2]);
    kcg_copy_array_uint8_4(
      &tmp12[3],
      (array_uint8_4 *) &_L1_ldAuth_2_build_init_2[12]);
    kcg_copy_array_uint8_4(&tmp18[3], &tmp12[3]);
    kcg_copy_array_uint32_8(&tmp10[8], &c_static_build_init_2);
    kcg_copy_array_uint32_16(&tmp9[0], &tmp10);
    /* @4/_L24= */
    for (idx = 0; idx < 4; idx++) {
      tmp11[idx] = /* @5/_L14= */(kcg_uint32) tmp12[idx][0] | kcg_lsl_uint32(
          /* @5/_L15= */(kcg_uint32) tmp12[idx][1],
          kcg_lit_uint32(8)) | kcg_lsl_uint32(
          /* @5/_L16= */(kcg_uint32) tmp12[idx][2],
          kcg_lit_uint32(16)) | kcg_lsl_uint32(
          /* @5/_L17= */(kcg_uint32) tmp12[idx][3],
          kcg_lit_uint32(24));
    }
    for (idx = 0; idx < 12; idx++) {
      tmp11[idx + 4] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp9[1], &tmp11);
    /* @1/_L34=(hash::blake2s::hash#1)/ */
    hash_hash_blake2s_2(
      &tmp9,
      kcg_lit_int32(80),
      kcg_lit_int32(0),
      (Hash_hash_blake2s *) &tmp22[0]);
    /* @1/_L280=(sys::tai#1)/ */
    tai_sys(kcg_true, &tmp15[0], &_L281_build_init_2, &_L282_build_init_2);
    tmp15[1] = _L281_build_init_2;
    tmp15[2] = _L282_build_init_2;
    kcg_copy_Hash_hash_blake2s(&tmp13[0], &_L6_DHDerive_1_build_init_2[0]);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_2_build_init_2[idx + 8] = kcg_lit_uint32(0);
      tmp13[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(
      &_L14_DHDerive_2_build_init_2[0],
      &_L5_DHDerive_2_build_init_2);
    /* @6/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_2_build_init_2,
      kcg_lit_int32(32),
      &tmp13,
      &_L6_DHDerive_2_build_init_2);
    for (idx = 0; idx < 13; idx++) {
      tmp15[idx + 3] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp14[0], &tmp15);
    kcg_copy_array_uint32_8(&tmp17[0], (array_uint32_8 *) &tmp22[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp17[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp16[0], &tmp17);
    for (idx = 0; idx < 3; idx++) {
      _L199_build_init_2[idx] = kcg_lit_uint32(0);
    }
    /* @1/_L73=(nacl::box::aead#2)/ */
    aead_nacl_box_1_1_20(
      &tmp14,
      kcg_lit_int32(12),
      &tmp16,
      kcg_lit_int32(32),
      &_L199_build_init_2,
      &_L6_DHDerive_2_build_init_2[1],
      &_L1_ldAuth_1_build_init_2,
      &_L74_build_init_2);
    kcg_copy_array_uint32_3(&tmp22[8], (array_uint32_3 *) &_L74_build_init_2[0][0]);
    kcg_copy_array_uint8_4(
      &tmp19[0],
      (array_uint8_4 *) &_L1_ldAuth_1_build_init_2[0]);
    kcg_copy_array_uint8_4(&tmp23[0], &tmp19[0]);
    kcg_copy_array_uint8_4(
      &tmp19[1],
      (array_uint8_4 *) &_L1_ldAuth_1_build_init_2[4]);
    kcg_copy_array_uint8_4(&tmp23[1], &tmp19[1]);
    kcg_copy_array_uint8_4(
      &tmp19[2],
      (array_uint8_4 *) &_L1_ldAuth_1_build_init_2[8]);
    kcg_copy_array_uint8_4(&tmp23[2], &tmp19[2]);
    kcg_copy_array_uint8_4(
      &tmp19[3],
      (array_uint8_4 *) &_L1_ldAuth_1_build_init_2[12]);
    kcg_copy_array_uint8_4(&tmp23[3], &tmp19[3]);
    kcg_copy_Key32_slideTypes(
      &_L321_build_init_2[2],
      &_L9_DHDerive_1_build_init_2.pk_y);
    kcg_copy_array_uint32_8(&_L321_build_init_2[10], &c_static_build_init_2);
    /* @7/_L24= */
    for (idx = 0; idx < 4; idx++) {
      _L321_build_init_2[idx + 18] = /* @8/_L14= */(kcg_uint32) tmp18[idx][0] |
        kcg_lsl_uint32(/* @8/_L15= */(kcg_uint32) tmp18[idx][1], kcg_lit_uint32(8)) |
        kcg_lsl_uint32(/* @8/_L16= */(kcg_uint32) tmp18[idx][2], kcg_lit_uint32(16)) |
        kcg_lsl_uint32(/* @8/_L17= */(kcg_uint32) tmp18[idx][3], kcg_lit_uint32(24));
    }
    kcg_copy_array_uint32_3(&_L321_build_init_2[22], (array_uint32_3 *) &tmp22[8]);
    /* @9/_L24= */
    for (idx = 0; idx < 4; idx++) {
      _L321_build_init_2[idx + 25] = /* @10/_L14= */(kcg_uint32) tmp19[idx][0] |
        kcg_lsl_uint32(/* @10/_L15= */(kcg_uint32) tmp19[idx][1], kcg_lit_uint32(8)) |
        kcg_lsl_uint32(/* @10/_L16= */(kcg_uint32) tmp19[idx][2], kcg_lit_uint32(16)) |
        kcg_lsl_uint32(/* @10/_L17= */(kcg_uint32) tmp19[idx][3], kcg_lit_uint32(24));
    }
    for (idx = 0; idx < 3; idx++) {
      _L321_build_init_2[idx + 29] = kcg_lit_uint32(0);
    }
    kcg_copy_StreamChunk_slideTypes(
      &tmp20[2],
      (StreamChunk_slideTypes *) &_L321_build_init_2[16]);
    kcg_copy_StreamChunk_slideTypes(
      &tmp20[1],
      (StreamChunk_slideTypes *) &_L321_build_init_2[0]);
    kcg_copy_StreamChunk_slideTypes(&_L323_build_init_2[0], &tmp20[1]);
    /* @1/_L155=(hash::blake2s::hash128#1)/ */
    hash128_hash_blake2s_3(
      &tmp20,
      kcg_lit_int32(116),
      kcg_lit_int32(32),
      &_L316_build_init_2);
    IfBlock1_clock_build_init_2 = now < (*peer).cookie.opened +
      cookieEdible_tindyguard_conf;
    kcg_copy_array_uint32_13(
      &_L323_build_init_2[1][0],
      (array_uint32_13 *) &_L321_build_init_2[16]);
    kcg_copy_array_uint32_3(
      &_L323_build_init_2[1][13],
      (array_uint32_3 *) &_L316_build_init_2[0]);
    kcg_copy_StreamChunk_slideTypes(
      &_L323_build_init_2[2],
      (array_uint32_16 *) &ZeroChunk_slideTypes);
    _L323_build_init_2[2][0] = _L316_build_init_2[3];
    /* @1/_L289= */
    if (*again) {
      *slength = kcg_lit_int32(-1);
    }
    else {
      *slength = kcg_lit_int32(148);
    }
    /* @1/IfBlock1: */
    if (IfBlock1_clock_build_init_2) {
      kcg_copy_StreamChunk_slideTypes(&tmp21[0], &(*peer).cookie.content);
      kcg_copy_StreamChunk_slideTypes(&tmp21[1], &_L323_build_init_2[0]);
      kcg_copy_StreamChunk_slideTypes(&tmp21[2], &_L323_build_init_2[1]);
      kcg_copy_StreamChunk_slideTypes(&tmp21[3], &_L323_build_init_2[2]);
      /* @1/IfBlock1:then:_L6=(hash::blake2s::hash128#2)/ */
      hash128_hash_blake2s_4(
        &tmp21,
        kcg_lit_int32(132),
        kcg_lit_int32(32),
        &_L6_build_init_2_then_IfBlock1);
      kcg_copy_array_uint32_16_4(initmsg, &_L323_build_init_2);
      (*initmsg)[2][1] = _L6_build_init_2_then_IfBlock1[0];
      (*initmsg)[2][2] = _L6_build_init_2_then_IfBlock1[1];
      (*initmsg)[2][3] = _L6_build_init_2_then_IfBlock1[2];
      (*initmsg)[2][4] = _L6_build_init_2_then_IfBlock1[3];
    }
    else {
      kcg_copy_array_uint32_16_4(initmsg, &_L323_build_init_2);
    }
    kcg_copy_Session_tindyguardTypes(
      soo,
      (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
    kcg_copy_KeyPair32_slideTypes(
      &(*soo).handshake.ephemeral,
      &_L9_DHDerive_1_build_init_2);
    /* @11/_L24= */
    for (idx = 0; idx < 4; idx++) {
      tmp22[idx + 11] = /* @12/_L14= */(kcg_uint32) tmp23[idx][0] |
        kcg_lsl_uint32(/* @12/_L15= */(kcg_uint32) tmp23[idx][1], kcg_lit_uint32(8)) |
        kcg_lsl_uint32(/* @12/_L16= */(kcg_uint32) tmp23[idx][2], kcg_lit_uint32(16)) |
        kcg_lsl_uint32(/* @12/_L17= */(kcg_uint32) tmp23[idx][3], kcg_lit_uint32(24));
    }
    tmp22[15] = kcg_lit_uint32(0);
    /* @1/_L66=(hash::blake2s::single#3)/ */
    single_hash_blake2s(&tmp22, kcg_lit_int32(60), &(*soo).handshake.ihash);
    kcg_copy_Hash_hash_blake2s(&tmp24[0], &_L6_DHDerive_2_build_init_2[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp24[idx + 8] = kcg_lit_uint32(0);
      (*soo).handshake.their[idx] = kcg_lit_uint32(0);
    }
    kcg_copy_HashChunk_hash_blake2s(&(*soo).handshake.chainingKey, &tmp24);
    (*soo).pid = pid;
    kcg_copy_Peer_tindyguardTypes(&(*soo).peer, peer);
    (*soo).our = _L3[0];
  }
  else {
    *again = kcg_false;
    *slength = nil_udp;
    for (idx = 0; idx < 4; idx++) {
      kcg_copy_StreamChunk_slideTypes(
        &(*initmsg)[idx],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
    kcg_copy_Session_tindyguardTypes(
      soo,
      (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
  }
}

#ifndef KCG_USER_DEFINED_INIT
void init_init_tindyguard_handshake(outC_init_tindyguard_handshake *outC)
{
  /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
  keyPair_init_nacl_box_20(&outC->Context_keyPair_2_ephemeral_1_build_init_2);
}
#endif /* KCG_USER_DEFINED_INIT */


void init_reset_tindyguard_handshake(outC_init_tindyguard_handshake *outC)
{
  /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
  keyPair_reset_nacl_box_20(&outC->Context_keyPair_2_ephemeral_1_build_init_2);
}

/*
  Expanded instances for: tindyguard::handshake::init/
  @1: (tindyguard::handshake::build_init#2)
  @3: @1/(tindyguard::handshake::DHDerive#1)
  @4: @1/(slideTypes::ldAuth#2)
  @5: @4/(slideTypes::ld32x1#1)
  @6: @1/(tindyguard::handshake::DHDerive#2)
  @7: @1/(slideTypes::ldAuth#3)
  @8: @7/(slideTypes::ld32x1#1)
  @9: @1/(slideTypes::ldAuth#4)
  @10: @9/(slideTypes::ld32x1#1)
  @11: @1/(slideTypes::ldAuth#1)
  @12: @11/(slideTypes::ld32x1#1)
  @2: @1/(tindyguard::handshake::ephemeral#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** init_tindyguard_handshake.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

