/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _aeadOpen_nacl_box_1_1_20_H_
#define _aeadOpen_nacl_box_1_1_20_H_

#include "kcg_types.h"
#include "cmp_M_uint8_16.h"
#include "setup_nacl_core_chacha_20.h"
#include "auth2_nacl_onetime_1_1.h"
#include "chacha_IETF_nacl_box_stream_1_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::box::aeadOpen/ */
extern void aeadOpen_nacl_box_1_1_20(
  /* @1/_L43/, @1/a/, _L5/, a/ */
  Mac_nacl_onetime *a_1_1_20,
  /* @1/_L44/, @1/msg2/, _L13/, _L7/, cm/ */
  array_uint32_16_1 *cm_1_1_20,
  /* @1/_L45/, @1/mlen2/, _L12/, _L6/, mlen/ */
  int_slideTypes mlen_1_1_20,
  /* @1/_L49/, @1/msg1/, _L18/, ad/ */
  array_uint32_16_1 *ad_1_1_20,
  /* @1/_L50/, @1/mlen1/, _L19/, adlen/ */
  int_slideTypes adlen_1_1_20,
  /* _L2/, nonce/ */
  Nonce_nacl_core_chacha *nonce_1_1_20,
  /* _L3/, key/ */
  Key_nacl_core *key_1_1_20,
  /* _L8/, msg/ */
  array_uint32_16_1 *msg_1_1_20,
  /* @1/_L47/, @1/failed/, _L4/, failed/ */
  kcg_bool *failed_1_1_20);

/*
  Expanded instances for: nacl::box::aeadOpen/
  @1: (nacl::onetime::verify2#1)
*/

#endif /* _aeadOpen_nacl_box_1_1_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** aeadOpen_nacl_box_1_1_20.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

