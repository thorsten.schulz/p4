/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "addrsFind_udp.h"

/* udp::addrsFind/ */
void addrsFind_udp(
  /* _L18/, addrList/ */
  _2_array *addrList,
  /* _L15/, _L21/, net/ */
  range_t_udp *net,
  /* _L24/, found/ */
  kcg_bool *found,
  /* _L14/, addr/ */
  IpAddress_udp *addr)
{
  IpAddress_udp acc;
  kcg_size idx;
  /* @1/_L10/, @2/_L10/, @2/o/ */
  kcg_bool _L10_addrsFind_it_1;

  kcg_copy_IpAddress_udp(addr, (IpAddress_udp *) &ipAny_udp);
  /* _L11= */
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_IpAddress_udp(&acc, addr);
    _L10_addrsFind_it_1 = (*addrList)[idx].ip.addr != kcg_lit_uint32(0) &&
      kcg_lit_uint32(0) == (((*addrList)[idx].ip.addr ^ (*net).net.addr) &
        (*net).mask.addr) && kcg_lit_uint32(0) != (*net).net.addr;
    /* @1/_L13= */
    if (_L10_addrsFind_it_1) {
      kcg_copy_IpAddress_udp(addr, &(*addrList)[idx].ip);
    }
    else {
      kcg_copy_IpAddress_udp(addr, &acc);
    }
    /* _L11= */
    if (!!_L10_addrsFind_it_1) {
      break;
    }
  }
  *found = !kcg_comp_IpAddress_udp(addr, (IpAddress_udp *) &ipAny_udp) ||
    kcg_comp_IpAddress_udp((IpAddress_udp *) &ipAny_udp, &(*net).net);
}

/*
  Expanded instances for: udp::addrsFind/
  @1: (udp::addrsFind_it#1)
  @2: @1/(udp::within#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** addrsFind_udp.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

