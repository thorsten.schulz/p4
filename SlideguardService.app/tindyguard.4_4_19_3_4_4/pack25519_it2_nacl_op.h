/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _pack25519_it2_nacl_op_H_
#define _pack25519_it2_nacl_op_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::pack25519_it2/ */
extern kcg_uint32 pack25519_it2_nacl_op(
  /* i/ */
  kcg_uint32 i,
  /* _L28/, t/ */
  gf_nacl_op *t);



#endif /* _pack25519_it2_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack25519_it2_nacl_op.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

