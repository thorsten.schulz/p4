/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _updatePeer_tindyguard_session_3_H_
#define _updatePeer_tindyguard_session_3_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::updatePeer/ */
extern void updatePeer_tindyguard_session_3(
  /* _L23/, pid/ */
  size_tindyguardTypes pid_3,
  /* _L21/, knownPeer/ */
  Peer_tindyguardTypes *knownPeer_3,
  /* _L18/, isNewTAI/ */
  array_bool_3 *isNewTAI_3,
  /* _L19/, s/ */
  array *s_3,
  /* _L25/, t_flag/ */
  array_bool_3 *t_flag_3,
  /* _L22/, updatedPeer/ */
  Peer_tindyguardTypes *updatedPeer_3);



#endif /* _updatePeer_tindyguard_session_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** updatePeer_tindyguard_session_3.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

