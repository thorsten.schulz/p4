/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "M_nacl_op.h"

/* nacl::op::M/ */
void M_nacl_op(
  /* _L52/, a/ */
  gf_nacl_op *a,
  /* _L51/, _L54/, b/ */
  gf_nacl_op *b,
  /* @1/_L6/, @1/o/, _L53/, o/ */
  gf_nacl_op *o)
{
  kcg_size idx;
  kcg_size idx_M_it4_1_mapfoldi_1_car25519_7;
  /* @3/_L25/ */
  kcg_int32 _L25_M_it5_1_M_it4_1_mapfoldi_1_car25519_7;
  /* @2/_L23/ */
  kcg_int32 _L23_M_it4_1_mapfoldi_1_car25519_7;
  kcg_int64 tmp_mapfoldi_1_car25519_7;
  gf_nacl_op tmp;
  /* @6/_L4/, @9/_L4/ */
  kcg_uint64 _L4_srs16_1_car25519_it1_1_car25519_8;
  kcg_bool every_srs16_1_car25519_it1_1_car25519_8;
  /* @5/_L11/, @8/_L4/, @9/_L1/, @9/i/ */
  kcg_int64 _L4_car25519_it1_1_car25519_8;
  /* @5/_L4/, @6/_L1/, @6/i/, @8/_L11/ */
  kcg_int64 _L11_car25519_it1_1_car25519_8;
  kcg_int64 tmp_car25519_it1_1_car25519_8;
  /* @1/_L4/ */
  kcg_int64 _L4_car25519_8;
  /* @1/_L3/, @4/_L3/ */
  gf_nacl_op _L3_car25519_8;
  /* @4/_L4/ */
  kcg_int64 _L4_car25519_7;
  /* _L49/ */
  array_int64_31 _L49;

  /* _L50= */
  for (idx = 0; idx < 15; idx++) {
    _L49[idx] = (*b)[idx + 1] * kcg_lit_int64(38);
  }
  kcg_copy_gf_nacl_op(&_L49[15], b);
  _L4_car25519_7 = kcg_lit_int64(0);
  /* @4/_L4= */
  for (idx = 0; idx < 16; idx++) {
    _L23_M_it4_1_mapfoldi_1_car25519_7 = /* _L45= */(kcg_int32) idx +
      kcg_lit_int32(15);
    tmp_mapfoldi_1_car25519_7 = kcg_lit_int64(0);
    /* @2/_L12= */
    for (
      idx_M_it4_1_mapfoldi_1_car25519_7 = 0;
      idx_M_it4_1_mapfoldi_1_car25519_7 < 16;
      idx_M_it4_1_mapfoldi_1_car25519_7++) {
      _L25_M_it5_1_M_it4_1_mapfoldi_1_car25519_7 =
        _L23_M_it4_1_mapfoldi_1_car25519_7 - /* @2/_L12= */(kcg_int32)
          idx_M_it4_1_mapfoldi_1_car25519_7;
      if (kcg_lit_int32(0) <= _L25_M_it5_1_M_it4_1_mapfoldi_1_car25519_7 &&
        _L25_M_it5_1_M_it4_1_mapfoldi_1_car25519_7 < kcg_lit_int32(31)) {
        tmp_car25519_it1_1_car25519_8 =
          _L49[_L25_M_it5_1_M_it4_1_mapfoldi_1_car25519_7];
      }
      else {
        tmp_car25519_it1_1_car25519_8 = kcg_lit_int64(0);
      }
      tmp_mapfoldi_1_car25519_7 = tmp_mapfoldi_1_car25519_7 +
        (*a)[idx_M_it4_1_mapfoldi_1_car25519_7] * tmp_car25519_it1_1_car25519_8;
    }
    _L11_car25519_it1_1_car25519_8 = _L4_car25519_7 +
      tmp_mapfoldi_1_car25519_7 + kcg_lit_int64(0x10000);
    _L4_srs16_1_car25519_it1_1_car25519_8 = /* @6/_L2= */(kcg_uint64)
        _L11_car25519_it1_1_car25519_8 >> kcg_lit_uint64(16);
    every_srs16_1_car25519_it1_1_car25519_8 = _L11_car25519_it1_1_car25519_8 <
      kcg_lit_int64(0);
    _L3_car25519_8[idx] = _L11_car25519_it1_1_car25519_8 -
      /* @5/_L19= */(kcg_int64)
        (/* @5/_L20= */(kcg_uint64) _L11_car25519_it1_1_car25519_8 &
          kcg_lit_uint64(0xFFFFFFFFFFFF0000));
    if (every_srs16_1_car25519_it1_1_car25519_8) {
      _L4_car25519_it1_1_car25519_8 = /* @6/_L3= */(kcg_int64)
          (kcg_lit_uint64(0xFFFF000000000000) | _L4_srs16_1_car25519_it1_1_car25519_8) -
        kcg_lit_int64(1);
    }
    else {
      _L4_car25519_it1_1_car25519_8 = /* @6/_L3= */(kcg_int64)
          _L4_srs16_1_car25519_it1_1_car25519_8 - kcg_lit_int64(1);
    }
    /* @7/_L2= */
    if (kcg_lit_int32(15) == /* @4/_L4= */(kcg_int32) idx) {
      _L4_car25519_8 = kcg_lit_int64(1);
    }
    else {
      _L4_car25519_8 = kcg_lit_int64(0);
    }
    _L4_car25519_7 = _L4_car25519_it1_1_car25519_8 +
      _L4_car25519_it1_1_car25519_8 * kcg_lit_int64(37) * _L4_car25519_8;
  }
  kcg_copy_gf_nacl_op(&tmp, &_L3_car25519_8);
  tmp[0] = _L4_car25519_7 + _L3_car25519_8[0];
  _L4_car25519_8 = kcg_lit_int64(0);
  /* @1/_L4= */
  for (idx = 0; idx < 16; idx++) {
    _L4_car25519_it1_1_car25519_8 = _L4_car25519_8 + tmp[idx] + kcg_lit_int64(
        0x10000);
    _L4_srs16_1_car25519_it1_1_car25519_8 = /* @9/_L2= */(kcg_uint64)
        _L4_car25519_it1_1_car25519_8 >> kcg_lit_uint64(16);
    every_srs16_1_car25519_it1_1_car25519_8 = _L4_car25519_it1_1_car25519_8 <
      kcg_lit_int64(0);
    _L3_car25519_8[idx] = _L4_car25519_it1_1_car25519_8 -
      /* @8/_L19= */(kcg_int64)
        (/* @8/_L20= */(kcg_uint64) _L4_car25519_it1_1_car25519_8 &
          kcg_lit_uint64(0xFFFFFFFFFFFF0000));
    if (every_srs16_1_car25519_it1_1_car25519_8) {
      _L11_car25519_it1_1_car25519_8 = /* @9/_L3= */(kcg_int64)
          (kcg_lit_uint64(0xFFFF000000000000) | _L4_srs16_1_car25519_it1_1_car25519_8) -
        kcg_lit_int64(1);
    }
    else {
      _L11_car25519_it1_1_car25519_8 = /* @9/_L3= */(kcg_int64)
          _L4_srs16_1_car25519_it1_1_car25519_8 - kcg_lit_int64(1);
    }
    /* @10/_L2= */
    if (kcg_lit_int32(15) == /* @1/_L4= */(kcg_int32) idx) {
      tmp_car25519_it1_1_car25519_8 = kcg_lit_int64(1);
    }
    else {
      tmp_car25519_it1_1_car25519_8 = kcg_lit_int64(0);
    }
    _L4_car25519_8 = _L11_car25519_it1_1_car25519_8 +
      _L11_car25519_it1_1_car25519_8 * kcg_lit_int64(37) *
      tmp_car25519_it1_1_car25519_8;
  }
  kcg_copy_gf_nacl_op(o, &_L3_car25519_8);
  (*o)[0] = _L4_car25519_8 + _L3_car25519_8[0];
}

/*
  Expanded instances for: nacl::op::M/
  @2: (nacl::op::M_it4#1)
  @3: @2/(nacl::op::M_it5#1)
  @4: (nacl::op::car25519#7)
  @5: @4/(nacl::op::car25519_it1#1)
  @6: @5/(nacl::op::srs16#1)
  @7: @5/(math::BoolToInt64#1)
  @1: (nacl::op::car25519#8)
  @8: @1/(nacl::op::car25519_it1#1)
  @9: @8/(nacl::op::srs16#1)
  @10: @8/(math::BoolToInt64#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** M_nacl_op.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

