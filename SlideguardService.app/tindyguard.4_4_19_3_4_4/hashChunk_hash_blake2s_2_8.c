/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hashChunk_hash_blake2s_2_8.h"

/* hash::blake2s::hashChunk/ */
void hashChunk_hash_blake2s_2_8(
  /* _L64/, msg/ */
  array_uint32_16_2 *msg_2_8,
  /* _L66/, len/ */
  size_slideTypes len_2_8,
  /* _L70/, keybytes/ */
  size_slideTypes keybytes_2_8,
  /* _L65/, chunk/ */
  HashChunk_hash_blake2s *chunk_2_8)
{
  array_uint32_8 acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  /* _L61/ */
  array_uint32_8 _L61_2_8;

  _L61_2_8[0] = IV_hash_blake2s[0] ^ (kcg_lit_uint32(16842752) |
      (kcg_lit_uint32(32) | kcg_lsl_uint32(
          /* _L72= */(kcg_uint32) keybytes_2_8,
          kcg_lit_uint32(8))));
  kcg_copy_array_uint32_7(&_L61_2_8[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
  /* _L60= */
  for (idx = 0; idx < 2; idx++) {
    kcg_copy_array_uint32_8(&acc, &_L61_2_8);
    /* _L60=(hash::blake2s::stream_it#7)/ */
    stream_it_hash_blake2s(
      /* _L60= */(kcg_int32) idx,
      &acc,
      &(*msg_2_8)[idx],
      len_2_8,
      &cond_iterw,
      &_L61_2_8);
    /* _L60= */
    if (!cond_iterw) {
      break;
    }
  }
  kcg_copy_array_uint32_8(&(*chunk_2_8)[0], (array_uint32_8 *) &_L61_2_8[0]);
  for (idx = 0; idx < 8; idx++) {
    (*chunk_2_8)[idx + 8] = kcg_lit_uint32(0);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hashChunk_hash_blake2s_2_8.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

