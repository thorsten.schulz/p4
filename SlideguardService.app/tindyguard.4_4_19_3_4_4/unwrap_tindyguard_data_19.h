/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _unwrap_tindyguard_data_19_H_
#define _unwrap_tindyguard_data_19_H_

#include "kcg_types.h"
#include "aeadOpen_nacl_box_19_1_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::data::unwrap/ */
extern void unwrap_tindyguard_data_19(
  /* _L131/, s/ */
  Session_tindyguardTypes *s_19,
  /* fail_in/ */
  kcg_bool fail_in_19,
  /* rlength/ */
  size_slideTypes rlength_19,
  /* datamsg/ */
  array_uint32_16_20 *datamsg_19,
  /* endpoint/ */
  peer_t_udp *endpoint_19,
  /* soo/ */
  Session_tindyguardTypes *soo_19,
  /* fail_flag/ */
  kcg_bool *fail_flag_19,
  /* len/ */
  length_t_udp *len_19,
  /* msg/ */
  array_uint32_16_19 *msg_19);



#endif /* _unwrap_tindyguard_data_19_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unwrap_tindyguard_data_19.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

