/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "aead_nacl_box_1_1_20.h"

/* nacl::box::aead/ */
void aead_nacl_box_1_1_20(
  /* _L2/, msg/ */
  array_uint32_16_1 *msg_1_1_20,
  /* _L10/, _L4/, mlen/ */
  int_slideTypes mlen_1_1_20,
  /* _L14/, ad/ */
  array_uint32_16_1 *ad_1_1_20,
  /* _L15/, adlen/ */
  int_slideTypes adlen_1_1_20,
  /* _L5/, nonce/ */
  Nonce_nacl_core_chacha *nonce_1_1_20,
  /* _L3/, key/ */
  Key_nacl_core *key_1_1_20,
  /* _L13/, a/ */
  Mac_nacl_onetime *a_1_1_20,
  /* _L1/, cm/ */
  array_uint32_16_1 *cm_1_1_20)
{
  /* _L17/ */
  StreamChunk_slideTypes _L17_1_1_20;
  /* _L16/ */
  State_nacl_core_chacha _L16_1_1_20;

  /* _L16=(nacl::core::chacha::setup#1)/ */
  setup_nacl_core_chacha_20(
    nonce_1_1_20,
    key_1_1_20,
    &_L16_1_1_20,
    &_L17_1_1_20);
  /* _L1=(nacl::box::stream::chacha_IETF#1)/ */
  chacha_IETF_nacl_box_stream_1_20(
    msg_1_1_20,
    mlen_1_1_20,
    &_L16_1_1_20,
    cm_1_1_20);
  /* _L13=(nacl::onetime::auth2#1)/ */
  auth2_nacl_onetime_1_1(
    ad_1_1_20,
    adlen_1_1_20,
    cm_1_1_20,
    mlen_1_1_20,
    &_L17_1_1_20,
    a_1_1_20);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** aead_nacl_box_1_1_20.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

