/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _mapReceived_tindyguard_session_4_3_H_
#define _mapReceived_tindyguard_session_4_3_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::mapReceived/ */
extern void mapReceived_tindyguard_session_4_3(
  /* _L12/, sid/ */
  size_tindyguardTypes sid_4_3,
  /* _L1/, length/ */
  array_int32_4 *length_4_3,
  /* _L2/, our/ */
  array_uint32_4 *our_4_3,
  /* _L11/, s/ */
  Session_tindyguardTypes *s_4_3,
  /* _L6/, residual_length_or_sid/ */
  array_int32_4 *residual_length_or_sid_4_3,
  /* _L10/, length_for_session/ */
  array_int32_4 *length_for_session_4_3,
  /* _L8/, for_init_response/ */
  kcg_bool *for_init_response_4_3);



#endif /* _mapReceived_tindyguard_session_4_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapReceived_tindyguard_session_4_3.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

