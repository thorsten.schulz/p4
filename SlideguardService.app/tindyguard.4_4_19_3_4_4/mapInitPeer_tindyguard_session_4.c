/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapInitPeer_tindyguard_session_4.h"

/* tindyguard::session::mapInitPeer/ */
void mapInitPeer_tindyguard_session_4(
  /* pid_req/ */
  array_int32_4 *pid_req_4,
  /* s/ */
  Session_tindyguardTypes *s_4,
  /* IfBlock1:, res_s_init/ */
  kcg_bool res_s_init_4,
  /* pid_not_taken/ */
  array_int32_4 *pid_not_taken_4,
  /* pid_cmd/ */
  size_tindyguardTypes *pid_cmd_4)
{
  size_tindyguardTypes acc;
  kcg_size idx;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_findNonNil_it_1;
  /* IfBlock1:else: */
  kcg_bool else_clock_IfBlock1_4;

  /* IfBlock1: */
  if (res_s_init_4) {
    kcg_copy_array_int32_4(pid_not_taken_4, pid_req_4);
    *pid_cmd_4 = PeerFromRxPacket_tindyguardTypes;
  }
  else {
    else_clock_IfBlock1_4 = (*s_4).pid != nil_tindyguard_session;
    /* IfBlock1:else: */
    if (else_clock_IfBlock1_4) {
      kcg_copy_array_int32_4(pid_not_taken_4, pid_req_4);
      *pid_cmd_4 = InvalidPeer_tindyguardTypes;
    }
    else {
      *pid_cmd_4 = InvalidPeer_tindyguardTypes;
      /* IfBlock1:else:else:_L7= */
      for (idx = 0; idx < 4; idx++) {
        acc = *pid_cmd_4;
        IfBlock1_clock_findNonNil_it_1 = acc == InvalidPeer_tindyguardTypes;
        /* @1/IfBlock1: */
        if (IfBlock1_clock_findNonNil_it_1) {
          /* @1/IfBlock1:then:_L1= */
          if ((*pid_req_4)[idx] != InvalidPeer_tindyguardTypes) {
            *pid_cmd_4 = (*pid_req_4)[idx];
          }
          else {
            *pid_cmd_4 = InvalidPeer_tindyguardTypes;
          }
          (*pid_not_taken_4)[idx] = InvalidPeer_tindyguardTypes;
        }
        else {
          *pid_cmd_4 = acc;
          (*pid_not_taken_4)[idx] = (*pid_req_4)[idx];
        }
      }
    }
  }
}

/*
  Expanded instances for: tindyguard::session::mapInitPeer/
  @1: (tindyguard::session::findNonNil_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapInitPeer_tindyguard_session_4.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

