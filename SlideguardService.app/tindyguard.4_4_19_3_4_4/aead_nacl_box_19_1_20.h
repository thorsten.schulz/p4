/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _aead_nacl_box_19_1_20_H_
#define _aead_nacl_box_19_1_20_H_

#include "kcg_types.h"
#include "chacha_IETF_nacl_box_stream_19_20.h"
#include "auth2_nacl_onetime_1_19.h"
#include "setup_nacl_core_chacha_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::box::aead/ */
extern void aead_nacl_box_19_1_20(
  /* _L2/, msg/ */
  array_uint32_16_19 *msg_19_1_20,
  /* _L10/, _L4/, mlen/ */
  int_slideTypes mlen_19_1_20,
  /* _L14/, ad/ */
  array_uint32_16_1 *ad_19_1_20,
  /* _L15/, adlen/ */
  int_slideTypes adlen_19_1_20,
  /* _L5/, nonce/ */
  Nonce_nacl_core_chacha *nonce_19_1_20,
  /* _L3/, key/ */
  Key_nacl_core *key_19_1_20,
  /* _L13/, a/ */
  Mac_nacl_onetime *a_19_1_20,
  /* _L1/, cm/ */
  array_uint32_16_19 *cm_19_1_20);



#endif /* _aead_nacl_box_19_1_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** aead_nacl_box_19_1_20.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

