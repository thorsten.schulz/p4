/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "auth2_nacl_onetime_1_1.h"

/* nacl::onetime::auth2/ */
void auth2_nacl_onetime_1_1(
  /* _L47/, msg1/ */
  array_uint32_16_1 *msg1_1_1,
  /* _L46/, _L50/, _L55/, mlen1/ */
  int_slideTypes mlen1_1_1,
  /* _L12/, msg2/ */
  array_uint32_16_1 *msg2_1_1,
  /* _L15/, _L18/, _L56/, mlen2/ */
  int_slideTypes mlen2_1_1,
  /* _L2/, _L36/, k/ */
  StreamChunk_slideTypes *k_1_1,
  /* _L34/, a/ */
  Mac_nacl_onetime *a_1_1)
{
  kcg_size idx;
  kcg_size idx_IETF_it1_1_1_IETF_it1_1;
  kcg_size idx_IETF_it1_1;
  kcg_bool cond_iterw;
  /* @2/_L12/, @4/_L21/, @8/_L21/ */
  kcg_int32 _L21_IETF_it1_2;
  /* @3/_L16/, @4/_L16/, @8/_L16/ */
  kcg_int32 _L16_IETF_it1_2;
  array_uint8_17 tmp_IETF_it1_1_1_IETF_it1_2;
  /* @10/_L12/, @6/_L12/, _L27/ */
  kcg_int32 _L12_it1_1_1_1_IETF_it1_1_1_IETF_it1_2;
  /* @11/_L16/, @7/_L16/ */
  kcg_int32 _L16_it1_1_1_1_1_it1_1_1_1_IETF_it1_1_1_IETF_it1_2;
  /* @12/_L2/, @13/_L2/, @34/_L1/ */
  kcg_uint32 _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2;
  /* @1/_L11/, @14/_L2/, @15/_L2/ */
  kcg_uint32 _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2;
  /* @1/_L44/, @31/_L2/, @31/hoo/, @5/_L44/, @9/_L44/, _L22/ */
  array_uint32_17 _L44_IETF_it1_1_1_IETF_it1_2;
  /* @1/_L43/, @5/_L43/, @9/_L43/ */
  array_uint32_33 _L43_IETF_it1_1_1_IETF_it1_2;
  /* @16/_L2/, @35/_L2/, @5/_L27/, @9/_L27/ */
  kcg_uint32 _L27_IETF_it1_1_1_IETF_it1_2;
  /* @1/_L18/, @5/_L18/, @9/_L18/ */
  kcg_uint32 _L18_IETF_it1_1_1_IETF_it1_2;
  /* @1/_L27/, @5/_L11/, @9/_L11/ */
  kcg_uint32 _L11_IETF_it1_1_1_IETF_it1_2;
  /* @1/_L12/, @5/_L12/, @9/_L12/ */
  array_uint32_16 _L12_IETF_it1_1_1_IETF_it1_2;
  /* @1/_L6/, @28/_L2/, @28/hoo/, @34/_L2/, @34/hoo/, @5/_L8/, @9/_L8/, _L31/ */
  array_uint32_17 _L8_IETF_it1_1_1_IETF_it1_2;
  /* @1/_L8/, @21/_L2/, @21/hoo/, @25/_L2/, @25/hoo/, @5/_L6/, @9/_L6/ */
  array_uint32_17 _L6_IETF_it1_1_1_IETF_it1_2;
  /* @17/_L2/, @20/_L15/, @24/_L15/ */
  kcg_uint32 _L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  /* @20/_L14/, @24/_L14/, @31/_L1/ */
  kcg_uint32 _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  /* @20/_L32/, @24/_L32/, @32/_L2/ */
  kcg_uint32 _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  /* @20/_L30/, @24/_L30/, @33/_L14/ */
  kcg_uint32 _L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  /* @20/_L38/, @24/_L38/, @33/_L15/ */
  kcg_uint32 _L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  /* @20/_L37/, @24/_L37/, @33/_L32/ */
  kcg_uint32 _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  /* @20/_L46/, @24/_L46/, @33/_L30/ */
  kcg_uint32 _L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  /* @20/_L42/, @24/_L42/, @33/_L37/ */
  kcg_uint32 _L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  kcg_uint32 noname_add1305_1_IETF_it1_1_1_IETF_it1_2;
  array_bool_4 tmp_IETF_it1_2;
  array_uint32_4_4 _1_tmp_IETF_it1_2;
  array_uint8_17 tmp;
  array_uint8_17 tmp2;
  /* @1/_L3/, @1/r/, @19/_L2/, @19/m/, _L59/, _L60/, _L61/, _L67/, r/ */
  array_uint8_17 _L59_1_1;
  /* @1/_L2/,
     @1/m/,
     @18/_L25/,
     @18/u32/,
     @19/_L25/,
     @19/u32/,
     @33/_L25/,
     @33/u32/,
     _L35/,
     _L69/,
     _L7/ */
  array_uint32_4 _L7_1_1;
  /* @18/_L15/,
     @18/_L30/,
     @18/_L38/,
     @18/_L42/,
     @19/_L15/,
     @19/_L30/,
     @19/_L38/,
     @19/_L42/,
     @29/_L2/,
     @33/_L42/ */
  kcg_uint32 _L30_st32x4_1_1_IETF_it1_1_1;
  /* @18/_L14/,
     @18/_L32/,
     @18/_L37/,
     @18/_L46/,
     @19/_L14/,
     @19/_L32/,
     @19/_L37/,
     @19/_L46/,
     @22/_L2/,
     @26/_L2/,
     @28/_L1/,
     @33/_L46/ */
  kcg_uint32 _L32_st32x4_1_1_IETF_it1_1_1;
  /* @1/_L1/,
     @1/_L33/,
     @1/h/,
     @1/hoo/,
     @28/_L5/,
     @28/h/,
     @31/_L5/,
     @31/h/,
     _L21/,
     _L52/ */
  array_uint32_17 hoo_IETF_it1_1_1;

  tmp2[16] = kcg_lit_uint8(0);
  tmp[16] = kcg_lit_uint8(1);
  _L59_1_1[16] = kcg_lit_uint8(0);
  _L7_1_1[0] = /* _L65= */(kcg_uint32) mlen1_1_1;
  _L7_1_1[1] = kcg_lit_uint32(0);
  _L7_1_1[2] = /* _L66= */(kcg_uint32) mlen2_1_1;
  _L7_1_1[3] = kcg_lit_uint32(0);
  tmp[0] = /* @18/_L8= */(kcg_uint8) _L7_1_1[0];
  tmp[4] = /* @18/_L26= */(kcg_uint8) _L7_1_1[1];
  tmp[8] = /* @18/_L34= */(kcg_uint8) _L7_1_1[2];
  tmp[12] = /* @18/_L41= */(kcg_uint8) _L7_1_1[3];
  _L32_st32x4_1_1_IETF_it1_1_1 = _L7_1_1[0] >> kcg_lit_uint32(8);
  tmp[1] = /* @18/_L7= */(kcg_uint8) _L32_st32x4_1_1_IETF_it1_1_1;
  _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(
      8);
  tmp[2] = /* @18/_L6= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1;
  tmp[3] = /* @18/_L5= */(kcg_uint8)
      (_L30_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8));
  _L32_st32x4_1_1_IETF_it1_1_1 = _L7_1_1[1] >> kcg_lit_uint32(8);
  tmp[5] = /* @18/_L29= */(kcg_uint8) _L32_st32x4_1_1_IETF_it1_1_1;
  _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(
      8);
  tmp[6] = /* @18/_L27= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1;
  tmp[7] = /* @18/_L31= */(kcg_uint8)
      (_L30_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8));
  _L32_st32x4_1_1_IETF_it1_1_1 = _L7_1_1[2] >> kcg_lit_uint32(8);
  tmp[9] = /* @18/_L36= */(kcg_uint8) _L32_st32x4_1_1_IETF_it1_1_1;
  _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(
      8);
  tmp[10] = /* @18/_L33= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1;
  tmp[11] = /* @18/_L35= */(kcg_uint8)
      (_L30_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8));
  _L32_st32x4_1_1_IETF_it1_1_1 = _L7_1_1[3] >> kcg_lit_uint32(8);
  tmp[13] = /* @18/_L44= */(kcg_uint8) _L32_st32x4_1_1_IETF_it1_1_1;
  _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(
      8);
  tmp[14] = /* @18/_L43= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1;
  tmp[15] = /* @18/_L45= */(kcg_uint8)
      (_L30_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8));
  /* _L7= */
  for (idx = 0; idx < 4; idx++) {
    _L7_1_1[idx] = (*k_1_1)[idx] & kMask32_nacl_onetime[idx];
  }
  _L59_1_1[0] = /* @19/_L8= */(kcg_uint8) _L7_1_1[0];
  _L59_1_1[4] = /* @19/_L26= */(kcg_uint8) _L7_1_1[1];
  _L59_1_1[8] = /* @19/_L34= */(kcg_uint8) _L7_1_1[2];
  _L59_1_1[12] = /* @19/_L41= */(kcg_uint8) _L7_1_1[3];
  _L32_st32x4_1_1_IETF_it1_1_1 = _L7_1_1[0] >> kcg_lit_uint32(8);
  _L59_1_1[1] = /* @19/_L7= */(kcg_uint8) _L32_st32x4_1_1_IETF_it1_1_1;
  _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(
      8);
  _L59_1_1[2] = /* @19/_L6= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1;
  _L59_1_1[3] = /* @19/_L5= */(kcg_uint8)
      (_L30_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8));
  _L32_st32x4_1_1_IETF_it1_1_1 = _L7_1_1[1] >> kcg_lit_uint32(8);
  _L59_1_1[5] = /* @19/_L29= */(kcg_uint8) _L32_st32x4_1_1_IETF_it1_1_1;
  _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(
      8);
  _L59_1_1[6] = /* @19/_L27= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1;
  _L59_1_1[7] = /* @19/_L31= */(kcg_uint8)
      (_L30_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8));
  _L32_st32x4_1_1_IETF_it1_1_1 = _L7_1_1[2] >> kcg_lit_uint32(8);
  _L59_1_1[9] = /* @19/_L36= */(kcg_uint8) _L32_st32x4_1_1_IETF_it1_1_1;
  _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(
      8);
  _L59_1_1[10] = /* @19/_L33= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1;
  _L59_1_1[11] = /* @19/_L35= */(kcg_uint8)
      (_L30_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8));
  _L32_st32x4_1_1_IETF_it1_1_1 = _L7_1_1[3] >> kcg_lit_uint32(8);
  _L59_1_1[13] = /* @19/_L44= */(kcg_uint8) _L32_st32x4_1_1_IETF_it1_1_1;
  _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(
      8);
  _L59_1_1[14] = /* @19/_L43= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1;
  _L59_1_1[15] = /* @19/_L45= */(kcg_uint8)
      (_L30_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8));
  for (idx = 0; idx < 17; idx++) {
    hoo_IETF_it1_1_1[idx] = kcg_lit_uint32(0);
  }
  /* _L43= */
  if (mlen1_1_1 > kcg_lit_int32(0)) {
    kcg_copy_array_uint32_4(
      &_1_tmp_IETF_it1_2[0],
      (array_uint32_4 *) &(*msg1_1_1)[0][0]);
    kcg_copy_array_uint32_4(
      &_1_tmp_IETF_it1_2[1],
      (array_uint32_4 *) &(*msg1_1_1)[0][4]);
    kcg_copy_array_uint32_4(
      &_1_tmp_IETF_it1_2[2],
      (array_uint32_4 *) &(*msg1_1_1)[0][8]);
    kcg_copy_array_uint32_4(
      &_1_tmp_IETF_it1_2[3],
      (array_uint32_4 *) &(*msg1_1_1)[0][12]);
    _L16_IETF_it1_2 = kcg_lit_int32(0) * kcg_lit_int32(64);
    _L21_IETF_it1_2 = (mlen1_1_1 - kcg_lit_int32(1) - _L16_IETF_it1_2) /
      StreamChunkLength_slideTypes;
    cond_iterw = mlen1_1_1 > _L16_IETF_it1_2 + kcg_lit_int32(64);
    for (idx = 0; idx < 4; idx++) {
      tmp_IETF_it1_2[idx] = kcg_true;
    }
    if (kcg_lit_int32(0) <= _L21_IETF_it1_2 && _L21_IETF_it1_2 < kcg_lit_int32(
        4)) {
      tmp_IETF_it1_2[_L21_IETF_it1_2] = cond_iterw;
    }
    /* @8/_L5= */
    for (idx = 0; idx < 4; idx++) {
      tmp_IETF_it1_1_1_IETF_it1_2[0] = /* @20/_L8= */(kcg_uint8)
          _1_tmp_IETF_it1_2[idx][0];
      tmp_IETF_it1_1_1_IETF_it1_2[4] = /* @20/_L26= */(kcg_uint8)
          _1_tmp_IETF_it1_2[idx][1];
      tmp_IETF_it1_1_1_IETF_it1_2[8] = /* @20/_L34= */(kcg_uint8)
          _1_tmp_IETF_it1_2[idx][2];
      tmp_IETF_it1_1_1_IETF_it1_2[12] = /* @20/_L41= */(kcg_uint8)
          _1_tmp_IETF_it1_2[idx][3];
      tmp_IETF_it1_1_1_IETF_it1_2[16] = kcg_lit_uint8(1);
      _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _1_tmp_IETF_it1_2[idx][0] >>
        kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[1] = /* @20/_L7= */(kcg_uint8)
          _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      _L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
        _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[2] = /* @20/_L6= */(kcg_uint8)
          _L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      tmp_IETF_it1_1_1_IETF_it1_2[3] = /* @20/_L5= */(kcg_uint8)
          (_L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
      _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _1_tmp_IETF_it1_2[idx][1] >>
        kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[5] = /* @20/_L29= */(kcg_uint8)
          _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      _L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
        _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[6] = /* @20/_L27= */(kcg_uint8)
          _L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      tmp_IETF_it1_1_1_IETF_it1_2[7] = /* @20/_L31= */(kcg_uint8)
          (_L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
      _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _1_tmp_IETF_it1_2[idx][2] >>
        kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[9] = /* @20/_L36= */(kcg_uint8)
          _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      _L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
        _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[10] = /* @20/_L33= */(kcg_uint8)
          _L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      tmp_IETF_it1_1_1_IETF_it1_2[11] = /* @20/_L35= */(kcg_uint8)
          (_L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
      _L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _1_tmp_IETF_it1_2[idx][3] >>
        kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[13] = /* @20/_L44= */(kcg_uint8)
          _L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      _L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
        _L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[14] = /* @20/_L43= */(kcg_uint8)
          _L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      tmp_IETF_it1_1_1_IETF_it1_2[15] = /* @20/_L45= */(kcg_uint8)
          (_L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
      noname_add1305_1_IETF_it1_1_1_IETF_it1_2 = kcg_lit_uint32(0);
      /* @21/_L1=, @9/_L44= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 17; idx_IETF_it1_1++) {
        _L32_st32x4_1_1_IETF_it1_1_1 =
          noname_add1305_1_IETF_it1_1_1_IETF_it1_2 + /* @22/_L8= */(kcg_uint32)
            tmp_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] +
          hoo_IETF_it1_1_1[idx_IETF_it1_1];
        noname_add1305_1_IETF_it1_1_1_IETF_it1_2 =
          _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8);
        _L6_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
          _L32_st32x4_1_1_IETF_it1_1_1 & kcg_lit_uint32(255);
        _L44_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
          /* @23/_L2= */(kcg_uint32) _L59_1_1[idx_IETF_it1_1];
      }
      /* @9/_L39= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 16; idx_IETF_it1_1++) {
        _L43_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
          _L44_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1 + 1] * kcg_lit_uint32(320);
      }
      kcg_copy_array_uint32_17(
        &_L43_IETF_it1_1_1_IETF_it1_2[16],
        &_L44_IETF_it1_1_1_IETF_it1_2);
      /* @9/_L8= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 17; idx_IETF_it1_1++) {
        _L12_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 = /* @9/_L8= */(kcg_int32)
            idx_IETF_it1_1 + kcg_lit_int32(16);
        _L8_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] = kcg_lit_uint32(0);
        /* @10/_L8= */
        for (
          idx_IETF_it1_1_1_IETF_it1_1 = 0;
          idx_IETF_it1_1_1_IETF_it1_1 < 17;
          idx_IETF_it1_1_1_IETF_it1_1++) {
          _L16_it1_1_1_1_1_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 =
            _L12_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 - /* @10/_L8= */(kcg_int32)
              idx_IETF_it1_1_1_IETF_it1_1;
          if (kcg_lit_int32(0) <=
            _L16_it1_1_1_1_1_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 &&
            _L16_it1_1_1_1_1_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 < kcg_lit_int32(33)) {
            _L30_st32x4_1_1_IETF_it1_1_1 =
              _L43_IETF_it1_1_1_IETF_it1_2[_L16_it1_1_1_1_1_it1_1_1_1_IETF_it1_1_1_IETF_it1_2];
          }
          else {
            _L30_st32x4_1_1_IETF_it1_1_1 = kcg_lit_uint32(0);
          }
          _L8_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
            _L8_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] +
            _L6_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1_1_IETF_it1_1] *
            _L30_st32x4_1_1_IETF_it1_1_1;
        }
      }
      _L11_IETF_it1_1_1_IETF_it1_2 = kcg_lit_uint32(0);
      /* @9/_L11= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 16; idx_IETF_it1_1++) {
        _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 = _L11_IETF_it1_1_1_IETF_it1_2 +
          _L8_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1];
        _L11_IETF_it1_1_1_IETF_it1_2 = _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 >>
          kcg_lit_uint32(8);
        _L12_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
          _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 & kcg_lit_uint32(255);
      }
      _L18_IETF_it1_1_1_IETF_it1_2 = _L11_IETF_it1_1_1_IETF_it1_2 +
        _L8_IETF_it1_1_1_IETF_it1_2[16];
      _L27_IETF_it1_1_1_IETF_it1_2 = (_L18_IETF_it1_1_1_IETF_it1_2 >>
          kcg_lit_uint32(2)) * kcg_lit_uint32(5);
      /* @9/_L27= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 16; idx_IETF_it1_1++) {
        _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 = _L27_IETF_it1_1_1_IETF_it1_2 +
          _L12_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1];
        _L27_IETF_it1_1_1_IETF_it1_2 = _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 >>
          kcg_lit_uint32(8);
        hoo_IETF_it1_1_1[idx_IETF_it1_1] =
          _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 & kcg_lit_uint32(255);
      }
      hoo_IETF_it1_1_1[16] = _L27_IETF_it1_1_1_IETF_it1_2 +
        (_L18_IETF_it1_1_1_IETF_it1_2 & kcg_lit_uint32(3));
      /* @8/_L5= */
      if (!tmp_IETF_it1_2[idx]) {
        break;
      }
    }
  }
  /* _L17= */
  if (mlen2_1_1 > kcg_lit_int32(0)) {
    kcg_copy_array_uint32_4(
      &_1_tmp_IETF_it1_2[0],
      (array_uint32_4 *) &(*msg2_1_1)[0][0]);
    kcg_copy_array_uint32_4(
      &_1_tmp_IETF_it1_2[1],
      (array_uint32_4 *) &(*msg2_1_1)[0][4]);
    kcg_copy_array_uint32_4(
      &_1_tmp_IETF_it1_2[2],
      (array_uint32_4 *) &(*msg2_1_1)[0][8]);
    kcg_copy_array_uint32_4(
      &_1_tmp_IETF_it1_2[3],
      (array_uint32_4 *) &(*msg2_1_1)[0][12]);
    _L16_IETF_it1_2 = kcg_lit_int32(0) * kcg_lit_int32(64);
    _L21_IETF_it1_2 = (mlen2_1_1 - kcg_lit_int32(1) - _L16_IETF_it1_2) /
      StreamChunkLength_slideTypes;
    cond_iterw = mlen2_1_1 > _L16_IETF_it1_2 + kcg_lit_int32(64);
    for (idx = 0; idx < 4; idx++) {
      tmp_IETF_it1_2[idx] = kcg_true;
    }
    if (kcg_lit_int32(0) <= _L21_IETF_it1_2 && _L21_IETF_it1_2 < kcg_lit_int32(
        4)) {
      tmp_IETF_it1_2[_L21_IETF_it1_2] = cond_iterw;
    }
    /* @4/_L5= */
    for (idx = 0; idx < 4; idx++) {
      tmp_IETF_it1_1_1_IETF_it1_2[0] = /* @24/_L8= */(kcg_uint8)
          _1_tmp_IETF_it1_2[idx][0];
      tmp_IETF_it1_1_1_IETF_it1_2[4] = /* @24/_L26= */(kcg_uint8)
          _1_tmp_IETF_it1_2[idx][1];
      tmp_IETF_it1_1_1_IETF_it1_2[8] = /* @24/_L34= */(kcg_uint8)
          _1_tmp_IETF_it1_2[idx][2];
      tmp_IETF_it1_1_1_IETF_it1_2[12] = /* @24/_L41= */(kcg_uint8)
          _1_tmp_IETF_it1_2[idx][3];
      tmp_IETF_it1_1_1_IETF_it1_2[16] = kcg_lit_uint8(1);
      _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _1_tmp_IETF_it1_2[idx][0] >>
        kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[1] = /* @24/_L7= */(kcg_uint8)
          _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      _L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
        _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[2] = /* @24/_L6= */(kcg_uint8)
          _L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      tmp_IETF_it1_1_1_IETF_it1_2[3] = /* @24/_L5= */(kcg_uint8)
          (_L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
      _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _1_tmp_IETF_it1_2[idx][1] >>
        kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[5] = /* @24/_L29= */(kcg_uint8)
          _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      _L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
        _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[6] = /* @24/_L27= */(kcg_uint8)
          _L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      tmp_IETF_it1_1_1_IETF_it1_2[7] = /* @24/_L31= */(kcg_uint8)
          (_L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
      _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _1_tmp_IETF_it1_2[idx][2] >>
        kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[9] = /* @24/_L36= */(kcg_uint8)
          _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      _L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
        _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[10] = /* @24/_L33= */(kcg_uint8)
          _L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      tmp_IETF_it1_1_1_IETF_it1_2[11] = /* @24/_L35= */(kcg_uint8)
          (_L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
      _L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _1_tmp_IETF_it1_2[idx][3] >>
        kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[13] = /* @24/_L44= */(kcg_uint8)
          _L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      _L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
        _L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
      tmp_IETF_it1_1_1_IETF_it1_2[14] = /* @24/_L43= */(kcg_uint8)
          _L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
      tmp_IETF_it1_1_1_IETF_it1_2[15] = /* @24/_L45= */(kcg_uint8)
          (_L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
      noname_add1305_1_IETF_it1_1_1_IETF_it1_2 = kcg_lit_uint32(0);
      /* @25/_L1=, @5/_L44= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 17; idx_IETF_it1_1++) {
        _L32_st32x4_1_1_IETF_it1_1_1 =
          noname_add1305_1_IETF_it1_1_1_IETF_it1_2 + /* @26/_L8= */(kcg_uint32)
            tmp_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] +
          hoo_IETF_it1_1_1[idx_IETF_it1_1];
        noname_add1305_1_IETF_it1_1_1_IETF_it1_2 =
          _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8);
        _L6_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
          _L32_st32x4_1_1_IETF_it1_1_1 & kcg_lit_uint32(255);
        _L44_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
          /* @27/_L2= */(kcg_uint32) _L59_1_1[idx_IETF_it1_1];
      }
      /* @5/_L39= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 16; idx_IETF_it1_1++) {
        _L43_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
          _L44_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1 + 1] * kcg_lit_uint32(320);
      }
      kcg_copy_array_uint32_17(
        &_L43_IETF_it1_1_1_IETF_it1_2[16],
        &_L44_IETF_it1_1_1_IETF_it1_2);
      /* @5/_L8= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 17; idx_IETF_it1_1++) {
        _L12_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 = /* @5/_L8= */(kcg_int32)
            idx_IETF_it1_1 + kcg_lit_int32(16);
        _L8_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] = kcg_lit_uint32(0);
        /* @6/_L8= */
        for (
          idx_IETF_it1_1_1_IETF_it1_1 = 0;
          idx_IETF_it1_1_1_IETF_it1_1 < 17;
          idx_IETF_it1_1_1_IETF_it1_1++) {
          _L16_it1_1_1_1_1_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 =
            _L12_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 - /* @6/_L8= */(kcg_int32)
              idx_IETF_it1_1_1_IETF_it1_1;
          if (kcg_lit_int32(0) <=
            _L16_it1_1_1_1_1_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 &&
            _L16_it1_1_1_1_1_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 < kcg_lit_int32(33)) {
            _L30_st32x4_1_1_IETF_it1_1_1 =
              _L43_IETF_it1_1_1_IETF_it1_2[_L16_it1_1_1_1_1_it1_1_1_1_IETF_it1_1_1_IETF_it1_2];
          }
          else {
            _L30_st32x4_1_1_IETF_it1_1_1 = kcg_lit_uint32(0);
          }
          _L8_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
            _L8_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] +
            _L6_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1_1_IETF_it1_1] *
            _L30_st32x4_1_1_IETF_it1_1_1;
        }
      }
      _L11_IETF_it1_1_1_IETF_it1_2 = kcg_lit_uint32(0);
      /* @5/_L11= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 16; idx_IETF_it1_1++) {
        _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 = _L11_IETF_it1_1_1_IETF_it1_2 +
          _L8_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1];
        _L11_IETF_it1_1_1_IETF_it1_2 = _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 >>
          kcg_lit_uint32(8);
        _L12_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] =
          _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 & kcg_lit_uint32(255);
      }
      _L18_IETF_it1_1_1_IETF_it1_2 = _L11_IETF_it1_1_1_IETF_it1_2 +
        _L8_IETF_it1_1_1_IETF_it1_2[16];
      _L27_IETF_it1_1_1_IETF_it1_2 = (_L18_IETF_it1_1_1_IETF_it1_2 >>
          kcg_lit_uint32(2)) * kcg_lit_uint32(5);
      /* @5/_L27= */
      for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 16; idx_IETF_it1_1++) {
        _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 = _L27_IETF_it1_1_1_IETF_it1_2 +
          _L12_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1];
        _L27_IETF_it1_1_1_IETF_it1_2 = _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 >>
          kcg_lit_uint32(8);
        hoo_IETF_it1_1_1[idx_IETF_it1_1] =
          _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 & kcg_lit_uint32(255);
      }
      hoo_IETF_it1_1_1[16] = _L27_IETF_it1_1_1_IETF_it1_2 +
        (_L18_IETF_it1_1_1_IETF_it1_2 & kcg_lit_uint32(3));
      /* @4/_L5= */
      if (!tmp_IETF_it1_2[idx]) {
        break;
      }
    }
  }
  _L32_st32x4_1_1_IETF_it1_1_1 = kcg_lit_uint32(0);
  /* @1/_L44=, @28/_L1= */
  for (idx = 0; idx < 17; idx++) {
    _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 +
      /* @29/_L8= */(kcg_uint32) tmp[idx] + hoo_IETF_it1_1_1[idx];
    _L32_st32x4_1_1_IETF_it1_1_1 = _L30_st32x4_1_1_IETF_it1_1_1 >>
      kcg_lit_uint32(8);
    _L8_IETF_it1_1_1_IETF_it1_2[idx] = _L30_st32x4_1_1_IETF_it1_1_1 &
      kcg_lit_uint32(255);
    _L44_IETF_it1_1_1_IETF_it1_2[idx] = /* @30/_L2= */(kcg_uint32) _L59_1_1[idx];
  }
  /* @1/_L39= */
  for (idx = 0; idx < 16; idx++) {
    _L43_IETF_it1_1_1_IETF_it1_2[idx] = _L44_IETF_it1_1_1_IETF_it1_2[idx + 1] *
      kcg_lit_uint32(320);
  }
  kcg_copy_array_uint32_17(
    &_L43_IETF_it1_1_1_IETF_it1_2[16],
    &_L44_IETF_it1_1_1_IETF_it1_2);
  /* @1/_L8= */
  for (idx = 0; idx < 17; idx++) {
    _L21_IETF_it1_2 = /* @1/_L8= */(kcg_int32) idx + kcg_lit_int32(16);
    _L6_IETF_it1_1_1_IETF_it1_2[idx] = kcg_lit_uint32(0);
    /* @2/_L8= */
    for (idx_IETF_it1_1 = 0; idx_IETF_it1_1 < 17; idx_IETF_it1_1++) {
      _L16_IETF_it1_2 = _L21_IETF_it1_2 - /* @2/_L8= */(kcg_int32) idx_IETF_it1_1;
      if (kcg_lit_int32(0) <= _L16_IETF_it1_2 && _L16_IETF_it1_2 <
        kcg_lit_int32(33)) {
        _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 =
          _L43_IETF_it1_1_1_IETF_it1_2[_L16_IETF_it1_2];
      }
      else {
        _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 = kcg_lit_uint32(0);
      }
      _L6_IETF_it1_1_1_IETF_it1_2[idx] = _L6_IETF_it1_1_1_IETF_it1_2[idx] +
        _L8_IETF_it1_1_1_IETF_it1_2[idx_IETF_it1_1] *
        _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2;
    }
  }
  _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 = kcg_lit_uint32(0);
  /* @1/_L11= */
  for (idx = 0; idx < 16; idx++) {
    _L27_IETF_it1_1_1_IETF_it1_2 = _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 +
      _L6_IETF_it1_1_1_IETF_it1_2[idx];
    _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 = _L27_IETF_it1_1_1_IETF_it1_2 >>
      kcg_lit_uint32(8);
    _L12_IETF_it1_1_1_IETF_it1_2[idx] = _L27_IETF_it1_1_1_IETF_it1_2 &
      kcg_lit_uint32(255);
  }
  _L18_IETF_it1_1_1_IETF_it1_2 = _L2_it1_1_2_2_IETF_it1_1_1_IETF_it1_2 +
    _L6_IETF_it1_1_1_IETF_it1_2[16];
  _L11_IETF_it1_1_1_IETF_it1_2 = (_L18_IETF_it1_1_1_IETF_it1_2 >>
      kcg_lit_uint32(2)) * kcg_lit_uint32(5);
  /* @1/_L27= */
  for (idx = 0; idx < 16; idx++) {
    _L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _L11_IETF_it1_1_1_IETF_it1_2 +
      _L12_IETF_it1_1_1_IETF_it1_2[idx];
    _L11_IETF_it1_1_1_IETF_it1_2 = _L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >>
      kcg_lit_uint32(8);
    hoo_IETF_it1_1_1[idx] = _L15_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 &
      kcg_lit_uint32(255);
  }
  hoo_IETF_it1_1_1[16] = _L11_IETF_it1_1_1_IETF_it1_2 +
    (_L18_IETF_it1_1_1_IETF_it1_2 & kcg_lit_uint32(3));
  _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = kcg_lit_uint32(0);
  /* @31/_L1= */
  for (idx = 0; idx < 17; idx++) {
    _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
      _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 + /* @32/_L8= */(kcg_uint32)
        minusp_nacl_onetime[idx] + hoo_IETF_it1_1_1[idx];
    _L14_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
      _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
    _L44_IETF_it1_1_1_IETF_it1_2[idx] =
      _L32_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 & kcg_lit_uint32(255);
  }
  kcg_copy_array_uint32_4(&_L7_1_1, (array_uint32_4 *) &(*k_1_1)[4]);
  tmp2[0] = /* @33/_L8= */(kcg_uint8) _L7_1_1[0];
  tmp2[4] = /* @33/_L26= */(kcg_uint8) _L7_1_1[1];
  tmp2[8] = /* @33/_L34= */(kcg_uint8) _L7_1_1[2];
  tmp2[12] = /* @33/_L41= */(kcg_uint8) _L7_1_1[3];
  _L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _L7_1_1[0] >> kcg_lit_uint32(8);
  tmp2[1] = /* @33/_L7= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  _L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
    _L30_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
  tmp2[2] = /* @33/_L6= */(kcg_uint8) _L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  tmp2[3] = /* @33/_L5= */(kcg_uint8)
      (_L38_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
  _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _L7_1_1[1] >> kcg_lit_uint32(8);
  tmp2[5] = /* @33/_L29= */(kcg_uint8) _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  _L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 =
    _L37_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
  tmp2[6] = /* @33/_L27= */(kcg_uint8) _L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  tmp2[7] = /* @33/_L31= */(kcg_uint8)
      (_L46_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
  _L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 = _L7_1_1[2] >> kcg_lit_uint32(8);
  tmp2[9] = /* @33/_L36= */(kcg_uint8) _L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2;
  noname_add1305_1_IETF_it1_1_1_IETF_it1_2 =
    _L42_st32x4_1_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8);
  tmp2[10] = /* @33/_L33= */(kcg_uint8) noname_add1305_1_IETF_it1_1_1_IETF_it1_2;
  tmp2[11] = /* @33/_L35= */(kcg_uint8)
      (noname_add1305_1_IETF_it1_1_1_IETF_it1_2 >> kcg_lit_uint32(8));
  _L32_st32x4_1_1_IETF_it1_1_1 = _L7_1_1[3] >> kcg_lit_uint32(8);
  tmp2[13] = /* @33/_L44= */(kcg_uint8) _L32_st32x4_1_1_IETF_it1_1_1;
  _L30_st32x4_1_1_IETF_it1_1_1 = _L32_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(
      8);
  tmp2[14] = /* @33/_L43= */(kcg_uint8) _L30_st32x4_1_1_IETF_it1_1_1;
  tmp2[15] = /* @33/_L45= */(kcg_uint8)
      (_L30_st32x4_1_1_IETF_it1_1_1 >> kcg_lit_uint32(8));
  _L12_it1_1_1_1_IETF_it1_1_1_IETF_it1_2 = - /* _L26= */(kcg_int32)
      (_L44_IETF_it1_1_1_IETF_it1_2[16] >> kcg_lit_uint32(7));
  _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 = kcg_lit_uint32(0);
  /* @34/_L1= */
  for (idx = 0; idx < 17; idx++) {
    _L27_IETF_it1_1_1_IETF_it1_2 = _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 +
      /* @35/_L8= */(kcg_uint32) tmp2[idx] +
      (_L44_IETF_it1_1_1_IETF_it1_2[idx] ^ ((_L44_IETF_it1_1_1_IETF_it1_2[idx] ^
            hoo_IETF_it1_1_1[idx]) & /* _L28= */(kcg_uint32)
            _L12_it1_1_1_1_IETF_it1_1_1_IETF_it1_2));
    _L2_it1_1_2_1_IETF_it1_1_1_IETF_it1_2 = _L27_IETF_it1_1_1_IETF_it1_2 >>
      kcg_lit_uint32(8);
    _L8_IETF_it1_1_1_IETF_it1_2[idx] = _L27_IETF_it1_1_1_IETF_it1_2 &
      kcg_lit_uint32(255);
  }
  /* _L34= */
  for (idx = 0; idx < 16; idx++) {
    (*a_1_1)[idx] = /* @36/_L2= */(kcg_uint8) _L8_IETF_it1_1_1_IETF_it1_2[idx];
  }
}

/*
  Expanded instances for: nacl::onetime::auth2/
  @1: (nacl::onetime::IETF_it1_1#1)
  @2: @1/(nacl::onetime::it1_1_1#1)
  @3: @2/(nacl::onetime::it1_1_1_1#1)
  @4: (nacl::onetime::IETF_it1#2)
  @5: @4/(nacl::onetime::IETF_it1_1#1)
  @6: @5/(nacl::onetime::it1_1_1#1)
  @7: @6/(nacl::onetime::it1_1_1_1#1)
  @8: (nacl::onetime::IETF_it1#1)
  @9: @8/(nacl::onetime::IETF_it1_1#1)
  @10: @9/(nacl::onetime::it1_1_1#1)
  @11: @10/(nacl::onetime::it1_1_1_1#1)
  @12: @5/(nacl::onetime::it1_1_2#1)
  @13: @9/(nacl::onetime::it1_1_2#1)
  @14: @5/(nacl::onetime::it1_1_2#2)
  @15: @9/(nacl::onetime::it1_1_2#2)
  @16: @1/(nacl::onetime::it1_1_2#1)
  @17: @1/(nacl::onetime::it1_1_2#2)
  @18: @1/(nacl::onetime::st32x4_1#1)
  @19: (nacl::onetime::st32x4_1#1)
  @20: @9/(nacl::onetime::st32x4_1#1)
  @21: @9/(nacl::onetime::add1305#1)
  @22: @21/(nacl::onetime::add1305_it1#1)
  @23: @9/(nacl::op::castU32#1)
  @24: @5/(nacl::onetime::st32x4_1#1)
  @25: @5/(nacl::onetime::add1305#1)
  @26: @25/(nacl::onetime::add1305_it1#1)
  @27: @5/(nacl::op::castU32#1)
  @28: @1/(nacl::onetime::add1305#1)
  @29: @28/(nacl::onetime::add1305_it1#1)
  @30: @1/(nacl::op::castU32#1)
  @31: (nacl::onetime::add1305#1)
  @32: @31/(nacl::onetime::add1305_it1#1)
  @33: (nacl::onetime::st32x4_1#2)
  @34: (nacl::onetime::add1305#2)
  @35: @34/(nacl::onetime::add1305_it1#1)
  @36: (nacl::op::castU8#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** auth2_nacl_onetime_1_1.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

