/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _pack25519_it1_nacl_op_H_
#define _pack25519_it1_nacl_op_H_

#include "kcg_types.h"
#include "pack25519_it3_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::pack25519_it1/ */
extern void pack25519_it1_nacl_op(
  /* @1/_L1/, @1/p/, _L1/, _L13/, t/ */
  gf_nacl_op *t,
  /* @1/_L8/, @1/po/, _L11/, to/ */
  gf_nacl_op *to);

/*
  Expanded instances for: nacl::op::pack25519_it1/
  @1: (nacl::op::sel25519#1)
*/

#endif /* _pack25519_it1_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack25519_it1_nacl_op.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

