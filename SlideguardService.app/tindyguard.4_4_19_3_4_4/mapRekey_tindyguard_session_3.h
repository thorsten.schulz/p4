/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _mapRekey_tindyguard_session_3_H_
#define _mapRekey_tindyguard_session_3_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::mapRekey/ */
extern void mapRekey_tindyguard_session_3(
  /* rekey/ */
  array_int32_3 *rekey_3,
  /* s/ */
  Session_tindyguardTypes *s_3,
  /* pid_cmd_in/ */
  size_tindyguardTypes pid_cmd_in_3,
  /* pid_not_taken/ */
  array_int32_3 *pid_not_taken_3,
  /* pid_cmd/ */
  size_tindyguardTypes *pid_cmd_3);



#endif /* _mapRekey_tindyguard_session_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapRekey_tindyguard_session_3.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

