/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "check_ip_19.h"

/* ip::check/ */
void check_ip_19(
  /* len/ */
  length_t_udp len_19,
  /* msg/ */
  array_uint32_16_19 *msg_19,
  /* expect/ */
  range_t_udp *expect_19,
  /* in/ */
  header_ip *in_19,
  /* checksum/ */
  kcg_bool checksum_19,
  /* payloadlen/ */
  length_t_udp *payloadlen_19,
  /* src/ */
  IpAddress_udp *src_19,
  /* failed/ */
  kcg_bool *failed_19)
{
  kcg_uint32 tmp;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_19;
  /* @1/_L8/, @1/dst/, IfBlock1:then:IfBlock2:then:_L24/ */
  IpAddress_udp _L24_then_IfBlock2_then_IfBlock1_19;
  /* IfBlock1:then:IfBlock2:then:_L12/ */
  kcg_bool _L12_then_IfBlock2_then_IfBlock1_19;
  /* @1/_L10/, @1/o/, IfBlock1:then:IfBlock2:then:_L5/ */
  kcg_bool _L5_then_IfBlock2_then_IfBlock1_19;
  /* IfBlock1:then:IfBlock2: */
  kcg_bool IfBlock2_clock_then_IfBlock1_19;
  /* IfBlock1:then:_L8/ */
  kcg_uint16 _L8_then_IfBlock1_19;

  IfBlock1_clock_19 = len_19 >= kcg_lit_int32(20);
  /* IfBlock1: */
  if (IfBlock1_clock_19) {
    _L8_then_IfBlock1_19 = /* IfBlock1:then:_L8=(sys::hton#3)/ */
      htons_sys_specialization(
        /* IfBlock1:then:_L15= */(kcg_uint16) ((*msg_19)[0][0] >> kcg_lit_uint32(16)));
    IfBlock2_clock_then_IfBlock1_19 = !(/* IfBlock1:then:_L2= */(kcg_uint8)
          (*msg_19)[0][0] != (*in_19).ihl_version ||
        /* IfBlock1:then:_L16= */(kcg_int32) _L8_then_IfBlock1_19 < (*in_19).tot_len ||
        /* IfBlock1:then:_L16= */(kcg_int32) _L8_then_IfBlock1_19 > len_19 ||
        /* IfBlock1:then:_L18= */(kcg_uint8) ((*msg_19)[0][2] >> kcg_lit_uint32(8)) !=
        (*in_19).protocol);
    /* IfBlock1:then:IfBlock2: */
    if (IfBlock2_clock_then_IfBlock1_19) {
      _L24_then_IfBlock2_then_IfBlock1_19.addr =
        /* IfBlock1:then:IfBlock2:then:_L4=(sys::hton#2)/ */
        htonl_sys_specialization((*msg_19)[0][4]);
      _L5_then_IfBlock2_then_IfBlock1_19 =
        _L24_then_IfBlock2_then_IfBlock1_19.addr != kcg_lit_uint32(0) &&
        kcg_lit_uint32(0) == ((_L24_then_IfBlock2_then_IfBlock1_19.addr ^
            (*expect_19).net.addr) & (*expect_19).mask.addr) && kcg_lit_uint32(
          0) != (*expect_19).net.addr;
      _L12_then_IfBlock2_then_IfBlock1_19 =
        _L5_then_IfBlock2_then_IfBlock1_19 && checksum_19;
      if (_L12_then_IfBlock2_then_IfBlock1_19) {
        tmp = /* IfBlock1:then:IfBlock2:then:_L7=(ip::ipsum_BEH#1)/ */
          ipsum_BEH_ip_19(msg_19, headerBytes_ip, kcg_lit_int32(0));
      }
      else {
        tmp = kcg_lit_uint32(4294901760);
      }
      *failed_19 = !_L5_then_IfBlock2_then_IfBlock1_19 || tmp != kcg_lit_uint32(
          4294901760);
      (*src_19).addr = /* IfBlock1:then:IfBlock2:then:_L2=(sys::hton#1)/ */
        htonl_sys_specialization((*msg_19)[0][3]);
      /* IfBlock1:then:IfBlock2:then:_L20= */
      if (*failed_19) {
        *payloadlen_19 = kcg_lit_int32(-1);
      }
      else {
        *payloadlen_19 = /* IfBlock1:then:_L16= */(kcg_int32)
            _L8_then_IfBlock1_19 - (*in_19).tot_len;
      }
    }
    else {
      *failed_19 = kcg_true;
      *payloadlen_19 = kcg_lit_int32(-1);
      kcg_copy_IpAddress_udp(src_19, (IpAddress_udp *) &ipAny_udp);
    }
  }
  else {
    *failed_19 = kcg_true;
    *payloadlen_19 = kcg_lit_int32(-1);
    kcg_copy_IpAddress_udp(src_19, (IpAddress_udp *) &ipAny_udp);
  }
}

/*
  Expanded instances for: ip::check/
  @1: (udp::within#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** check_ip_19.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

