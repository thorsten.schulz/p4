/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "discardSent_tindyguard_session_3.h"

/* tindyguard::session::discardSent/ */
void discardSent_tindyguard_session_3(
  /* @1/_L1/, @1/i/, _L8/, slots/ */
  size_tindyguardTypes slots_3,
  /* _L1/, lenbuf/ */
  size_tindyguardTypes lenbuf_3,
  /* _L2/, taken/ */
  array_bool_3 *taken_3,
  /* _L11/, avail/ */
  size_tindyguardTypes *avail_3,
  /* _L7/, lenbufoo/ */
  size_tindyguardTypes *lenbufoo_3)
{
  size_tindyguardTypes acc;
  kcg_size idx;

  *lenbufoo_3 = lenbuf_3;
  /* _L4= */
  for (idx = 0; idx < 3; idx++) {
    acc = *lenbufoo_3;
    /* @2/_L3= */
    if ((*taken_3)[idx]) {
      *lenbufoo_3 = nil_udp;
    }
    else {
      *lenbufoo_3 = acc;
    }
    /* _L4= */
    if (!!(*taken_3)[idx]) {
      break;
    }
  }
  /* _L11= */
  if (*lenbufoo_3 != nil_udp) {
    *avail_3 = slots_3 - kcg_lit_int32(1);
  }
  else {
    *avail_3 = slots_3;
  }
}

/*
  Expanded instances for: tindyguard::session::discardSent/
  @2: (tindyguard::session::anyone_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** discardSent_tindyguard_session_3.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

