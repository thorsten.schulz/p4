/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "echoRequest_icmp_19.h"

/* icmp::echoRequest/ */
void echoRequest_icmp_19(
  /* @1/dst/, _L32/, dst/ */
  IpAddress_udp *dst_19,
  /* @1/src/, _L33/, src/ */
  IpAddress_udp *src_19,
  /* _L35/, id/ */
  kcg_uint32 id_19,
  /* _L81/, len/ */
  length_t_udp *len_19,
  /* _L71/, msg/ */
  array_uint32_16_19 *msg_19,
  /* _L26/, seqoo/ */
  kcg_uint32 *seqoo_19,
  outC_echoRequest_icmp_19 *outC)
{
  array_uint32_16_1 tmp;
  /* _L84/ */
  kcg_uint32 _L84_19;
  /* _L43/, _L50/ */
  kcg_uint32 _L50_19;
  /* _L44/ */
  kcg_uint32 _L44_19;
  /* @1/_L36/, _L42/, _L59/ */
  kcg_uint32 _L42_19;
  /* @1/_L10/, @1/data/, _L31/ */
  array_uint32_5 _L31_19;
  /* @1/_L16/ */
  kcg_uint16 _L16_hdr_1;
  /* @1/_L17/ */
  kcg_uint16 _L17_hdr_1;
  kcg_size idx;

  _L31_19[2] = kcg_lit_uint32(320);
  _L31_19[3] = /* @1/_L8=(sys::hton#1)/ */
    htonl_sys_specialization((*src_19).addr);
  _L31_19[4] = /* @1/_L9=(sys::hton#2)/ */
    htonl_sys_specialization((*dst_19).addr);
  _L42_19 = /* @1/_L36=(sys::hton#6)/ */
    htonl_sys_specialization(kcg_lit_uint32(278));
  _L31_19[0] = kcg_lit_uint32(69) | _L42_19;
  _L17_hdr_1 = /* @1/_L17=(sys::hton#4)/ */
    htons_sys_specialization(HEADER_icmp_ip.frag_off);
  _L16_hdr_1 = /* @1/_L16=(sys::hton#5)/ */
    htons_sys_specialization(HEADER_icmp_ip.id);
  _L31_19[1] = /* @1/_L26= */(kcg_uint32) _L16_hdr_1 | kcg_lsl_uint32(
      /* @1/_L27= */(kcg_uint32) _L17_hdr_1,
      kcg_lit_uint32(16));
  *len_19 = kcg_lit_int32(278);
  *seqoo_19 = outC->seq_19 + kcg_lit_uint32(1);
  _L42_19 = /* _L42=(sys::hton#1)/ */
    htonl_sys_specialization(
      *seqoo_19 | kcg_lsl_uint32(id_19, kcg_lit_uint32(16)));
  /* _L43=(sys::tai#1)/ */ tai_sys(kcg_false, &_L50_19, &_L44_19, &_L84_19);
  kcg_copy_array_uint32_5(&tmp[0][0], &_L31_19);
  tmp[0][5] = kcg_lit_uint32(8);
  tmp[0][6] = _L42_19;
  tmp[0][7] = _L44_19;
  tmp[0][8] = kcg_lit_uint32(0);
  tmp[0][9] = _L84_19 / kcg_lit_uint32(1000);
  tmp[0][10] = kcg_lit_uint32(0);
  kcg_copy_array_uint32_5(&tmp[0][11], (array_uint32_5 *) &payload_icmp);
  _L50_19 = /* _L50=(ip::ipsum_BEH#1)/ */
    ipsum_BEH_ip_1(&tmp, kcg_lit_int32(258), headerBytes_ip);
  tmp[0][5] = kcg_lit_uint32(8) | _L50_19;
  _L42_19 = /* _L59=(ip::ipsum_BEH#2)/ */
    ipsum_BEH_ip_1(&tmp, headerBytes_ip, kcg_lit_int32(0));
  tmp[0][2] = _L31_19[2] | _L42_19;
  kcg_copy_chunk_t_udp(&(*msg_19)[0], &tmp[0]);
  for (idx = 0; idx < 18; idx++) {
    kcg_copy_chunk_t_udp(
      &(*msg_19)[idx + 1],
      (chunk_t_udp *) &ZeroChunk_slideTypes);
  }
  outC->seq_19 = *seqoo_19;
}

#ifndef KCG_USER_DEFINED_INIT
void echoRequest_init_icmp_19(outC_echoRequest_icmp_19 *outC)
{
  outC->seq_19 = kcg_lit_uint32(0);
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void echoRequest_reset_icmp_19(outC_echoRequest_icmp_19 *outC)
{
  outC->seq_19 = kcg_lit_uint32(0);
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

/*
  Expanded instances for: icmp::echoRequest/
  @1: (ip::hdr#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** echoRequest_icmp_19.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

