/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapReceived_tindyguard_session_4_3.h"

/* tindyguard::session::mapReceived/ */
void mapReceived_tindyguard_session_4_3(
  /* _L12/, sid/ */
  size_tindyguardTypes sid_4_3,
  /* _L1/, length/ */
  array_int32_4 *length_4_3,
  /* _L2/, our/ */
  array_uint32_4 *our_4_3,
  /* _L11/, s/ */
  Session_tindyguardTypes *s_4_3,
  /* _L6/, residual_length_or_sid/ */
  array_int32_4 *residual_length_or_sid_4_3,
  /* _L10/, length_for_session/ */
  array_int32_4 *length_for_session_4_3,
  /* _L8/, for_init_response/ */
  kcg_bool *for_init_response_4_3)
{
  kcg_bool acc;
  kcg_size idx;
  /* @1/IfBlock1:then:_L24/ */
  kcg_bool _L24_mapRxd_it_1_then_IfBlock1_3;
  /* @1/IfBlock1:then:_L19/,
     @1/IfBlock1:then:_L22/,
     @1/IfBlock1:then:take_packet/ */
  kcg_bool _L22_mapRxd_it_1_then_IfBlock1_3;
  /* @1/IfBlock1:then:_L1/ */
  kcg_bool _L1_mapRxd_it_1_then_IfBlock1_3;
  /* @1/IfBlock1:then:_L5/ */
  kcg_bool _L5_mapRxd_it_1_then_IfBlock1_3;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_mapRxd_it_1_3;

  *for_init_response_4_3 = kcg_false;
  /* _L8= */
  for (idx = 0; idx < 4; idx++) {
    acc = *for_init_response_4_3;
    IfBlock1_clock_mapRxd_it_1_3 = (*length_4_3)[idx] >= kcg_lit_int32(3);
    /* @1/IfBlock1: */
    if (IfBlock1_clock_mapRxd_it_1_3) {
      _L24_mapRxd_it_1_then_IfBlock1_3 = (*length_4_3)[idx] == kcg_lit_int32(148);
      _L1_mapRxd_it_1_then_IfBlock1_3 = (*s_4_3).pid ==
        InvalidPeer_tindyguardTypes && _L24_mapRxd_it_1_then_IfBlock1_3 && !acc;
      _L5_mapRxd_it_1_then_IfBlock1_3 = (*our_4_3)[idx] == (*s_4_3).our;
      _L22_mapRxd_it_1_then_IfBlock1_3 = _L5_mapRxd_it_1_then_IfBlock1_3 ||
        _L1_mapRxd_it_1_then_IfBlock1_3;
      *for_init_response_4_3 = _L1_mapRxd_it_1_then_IfBlock1_3 || acc;
      /* @1/IfBlock1:then:_L8= */
      if (_L22_mapRxd_it_1_then_IfBlock1_3) {
        (*residual_length_or_sid_4_3)[idx] = sid_4_3;
        (*length_for_session_4_3)[idx] = (*length_4_3)[idx];
      }
      else {
        /* @1/IfBlock1:then:_L30= */
        if (!(_L5_mapRxd_it_1_then_IfBlock1_3 ||
            _L24_mapRxd_it_1_then_IfBlock1_3) && sid_4_3 == kcg_lit_int32(2)) {
          (*residual_length_or_sid_4_3)[idx] = nil_udp;
        }
        else {
          (*residual_length_or_sid_4_3)[idx] = (*length_4_3)[idx];
        }
        (*length_for_session_4_3)[idx] = nil_udp;
      }
    }
    else {
      *for_init_response_4_3 = acc;
      (*residual_length_or_sid_4_3)[idx] = (*length_4_3)[idx];
      (*length_for_session_4_3)[idx] = nil_udp;
    }
  }
}

/*
  Expanded instances for: tindyguard::session::mapReceived/
  @1: (tindyguard::session::mapRxd_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapReceived_tindyguard_session_4_3.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

