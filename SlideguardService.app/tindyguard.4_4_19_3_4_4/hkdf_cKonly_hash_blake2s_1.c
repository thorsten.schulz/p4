/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hkdf_cKonly_hash_blake2s_1.h"

/* hash::blake2s::hkdf_cKonly/ */
void hkdf_cKonly_hash_blake2s_1(
  /* _L4/, ikm/ */
  array_uint32_16_1 *ikm_1,
  /* _L5/, ikmlen/ */
  size_slideTypes ikmlen_1,
  /* _L3/, chainingKey/ */
  HashChunk_hash_blake2s *chainingKey_1,
  /* @1/hashoo/, @1/okm/, _L14/, _L16/, cKout/ */
  HashChunk_hash_blake2s *cKout_1)
{
  HashChunk_hash_blake2s tmp;
  /* @1/IfBlock1:then:_L10/ */
  array_uint32_16_1 _L10_hkdf_expand_it1_1_then_IfBlock1;

  kcg_copy_StreamChunk_slideTypes(
    &_L10_hkdf_expand_it1_1_then_IfBlock1[0],
    (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  _L10_hkdf_expand_it1_1_then_IfBlock1[0][0] = kcg_lit_uint32(1);
  /* _L18=(hash::blake2s::hmacChunk#1)/ */
  hmacChunk_hash_blake2s_1(ikm_1, ikmlen_1, chainingKey_1, &tmp);
  /* @1/IfBlock1:then:_L5=(hash::blake2s::hmacChunk#2)/ */
  hmacChunk_hash_blake2s_1(
    &_L10_hkdf_expand_it1_1_then_IfBlock1,
    kcg_lit_int32(1),
    &tmp,
    cKout_1);
}

/*
  Expanded instances for: hash::blake2s::hkdf_cKonly/
  @1: (hash::blake2s::hkdf_expand_it1#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_cKonly_hash_blake2s_1.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

