/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "assertGoodKey_nacl_box.h"

/* nacl::box::assertGoodKey/ */
kcg_bool assertGoodKey_nacl_box(/* _L3/, k/ */ Key32_slideTypes *k)
{
  int_slideTypes tmp;
  kcg_size idx;
  /* @1/ITB_Output/, @1/_L2/, _L6/, failed/ */
  kcg_bool failed;

  tmp = kcg_lit_int32(0);
  /* _L2= */
  for (idx = 0; idx < 5; idx++) {
    tmp = tmp + kcg_lit_int32(1) + /* @2/_L1=(M::cmp#1)/ */
      cmp_M_uint32_8(k, (array_uint32_8 *) &badKey_nacl_box[idx]);
  }
  failed = tmp != kcg_lit_int32(0);
  return failed;
}

/*
  Expanded instances for: nacl::box::assertGoodKey/
  @1: (math::IntToBool#1)
  @2: (nacl::box::isGoodKey_it1#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** assertGoodKey_nacl_box.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

