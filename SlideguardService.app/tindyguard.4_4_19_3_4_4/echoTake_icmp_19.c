/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "echoTake_icmp_19.h"

/* icmp::echoTake/ */
void echoTake_icmp_19(
  /* _L1/, len/ */
  length_t_udp len_19,
  /* _L2/, msg/ */
  array_uint32_16_19 *msg_19,
  /* _L50/, host/ */
  IpAddress_udp *host_19,
  /* lenoo/ */
  length_t_udp *lenoo_19,
  /* msgoo/ */
  array_uint32_16_19 *msgoo_19,
  /* seqoo/ */
  kcg_uint32 *seqoo_19,
  /* _L33/, src/ */
  IpAddress_udp *src_19,
  /* id/ */
  kcg_uint32 *id_19,
  /* _L60/, fail1/, failed/ */
  kcg_bool *failed_19,
  /* badChecksum/ */
  kcg_bool *badChecksum_19,
  /* badPayload/ */
  kcg_bool *badPayload_19)
{
  range_t_udp tmp_str;
  /* IfBlock1:else: */
  kcg_bool else_clock_IfBlock1_19;
  /* IfBlock1:else:then:_L22/ */
  kcg_uint32 _L22_then_else_IfBlock1_19;
  /* IfBlock1:else:then:_L32/, IfBlock2:then:_L3/ */
  kcg_uint32 _L32_then_else_IfBlock1_19;
  /* IfBlock1:else:then:_L36/, IfBlock2:then:_L9/ */
  kcg_uint32 _L36_then_else_IfBlock1_19;
  /* IfBlock2:, _L34/ */
  kcg_bool IfBlock2_clock_19;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_19;
  /* _L32/, pll/ */
  length_t_udp _L32_19;

  kcg_copy_IpAddress_udp(&tmp_str.net, host_19);
  kcg_copy_IpAddress_udp(&tmp_str.mask, (IpAddress_udp *) &ipBroadcast_udp);
  /* _L32=(ip::check#1)/ */
  check_ip_19(
    len_19,
    msg_19,
    &tmp_str,
    (header_ip *) &HEADER_icmp_ip,
    kcg_false,
    &_L32_19,
    src_19,
    &IfBlock2_clock_19);
  *failed_19 = ((*msg_19)[0][5] & kcg_lit_uint32(65280)) != kcg_lit_uint32(0) ||
    IfBlock2_clock_19;
  IfBlock2_clock_19 = !*failed_19;
  /* IfBlock2: */
  if (IfBlock2_clock_19) {
    _L36_then_else_IfBlock1_19 = /* IfBlock2:then:_L9=(sys::hton#1)/ */
      htonl_sys_specialization((*msg_19)[0][6]);
    _L32_then_else_IfBlock1_19 = /* IfBlock2:then:_L3=(ip::ipsum_BEH#5)/ */
      ipsum_BEH_ip_19(
        msg_19,
        HEADER_icmp_ip.tot_len + _L32_19 - headerBytes_ip,
        headerBytes_ip);
    *seqoo_19 = kcg_lit_uint32(65535) & _L36_then_else_IfBlock1_19;
    *id_19 = _L36_then_else_IfBlock1_19 >> kcg_lit_uint32(16);
    *badChecksum_19 = _L32_then_else_IfBlock1_19 != kcg_lit_uint32(4294901760);
  }
  else {
    *badChecksum_19 = kcg_false;
    *seqoo_19 = kcg_lit_uint32(0);
    *id_19 = kcg_lit_uint32(0);
  }
  IfBlock1_clock_19 = IfBlock2_clock_19 && /* _L55= */(kcg_uint8)
      (*msg_19)[0][5] == ECHOREPLY_icmp;
  /* IfBlock1: */
  if (IfBlock1_clock_19) {
    *lenoo_19 = nil_udp;
    kcg_copy_array_uint32_16_19(msgoo_19, msg_19);
    *badPayload_19 = _L32_19 != payloadlen_icmp || !kcg_comp_array_uint32_5(
        (array_uint32_5 *) &(*msg_19)[0][11],
        (array_uint32_5 *) &payload_icmp);
  }
  else {
    *badPayload_19 = kcg_false;
    else_clock_IfBlock1_19 = IfBlock2_clock_19 && /* _L55= */(kcg_uint8)
        (*msg_19)[0][5] == ECHO_icmp;
    /* IfBlock1:else: */
    if (else_clock_IfBlock1_19) {
      _L32_then_else_IfBlock1_19 = ((*msg_19)[0][2] & kcg_lit_uint32(0xFFFF)) -
        kcg_lit_uint32(1);
      _L22_then_else_IfBlock1_19 =
        /* IfBlock1:else:then:_L22=(ip::ipsum_BEH#3)/ */
        ipsum_BEH_ip_19(
          msg_19,
          kcg_lit_int32(24) + _L32_19 - kcg_lit_int32(4),
          kcg_lit_int32(24));
      kcg_copy_array_uint32_16_19(msgoo_19, msg_19);
      (*msgoo_19)[0][5] = _L22_then_else_IfBlock1_19;
      (*msgoo_19)[0][4] = (*msg_19)[0][3];
      (*msgoo_19)[0][3] = (*msg_19)[0][4];
      (*msgoo_19)[0][2] = _L32_then_else_IfBlock1_19;
      _L36_then_else_IfBlock1_19 =
        /* IfBlock1:else:then:_L36=(ip::ipsum_BEH#4)/ */
        ipsum_BEH_ip_19(msgoo_19, headerBytes_ip, kcg_lit_int32(0));
      *lenoo_19 = len_19;
      (*msgoo_19)[0][2] = _L36_then_else_IfBlock1_19 | _L32_then_else_IfBlock1_19;
    }
    else {
      *lenoo_19 = nil_udp;
      kcg_copy_array_uint32_16_19(msgoo_19, msg_19);
    }
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** echoTake_icmp_19.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

