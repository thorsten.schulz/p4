/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "respond_tindyguard_handshake_4.h"

/* tindyguard::handshake::respond/ */
void respond_tindyguard_handshake_4(
  /* _L21/, rlength/ */
  length_t_udp rlength_4,
  /* _L12/, initmsg/ */
  array_uint32_16_4 *initmsg_4,
  /* _L32/, endpoint/ */
  peer_t_udp *endpoint_4,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_4,
  /* session/ */
  Session_tindyguardTypes *session_4,
  /* knownPeer/ */
  _4_array *knownPeer_4,
  /* _L34/, now/ */
  kcg_int64 now_4,
  /* _L24/, slength/ */
  length_t_udp *slength_4,
  /* _L25/, responsemsg/ */
  array_uint32_16_4 *responsemsg_4,
  /* _L3/, soo/ */
  Session_tindyguardTypes *soo_4,
  /* _L33/, failed/ */
  kcg_bool *failed_4,
  /* _L30/, noPeer/ */
  kcg_bool *noPeer_4,
  /* _L1/, weakKey/ */
  kcg_bool *weakKey_4,
  outC_respond_tindyguard_handshake_4 *outC)
{
  kcg_bool tmp;
  array_uint32_16_1 tmp1;
  array_uint32_16 tmp2;
  array_uint32_16 tmp3;
  array_uint32_8_2 tmp4;
  array_uint32_16 tmp5;
  array_uint32_8_2 tmp6;
  HashChunk_hash_blake2s tmp7;
  array_uint32_16_1 tmp8;
  array_uint32_16 tmp9;
  array_uint32_16_1 tmp10;
  array_uint32_16 tmp11;
  array_uint32_16 tmp12;
  array_uint32_16 tmp13;
  array_uint32_16 tmp14;
  array_uint8_4_4 tmp15;
  kcg_size idx;
  array_uint32_16_2 tmp16;
  array_uint32_16_3 tmp17;
  /* _L31/ */
  kcg_bool _L31_4;
  /* _L8/ */
  kcg_bool _L8_4;
  /* _L9/ */
  Session_tindyguardTypes _L9_4;
  kcg_bool op_call;
  /* @2/IfBlock1:else:_L2/ */
  array_uint32_8 _L2_ephemeral_1_build_response_1_else_IfBlock1;
  /* @2/IfBlock1:else:_L1/ */
  kcg_bool _L1_ephemeral_1_build_response_1_else_IfBlock1;
  /* @2/IfBlock1:else:_L6/ */
  kcg_bool _L6_ephemeral_1_build_response_1_else_IfBlock1;
  /* @1/_L205/,
     @1/_L513/,
     @1/ek/,
     @2/IfBlock1:else:_L4/,
     @2/kp/,
     @3/_L9/,
     @3/sk/,
     @4/_L9/,
     @4/sk/ */
  KeyPair32_slideTypes kp_partial_ephemeral_1_build_response_1;
  kcg_bool op_call_ephemeral_1_build_response_1;
  /* @3/_L14/ */
  array_uint32_16_1 _L14_DHDerive_3_build_response_1;
  /* @1/_L269/, @3/_L3/, @3/fail/ */
  kcg_bool _L3_DHDerive_3_build_response_1;
  /* @3/_L5/ */
  array_uint32_16 _L5_DHDerive_3_build_response_1;
  /* @4/_L14/ */
  array_uint32_16_1 _L14_DHDerive_4_build_response_1;
  /* @1/_L274/, @4/_L3/, @4/fail/ */
  kcg_bool _L3_DHDerive_4_build_response_1;
  /* @4/_L5/ */
  array_uint32_16 _L5_DHDerive_4_build_response_1;
  /* @1/_L290/, @5/_L1/, @5/a/ */
  Mac_nacl_onetime _L1_ldAuth_6_build_response_1;
  /* @1/IfBlock1:else: */
  kcg_bool else_clock_build_response_1_IfBlock1;
  /* @1/IfBlock1:else:then:_L8/ */
  Mac_hash_blake2s _L8_build_response_1_then_else_IfBlock1;
  array_uint32_16_1 noname_build_response_1;
  /* @1/_L520/, @1/m_rsp1/ */
  StreamChunk_slideTypes m_rsp1_build_response_1;
  /* @1/_L506/, @1/m_rsp2/ */
  StreamChunk_slideTypes m_rsp2_build_response_1;
  /* @1/_L276/ */
  array_uint32_8_3 _L276_build_response_1;
  /* @1/_L294/ */
  array_uint32_3 _L294_build_response_1;
  /* @1/_L305/ */
  array_uint32_8_2 _L305_build_response_1;
  /* @1/_L379/ */
  array_uint32_16_1 _L379_build_response_1;
  /* @1/_L481/ */
  kcg_bool _L481_build_response_1;
  /* @1/_L482/ */
  array_uint32_1 _L482_build_response_1;
  /* @1/_L515/ */
  Mac_hash_blake2s _L515_build_response_1;

  /* _L9=(tindyguard::handshake::eval_init#1)/ */
  eval_init_tindyguard_handshake_4(
    initmsg_4,
    rlength_4,
    endpoint_4,
    sks_4,
    knownPeer_4,
    session_4,
    &_L9_4,
    &_L8_4,
    noPeer_4);
  _L31_4 = !_L8_4;
  if (_L31_4) {
    kcg_copy_StreamChunk_slideTypes(&tmp16[0], &_L9_4.peer.hcache.salt);
    m_rsp1_build_response_1[0] = MsgType_Response_tindyguard;
    m_rsp1_build_response_1[2] = _L9_4.their;
    kcg_copy_StreamChunk_slideTypes(
      &_L379_build_response_1[0],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    /* @1/_L481=(sys::random#1)/ */
    random01_sys_specialization(
      &_L481_build_response_1,
      &_L482_build_response_1);
    m_rsp1_build_response_1[1] = _L482_build_response_1[0];
    /* @2/IfBlock1:else:_L1=(sys::random#1)/ */
    random08_sys_specialization(
      &_L1_ephemeral_1_build_response_1_else_IfBlock1,
      &_L2_ephemeral_1_build_response_1_else_IfBlock1);
    _L6_ephemeral_1_build_response_1_else_IfBlock1 =
      !_L1_ephemeral_1_build_response_1_else_IfBlock1;
    if (_L6_ephemeral_1_build_response_1_else_IfBlock1) {
      /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
      keyPair_nacl_box_20(
        &_L2_ephemeral_1_build_response_1_else_IfBlock1,
        &op_call_ephemeral_1_build_response_1,
        &kp_partial_ephemeral_1_build_response_1,
        &tmp,
        &outC->Context_keyPair_2_ephemeral_1_build_response_1);
    }
    else {
      kcg_copy_KeyPair32_slideTypes(
        &kp_partial_ephemeral_1_build_response_1,
        (KeyPair32_slideTypes *) &ZeroKeyPair_slideTypes);
      tmp = kcg_true;
    }
    /* @4/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &kp_partial_ephemeral_1_build_response_1,
      &_L9_4.peer.tpub,
      kcg_true,
      &_L3_DHDerive_4_build_response_1,
      (Key32_slideTypes *) &_L5_DHDerive_4_build_response_1[0]);
    /* @3/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &kp_partial_ephemeral_1_build_response_1,
      &_L9_4.handshake.their,
      kcg_true,
      &_L3_DHDerive_3_build_response_1,
      (Key32_slideTypes *) &_L5_DHDerive_3_build_response_1[0]);
    op_call = tmp || _L3_DHDerive_3_build_response_1 ||
      _L3_DHDerive_4_build_response_1 || _L481_build_response_1;
    kcg_copy_psk_tindyguardTypes(&tmp2[0], &_L9_4.peer.preshared);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_4_build_response_1[idx + 8] = kcg_lit_uint32(0);
      _L5_DHDerive_3_build_response_1[idx + 8] = kcg_lit_uint32(0);
      tmp2[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp1[0], &tmp2);
    kcg_copy_Key32_slideTypes(
      &tmp9[0],
      &kp_partial_ephemeral_1_build_response_1.pk_y);
    for (idx = 0; idx < 8; idx++) {
      tmp9[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp8[0], &tmp9);
    /* @1/_L257=(hash::blake2s::hkdf_cKonly#2)/ */
    hkdf_cKonly_hash_blake2s_1(
      &tmp8,
      kcg_lit_int32(32),
      &_L9_4.handshake.chainingKey,
      &tmp7);
    kcg_copy_array_uint32_16(
      &_L14_DHDerive_3_build_response_1[0],
      &_L5_DHDerive_3_build_response_1);
    /* @3/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_3_build_response_1,
      kcg_lit_int32(32),
      &tmp7,
      &tmp6);
    kcg_copy_Hash_hash_blake2s(&tmp5[0], &tmp6[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp5[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(
      &_L14_DHDerive_4_build_response_1[0],
      &_L5_DHDerive_4_build_response_1);
    /* @4/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_4_build_response_1,
      kcg_lit_int32(32),
      &tmp5,
      &tmp4);
    kcg_copy_Hash_hash_blake2s(&tmp3[0], &tmp4[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp3[idx + 8] = kcg_lit_uint32(0);
    }
    /* @1/_L276=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_3(
      &tmp1,
      kcg_lit_int32(32),
      &tmp3,
      &_L276_build_response_1);
    kcg_copy_Hash_hash_blake2s(&tmp13[0], &_L9_4.handshake.ihash);
    kcg_copy_Key32_slideTypes(
      &tmp13[8],
      &kp_partial_ephemeral_1_build_response_1.pk_y);
    /* @1/_L259=(hash::blake2s::single#4)/ */
    single_hash_blake2s(
      &tmp13,
      kcg_lit_int32(64),
      (Hash_hash_blake2s *) &tmp12[0]);
    kcg_copy_Hash_hash_blake2s(&tmp12[8], &_L276_build_response_1[1]);
    /* @1/_L287=(hash::blake2s::single#5)/ */
    single_hash_blake2s(
      &tmp12,
      kcg_lit_int32(64),
      (Hash_hash_blake2s *) &tmp11[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp11[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp10[0], &tmp11);
    for (idx = 0; idx < 3; idx++) {
      _L294_build_response_1[idx] = kcg_lit_uint32(0);
    }
    /* @1/_L290=(nacl::box::aead#1)/ */
    aead_nacl_box_1_1_20(
      &_L379_build_response_1,
      kcg_lit_int32(0),
      &tmp10,
      kcg_lit_int32(32),
      &_L294_build_response_1,
      &_L276_build_response_1[2],
      &_L1_ldAuth_6_build_response_1,
      &noname_build_response_1);
    kcg_copy_array_uint8_4(
      &tmp15[0],
      (array_uint8_4 *) &_L1_ldAuth_6_build_response_1[0]);
    kcg_copy_array_uint8_4(
      &tmp15[1],
      (array_uint8_4 *) &_L1_ldAuth_6_build_response_1[4]);
    kcg_copy_array_uint8_4(
      &tmp15[2],
      (array_uint8_4 *) &_L1_ldAuth_6_build_response_1[8]);
    kcg_copy_array_uint8_4(
      &tmp15[3],
      (array_uint8_4 *) &_L1_ldAuth_6_build_response_1[12]);
    kcg_copy_Hash_hash_blake2s(&tmp14[0], &_L276_build_response_1[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp14[idx + 8] = kcg_lit_uint32(0);
    }
    /* @1/_L305=(hash::blake2s::hkdf#2)/ */
    hkdf_hash_blake2s_1_2(
      &_L379_build_response_1,
      kcg_lit_int32(0),
      &tmp14,
      &_L305_build_response_1);
    kcg_copy_Key32_slideTypes(
      &m_rsp1_build_response_1[3],
      &kp_partial_ephemeral_1_build_response_1.pk_y);
    /* @5/_L24= */
    for (idx = 0; idx < 4; idx++) {
      m_rsp1_build_response_1[idx + 11] = /* @6/_L14= */(kcg_uint32)
          tmp15[idx][0] | kcg_lsl_uint32(
          /* @6/_L15= */(kcg_uint32) tmp15[idx][1],
          kcg_lit_uint32(8)) | kcg_lsl_uint32(
          /* @6/_L16= */(kcg_uint32) tmp15[idx][2],
          kcg_lit_uint32(16)) | kcg_lsl_uint32(
          /* @6/_L17= */(kcg_uint32) tmp15[idx][3],
          kcg_lit_uint32(24));
    }
    m_rsp1_build_response_1[15] = kcg_lit_uint32(0);
    kcg_copy_StreamChunk_slideTypes(&tmp16[1], &m_rsp1_build_response_1);
    /* @1/_L515=(hash::blake2s::hash128#7)/ */
    hash128_hash_blake2s_2(
      &tmp16,
      kcg_lit_int32(60),
      kcg_lit_int32(32),
      &_L515_build_response_1);
    kcg_copy_array_uint32_3(
      &m_rsp2_build_response_1[0],
      (array_uint32_3 *) &_L515_build_response_1[1]);
    for (idx = 0; idx < 13; idx++) {
      m_rsp2_build_response_1[idx + 3] = kcg_lit_uint32(0);
    }
    m_rsp1_build_response_1[15] = _L515_build_response_1[0];
    *weakKey_4 = op_call;
    /* @1/IfBlock1:, @1/_L367= */
    if (op_call) {
      for (idx = 0; idx < 4; idx++) {
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_4)[idx],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
      }
      *slength_4 = nil_udp;
      kcg_copy_Session_tindyguardTypes(
        soo_4,
        (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
    }
    else {
      else_clock_build_response_1_IfBlock1 = now_4 < _L9_4.peer.cookie.opened +
        cookieEdible_tindyguard_conf;
      kcg_copy_secret_tindyguardTypes(&(*soo_4).ot, &_L305_build_response_1[1]);
      kcg_copy_secret_tindyguardTypes(&(*soo_4).to, &_L305_build_response_1[0]);
      (*soo_4).ot_cnt = kcg_lit_uint64(0);
      (*soo_4).to_cnt = kcg_lit_uint64(0);
      (*soo_4).to_cnt_cache = kcg_lit_uint64(0);
      (*soo_4).our = _L482_build_response_1[0];
      (*soo_4).their = _L9_4.their;
      (*soo_4).rx_bytes = kcg_lit_int64(0);
      (*soo_4).rx_cnt = kcg_lit_int32(0);
      (*soo_4).tx_cnt = kcg_lit_int32(0);
      (*soo_4).txTime = kcg_lit_int64(-1);
      (*soo_4).sTime = _L9_4.peer.endpoint.mtime;
      (*soo_4).pid = _L9_4.pid;
      kcg_copy_State_tindyguard_handshake(
        &(*soo_4).handshake,
        (State_tindyguard_handshake *) &EmptyState_tindyguard_handshake);
      kcg_copy_Peer_tindyguardTypes(&(*soo_4).peer, &_L9_4.peer);
      (*soo_4).transmissive = kcg_false;
      (*soo_4).gotKeepAlive = kcg_false;
      (*soo_4).sentKeepAlive = kcg_false;
      /* @1/IfBlock1:else: */
      if (else_clock_build_response_1_IfBlock1) {
        kcg_copy_StreamChunk_slideTypes(&tmp17[0], &_L9_4.peer.cookie.content);
        kcg_copy_StreamChunk_slideTypes(&tmp17[1], &m_rsp1_build_response_1);
        kcg_copy_StreamChunk_slideTypes(&tmp17[2], &m_rsp2_build_response_1);
        /* @1/IfBlock1:else:then:_L8=(hash::blake2s::hash128#6)/ */
        hash128_hash_blake2s_3(
          &tmp17,
          kcg_lit_int32(76),
          kcg_lit_int32(32),
          &_L8_build_response_1_then_else_IfBlock1);
        kcg_copy_StreamChunk_slideTypes(&(*responsemsg_4)[0], &m_rsp1_build_response_1);
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_4)[2],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_4)[3],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
        kcg_copy_StreamChunk_slideTypes(&(*responsemsg_4)[1], &m_rsp2_build_response_1);
        (*responsemsg_4)[1][3] = _L8_build_response_1_then_else_IfBlock1[0];
        (*responsemsg_4)[1][4] = _L8_build_response_1_then_else_IfBlock1[1];
        (*responsemsg_4)[1][5] = _L8_build_response_1_then_else_IfBlock1[2];
        (*responsemsg_4)[1][6] = _L8_build_response_1_then_else_IfBlock1[3];
        *slength_4 = kcg_lit_int32(92);
      }
      else {
        kcg_copy_StreamChunk_slideTypes(&(*responsemsg_4)[0], &m_rsp1_build_response_1);
        kcg_copy_StreamChunk_slideTypes(&(*responsemsg_4)[1], &m_rsp2_build_response_1);
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_4)[2],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_4)[3],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
        *slength_4 = kcg_lit_int32(92);
      }
    }
  }
  else {
    *weakKey_4 = kcg_true;
    for (idx = 0; idx < 4; idx++) {
      kcg_copy_StreamChunk_slideTypes(
        &(*responsemsg_4)[idx],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
    *slength_4 = nil_udp;
    kcg_copy_Session_tindyguardTypes(soo_4, session_4);
  }
  *failed_4 = *weakKey_4 || _L8_4;
}

#ifndef KCG_USER_DEFINED_INIT
void respond_init_tindyguard_handshake_4(
  outC_respond_tindyguard_handshake_4 *outC)
{
  /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
  keyPair_init_nacl_box_20(
    &outC->Context_keyPair_2_ephemeral_1_build_response_1);
}
#endif /* KCG_USER_DEFINED_INIT */


void respond_reset_tindyguard_handshake_4(
  outC_respond_tindyguard_handshake_4 *outC)
{
  /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
  keyPair_reset_nacl_box_20(
    &outC->Context_keyPair_2_ephemeral_1_build_response_1);
}

/*
  Expanded instances for: tindyguard::handshake::respond/
  @1: (tindyguard::handshake::build_response#1)
  @3: @1/(tindyguard::handshake::DHDerive#3)
  @4: @1/(tindyguard::handshake::DHDerive#4)
  @5: @1/(slideTypes::ldAuth#6)
  @6: @5/(slideTypes::ld32x1#1)
  @2: @1/(tindyguard::handshake::ephemeral#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** respond_tindyguard_handshake_4.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

