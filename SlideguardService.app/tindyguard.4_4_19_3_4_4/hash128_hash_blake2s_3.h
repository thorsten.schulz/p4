/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _hash128_hash_blake2s_3_H_
#define _hash128_hash_blake2s_3_H_

#include "kcg_types.h"
#include "stream_it_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hash128/ */
extern void hash128_hash_blake2s_3(
  /* _L64/, msg/ */
  array_uint32_16_3 *msg_3,
  /* _L66/, len/ */
  size_slideTypes len_3,
  /* _L70/, keybytes/ */
  size_slideTypes keybytes_3,
  /* _L75/, mac/ */
  Mac_hash_blake2s *mac_3);



#endif /* _hash128_hash_blake2s_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hash128_hash_blake2s_3.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

