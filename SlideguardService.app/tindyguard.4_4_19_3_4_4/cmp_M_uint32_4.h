/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _cmp_M_uint32_4_H_
#define _cmp_M_uint32_4_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* M::cmp/ */
extern int_slideTypes cmp_M_uint32_4(
  /* _L2/, x/ */
  array_uint32_4 *x_uint32_4,
  /* _L3/, y/ */
  array_uint32_4 *y_uint32_4);



#endif /* _cmp_M_uint32_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** cmp_M_uint32_4.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

