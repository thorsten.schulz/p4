/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _bindings_tindyguard_4_4_19_3_4_4_H_
#define _bindings_tindyguard_4_4_19_3_4_4_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "mapRekey_tindyguard_session_3.h"
#include "mapOutgoing_tindyguard_session_4.h"
#include "collectCrops_tindyguard_session_19_3.h"
#include "mapInitPeer_tindyguard_session_4.h"
#include "discardSent_tindyguard_session_3.h"
#include "updatePeer_tindyguard_session_3.h"
#include "mapReceived_tindyguard_session_4_3.h"
#include "addrsFind_udp.h"
#include "check_tindyguard_handshake_19.h"
#include "player_tindyguard_session_4_19_4_4.h"
#include "mergeApplicationBuffer_tindyguard_data_19_4_4.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  array_bool_4 init_mergeSocket_5;
  array_uint32_16_20_4 /* @1/bufferoo/ */ mem_bufferoo_mergeSocket_5_19_3;
  _5_array /* @1/endpointoo/ */ mem_endpointoo_mergeSocket_5_19_3;
  array_uint32_4 /* @1/ouroo/ */ mem_ouroo_mergeSocket_5_19_3;
  kcg_bool /* shutdown/ */ mem_shutdown_4_4_19_3_4_4;
  kcg_bool init;
  kcg_bool init1;
  array_int32_3 /* wipe_machine:main:ride_machine:ride:_L97/,
     wipe_machine:main:ride_machine:ride:reKeyList/ */ reKeyList_ride_ride_machine_main_wipe_machine_4_4_19_3_4_4;
  array_int32_4 /* wipe_machine:main:ride_machine:ride:_L18/,
     wipe_machine:main:ride_machine:ride:txmlength_kept/ */ txmlength_kept_ride_ride_machine_main_wipe_machine_4_4_19_3_4_4;
  array /* wipe_machine:main:ride_machine:ride:_L50/,
     wipe_machine:main:ride_machine:ride:_L86/,
     wipe_machine:main:ride_machine:ride:sInfo/ */ sInfo_ride_ride_machine_main_wipe_machine_4_4_19_3_4_4;
  array_int32_4 /* wipe_machine:main:ride_machine:ride:_L4/,
     wipe_machine:main:ride_machine:ride:_L81/,
     wipe_machine:main:ride_machine:ride:rxdlength_or_sid/ */ rxdlength_or_sid_ride_ride_machine_main_wipe_machine_4_4_19_3_4_4;
  SSM_ST_ride_machine_main_wipe_machine /* wipe_machine:main:ride_machine: */ ride_machine_state_nxt_main_wipe_machine_4_4_19_3_4_4;
  fd_t_udp /* socket/ */ socket_4_4_19_3_4_4;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_player_tindyguard_session_4_19_4_4 /* wipe_machine:main:ride_machine:ride:_L46=(tindyguard::session::player#6)/ */ Context_player_6[3];
  outC_check_tindyguard_handshake_19 /* @1/IfBlock1:then:_L8=(tindyguard::handshake::check#2)/ */ Context_check_2_mergeSocket_5[4];
  outC_mergeApplicationBuffer_tindyguard_data_19_4_4 /* wipe_machine:main:ride_machine:ride:_L9=(tindyguard::data::mergeApplicationBuffer#5)/ */ Context_mergeApplicationBuffer_5[4];
  /* ----------------- no clocks of observable data ------------------ */
} outC_bindings_tindyguard_4_4_19_3_4_4;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::bindings/ */
extern void bindings_tindyguard_4_4_19_3_4_4(
  /* length/ */
  array_int32_4 *length_4_4_19_3_4_4,
  /* out/ */
  array_uint32_16_19_4 *out_4_4_19_3_4_4,
  /* knownPeers/ */
  _4_array *knownPeers_4_4_19_3_4_4,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_4_4_19_3_4_4,
  /* shutdown/ */
  kcg_bool shutdown_4_4_19_3_4_4,
  /* port/ */
  port_t_udp port_4_4_19_3_4_4,
  /* if_hint/ */
  range_t_udp *if_hint_4_4_19_3_4_4,
  /* length_in/ */
  array_int32_4 *length_in_4_4_19_3_4_4,
  /* in/ */
  array_uint32_16_19_4 *in_4_4_19_3_4_4,
  /* taken/ */
  size_tindyguardTypes *taken_4_4_19_3_4_4,
  /* st_buffer_avail/ */
  size_tindyguardTypes *st_buffer_avail_4_4_19_3_4_4,
  /* updatedPeers/ */
  _4_array *updatedPeers_4_4_19_3_4_4,
  /* no_peer_mask/ */
  kcg_uint64 *no_peer_mask_4_4_19_3_4_4,
  /* _L237/, fd_failed/, fdfail/ */
  kcg_bool *fd_failed_4_4_19_3_4_4,
  outC_bindings_tindyguard_4_4_19_3_4_4 *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void bindings_reset_tindyguard_4_4_19_3_4_4(
  outC_bindings_tindyguard_4_4_19_3_4_4 *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void bindings_init_tindyguard_4_4_19_3_4_4(
  outC_bindings_tindyguard_4_4_19_3_4_4 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::bindings/
  @1: (tindyguard::data::mergeSocket#5)
*/

#endif /* _bindings_tindyguard_4_4_19_3_4_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bindings_tindyguard_4_4_19_3_4_4.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

