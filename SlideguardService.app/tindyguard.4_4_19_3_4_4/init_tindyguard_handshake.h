/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _init_tindyguard_handshake_H_
#define _init_tindyguard_handshake_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "hash128_hash_blake2s_4.h"
#include "hash_hash_blake2s_2.h"
#include "hash128_hash_blake2s_3.h"
#include "aead_nacl_box_1_1_20.h"
#include "hkdf_hash_blake2s_1_2.h"
#include "scalarmultDonna_nacl_box.h"
#include "single_hash_blake2s.h"
#include "hkdf_cKonly_hash_blake2s_1.h"
#include "keyPair_nacl_box_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_keyPair_nacl_box_20 /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */ Context_keyPair_2_ephemeral_1_build_init_2;
  /* ----------------- no clocks of observable data ------------------ */
} outC_init_tindyguard_handshake;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::handshake::init/ */
extern void init_tindyguard_handshake(
  /* peer/ */
  Peer_tindyguardTypes *peer,
  /* pid/ */
  size_tindyguardTypes pid,
  /* sks/ */
  KeySalted_tindyguardTypes *sks,
  /* now/ */
  kcg_int64 now,
  /* slength/ */
  length_t_udp *slength,
  /* initmsg/ */
  array_uint32_16_4 *initmsg,
  /* soo/ */
  Session_tindyguardTypes *soo,
  /* again/ */
  kcg_bool *again,
  outC_init_tindyguard_handshake *outC);

extern void init_reset_tindyguard_handshake(
  outC_init_tindyguard_handshake *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void init_init_tindyguard_handshake(
  outC_init_tindyguard_handshake *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::handshake::init/
  @1: (tindyguard::handshake::build_init#2)
  @2: @1/(tindyguard::handshake::ephemeral#1)
*/

#endif /* _init_tindyguard_handshake_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** init_tindyguard_handshake.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

