/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "inv25519_nacl_op.h"

/* nacl::op::inv25519/ */
void inv25519_nacl_op(
  /* _L1/, i/ */
  gf_nacl_op *i,
  /* _L2/, o/ */
  gf_nacl_op *o)
{
  gf_nacl_op acc;
  kcg_size idx;

  kcg_copy_gf_nacl_op(o, i);
  /* _L2= */
  for (idx = 0; idx < 254; idx++) {
    kcg_copy_gf_nacl_op(&acc, o);
    /* _L2=(nacl::op::inv25519_it1#1)/ */
    inv25519_it1_nacl_op(/* _L2= */(kcg_int32) idx, &acc, i, o);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** inv25519_nacl_op.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

