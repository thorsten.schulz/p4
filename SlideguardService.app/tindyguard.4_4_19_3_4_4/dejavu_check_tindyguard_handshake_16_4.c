/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "dejavu_check_tindyguard_handshake_16_4.h"

/* tindyguard::handshake::dejavu_check/ */
void dejavu_check_tindyguard_handshake_16_4(
  /* _L1/, _L33/, mac/ */
  Mac_hash_blake2s *mac_16_4,
  /* _L32/, dejavu/ */
  kcg_bool *dejavu_16_4,
  outC_dejavu_check_tindyguard_handshake_16_4 *outC)
{
  kcg_bool acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_dejavu_bucket_it_1_4;
  /* @1/IfBlock1:then:IfBlock2:then:_L6/ */
  kcg_uint32 _L6_dejavu_bucket_it_1_then_IfBlock2_then_IfBlock1_4;
  /* @1/IfBlock1:then:IfBlock2: */
  kcg_bool IfBlock2_clock_dejavu_bucket_it_1_then_IfBlock1_4;
  kcg_size idx_dejavu_bucket_it_1;
  /* _L23/ */
  kcg_uint32 _L23_16_4;

  *dejavu_16_4 = kcg_false;
  _L23_16_4 = (((((((kcg_lit_uint32(177573) ^ (*mac_16_4)[0]) * kcg_lit_uint32(
                  33)) ^ (*mac_16_4)[1]) * kcg_lit_uint32(33)) ^
          (*mac_16_4)[2]) * kcg_lit_uint32(33)) ^ (*mac_16_4)[3]) %
    kcg_lit_uint32(16);
  /* _L28= */
  for (idx = 0; idx < 16; idx++) {
    acc = *dejavu_16_4;
    IfBlock1_clock_dejavu_bucket_it_1_4 = /* _L28= */(kcg_uint32) idx == _L23_16_4;
    /* @1/IfBlock1: */
    if (IfBlock1_clock_dejavu_bucket_it_1_4) {
      cond_iterw = kcg_false;
      *dejavu_16_4 = kcg_false;
      /* @1/IfBlock1:then:_L10= */
      for (
        idx_dejavu_bucket_it_1 = 0;
        idx_dejavu_bucket_it_1 < 4;
        idx_dejavu_bucket_it_1++) {
        *dejavu_16_4 = *dejavu_16_4 || kcg_comp_Mac_hash_blake2s(
            &outC->list_dejavu_bucket_it_1_then_IfBlock1_4[idx][idx_dejavu_bucket_it_1],
            mac_16_4);
      }
      IfBlock2_clock_dejavu_bucket_it_1_then_IfBlock1_4 = !*dejavu_16_4;
      /* @1/IfBlock1:then:IfBlock2: */
      if (IfBlock2_clock_dejavu_bucket_it_1_then_IfBlock1_4) {
        _L6_dejavu_bucket_it_1_then_IfBlock2_then_IfBlock1_4 =
          outC->cnt_dejavu_bucket_it_1_then_IfBlock1_4[idx] % kcg_lit_uint32(4);
        if (_L6_dejavu_bucket_it_1_then_IfBlock2_then_IfBlock1_4 <
          kcg_lit_uint32(4)) {
          kcg_copy_Mac_hash_blake2s(
            &outC->list_dejavu_bucket_it_1_then_IfBlock1_4[idx][_L6_dejavu_bucket_it_1_then_IfBlock2_then_IfBlock1_4],
            mac_16_4);
        }
        outC->cnt_dejavu_bucket_it_1_then_IfBlock1_4[idx] =
          outC->cnt_dejavu_bucket_it_1_then_IfBlock1_4[idx] + kcg_lit_uint32(1);
      }
    }
    else {
      cond_iterw = kcg_true;
      *dejavu_16_4 = acc;
    }
    /* _L28= */
    if (!cond_iterw) {
      break;
    }
  }
}

#ifndef KCG_USER_DEFINED_INIT
void dejavu_check_init_tindyguard_handshake_16_4(
  outC_dejavu_check_tindyguard_handshake_16_4 *outC)
{
  array_uint32_4 tmp;
  array_uint32_4_4 tmp1;
  kcg_size idx;

  for (idx = 0; idx < 16; idx++) {
    outC->cnt_dejavu_bucket_it_1_then_IfBlock1_4[idx] = kcg_lit_uint32(0);
  }
  for (idx = 0; idx < 4; idx++) {
    tmp[idx] = kcg_lit_uint32(0);
  }
  for (idx = 0; idx < 4; idx++) {
    kcg_copy_array_uint32_4(&tmp1[idx], &tmp);
  }
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_array_uint32_4_4(
      &outC->list_dejavu_bucket_it_1_then_IfBlock1_4[idx],
      &tmp1);
  }
}
#endif /* KCG_USER_DEFINED_INIT */


void dejavu_check_reset_tindyguard_handshake_16_4(
  outC_dejavu_check_tindyguard_handshake_16_4 *outC)
{
  array_uint32_4 tmp;
  array_uint32_4_4 tmp1;
  kcg_size idx;

  for (idx = 0; idx < 16; idx++) {
    outC->cnt_dejavu_bucket_it_1_then_IfBlock1_4[idx] = kcg_lit_uint32(0);
  }
  for (idx = 0; idx < 4; idx++) {
    tmp[idx] = kcg_lit_uint32(0);
  }
  for (idx = 0; idx < 4; idx++) {
    kcg_copy_array_uint32_4(&tmp1[idx], &tmp);
  }
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_array_uint32_4_4(
      &outC->list_dejavu_bucket_it_1_then_IfBlock1_4[idx],
      &tmp1);
  }
}

/*
  Expanded instances for: tindyguard::handshake::dejavu_check/
  @1: (tindyguard::handshake::dejavu_bucket_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** dejavu_check_tindyguard_handshake_16_4.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

