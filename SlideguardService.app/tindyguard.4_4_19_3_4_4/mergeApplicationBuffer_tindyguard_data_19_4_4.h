/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _mergeApplicationBuffer_tindyguard_data_19_4_4_H_
#define _mergeApplicationBuffer_tindyguard_data_19_4_4_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  array_uint32_16_19 /* txmbuffer/ */ mem_txmbuffer_19_4_4;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_mergeApplicationBuffer_tindyguard_data_19_4_4;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::data::mergeApplicationBuffer/ */
extern void mergeApplicationBuffer_tindyguard_data_19_4_4(
  /* ii/ */
  size_tindyguardTypes ii_19_4_4,
  /* no_peer_acc/ */
  kcg_uint64 no_peer_acc_19_4_4,
  /* txm_residual_length/ */
  size_slideTypes txm_residual_length_19_4_4,
  /* length/ */
  array_int32_4 *length_19_4_4,
  /* msg/ */
  array_uint32_16_19_4 *msg_19_4_4,
  /* knownPeer/ */
  _4_array *knownPeer_19_4_4,
  /* ioo/ */
  size_tindyguardTypes *ioo_19_4_4,
  /* no_peer_mask/ */
  kcg_uint64 *no_peer_mask_19_4_4,
  /* pid_req/ */
  size_tindyguardTypes *pid_req_19_4_4,
  /* txmbuffer/ */
  array_uint32_16_19 *txmbuffer_19_4_4,
  /* txmlength/ */
  size_slideTypes *txmlength_19_4_4,
  /* sid/ */
  size_tindyguardTypes *sid_19_4_4,
  outC_mergeApplicationBuffer_tindyguard_data_19_4_4 *outC);

extern void mergeApplicationBuffer_reset_tindyguard_data_19_4_4(
  outC_mergeApplicationBuffer_tindyguard_data_19_4_4 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void mergeApplicationBuffer_init_tindyguard_data_19_4_4(
  outC_mergeApplicationBuffer_tindyguard_data_19_4_4 *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _mergeApplicationBuffer_tindyguard_data_19_4_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mergeApplicationBuffer_tindyguard_data_19_4_4.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

