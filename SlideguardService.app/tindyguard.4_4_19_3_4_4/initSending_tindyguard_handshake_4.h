/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _initSending_tindyguard_handshake_4_H_
#define _initSending_tindyguard_handshake_4_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "init_tindyguard_handshake.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_init_tindyguard_handshake /* _L3=(tindyguard::handshake::init#1)/ */ Context_init_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_initSending_tindyguard_handshake_4;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::handshake::initSending/ */
extern void initSending_tindyguard_handshake_4(
  /* s/ */
  Session_tindyguardTypes *s_4,
  /* _L8/, pid/ */
  size_tindyguardTypes pid_4,
  /* _L15/, knownPeers/ */
  _4_array *knownPeers_4,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_4,
  /* socket/ */
  fd_t_udp socket_4,
  /* @1/now/, now/ */
  kcg_int64 now_4,
  /* @1/soo/, _L12/, soo/ */
  Session_tindyguardTypes *soo_4,
  /* @1/IfBlock1:, @1/failed/, _L10/, fdfail/ */
  kcg_bool *fdfail_4,
  /* _L6/, again/ */
  kcg_bool *again_4,
  outC_initSending_tindyguard_handshake_4 *outC);

extern void initSending_reset_tindyguard_handshake_4(
  outC_initSending_tindyguard_handshake_4 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void initSending_init_tindyguard_handshake_4(
  outC_initSending_tindyguard_handshake_4 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::handshake::initSending/
  @1: (tindyguard::session::updateTx#1)
*/

#endif /* _initSending_tindyguard_handshake_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** initSending_tindyguard_handshake_4.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

