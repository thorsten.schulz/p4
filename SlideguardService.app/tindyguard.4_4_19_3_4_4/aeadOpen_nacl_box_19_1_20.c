/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "aeadOpen_nacl_box_19_1_20.h"

/* nacl::box::aeadOpen/ */
void aeadOpen_nacl_box_19_1_20(
  /* @1/_L43/, @1/a/, _L5/, a/ */
  Mac_nacl_onetime *a_19_1_20,
  /* @1/_L44/, @1/msg2/, _L13/, _L7/, cm/ */
  array_uint32_16_19 *cm_19_1_20,
  /* @1/_L45/, @1/mlen2/, _L12/, _L6/, mlen/ */
  int_slideTypes mlen_19_1_20,
  /* @1/_L49/, @1/msg1/, _L18/, ad/ */
  array_uint32_16_1 *ad_19_1_20,
  /* @1/_L50/, @1/mlen1/, _L19/, adlen/ */
  int_slideTypes adlen_19_1_20,
  /* _L2/, nonce/ */
  Nonce_nacl_core_chacha *nonce_19_1_20,
  /* _L3/, key/ */
  Key_nacl_core *key_19_1_20,
  /* _L8/, msg/ */
  array_uint32_16_19 *msg_19_1_20,
  /* @1/_L47/, @1/failed/, _L4/, failed/ */
  kcg_bool *failed_19_1_20)
{
  Mac_nacl_onetime tmp;
  /* _L1/ */
  State_nacl_core_chacha _L1_19_1_20;
  /* _L14/ */
  kcg_bool _L14_19_1_20;
  /* @1/_L46/, @1/k/, _L17/ */
  StreamChunk_slideTypes _L17_19_1_20;
  kcg_size idx;

  /* _L1=(nacl::core::chacha::setup#1)/ */
  setup_nacl_core_chacha_20(
    nonce_19_1_20,
    key_19_1_20,
    &_L1_19_1_20,
    &_L17_19_1_20);
  /* @1/_L41=(nacl::onetime::auth2#1)/ */
  auth2_nacl_onetime_1_19(
    ad_19_1_20,
    adlen_19_1_20,
    cm_19_1_20,
    mlen_19_1_20,
    &_L17_19_1_20,
    &tmp);
  *failed_19_1_20 = kcg_lit_int32(0) != /* @1/_L42=(M::cmp#1)/ */
    cmp_M_uint8_16(&tmp, a_19_1_20);
  _L14_19_1_20 = !*failed_19_1_20;
  if (_L14_19_1_20) {
    /* _L8=(nacl::box::stream::chacha_IETF#1)/ */
    chacha_IETF_nacl_box_stream_19_20(
      cm_19_1_20,
      mlen_19_1_20,
      &_L1_19_1_20,
      msg_19_1_20);
  }
  else {
    for (idx = 0; idx < 19; idx++) {
      kcg_copy_StreamChunk_slideTypes(
        &(*msg_19_1_20)[idx],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
  }
}

/*
  Expanded instances for: nacl::box::aeadOpen/
  @1: (nacl::onetime::verify2#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** aeadOpen_nacl_box_19_1_20.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

