/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "collectCrops_tindyguard_session_19_3.h"

/* tindyguard::session::collectCrops/ */
void collectCrops_tindyguard_session_19_3(
  /* _L5/, worker_sid/ */
  size_tindyguardTypes worker_sid_19_3,
  /* _L2/, rxmlength/ */
  array_int32_3 *rxmlength_19_3,
  /* _L1/, rxmbuffer/ */
  array_uint32_16_19_3 *rxmbuffer_19_3,
  /* _L4/, length_in/ */
  length_t_udp *length_in_19_3,
  /* _L3/, in/ */
  array_uint32_16_19 *in_19_3)
{
  kcg_size idx;

  if (kcg_lit_int32(0) <= worker_sid_19_3 && worker_sid_19_3 < kcg_lit_int32(
      3)) {
    *length_in_19_3 = (*rxmlength_19_3)[worker_sid_19_3];
    kcg_copy_array_uint32_16_19(in_19_3, &(*rxmbuffer_19_3)[worker_sid_19_3]);
  }
  else {
    *length_in_19_3 = nil_udp;
    for (idx = 0; idx < 19; idx++) {
      kcg_copy_chunk_t_udp(&(*in_19_3)[idx], (chunk_t_udp *) &Zero_udp);
    }
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** collectCrops_tindyguard_session_19_3.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

