/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _wrap_tindyguard_data_19_H_
#define _wrap_tindyguard_data_19_H_

#include "kcg_types.h"
#include "aead_nacl_box_19_1_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::data::wrap/ */
extern void wrap_tindyguard_data_19(
  /* @1/_L1/, @1/i/, _L65/, len/ */
  size_slideTypes len_19,
  /* msg/ */
  array_uint32_16_19 *msg_19,
  /* s/ */
  Session_tindyguardTypes *s_19,
  /* slength/ */
  length_t_udp *slength_19,
  /* _L75/, soffs/ */
  length_t_udp *soffs_19,
  /* datamsg/ */
  array_uint32_16_20 *datamsg_19,
  /* soo/ */
  Session_tindyguardTypes *soo_19,
  /* _L73/, fail/, failed/ */
  kcg_bool *failed_19);

/*
  Expanded instances for: tindyguard::data::wrap/
  @1: (M::dec#2)
*/

#endif /* _wrap_tindyguard_data_19_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** wrap_tindyguard_data_19.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

