/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "updatePeer_tindyguard_session_3.h"

/* tindyguard::session::updatePeer/ */
void updatePeer_tindyguard_session_3(
  /* _L23/, pid/ */
  size_tindyguardTypes pid_3,
  /* _L21/, knownPeer/ */
  Peer_tindyguardTypes *knownPeer_3,
  /* _L18/, isNewTAI/ */
  array_bool_3 *isNewTAI_3,
  /* _L19/, s/ */
  array *s_3,
  /* _L25/, t_flag/ */
  array_bool_3 *t_flag_3,
  /* _L22/, updatedPeer/ */
  Peer_tindyguardTypes *updatedPeer_3)
{
  Peer_tindyguardTypes acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_updatePeer_it_1;
  /* @1/IfBlock1:then:_L13/ */
  Peer_tindyguardTypes _L13_updatePeer_it_1_then_IfBlock1;
  /* @1/IfBlock1:then:_L17/ */
  Peer_tindyguardTypes _L17_updatePeer_it_1_then_IfBlock1;

  kcg_copy_Peer_tindyguardTypes(updatedPeer_3, knownPeer_3);
  (*updatedPeer_3).inhibitInit = kcg_false;
  /* _L12= */
  for (idx = 0; idx < 3; idx++) {
    kcg_copy_Peer_tindyguardTypes(&acc, updatedPeer_3);
    IfBlock1_clock_updatePeer_it_1 = pid_3 == (*s_3)[idx].pid;
    /* @1/IfBlock1: */
    if (IfBlock1_clock_updatePeer_it_1) {
      kcg_copy_Peer_tindyguardTypes(&_L17_updatePeer_it_1_then_IfBlock1, &acc);
      _L17_updatePeer_it_1_then_IfBlock1.inhibitInit = kcg_true;
      _L17_updatePeer_it_1_then_IfBlock1.timedOut = (*t_flag_3)[idx];
      /* @1/IfBlock1:then:_L13= */
      if ((*isNewTAI_3)[idx] || (*s_3)[idx].peer.endpoint.mtime >
        acc.endpoint.mtime || (*s_3)[idx].peer.endpoint.mtime <= kcg_lit_int64(
          0)) {
        kcg_copy_Peer_tindyguardTypes(
          &_L13_updatePeer_it_1_then_IfBlock1,
          &_L17_updatePeer_it_1_then_IfBlock1);
        _L13_updatePeer_it_1_then_IfBlock1.bestSession = /* _L12= */(kcg_int32) idx;
        kcg_copy_peer_t_udp(
          &_L13_updatePeer_it_1_then_IfBlock1.endpoint,
          &(*s_3)[idx].peer.endpoint);
      }
      else {
        kcg_copy_Peer_tindyguardTypes(
          &_L13_updatePeer_it_1_then_IfBlock1,
          &_L17_updatePeer_it_1_then_IfBlock1);
      }
      cond_iterw = !(*isNewTAI_3)[idx];
      /* @1/IfBlock1:then:_L9= */
      if ((*isNewTAI_3)[idx]) {
        kcg_copy_Peer_tindyguardTypes(
          updatedPeer_3,
          &_L13_updatePeer_it_1_then_IfBlock1);
        kcg_copy_TAI_tindyguardTypes(&(*updatedPeer_3).tai, &(*s_3)[idx].peer.tai);
      }
      else {
        kcg_copy_Peer_tindyguardTypes(
          updatedPeer_3,
          &_L13_updatePeer_it_1_then_IfBlock1);
      }
    }
    else {
      cond_iterw = kcg_true;
      kcg_copy_Peer_tindyguardTypes(updatedPeer_3, &acc);
    }
    /* _L12= */
    if (!cond_iterw) {
      break;
    }
  }
}

/*
  Expanded instances for: tindyguard::session::updatePeer/
  @1: (tindyguard::session::updatePeer_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** updatePeer_tindyguard_session_3.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

