/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hash_hash_blake2s_2.h"

/* hash::blake2s::hash/ */
void hash_hash_blake2s_2(
  /* _L64/, msg/ */
  array_uint32_16_2 *msg_2,
  /* _L66/, len/ */
  size_slideTypes len_2,
  /* _L70/, keybytes/ */
  size_slideTypes keybytes_2,
  /* _L61/, hash/ */
  Hash_hash_blake2s *hash_2)
{
  array_uint32_8 acc;
  kcg_bool cond_iterw;
  kcg_size idx;

  (*hash_2)[0] = IV_hash_blake2s[0] ^ (kcg_lit_uint32(16842784) |
      kcg_lsl_uint32(/* _L72= */(kcg_uint32) keybytes_2, kcg_lit_uint32(8)));
  kcg_copy_array_uint32_7(&(*hash_2)[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
  /* _L60= */
  for (idx = 0; idx < 2; idx++) {
    kcg_copy_Hash_hash_blake2s(&acc, hash_2);
    /* _L60=(hash::blake2s::stream_it#1)/ */
    stream_it_hash_blake2s(
      /* _L60= */(kcg_int32) idx,
      &acc,
      &(*msg_2)[idx],
      len_2,
      &cond_iterw,
      hash_2);
    /* _L60= */
    if (!cond_iterw) {
      break;
    }
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hash_hash_blake2s_2.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

