/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _singleChunk_hash_blake2s_H_
#define _singleChunk_hash_blake2s_H_

#include "kcg_types.h"
#include "stream_it_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::singleChunk/ */
extern void singleChunk_hash_blake2s(
  /* _L64/, msg/ */
  StreamChunk_slideTypes *msg,
  /* _L66/, len/ */
  size_slideTypes len,
  /* _L65/, chunk/ */
  HashChunk_hash_blake2s *chunk);



#endif /* _singleChunk_hash_blake2s_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** singleChunk_hash_blake2s.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

