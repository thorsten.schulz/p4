/* -------------------------- FILE PROLOGUE -------------------------------- */

/**
 *  @if INCLUDE_HEADER
 *  @copyright
 *    (C) Copyright SYSGO AG.
 *    Ulm, Germany
 *    All rights reserved.
 *  @endif
 *
 *  @file
 *    $RCSfile: lwip_config.c,v $
 *
 *  @purpose
 *    lwIP Configuration Parameters
 *
 *  @if INCLUDE_HEADER
 *  @cfg_management
 *    $Id: lwip_config.c,v 1.17 2017/02/16 09:52:17 tmu Exp $
 *    $Author: tmu $
 *    $Date: 2017/02/16 09:52:17 $
 *    $Revision: 1.17 $
 *    $State: Exp $
 *  @endif
 */

/* ------------------------- FILE INCLUSION -------------------------------- */

#include <sys/cdefs.h>
__RCSID("$Id: lwip_config.c,v 1.17 2017/02/16 09:52:17 tmu Exp $");

#include "lwip_config.h"

/* ----------------------- OBJECT DECLARATIONS ----------------------------- */

struct _lwip_config __lwip_config = {
	.version = VERSION_LWIP_CONFIGURABLES,
	.interfaces = {
		{
			.mode = MODE_LOOPBACK,
			.ifname = "lo"
		},
		{
			.mode = MODE_ETH,
			.ifname = "i1",
			.dev.eth.device = "/ssw/eth0:1",
			.mtu = 1500,

			.enable_ipv4 = 1,
			.target_ip  = { 192, 168,   0,   3 },
			.netmask    = { 255, 255, 255,   0 },
			.gateway_ip = { 192, 168,   0,   1 },

			.enable_ipv6 = 0,
		},
		{
			.mode = MODE_NONE,
			.ifname = "i2",
		},
		{
			.mode = MODE_NONE,
			.ifname = "i3",
		},
		{
			.mode = MODE_NONE,
			.ifname = "i4",
		},
		{
			.mode = MODE_NONE,
			.ifname = "i5",
		},
	},
	.nameservers = {
		{
			.server_ip = {   0,   0,   0,   0 },
		},
		{
			.server_ip = {   0,   0,   0,   0 },
		},
		{
			.server_ip = {   0,   0,   0,   0 },
		},
		{
			.server_ip = {   0,   0,   0,   0 },
		},
	},
};
