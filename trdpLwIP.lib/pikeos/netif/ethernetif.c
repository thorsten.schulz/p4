
/**
 * @if INCLUDE_HEADER
 * @copyright
 *   (C) Copyright SYSGO AG.
 *   Ulm, Germany
 *   All rights reserved.
 * @endif
 *
 * @file
 *
 * @purpose
 *   lwIP PikeOS/POSIX port, network driver interface
 *
 * @if INCLUDE_HEADER
 * @cfg_management
 *   $Id: ethernetif.c,v 1.64 2017/02/20 09:24:53 tmu Exp $
 *   $HeadURL: $
 *   $Author: tmu $
 *   $Date: 2017/02/20 09:24:53 $
 *   $Revision: 1.64 $
 * @endif
 */

/* ----------------------- UPSTREAM CODE HEADER ---------------------------- */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

/* ------------------------- RCS ID ---------------------------------------- */

#include <sys/cdefs.h>
__RCSID("$Id: ethernetif.c,v 1.64 2017/02/20 09:24:53 tmu Exp $");

/* ------------------------- FILE INCLUSION -------------------------------- */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/qport.h>

#include "lwip/opt.h"
#include "lwip/def.h"
#include "lwip/igmp.h"
#include "lwip/mem.h"
#include "lwip/pbuf.h"
#include "lwip/sys.h"
#include "lwip/stats.h"
#include "lwip/snmp.h"
#include "lwip/tcpip.h"

#include "lwip/inet.h"

#include "lwip/ethip6.h"
#include "lwip/mld6.h"
#include "lwip/etharp.h"
#include "arch/ethernetif.h"
#include "arch/cc.h"

#include "lwip_config.h"

#include <vm.h>
#include <drv/drv_net_api.h>
#include "arch/sbuf_wrapper.h"

#include <ddapi/ddapi.h>

/* ------------------- CONSTANT / MACRO DEFINITIONS ------------------------ */

/* Debug print wrapper for device driver threads. */
#if 0
#define DDEBUG(x)	dd_printf x
#else
#define DDEBUG(x)
#endif

/* If defined, prints hexdump of frames to stderr. */
#undef DUMP_PACKET

#ifdef FIOGETFD
/*
 * If defined, uses device driver thread with circular queue as input buffer
 * for frame reception.
 */
# define USE_CQ
#endif

#ifdef USE_CQ
/*
 * Create a DDAPI (aka native PikeOS) thread which reads data from the
 * input device (e.g. Ethernet file provider) and feeds a circular
 * queue. The input queue holds CQ_SIZE_IN buffers. In case the DD
 * thread would overflow the queue, it will suspend on an event and
 * wait for the app to drain the queue. Once the queue reaches
 * CQ_REFILL_THR_IN used buffers (CQ_SIZE_IN - CQ_REFILL_THR_IN empty
 * buffers) the thread will be signaled to resume reading data.
 *
 * The synchronization between the local DDAPI thread using the
 * circular queue imposes less overhead than the 'usual' path through
 * read().
 */

/* Device driver thread stack size. */
#define DD_STACK		0x1000
/* Size of circular queue. */
#define CQ_SIZE_IN		32
/* Queue size when to signal other end of queue. */
#define CQ_REFILL_THR_IN	(CQ_SIZE_IN / 4)

/* Access to cq::m */
#define QB(q, m)	(q).m

/* Access to cq::bufs[i] */
#define QBIDX(q, i)	&(q)->bufs[i]
#endif

/* ------------------------ TYPE DECLARATIONS ------------------------------ */

#ifdef USE_CQ
/* A buffer on the circular queue, an Ethernet frame and its size */
struct buf {
	char *data;
	unsigned len;
};

/* Circular queue. */
struct cq {
	struct buf *bufs;	/* Data buffers, allocated in cq_alloc(). */
	int num_bufs;		/* Number of buffers. */

	int start;		/* Read  index, beginning of data. */
	int end;		/* Write index, end of data. */
	int size;		/* Fill count. */

	/* Queue direction as seen from the application thread end. */
	enum cq_dir { CQ_IN, CQ_OUT } dir;

	dd_thread_t dd_thr;	/* DD thread descriptor. */
	pthread_mutex_t mtx;	/* I/O synchronisation mutex. */
	pthread_cond_t cv;	/* I/O synchronisation condition variable. */
};
#endif

/* Ethernet interface descriptor. */
struct ethernetif {
	/* Interface number. */
	int ifnum;
	/* Input file descriptor. */
	int fd_in;
	/* Output file descriptor. */
	int fd_out;
	/* Interface mode. */
	enum i_mode mode;
	/*
	 * maximum frame size (size of areas pointed to by
	 * ethernetif::{inbuf,outbuf} and buf::data).
	 */
	int frame_size;
	/* Read buffer, used as argument to read() in non-CQ mode. */
	char *inbuf;
	/*
	 * Write buffer. A PBUF chain will first be copied to this buffer
	 * before passing it to write(). Non-chained PBUFs are written
	 * directly from the PBUF payload.
	 */
	char *outbuf;

#ifdef USE_CQ
	/* Pointer to PSSW file descriptor. If non-NULL CQ mode is in effect. */
	void *ssw_fd_in;
	/* Circular queue for input path. */
	struct cq cq_in;
#endif

	/* SBUF descriptor. */
	drv_sbuf_desc_t net_shm;
};
/* ----------------------- FUNCTION DECLARATIONS --------------------------- */

static void ethernetif_thread(void *arg);
#if LWIP_IPV4 && LWIP_IGMP
/* netif_igmp_mac_filter_fn is a function _pointer_ type... sigh! */
static err_t igmp_mac_filter(struct netif *, const ip4_addr_t *,
			     enum netif_mac_filter_action);
#endif
#if LWIP_IPV6 && LWIP_IPV6_MLD
/* netif_mld_mac_filter_fn is a function _pointer_ type... sigh! */
static err_t mld_mac_filter(struct netif *, const ip6_addr_t *,
			    enum netif_mac_filter_action);
#endif

#ifdef USE_CQ
static void dd_input_thread(void *);
#endif

/* -------------------------- LOCAL FUNCTIONS ------------------------------ */

#ifdef USE_CQ

/* ********************************************************************
 * CQ stuff (p4wqueue reinvented)
 */

/* Allocate a circular queue with 'num_bufs' buffers. */
static int
cq_alloc(struct cq *q, enum cq_dir dir, int num_bufs, int bsize)
{
	int n;

	q->dir = dir;
	q->size = 0;
	q->start = 0;
	q->end = 0;
	q->num_bufs = 0;
	if (!num_bufs)
		return EINVAL;

	if ((q->bufs = malloc(num_bufs * sizeof(struct buf))) == NULL)
		return ENOMEM;

	q->num_bufs = num_bufs;
	for (n = 0; n < num_bufs; n++) {
		if ((q->bufs[n].data = malloc(bsize)) == NULL) {
			while (--n >= 0)
				free(q->bufs[n].data);
			free(q->bufs);
			return ENOMEM;
		}
		QB(q->bufs[n], len) = 0;
	}

	return 0;
}

#ifdef unused
/* Free a previously allocated circular queue. */
static int
cq_free(struct cq *q)
{
	int n;

	for (n = 0; n < q->num_bufs; n++)
		free(q->bufs[n].data);
	free(q->bufs);
	q->bufs = NULL;
	q->size = 0;
	q->start = 0;
	q->end = 0;
	q->num_bufs = 0;
	return 0;
}
#endif

/*
 * Return next free write buffer of circular queue.
 * Returns 0 on success, error code otherwise.
 */
static int
cq_put_getbuf(struct cq *q, char **buf, int *idx)
{
	struct buf *bd;

	if (q->size == q->num_bufs)	/* Queue full? */
		return EAGAIN;

	*idx = q->end;
	bd = QBIDX(q, q->end);
	*buf = bd->data;
	return 0;
}

/*
 * Commit next write buffer to circular queue. Signals 'other end' of
 * queue if required.
 * Returns 0 on success, error code otherwise.
 */
static int
cq_put_buf(struct cq *q, int idx, unsigned int size)
{
	struct buf *bd;

	if (q->end != idx) {
		DDEBUG(("%s@%d: uhoh\n", __func__, __LINE__));
		return EINVAL;
	} else if (!size) {
		DDEBUG(("%s@%d: sz uhoh\n", __func__, __LINE__));
		return EINVAL;
	} else {
		bd = QBIDX(q, idx);
		bd->len = size;
		q->end++;
		if (q->end >= q->num_bufs)
			q->end = 0;

		if (q->size == 0) {
			q->size = 1;
			if (q->dir == CQ_IN)
				pthread_cond_signal(&q->cv);
			else
				dd_event_signal(&q->dd_thr);
		} else
			q->size++;
	}
	return 0;
}

#ifdef unused
/*
 * Copy data from 'buf' to to circular queue.
 * Returns 0 on success, error code otherwise.
 */
static int
cq_put_data(struct cq *q, const char *buf, unsigned int size)
{
	char *data;
	int idx;

	if (cq_put_getbuf(q, &data, &idx) != 0)
		return EAGAIN;

	memcpy(data, buf, size);
	cq_put_buf(q, idx, size);
	return 0;
}
#endif

/*
 * Return length and address of oldest buffer.
 * Returns 0 on empty queue.
 */
static int
cq_get_buf(struct cq *q, char **buf, int *idx)
{
	struct buf *bd;
	int size;

	if (q->size == 0)
		return 0;

	*idx = q->start;
	bd = QBIDX(q, q->start);
	*buf = bd->data;
	size = bd->len;
	return size;
}

/*
 * 'Pop' oldest buffer (identified by idx) from queue. Signal 'other
 * end of queue' if required.
 * Returns 0 on success, error code otherwise.
 */
static int
cq_free_buf(struct cq *q, int idx)
{
	if (q->start != idx) {
		DDEBUG(("%s@%d: uhoh\n", __func__, __LINE__));
		return EINVAL;
	} else {
		q->start++;
		if (q->start >= q->num_bufs)
			q->start = 0;

		q->size--;
		if (q->size == CQ_REFILL_THR_IN) {
			if (q->dir == CQ_IN)
				dd_event_signal(&q->dd_thr);
			else
				pthread_cond_signal(&q->cv);
		}
	}
	return 0;
}

#ifdef unused
/*
 * Copy most recent buffer of circular queue to 'buf'.
 * Returns buffer size upon success, else 0.
 */
static int
cq_get_data(struct cq *q, char *buf)
{
	char *data;
	int size;
	int idx;

	if ((size = cq_get_buf(q, &data, &idx)) == 0)
		return 0;

	memcpy(buf, data, size);
	cq_free_buf(q, idx);
	return size;
}
#endif
#endif

static __printflike(2, 0) void
vwarnc(int code, const char *fmt, va_list ap)
{
	char ebuf[_ERR_TEXTMAX];	/* 64 */

	flockfile(stderr);
	if (fmt != NULL) {
		vfprintf(stderr, fmt, ap);
		fprintf(stderr, ": ");
	}
	strerror_r(code, ebuf, sizeof(ebuf));
	fprintf(stderr, "%s\n", ebuf);
	funlockfile(stderr);
}

static __printflike(2, 3) void
warnc(int code, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vwarnc(code, fmt, ap);
	va_end(ap);
}

static __printflike(1, 2) void
warn(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vwarnc(errno, fmt, ap);
	va_end(ap);
}

/* ******************************************************************** */

/*
 * Open queuing port 'path' identified as  'name' with open 'flags'. 
 * Store queue block/message size in 'blksize' upon successful completion..
 * Returns file descriptor >=0 on success, -1 with errno set on error.
 */
static int
open_eth_port(const char *path, const char *name, int flags, int *blksize)
{
	struct stat sb;
	int fd;

	fd = open(path, flags);
	if (fd == -1) {
		warn("open %s \"%s\"", name, path);
		return -1;
	}
	if (fstat(fd, &sb) == -1) {
		warn("fstat %s \"%s\"", name, path);
		close(fd);
		return -1;
	}
	*blksize = sb.st_blksize;
	return fd;
}

/* Initialize Ethernet interface. */
static err_t
low_level_init(struct netif *netif)
{
	struct ethernetif *ethernetif = (struct ethernetif *)netif->state;
	struct _lwip_config_if *cif;
	struct timespec to;
	char name[32];
	int r;

	/* set MAC hardware address length. */
	netif->hwaddr_len = 6 ; /* ETHARP_HWADDR_LEN, sizeof(netif->hwaddr) */

	/*
	 * XXX Could poll DRV_NET_IOCTL_GET_LINK_STAT once in a while
	 * in MODE_ETH and set NETIF_FLAG_LINK_xxx accordingly.
	 */

	/*
	 * Note that we treat MODE_PORT as Ethernet, even if it looks
	 * like a point-to-point interface.
	 */
	netif->flags =
		NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP |
		NETIF_FLAG_ETHERNET | NETIF_FLAG_LINK_UP;
	/* Set device name */
	cif = &__lwip_config.interfaces[ethernetif->ifnum];
	memcpy(netif->name, cif->ifname, 2);
	/* Maximum transfer unit */
	if (cif->mtu < 68) {
		/*
		 * Uninitialised or otherwise silly configuration. Replace
		 * by default MTU.
		 */
		netif->mtu = 1500;
	} else
		netif->mtu = cif->mtu;

	ethernetif->mode = cif->mode;
	if (ethernetif->mode == MODE_PORT) {
		int rx_blksize;
		int tx_blksize;
		
		ethernetif->net_shm.rx = NULL;
		ethernetif->net_shm.tx = NULL;
		/* Open outgoing port */
		ethernetif->fd_out = open_eth_port(cif->dev.port.tx_port,
						   "tx_port", O_WRONLY,
						   &tx_blksize);
		if (ethernetif->fd_out == -1)
			return ERR_IF;

		/* Open incoming port */
		ethernetif->fd_in = open_eth_port(cif->dev.port.rx_port,
						  "rx_port", O_RDONLY,
						  &rx_blksize);
		if (ethernetif->fd_in == -1) {
			close(ethernetif->fd_out);
			ethernetif->fd_out = -1;
			return ERR_IF;
		}
		if (tx_blksize != rx_blksize) {
			fprintf(stderr,
				"block/message size of \"%s\" and \"%s\" "
				"do not match %d/%d\n",
				cif->dev.port.rx_port, cif->dev.port.tx_port,
				rx_blksize, tx_blksize);
			close(ethernetif->fd_out);
			ethernetif->fd_out = -1;
			close(ethernetif->fd_in);
			ethernetif->fd_in = -1;
			return ERR_IF;
		}
		ethernetif->frame_size = tx_blksize;

		/* Set port timeout. */
		if (cif->tx_to == (unsigned)-1) {
			/* Infinite timeout */
			to.tv_sec = ~0;
			to.tv_nsec = 0;
		} else if (cif->tx_to == 0) {
			/* Zero timeout, i.e. non-blocking I/O */
			to.tv_sec = 0;
			to.tv_nsec = 0;
		} else {
			to.tv_sec = cif->tx_to / 1000;
			to.tv_nsec = (cif->tx_to % 1000) * 1000 * 1000;
		}
		if (ioctl(ethernetif->fd_out, QPORT_STOUT, &to) == -1)
			warn("Ethernet device \"%c%c\": "
			     "failed to set transmit timeout",
			     netif->name[0], netif->name[1]);

		memcpy(netif->hwaddr, cif->target_mac, netif->hwaddr_len);
	} else {
		struct stat st;
		int oflags;
		int n;

		oflags = O_RDWR | O_MAP;
		for (n = 0; n < 2; oflags ^= O_MAP, n++) {
			ethernetif->net_shm.rx = NULL;
			ethernetif->net_shm.tx = NULL;
#ifndef USE_CQ
			/* O_MAP only makes sense if USE_CQ is defined. */
			if ((oflags & O_MAP) != 0)	
				continue;
#endif
			/* Open FP */
			ethernetif->fd_in = open(cif->dev.eth.device, oflags);
			if (ethernetif->fd_in < 0) {
				LWIP_DEBUGF(ARCH_DEBUG,
					    ("open Ethernet device \"%s\" "
					     "O_RD_WR%s: %s\n",
					     cif->dev.eth.device,
					     oflags & O_MAP ? "|O_MAP" : "",
					     strerror(errno)));
				continue;
			}
			if (oflags & O_MAP) {
				dd_iomem_region_t region;
				size_t sbuf_map_size;

				LWIP_DEBUGF(ARCH_DEBUG,
					    ("%s: %s Ethernet device "
					     "\"%s\": SBUF mode capable\n",
					     __func__, cif->ifname,
					     cif->dev.eth.device));


				r = _sbuf_init(ethernetif->fd_in,
					       &sbuf_map_size);
				if (r != 0) {
					LWIP_DEBUGF(ARCH_DEBUG,
						    ("%s: %s Ethernet device "
						     "\"%s\": "
						     "_sbuf_init: %s\n",
						     __func__, cif->ifname,
						     cif->dev.eth.device,
						     strerror(-r)));
				close_retry:
					/*
					 * Device opened but SBUF mode set
					 * failed, or was it the size
					 * inquiry, we don't know. Close
					 * device and try again.
					 */
					if (close(ethernetif->fd_in) < 0) {
						warn("close Ethernet device"
						     " \"%s\"",
						     cif->dev.eth.device);
						return ERR_IF;
					}
					continue;
				}
				/*
				 * could init sbuf mode
				 * now, get some free address space and map
				 * the sbuf into it
				 */
				r = dd_iomem_alloc_region(sbuf_map_size,
							  PAGE_SIZE, 0,
							  &region);
				if (r != 0) {
					warnc(r, "%s: %s Ethernet device "
					      "\"%s\": "
					      "dd_iomem_alloc_region(%zu)",
					      __func__, cif->ifname,
					      cif->dev.eth.device,
					      sbuf_map_size);
					goto close_retry;
				}
				r = _sbuf_map(ethernetif->fd_in,
					      region.virt,
					      &ethernetif->net_shm);
				if (r < 0) {
					ethernetif->net_shm.rx = NULL;
					ethernetif->net_shm.tx = NULL;
					dd_iomem_free_region(&region);
					warnc(-r, "%s: %s Ethernet device "
					      "\"%s\": "
					      "Failed to map SBUF",
					      __func__, cif->ifname,
					      cif->dev.eth.device);
					goto close_retry;
				}
				LWIP_DEBUGF(ARCH_DEBUG,
					    ("%s: %s Ethernet device "
					     "\"%s\": SBUF enabled\n",
					     __func__, cif->ifname,
					     cif->dev.eth.device));
				break;
			} else {
				LWIP_DEBUGF(ARCH_DEBUG,
					    ("%s: %s Ethernet device "
					     "\"%s\": simple FP mode\n",
					     __func__, cif->ifname,
					     cif->dev.eth.device));
			}
		}
		if (ethernetif->fd_in == -1) {
			warn("open Ethernet device \"%s\"",
			     cif->dev.eth.device);
			return ERR_IF;
		}
		/* FP is bidirectional. */
		ethernetif->fd_out = ethernetif->fd_in;

		if (ioctl(ethernetif->fd_in, DRV_NET_IOCTL_GET_MAC,
			  netif->hwaddr) == -1) {
			warn("Failed to inquire MAC address for \"%c%c\"",
				netif->name[0], netif->name[1]);
			close(ethernetif->fd_in);
			ethernetif->fd_in = -1;
			ethernetif->fd_out = -1;
			ethernetif->net_shm.rx = ethernetif->net_shm.tx = NULL;
		} else if (fstat(ethernetif->fd_in, &st) < 0) {
			warn("fstat() failed for \"%c%c\"",
			     netif->name[0], netif->name[1]);
			close(ethernetif->fd_in);
			ethernetif->fd_in = -1;
			ethernetif->fd_out = -1;
			ethernetif->net_shm.rx = ethernetif->net_shm.tx = NULL;
		} else {
			ethernetif->frame_size = st.st_blksize;

#if LWIP_IGMP || (LWIP_IPV6 && LWIP_IPV6_MLD)
			{
				int has_mcast;

				/*
				 * Check for driver multicast support ...
				 * ... in a most peculiar way.
				 */
				has_mcast = ioctl(
					ethernetif->fd_in,
					DRV_NET_IOCTL_RST_MCAST, NULL) == 0;

#if LWIP_IGMP
				if (has_mcast) {
					netif->flags |= NETIF_FLAG_IGMP;
					netif_set_igmp_mac_filter(
						netif, igmp_mac_filter);
				}
#endif
#if LWIP_IPV6 && LWIP_IPV6_MLD
				if (has_mcast) {
					netif->flags |= NETIF_FLAG_MLD6;
					netif_set_mld_mac_filter(
						netif, mld_mac_filter);
				}
#endif
			}
#endif
		}
	}

	/*
	 * RFC791 
	 *  Every internet module must be able to forward a
	 *  datagram of 68 octets without further fragmentation.
	 */
	if (ethernetif->frame_size < SIZEOF_ETH_HDR + 68) {
		fprintf(stderr,
			"%s: %s device frame size %d is too small (need %u)\n",
			__func__, cif->ifname,
			ethernetif->frame_size, SIZEOF_ETH_HDR + 68);
		close(ethernetif->fd_in);
		if (ethernetif->fd_in != ethernetif->fd_out)
			close(ethernetif->fd_out);
		ethernetif->fd_out = -1;
		ethernetif->fd_in = -1;
		return ERR_IF;
	}

	LWIP_DEBUGF(ARCH_DEBUG,
		    ("%s: %s device frame size %d MTU %u\n",
		     __func__, cif->ifname,
		     ethernetif->frame_size, netif->mtu));
	if (ethernetif->frame_size - SIZEOF_ETH_HDR < netif->mtu) {
#ifdef LWIP_DEBUG
		unsigned o_mtu;

		o_mtu = netif->mtu;
#endif
		netif->mtu = ethernetif->frame_size - SIZEOF_ETH_HDR;
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("%s: %s Ethernet device "
			     "\"%s\": MTU reduced from %u to %u\n",
			     __func__, cif->ifname, cif->dev.eth.device,
			     o_mtu, netif->mtu));
	}
	    
	/* Allocate interface buffers. */
	if (ethernetif->fd_out != -1 && ethernetif->fd_in != -1) {
		ethernetif->outbuf = NULL;
		if ((ethernetif->inbuf =
		     malloc(ethernetif->frame_size)) == NULL ||
		    (ethernetif->outbuf =
		     malloc(ethernetif->frame_size)) == NULL) {
			fprintf(stderr,
				"failed to allocate interface buffers (2*%d) "
				" for \"%c%c\"\n",
				ethernetif->frame_size,
				netif->name[0], netif->name[1]);
			free(ethernetif->inbuf);
			free(ethernetif->outbuf);
			close(ethernetif->fd_in);
			if (ethernetif->fd_in != ethernetif->fd_out)
				close(ethernetif->fd_out);
			ethernetif->fd_in = -1;
			ethernetif->fd_out = -1;
			return ERR_IF;
		}
	}

	/* Fire ethernetif thread(s). */
#ifdef USE_CQ
	{
		ethernetif->ssw_fd_in = NULL;
		/*
		 * The CQ mode of operation is only a win for FP based
		 * interfaces.
		 */
		if (cif->mode == MODE_ETH) {
			void *ssw_fd;

			ssw_fd = NULL;
			if (ioctl(ethernetif->fd_in,
				  FIOGETFD, &ssw_fd) != -1 &&
			    ssw_fd != NULL) {
				static char cq_name[] = "if:XX:rx";

				cq_name[3] = netif->name[0];
				cq_name[4] = netif->name[1];
				if ((r = pthread_mutex_init(
					     &ethernetif->cq_in.mtx,
					     NULL)) == 0 &&
				    (r = pthread_cond_init(
					    &ethernetif->cq_in.cv,
					    NULL)) == 0 &&
				    (r = cq_alloc(
					    &ethernetif->cq_in, CQ_IN,
					    CQ_SIZE_IN,
					    ethernetif->net_shm.rx == NULL ?
					    ethernetif->frame_size :
					    (int)sizeof(vm_io_buf_id_t))) == 0) {
					ethernetif->ssw_fd_in = ssw_fd;
					pthread_mutex_setname_np(
						&ethernetif->cq_in.mtx,
						cq_name);
					pthread_cond_setname_np(
						&ethernetif->cq_in.cv,
						cq_name);
					r = dd_create_thread(
						&ethernetif->cq_in.dd_thr, 0,
						dd_input_thread, DD_STACK,
						dd_priority(DD_PRIO_MEDIUM),
						cq_name,
						1, ethernetif);
					if (r != 0) {
						pthread_cond_destroy(
							&ethernetif->cq_in.cv);
						pthread_mutex_destroy(
							&ethernetif->cq_in.mtx);
						ethernetif->ssw_fd_in = NULL;
						warnc(r,
						      "%s: %s Ethernet device "
						      "\"%s\": "
						      "create CQ DD thread",
						      __func__, cif->ifname,
						      cif->dev.eth.device);
					}
				} else {
					warnc(r, "%s: %s Ethernet device "
					      "\"%s\": initialize CQ DD thread",
					      __func__, cif->ifname,
					      cif->dev.eth.device);
				}
			}
		}
	}
#endif
	snprintf(name, sizeof(name), "%s:%c%c", ETHIF_THREAD_NAME,
		 netif->name[0], netif->name[1]);
	sys_thread_new(name, ethernetif_thread, netif,
		       ETHIF_THREAD_STACKSIZE, ETHIF_THREAD_PRIO);
	return ERR_OK;
}

#ifdef DUMP_PACKET

/****************************************
* dump a memory block (HEX & ASCII)
****************************************/

#include <stdio.h>
#include <ctype.h>

#define NBPL	16			/* # of dumped bytes per line */

#ifndef min
# define min(a,b)		((a)<(b)?(a):(b))
#endif

static int
dump_block(FILE *fp, const void *data, int size)
{
	const unsigned char *blk = data;
	char ascii[NBPL + 1];		/* ASCII dump buffer */
	char hex[NBPL + 1][3];		/* hex dump buffer */
	int i;
	int ix;
	int c;

	/*
	 * loop through bytes;
	 * i is counting down total bytes
	 * ix is index into byte in line
	 */
	for (i = size, ix = 0;; --i, ++ix) {
		/* end of line reached or end of last line */
		if (ix >= NBPL ||	  /* end of line reached */
		    (i <= 0 && ix > 0)) { /* no more bytes && anything to print */
			fprintf(fp, "\t%-*s   %s\n", NBPL * 3, hex[0], ascii);
			ix = 0;
		}

		/* check end of buffer reached */
		if (i <= 0)
			break;

		/* check start of a new line reached */
		if (ix == 0)
			memset(ascii, 0, (sizeof ascii));

		/* get the current byte */
		c = (unsigned char)(*blk++);

		/* put current byte into dump buffers */
		sprintf(hex[ix], "%2.2x ", c);
		ascii[ix] = isprint(c) ? c : '.';
	}

	return 0;
}
#endif

#if LWIP_IPV4 && LWIP_IGMP
static err_t
igmp_mac_filter(struct netif *netif, const ip4_addr_t *group,
		enum netif_mac_filter_action action)
{
	struct ethernetif *ethernetif = (struct ethernetif *)netif->state;
	err_t ret;

	if (action == NETIF_ADD_MAC_FILTER || action == NETIF_DEL_MAC_FILTER) {
		drv_net_mac_cmd_t mac;

		mac.addr[0] = 0x01;
		mac.addr[1] = 0x00;
		mac.addr[2] = 0x5e;
		mac.addr[3] = ((const unsigned char *)&group->addr)[1] & 0x7f;
		mac.addr[4] = ((const unsigned char *)&group->addr)[2];
		mac.addr[5] = ((const unsigned char *)&group->addr)[3];

		if (ioctl(ethernetif->fd_in, action == NETIF_ADD_MAC_FILTER ?
			  DRV_NET_IOCTL_ADD_MCAST : DRV_NET_IOCTL_DEL_MCAST,
			  &mac) == -1) {
			fprintf(stderr,
				"netif \"%c%c\" %s_MCAST failed, errno=%d\n",
				netif->name[0], netif->name[1],
				action == NETIF_ADD_MAC_FILTER ? "ADD" : "DEL",
				errno);
				ret = ERR_IF;
		} else 	if (ioctl(ethernetif->fd_in,
				  DRV_NET_IOCTL_UPDATE_MCAST, NULL) == -1) {
			fprintf(stderr,
				"netif \"%c%c\" UPDATE_MCAST failed, "
				"errno=%d\n",
				netif->name[0], netif->name[1], errno);
			ret = ERR_IF;
		} else {
			LWIP_DEBUGF(ARCH_DEBUG,
				    ("%s: netif \"%c%c\" %s_MCAST "
				     "%02x:%02x:%02x:%02x:%02x:%02x\n",
				     __func__,
				     netif->name[0], netif->name[1],
				     action == NETIF_ADD_MAC_FILTER ?
				     "ADD" : "DEL",
				     mac.addr[0], mac.addr[1], mac.addr[2],
				     mac.addr[3], mac.addr[4], mac.addr[5]));
			ret = ERR_OK;
		}
	} else {
		ret = ERR_ARG;
	}

	return ret;
}
#endif

#if LWIP_IPV6 && LWIP_IPV6_MLD
static err_t
mld_mac_filter(struct netif *netif, const ip6_addr_t *group,
		enum netif_mac_filter_action action)
{
	struct ethernetif *ethernetif = (struct ethernetif *)netif->state;
	err_t ret;

	if (action == NETIF_ADD_MAC_FILTER || action == NETIF_DEL_MAC_FILTER) {
		drv_net_mac_cmd_t mac;
		u32_t addr;

		addr = lwip_htonl(group->addr[3]);
		mac.addr[0] = 0x33;
		mac.addr[1] = 0x33;
		mac.addr[2] = (addr & 0xff000000) >> 24;
		mac.addr[3] = (addr & 0x00ff0000) >> 16;
		mac.addr[4] = (addr & 0x0000ff00) >>  8;
		mac.addr[5] = (addr & 0x000000ff) >>  0;

		if (ioctl(ethernetif->fd_in, action == NETIF_ADD_MAC_FILTER ?
			  DRV_NET_IOCTL_ADD_MCAST : DRV_NET_IOCTL_DEL_MCAST,
			  &mac) == -1) {
			fprintf(stderr,
				"netif \"%c%c\" %s_MCAST failed, errno=%d\n",
				netif->name[0], netif->name[1],
				action == NETIF_ADD_MAC_FILTER ? "ADD" : "DEL",
				errno);
				ret = ERR_IF;
		} else 	if (ioctl(ethernetif->fd_in,
				  DRV_NET_IOCTL_UPDATE_MCAST, NULL) == -1) {
			fprintf(stderr,
				"netif \"%c%c\" UPDATE_MCAST failed, "
				"errno=%d\n",
				netif->name[0], netif->name[1], errno);
			ret = ERR_IF;
		} else {
			LWIP_DEBUGF(ARCH_DEBUG,
				    ("%s: netif \"%c%c\" %s_MCAST "
				     "%02x:%02x:%02x:%02x:%02x:%02x\n",
				     __func__,
				     netif->name[0], netif->name[1],
				     action == NETIF_ADD_MAC_FILTER ?
				     "ADD" : "DEL",
				     mac.addr[0], mac.addr[1], mac.addr[2],
				     mac.addr[3], mac.addr[4], mac.addr[5]));
			ret = ERR_OK;
		}
	} else {
		ret = ERR_ARG;
	}

	return ret;
}
#endif

/*
 * low_level_output():
 *
 * Should do the actual transmission of the packet. The packet is
 * contained in the pbuf that is passed to the function. This pbuf
 * might be chained.
 *
 */
static err_t
low_level_output(struct netif *netif, struct pbuf *p)
{
	struct ethernetif *ethernetif = (struct ethernetif *)netif->state;
	ssize_t r;
	char *outbuf;

	if (ethernetif->net_shm.tx == NULL) {
		if (p->len == p->tot_len) {
			/* Unchained pbuf, write directly out of pbuf. */
			outbuf = p->payload;
		} else {
			outbuf = ethernetif->outbuf;
			/*
			 * XXX p->tot_len assumed to be smaller than device
			 *     frame size
			 */
			pbuf_copy_partial(p, outbuf, ethernetif->frame_size, 0);
		}

#ifdef DUMP_PACKET
		fprintf(stderr, "%s: \"%c%c\" put %d bytes\n",
			__func__, netif->name[0], netif->name[1], p->tot_len);
		dump_block(stderr, outbuf, min(p->tot_len, 64));
#endif
		r = write(ethernetif->fd_out, outbuf, p->tot_len);
	} else {
		vm_io_buf_id_t tx;
		P4_size_t size;
		P4_size_t copysize;
		void *txbuf;

		tx = vm_io_sbuf_tx_alloc(&ethernetif->net_shm, 1);
		if (tx == VM_IO_BUF_ID_INVALID) {
			/*
			 * No buffer available in tx ring for now we treat
			 * this as a device error.
			 * TODO: create a output dd_thread, hand it over the
			 *       work and wait till it finishes
			 */
			LWIP_DEBUGF(NETIF_DEBUG,
				    ("%s: \"%c%c\" "
				     "SBUF ring full! packet dropped\n",
				     __func__,
				     netif->name[0], netif->name[1]));
			STATS_INC(link.drop);
			return ERR_IF;
		}

		/* Get buffer address and size. */
		txbuf = (void *)vm_io_sbuf_tx_buf_addr(
			&ethernetif->net_shm, tx);
		size = vm_io_sbuf_buf_max_size(&ethernetif->net_shm);

		if (p->tot_len > size) {
			/*
			 * Packet gets truncated.
			 * FIXME: should we raise a error here?
			 */
			copysize = size;
		} else {
			copysize = p->tot_len;
		}

		if (p->len == copysize) {
			/* Optimize single PBUF case. */
			memcpy(txbuf, p->payload, copysize);
		} else {
			pbuf_copy_partial(p, txbuf, copysize, 0);
		}

#ifdef DUMP_PACKET
		fprintf(stderr, "%s: \"%c%c\" put %d bytes\n",
			__func__, netif->name[0], netif->name[1], p->tot_len);
		dump_block(stderr, txbuf, min(p->tot_len, 64));
#endif
		/* Signal SBUF and indicate successful completion. */
		vm_io_sbuf_tx_ready(&ethernetif->net_shm, tx, copysize);
		r = 0;
	}
	if (r == -1) {
		/* EAGAIN is expected with MODE_PORT and finite timeouts. */
		if (ethernetif->mode != MODE_PORT || errno != EAGAIN) {
			STATS_INC(link.err);
			warn("%s: \"%c%c\" write",
			     __func__, netif->name[0], netif->name[1]);
		}
		return ERR_IF;
	}

	STATS_INC(link.xmit);

	return ERR_OK;
}

#ifdef USE_CQ
static void
dd_input_thread(void *a)
{
	struct ethernetif *ethernetif = a;
	P4_e_t ve;
	char *buf;
	int idx;
	P4_size_t len;

	dd_thread_event_enable(DD_EV_SCOPE_TASK);

	for (;;) {
		/* Get a CQ buffer to read data to. */
		while (cq_put_getbuf(&ethernetif->cq_in, &buf, &idx) != 0)
			dd_thread_ev_wait(DD_EV_CONSUME_ALL, NULL, NULL);

		/* Try to read some useful data. */
		do {
			if (ethernetif->net_shm.rx == NULL) {
				/* no sbuf mode */
				ve = vm_read(ethernetif->ssw_fd_in,
					     buf, ethernetif->frame_size, &len);
			} else {
				/* use sbuf to get the package */
				vm_io_buf_id_t *rx;

				len = sizeof(vm_io_buf_id_t);
				rx = (vm_io_buf_id_t *)buf;
				/* get usable buffer - blocking */
				*rx = vm_io_sbuf_rx_get(
					&ethernetif->net_shm, 0);

				/*
				 * we only reach this code here, if
				 * vm_io_sbuf_rx_get returned; return ok.
				 * NOTE: The buffer itself has a error flag
				 * that could indicate an error, but it
				 * isn't supported yet...
				 */
				ve = P4_E_OK;
			}

			if (ve != P4_E_OK) {
				STATS_INC(link.err);
				DDEBUG(("%s: if#%d ve=%d buf=%p idx=%d\n",
					__func__, ethernetif->ifnum, ve,
					buf, idx));
			}
		} while (ve != P4_E_OK && len > 0);

		STATS_INC(link.recv);

		/* Commit data to CQ. */
		cq_put_buf(&ethernetif->cq_in, idx, len);
	}
}
#endif

/*
 * low_level_input():
 *
 * Should allocate a pbuf and transfer the bytes of the incoming
 * packet from the interface into the pbuf.
 *
 */
static struct pbuf *
low_level_input(struct netif *netif)
{
	struct ethernetif *ethernetif = (struct ethernetif *)netif->state;
	struct pbuf *p;
	ssize_t len;

#ifdef USE_CQ
	if (ethernetif->ssw_fd_in != NULL) {
		vm_io_buf_id_t rx;
		char *buf;
		int flags;
		int idx;

		/* Synchronize with DD thread. */
		pthread_mutex_lock(&ethernetif->cq_in.mtx);
		flags = dd_crit_enter(&ethernetif->cq_in.dd_thr);

		buf = NULL;		/* silence compiler */
		idx = -1;
		rx = 0;

		/* Wait for new buffer to become availale. */
		while ((len = cq_get_buf(&ethernetif->cq_in, &buf, &idx)) == 0)
			pthread_cond_wait(&ethernetif->cq_in.cv,
					  &ethernetif->cq_in.mtx);
		LWIP_ASSERT("CQ no data", buf != NULL);
		LWIP_ASSERT("CQ index wrong", idx != -1);

		dd_crit_leave(&ethernetif->cq_in.dd_thr, flags);
		pthread_mutex_unlock(&ethernetif->cq_in.mtx);

		/*
		 * 'buf' is an SBUF ID if SBUF mode is enabled, otherwise
		 * it's a data buffer address.
		 */

		if (ethernetif->net_shm.rx != NULL) {
			void *rxbuf;

			/* Retrieve SBUF ID. */
			if (len != sizeof(vm_io_buf_id_t)) {
				/* Should not happen. */
				LWIP_DEBUGF(ARCH_DEBUG,
					    ("%s:invalid buffer length "
					     "reported from dd_thread\n",
					     __func__));
				return NULL;
			}

			rx = *(vm_io_buf_id_t *)buf;

			/* Get start of data area. */
			rxbuf = (void *)vm_io_sbuf_rx_buf_addr(
				&ethernetif->net_shm, rx);
			/* Get buffer size. */
			len = vm_io_sbuf_rx_buf_size(&ethernetif->net_shm, rx);
			if (len > ethernetif->frame_size) {
				/* XXX Is this needed? */
				len = ethernetif->frame_size;
			}

			/* Point buf to SBUF data buffer address. */
			buf = rxbuf;
		}
			

#ifdef DUMP_PACKET
		fprintf(stderr, "%s: \"%c%c\" got %d bytes\n",
			__func__, netif->name[0], netif->name[1], len);
		dump_block(stderr, buf, min(len, 64));
#endif
		/* Allocate a pbuf (chain) and copy data. */
		if ((p = pbuf_alloc(PBUF_LINK, len, PBUF_POOL)) != NULL) {
			pbuf_take(p, buf, len);
		} else {
			STATS_INC(link.memerr);
		}

		/* Synchronize with DD thread, release CQ buffer. */
		flags = dd_crit_enter(&ethernetif->cq_in.dd_thr);
		cq_free_buf(&ethernetif->cq_in, idx);
		dd_crit_leave(&ethernetif->cq_in.dd_thr, flags);

		if (ethernetif->net_shm.rx != NULL) {
			/* Signal SBUF we are done. */
			vm_io_sbuf_rx_free(&ethernetif->net_shm, rx);
		}
	} else
#endif
	{
		/* Read data from device. */
		len = read(ethernetif->fd_in, ethernetif->inbuf,
			   ethernetif->frame_size);
		if (len < 0) {
			STATS_INC(link.err);
			warn("%s: \"%c%c\" read",
			     __func__, netif->name[0], netif->name[1]);
			return NULL;
		}
		STATS_INC(link.recv);

#ifdef DUMP_PACKET
		fprintf(stderr, "%s: \"%c%c\" got %d bytes\n",
			__func__, netif->name[0], netif->name[1], len);
		dump_block(stderr, ethernetif->inbuf, min(len, 64));
#endif

		/* Allocate a pbuf (chain) and copy data. */
		if ((p = pbuf_alloc(PBUF_LINK, len, PBUF_POOL)) != NULL) {
			pbuf_take(p, ethernetif->inbuf, len);
		} else {
			STATS_INC(link.memerr);
		}
	}

	return p;
}

/*
 * ethernetif_thread():
 *
 * ethernet "driver" main loop
 *
 */
static void
ethernetif_thread(void *arg)
{
	struct netif * const netif = (struct netif *)arg;
	struct pbuf *p;

	for (;;) {
		/*
		 * low_level_input() blocks into read() on Ethernet,
		 * receives packet and passes it back in a PBUF p.
		 */
		if ((p = low_level_input(netif)) != NULL) {
			if (netif->input(p, netif) != ERR_OK) {
				LWIP_DEBUGF(NETIF_DEBUG,
					    ("ethernetif_thread: %c%c "
					     "input error\n",
					     netif->name[0], netif->name[1]));
				pbuf_free(p);
			}
		} else {
			/* no packet could be read, silently ignore this */
		}
	}
}

/* ------------------ GLOBAL FUNCTION DEFINITIONS -------------------------- */

/*
 * ethernetif_init():
 *
 * Should be called at the beginning of the program to set up the
 * network interface. It calls the function low_level_init() to do the
 * actual setup of the hardware.
 *
 */
err_t
lwip_ethernetif_init(struct netif *netif)
{
	struct ethernetif *ethernetif;
	int ifnum;

	ifnum = (int)(uintptr_t)netif->state;
	ethernetif = malloc(sizeof(struct ethernetif));

	if (ethernetif == NULL) {
		LWIP_DEBUGF(NETIF_DEBUG,
			    ("ethernetif_init: malloc failure (sz=%zu)\n",
			     sizeof(struct ethernetif)));
		return ERR_MEM;
	}

	ethernetif->ifnum = ifnum;
	netif->state = ethernetif;

#if LWIP_NETIF_HOSTNAME
	/* Initialize interface hostname */
	netif->hostname = "lwip";		/* XXX */
#endif /* LWIP_NETIF_HOSTNAME */

	NETIF_INIT_SNMP(netif, snmp_ifType_ethernet_csmacd, 0);

	/* Set our "softlink" and "hardlink" access to the Ethernet wire */
#if LWIP_IPV4
	netif->output = etharp_output;
#endif
#if LWIP_IPV6
	netif->output_ip6 = ethip6_output;
#endif
	netif->linkoutput = low_level_output;

	return low_level_init(netif);
}
