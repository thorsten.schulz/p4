/**
 ***********************************************************************
 *
 * @copyright
 *	(C) Copyright SYSGO AG.
 *	Ulm, Germany
 *	All rights reserved.
 *
 * @file 
 *	$RCSfile: pbuf_util.c,v $
 * 
 * @purpose
 *	POSIX personality.
 *
 * @cfg_management
 *	$Id: pbuf_util.c,v 1.10 2016/12/13 15:35:49 tmu Exp $
 *	$Author: tmu $
 *	$Date: 2016/12/13 15:35:49 $
 *	$Revision: 1.10 $
 *	$State: Exp $
 *
 ***********************************************************************
 */

/**
 * @file
 * Packet buffer management utilities.
 *
 */

#include <sys/cdefs.h>
__RCSID("$Id: pbuf_util.c,v 1.10 2016/12/13 15:35:49 tmu Exp $");

#include <string.h>

#include "lwip/opt.h"
#include "lwip/def.h"
#include "lwip/mem.h"
#include "lwip/memp.h"
#include "lwip/pbuf.h"
#include "lwip/sys.h"

#ifndef min
# define min(a, b)	((a) < (b) ? (a) : (b))
#endif

/**
 * Copy data from pbuf chain to memory buffer
 *
 * @param p pbuf to copy data from
 * @param len how many bytes to copy
 * @param buf memory buffer to copy data to
 */
void
lwip_pbuf_copydata(struct pbuf *p, unsigned len, void *buf)
{
	unsigned count;
	char *b;

	for (b = buf; p != NULL && len > 0; p = p->next) {
		count = min(len, p->len);
		memcpy(b, p->payload, count);
		b += count;
		len -= count;
	}
}

/**
 * Copy data from memory buffer to pbuf chain
 *
 * @param p pbuf to copy data from
 * @param len how many bytes to copy
 * @param buf memory buffer to copy data to
 */
struct pbuf *
lwip_pbuf_bufget(void *buf, unsigned len)
{
	struct pbuf *p;
	struct pbuf *q;
	char *bufptr;

	/* We allocate a pbuf chain of pbufs from the pool for the
	 * incoming packet data.
	 */
	p = pbuf_alloc(PBUF_LINK, len, PBUF_POOL);
	if (p != NULL) {
		/* We iterate over the pbuf chain until we have read the
		 * entire packet into the pbuf.
		 */
		bufptr = buf;
		for (q = p; q != NULL; q = q->next) {
			/* Read enough bytes to fill this pbuf in the
			 * chain. The available data in the pbuf is given by
			 * the q->len variable.
			 */
			memcpy(q->payload, bufptr, q->len);
			bufptr += q->len;
		}
	}
	return p;
}
