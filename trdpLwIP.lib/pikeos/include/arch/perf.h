
/**
 * @if INCLUDE_HEADER
 * @copyright
 *   (C) Copyright SYSGO AG.
 *   Ulm, Germany
 *   All rights reserved.
 * @endif
 *
 * @file
 *
 * @purpose
 *   lwIP PikeOS/POSIX perfomance meaesurment support
 *
 * @if INCLUDE_HEADER
 * @cfg_management
 *   $Id: perf.h,v 1.4 2016/12/13 15:35:49 tmu Exp $
 *   $HeadURL: $
 *   $Author: tmu $
 *   $Date: 2016/12/13 15:35:49 $
 *   $Revision: 1.4 $
 * @endif
 */

#ifndef LWIP_ARCH_PERF_H
#define LWIP_ARCH_PERF_H

/* ------------------------- FILE INCLUSION -------------------------------- */

#ifdef LWIP_PERF
#include <ddapi/ddapi.h>
#endif

/* ------------------- CONSTANT / MACRO DEFINITIONS ------------------------ */

#ifdef LWIP_PERF

#define PERF_START	{				\
		uint64_t __perf_start;			\
		uint64_t __perf_end;			\
		__perf_start = dd_get_time_raw();

#define PERF_STOP(x)							\
		__perf_end = dd_get_time_raw();				\
		_lwip_arch_perf_print(__perf_start, __perf_end, x);	\
	}

#else

#define PERF_START
#define PERF_STOP(x) 

#endif

/* ----------------------- FUNCTION DECLARATIONS --------------------------- */

#ifdef LWIP_PERF
void _lwip_arch_perf_print(uint64_t, uint64_t, const char *);
#endif

#endif /* LWIP_ARCH_PERF_H */
