/**
 *  @if INCLUDE_HEADER
 *  @copyright
 *    (C) Copyright SYSGO AG.
 *    Klein-Winternheim, Germany
 *    All rights reserved.
 *  @endif
 *
 *  @file
 *    sbuf.h
 *
 *  @purpose
 *    libsbuf API wrapper for POSIX/lwip
 *
 *  @if INCLUDE_HEADER
 *  @cfg_management
 *    $Id: sbuf_wrapper.h,v 1.6 2017/01/18 15:01:18 tmu Exp $
 *    $Author: tmu $
 *    $Date: 2017/01/18 15:01:18 $
 *    $Revision: 1.6 $
 *    $State: Exp $
 *  @endif
 */

#ifndef LWIP_ARCH_SBUF_WRAPPER_H
#define LWIP_ARCH_SBUF_WRAPPER_H

/* ------------------------- FILE INCLUSION -------------------------------- */

/* size_t. */
#include <stddef.h>
/* drv_sbuf_desc_t. */
#include <vm_io_sbuf.h>

/* ----------------------- FUNCTION DECLARATIONS --------------------------- */

/**
 * @param[in]           fd      File descriptor of the open driver file
 * @param[out]          vsize   Space in the virtual address space that will be
 *                              needed to map the buffers
 */
int _sbuf_init(int fd, size_t *vsize);

/**
 * @param[in]           fd      File descriptor of the open driver file
 * @param[in]           vaddr   Address in the virtual space to which
 *                              the sbuf will be mapped to
 * @param[inout]        shm     Control structure of the buffer (will be filled)
 */
int _sbuf_map(int fd, void *vaddr, drv_sbuf_desc_t *shm);

#endif /* LWIP_ARCH_SBUF_WRAPPER_H */
