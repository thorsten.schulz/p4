
/**
 * @if INCLUDE_HEADER
 * @copyright
 *   (C) Copyright SYSGO AG.
 *   Ulm, Germany
 *   All rights reserved.
 * @endif
 *
 * @file
 *
 * @purpose
 *   lwIP PikeOS/POSIX Ethernet driver
 *
 * @if INCLUDE_HEADER
 * @cfg_management
 *   $Id: ethernetif.h,v 1.5 2016/12/13 15:35:49 tmu Exp $
 *   $HeadURL: $
 *   $Author: tmu $
 *   $Date: 2016/12/13 15:35:49 $
 *   $Revision: 1.5 $
 * @endif
 */

#ifndef LWIP_ARCH_ETHERNETIF_H
#define LWIP_ARCH_ETHERNETIF_H

/* ------------------------- FILE INCLUSION -------------------------------- */

/* err_t. */
#include "lwip/err.h"

/* ------------------------ TYPE DECLARATIONS ------------------------------ */

/* forward. */
struct netif;

/* ----------------------- FUNCTION DECLARATIONS --------------------------- */

/* XXX For sys_arch.c, exported by ethernetif.c. */
err_t lwip_ethernetif_init(struct netif *);

#endif /* LWIP_ARCH_ETHERNETIF_H */
