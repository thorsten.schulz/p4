
/**
 * @if INCLUDE_HEADER
 * @copyright
 *   (C) Copyright SYSGO AG.
 *   Ulm, Germany
 *   All rights reserved.
 * @endif
 *
 * @file
 *
 * @purpose
 *   lwIP PikeOS/POSIX port
 *
 * @if INCLUDE_HEADER
 * @cfg_management
 *   $Id: sys_arch.h,v 1.18 2017/02/15 15:45:01 tmu Exp $
 *   $HeadURL: $
 *   $Author: tmu $
 *   $Date: 2017/02/15 15:45:01 $
 *   $Revision: 1.18 $
 * @endif
 */

#ifndef LWIP_ARCH_SYS_ARCH_H
#define LWIP_ARCH_SYS_ARCH_H

/* ------------------------- FILE INCLUSION -------------------------------- */

#include <assert.h>
#include <pthread.h>
#include <sys/types.h>
#include "arch/cc.h"

/* ------------------- CONSTANT / MACRO DEFINITIONS ------------------------ */

#define SYS_MBOX_NULL NULL
#define SYS_SEM_NULL  NULL

/* Control flag for lwip_sys_ethifup(). */
#define LWIP_TRYDHCP	0x1

/*
 * If LWIP_DNS_API_HOSTENT_STORAGE is non-zero, netdb API functions such as
 * gethostbyname() or gethostbyaddr() return pointers to thread-specifc
 * hostent structures (i.e. APIs will be thread-safe).
 */
#define LWIP_DNS_API_HOSTENT_STORAGE 1

#if LWIP_DNS_API_HOSTENT_STORAGE
/* Namespace: */
#define sys_thread_hostent(x)	lwip_sys_thread_hostent(x)
#endif

/* ------------------------ TYPE DECLARATIONS ------------------------------ */

/* Forward. */
struct sys_sem;
/* Forward. */
struct sys_mbox;
/* Forward. */
struct sys_thread;

#if SYS_LIGHTWEIGHT_PROT
/* Protection level indicator; has no meaning in this port. */
typedef u32_t sys_prot_t;
#endif

#if !LWIP_COMPAT_MUTEX
/*
 * pthread_mutex_t is a handle (pointer to structure), so use a void pointer
 * as alias; saves allocation of a structure containing a pthread_mutex_t
 * object.
 */
typedef void *sys_mutex_t;
#endif

/* Hide details of struct sys_sem. */
typedef struct sys_sem *sys_sem_t;

/* Hide details of struct sys_mbox. */
typedef struct sys_mbox *sys_mbox_t;

/* Hide details of struct sys_thread. */
typedef struct sys_thread *sys_thread_t;

/* ----------------------- OBJECT DECLARATIONS ----------------------------- */

extern const int lwip_ipv4;
extern const int lwip_ipv6;

/* ---------------------- INLINE FUNCTION DEFIITIONS ----------------------- */

#if !LWIP_COMPAT_MUTEX

static inline int
lwip_sys_mutex_valid(sys_mutex_t *m)
{
	return m != NULL && *m != NULL;
}

static inline void
lwip_sys_mutex_set_invalid(sys_mutex_t *m)
{
	if (m != NULL)
		*m = NULL;
}

static inline void
lwip_sys_mutex_lock(sys_mutex_t *m)
{
	pthread_mutex_t * const mtx = (pthread_mutex_t *)m;
	int r;

	r = pthread_mutex_lock(mtx);
	assert(r == 0);
	LWIP_UNUSED_ARG(r);
}

static inline void
lwip_sys_mutex_unlock(sys_mutex_t *m)
{
	pthread_mutex_t * const mtx = (pthread_mutex_t *)m;
	int r;

	r = pthread_mutex_unlock(mtx);
	assert(r == 0);
	LWIP_UNUSED_ARG(r);
}

#endif

static inline int
lwip_sys_sem_valid(sys_sem_t *s)
{
	return s != NULL && *s != NULL;
}

static inline void
lwip_sys_sem_set_invalid(sys_sem_t *s)
{
	if (s != NULL)
		*s = NULL;
}

static inline int
lwip_sys_mbox_valid(sys_mbox_t *mb)
{
	return mb != NULL && *mb != NULL;
}

static inline void
lwip_sys_mbox_set_invalid(sys_mbox_t *mb)
{
	if (mb != NULL)
		*mb = NULL;
}

/* ----------------------- FUNCTION DECLARATIONS --------------------------- */

int lwip_sys_ethifup(int);
int _lwip_prop_configure(void);
int _inet_pton_func(int, const char *, void *);
const char * _inet_ntop_func(int, const void *, char *, u32_t /*socklen_t*/);
#if LWIP_DNS_API_HOSTENT_STORAGE
struct hostent *sys_thread_hostent(struct hostent *);
#endif

#endif /* LWIP_ARCH_SYS_ARCH_H */
