
/**
 * @if INCLUDE_HEADER
 * @copyright
 *   (C) Copyright SYSGO AG.
 *   Ulm, Germany
 *   All rights reserved.
 * @endif
 *
 * @file
 *
 * @purpose
 *   lwIP PikeOS/POSIX compilation environment
 *
 * @if INCLUDE_HEADER
 * @cfg_management
 *   $Id: cc.h,v 1.27 2016/12/13 15:35:49 tmu Exp $
 *   $HeadURL: $
 *   $Author: tmu $
 *   $Date: 2016/12/13 15:35:49 $
 *   $Revision: 1.27 $
 * @endif
 */

#ifndef LWIP_ARCH_CC_H
#define LWIP_ARCH_CC_H

/* ------------------------- FILE INCLUSION -------------------------------- */

#include <errno.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
/* _arc4random(). */
#undef __BSD_VISIBLE
#define __BSD_VISIBLE	1
#include <stdlib.h>
#undef __BSD_VISIBLE
#define __BSD_VISIBLE	0
#include <string.h>
#include <sys/param.h>
#include <sys/select.h>
#include <machine/endian.h>

/* ------------------- CONSTANT / MACRO DEFINITIONS ------------------------ */

#define LWIP_TIMEVAL_PRIVATE			0
#define LWIP_COMPAT_SOCKETS			0
#define LWIP_POSIX_SOCKETS_IO_NAMES		0
#define LWIP_COMPAT_MUTEX			0
#define LWIP_DNS_API_DECLARE_H_ERRNO		0

#define LWIP_RAND()	_arc4random()

#if __SIZEOF_LONG__ > 4
#define IPV6_FRAG_COPYHEADER   1
#endif

/* Translate <machine/endian.h> to lwIP requirements. */
#if _BYTE_ORDER == _BIG_ENDIAN
#define BYTE_ORDER	BIG_ENDIAN
#elif _BYTE_ORDER == _LITTLE_ENDIAN
#define BYTE_ORDER	LITTLE_ENDIAN
#else
# error Byte order unknown.
#endif

/* Type for internal err_t, override default which is s8_t. */
#define LWIP_ERR_T int

/*  Compiler hints for packing lwip's structures */
#define PACK_STRUCT_FIELD(x)	x
#define PACK_STRUCT_STRUCT	__attribute__((packed))
#define PACK_STRUCT_BEGIN
#define PACK_STRUCT_END

#if !defined(POSIXDEBUG)
# define LWIP_PLATFORM_BREAKPOINT()
#else
# include <sys/debug.h>
# define LWIP_PLATFORM_BREAKPOINT()		gdb_breakpoint()
#endif

/* Diagnostic message. */
#define LWIP_PLATFORM_DIAG(x)	do {		\
		printf x;			\
	} while (0)

/* Fatal error message (stop execution). */
#define LWIP_PLATFORM_ASSERT(x) do {					\
		fprintf(stderr,						\
			"Assertion \"%s\" failed at line %d in %s\n",	\
			x, __LINE__, __FILE__);				\
		LWIP_PLATFORM_BREAKPOINT();				\
		abort();						\
	} while (0)

#endif /* LWIP_ARCH_CC_H */
