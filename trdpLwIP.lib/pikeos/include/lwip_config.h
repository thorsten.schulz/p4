/*-------------------------- FILE PROLOGUE ---------------------------*/

/**
 *  @if INCLUDE_HEADER
 *  @copyright
 *    (C) Copyright SYSGO AG.
 *    Ulm, Germany
 *    All rights reserved.
 *  @endif
 *
 *  @file
 *    $RCSfile: lwip_config.h,v $
 *
 *  @purpose
 *    Data Structure for lwIP Configuration Parameters
 *
 *  @if INCLUDE_HEADER
 *  @cfg_management
 *    $Id: lwip_config.h,v 1.18 2017/02/16 10:27:18 tmu Exp $
 *    $Author: tmu $
 *    $Date: 2017/02/16 10:27:18 $
 *    $Revision: 1.18 $
 *    $State: Exp $
 *  @endif
 */

#ifndef _LWIP_CONFIG_H
#define _LWIP_CONFIG_H

/* ------------------- CONSTANT / MACRO DEFINITIONS ------------------------ */

/** @brief 
 *   Version identifier for struct _lwip_config.
 *  @hideinitializer
 */
#define VERSION_LWIP_CONFIGURABLES	0x00010004	/* 1.4 */

/** @brief
 *   Length of a path used to access network interface ports and
 *   Ethernet file providers. The value 64 should be sufficient, a
 *   value of 256 (VM_MAX_PATHNAME_LEN) would be way over the top for
 *   this specific usage.
 */
#define LWIP_CONFIG_PATH_MAX		64

/** @brief
 *   Length of an lwIP interface name (includes terminating '\0').
 */
#define LWIP_IFNAME_MAX			3

/** @brief
 *   Number of interfaces to support.
 *   Note: This is *not* a tuneable parameter.
 */
#define LWIP_NUM_IF			6

/** @brief
 *   Number of IPv6 addresses per interface.
 *   Note: This is *not* a tuneable parameter.
 */
#define LWIP_NUM_ADDR_IPV6		3

/** @brief
 *   Number of DNS name servers allowed to be configured.
 *   Note: This is *not* a tuneable parameter.
 */
#define LWIP_NUM_NS			4

/** @brief
 *   Number of DNS name servers allowed to be configured through DHCP
 *   per supported interface.
 *   Note: This is *not* a tuneable parameter.
 */
#define LWIP_NUM_NS_PER_IF		2

/** @brief
 *   Total number of DNS name servers allowed to be configured through DHCP.
 *   Note: This is *not* a tuneable parameter.
 */
#define LWIP_NUM_NS_DHCP		(LWIP_NUM_NS_PER_IF * LWIP_NUM_IF)

/* ------------------------ TYPE DECLARATIONS ------------------------------ */

/** @brief
 *   Tuneable parameters.
 */
struct _lwip_config {
	/** Version identifier of the configuration record.
	 * If an unknown version identifier is found the configuration
	 * structure is ignored.
	 */
	unsigned version;

	struct _lwip_config_if {
		/** Interface types/modes supported:
		 *   MODE_ETH      PikeOS (Ethernet) file provider
		 *   MODE_PORT     PikeOS ports, set target_mac[] to unique
		 *                 address 
		 *   MODE_LOOPBACK Hardwired 127.1 (localhost), no further
		 *                 configuration settings required
		 *   MODE_NONE     disable/ignore interface
		 */
		enum i_mode {
			MODE_NONE, MODE_ETH, MODE_PORT, MODE_LOOPBACK
		} mode;

		/** lwIP interface name. */
		char ifname[LWIP_IFNAME_MAX];

		/** Hardware address of the Ethernet interface.
		 *  Read from file provider and updated in here for MODE_ETH,
		 *  configure to unique address for MODE_PORT.
		 */
		unsigned char target_mac[6];

		/** Interface mode-dependent configuration data. */
		union { 
			/** Configuration values for MODE_PORT. */
			struct {
				/** Name of Ethernet transmit port. */
				char tx_port[LWIP_CONFIG_PATH_MAX];	
				/** Name of Ethernet receive port. */
				char rx_port[LWIP_CONFIG_PATH_MAX];	
			} port;
			/** Configuration values for MODE_ETH. */
			struct {
				/** Path to the Ethernet file provider. */
				char device[LWIP_CONFIG_PATH_MAX];	
			} eth;
		} dev;

/* 0x00010004 */
		/** Interface MTU */
		unsigned mtu;

		/* IPv4 configuration. */

/* 0x00010004 */
		/**
		 * Flag, if non-zero attempt IPv4 configuration.
		 */
		int enable_ipv4;

		/** "Target" IP address.
		 * If set to '0.0.0.0', use DHCP to configure IP addresses.
		 */
		unsigned char target_ip[4];
		/** Netmask. */
		unsigned char netmask[4];
		/** Default route (gateway) IP address, or '0.0.0.0' if not
		 * default interface.
		 */
		unsigned char gateway_ip[4];

/* 0x00010004 */
		/* IPv6 configuration. */

		/**
		 * Flag, if non-zero attempt IPv6 configuration.
		 */
		int enable_ipv6;

		/** "Target" IPv6 addresses, up to three per interface. A
		 * link-local address is always added, the number of
		 * addresses used is limited by LWIP_IPV6_NUM_ADDRESSES.
		 * all-zero address values are ignored.
		 */
		unsigned char target_ipv6[LWIP_NUM_ADDR_IPV6][16];

		/**
		 * Flag, if non-zero attempt stateless address
		 * auto-configuration (SLAAC).
		 * If target_ipv6[] contains LWIP_NUM_ADDR_IPV6 non-zero
		 * addresses, then auto-configuration will not be started.
		 */
		int enable_auto_config;

/* 0x00010002 */
		/** Write timeout for queuing port based interfaces in
		 *  milliseconds. \n
		 *  A value of zero configures non-blocking operation, a value
		 *  of ((unsigned)-1) configures infinite timeout. If
		 */
		unsigned int tx_to;
	} interfaces[LWIP_NUM_IF];

/* 0x00010003 */
	struct _lwip_config_ns {
		/** DNS name server IP address. */
		unsigned char server_ip[4];
	} nameservers[LWIP_NUM_NS];

};

/* ----------------------- OBJECT DECLARATIONS ----------------------------- */

extern struct _lwip_config __lwip_config;

#endif
