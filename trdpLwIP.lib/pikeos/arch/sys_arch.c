
/**
 * @if INCLUDE_HEADER
 * @copyright
 *   (C) Copyright SYSGO AG.
 *   Ulm, Germany
 *   All rights reserved.
 * @endif
 *
 * @file
 *   $RCSfile: sys_arch.c,v $
 *
 * @purpose
 *   lwIP PikeOS/POSIX port
 *
 * @if INCLUDE_HEADER
 * @cfg_management
 *   $Id: sys_arch.c,v 1.50 2017/02/15 12:46:20 tmu Exp $
 *   $HeadURL: $
 *   $Author: tmu $
 *   $Date: 2017/02/15 12:46:20 $
 *   $Revision: 1.50 $
 * @endif
 */

/* ----------------------- UPSTREAM CODE HEADER ---------------------------- */

/*
 * Copyright (c) 2001-2003 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

/* ------------------------- RCS ID ---------------------------------------- */

#include <sys/cdefs.h>
__RCSID("$Id: sys_arch.c,v 1.50 2017/02/15 12:46:20 tmu Exp $");

/* ------------------- CONSTANT / MACRO DEFINITIONS ------------------------ */

/*
 * Define this to use POSIX semaphores instead of mutex/cv for sys_sem
 * semaphores (note that lwIP forces semaphores to binary semaphores so they
 * aren't really compatible to POSIX (counting) semaphores, see
 * sys_sem_signal()). The gain of USE_SEMAPHORE is marginal.
 */
#undef USE_SEMAPHORE

/* ------------------------- FILE INCLUSION -------------------------------- */

#if __BSD_VISIBLE == 0
/* Let <errno.h> declare _sys_errlist[]. */
#undef __BSD_VISIBLE
#define __BSD_VISIBLE	1
#include <errno.h>
#undef __BSD_VISIBLE
#define __BSD_VISIBLE	0
#endif

#include "lwip/debug.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/param.h>
#include <pthread.h>
#ifdef USE_SEMAPHORE
#include <semaphore.h>
#endif
#include <assert.h>

#include "lwip/sys.h"
#include "lwip/opt.h"
#include "lwip/stats.h"
#include "lwip/tcpip.h"
#include "lwip/etharp.h"
#include "lwip/mld6.h"
#include "lwip/netifapi.h"

#include "lwip_config.h"

#if LWIP_DNS_API_HOSTENT_STORAGE
#include "lwip/netdb.h"
#endif

/* ------------------- CONSTANT / MACRO DEFINITIONS ------------------------ */

/*
 * Default size of a sys_mbox mailbox if size is zero in call to
 * sys_mbox_new().
 */
/* XXX Check this value, add debug facilities to inspect maximum mbox size */
#define SYS_MBOX_SIZE 128

/* ------------------------ TYPE DECLARATIONS ------------------------------ */

struct sys_mbox {
	int first;
	int last;
	int count;
	int size;
	pthread_mutex_t mtx;
	pthread_cond_t cv;
	void **msgs;
};

#ifdef USE_SEMAPHORE
struct sys_sem {
	sem_t sema;
};
#else
struct sys_sem {
	unsigned int c;
	pthread_cond_t cond;
	pthread_mutex_t mutex;
};
#endif

struct sys_thread {
	pthread_t pthread;		/* Thread ID. */
	void *(*func)(void *);		/* Thread start function. */
	void *arg;			/* Thread function argument. */
};

#if LWIP_DNS_API_HOSTENT_STORAGE
/**
 * Storage for return data of netdb API functions gethostbyname() and
 * gethostbyaddr().
 * Note: This matches the current capabilities of lwIP. Once multiple
 *       address an aliases are supported, we might need a data buffer from
 *       which to allocate storage for the various hostent fields and
 *       arrays.
 */
struct hostent_data {
	/** Host address (just one). */
	ip_addr_t addr;
	/** Address pointers plus array terminating NULL. */
	ip_addr_t *addr_ptrs[2];
	/** Aliases array (just the terminating NULL). */
	char *aliases[1];
	/* Hostname. */
	char name[DNS_MAX_NAME_LENGTH + 1];
};

/** Thread-specific data accessible through hostdata_key. */
struct hostdata {
	/** gethostbyaddr() and gethostbyname() return pointer to this. */
	struct hostent host;
	/** Storage for hostdata::host. */
	struct hostent_data data;
};
#endif

/* ----------------------- OBJECT DECLARATIONS ----------------------------- */

static pthread_mutex_t tattr_mutex;
static pthread_attr_t tattr;

static pthread_mutex_t lwprot_mutex;	/* PTHREAD_MUTEX_RECURSIVE */

#if LWIP_DNS_API_HOSTENT_STORAGE
static pthread_key_t hostdata_key;
#endif

/* lwIP network stack configuration. */
const int lwip_ipv4 = LWIP_IPV4;
const int lwip_ipv6 = LWIP_IPV6;

/* -------------------- LOCAL FUNCTION DECLARATIONS ------------------------ */

static struct sys_sem *sys_sem_new_(u8_t count, const char *name);
static void sys_sem_free_(struct sys_sem *sem);
#if LWIP_DNS_API_HOSTENT_STORAGE
static void hostdata_free(void *);
#endif

/*----------------------------------------------------------------------*/
static s32_t
tsdiff(struct timespec *t1, struct timespec *t0)
{
	s32_t tdiff;

	/* Difference between two struct timespec objects in
	 * milliseconds.
	 */
	tdiff = (t1->tv_sec - t0->tv_sec) * 1000;
	tdiff += (t1->tv_nsec - t0->tv_nsec) / 1000000;
	return tdiff;
}

/*----------------------------------------------------------------------*/
static void
lwto2absto(u32_t timeout, struct timespec *now, struct timespec *to)
{
	struct timespec _now;

	if (!now) {
		(void)clock_gettime(CLOCK_REALTIME, &_now);
		now = &_now;
	}
	to->tv_sec = now->tv_sec + timeout / 1000;
	to->tv_nsec = now->tv_nsec + (timeout % 1000) * 1000 * 1000;
	if (to->tv_nsec >= 1000 * 1000 * 1000) {
		++to->tv_sec;
		to->tv_nsec -= 1000 * 1000 * 1000;
	}
}

/*----------------------------------------------------------------------*/
static void *
sys_thread_wrapper(void *a)
{
	struct sys_thread * const thr = a;
	sigset_t set;
	void *ret;

	/* Block all signals. */
	if (sigfillset(&set) != 0 ||
	    pthread_sigmask(SIG_BLOCK, &set, NULL) != 0)
		fprintf(stderr, "%s(): failed to block signals.\n", __func__);

	/* And now for the real work ... */
	ret = (*thr->func)(thr->arg);
	free(thr);
	return ret;
}

/*----------------------------------------------------------------------*/
#if LWIP_DNS_API_HOSTENT_STORAGE
static void
hostdata_free(void *ptr)
{
	LWIP_DEBUGF(ARCH_DEBUG | DNS_DEBUG,
		    ("%s: thread ID %p hostdata %p\n",
		     __func__, pthread_self(), ptr));
	free(ptr);
}
#endif

/*----------------------------------------------------------------------*/
sys_thread_t
lwip_sys_thread_new(const char *name, void (*function)(void *arg), void *arg,
		    int stacksize, int prio)
{
	struct sys_thread *st;
	struct sched_param sp;
	int r;

	/* 
	 * XXX
	 * Instead of having each created thread block signals,
	 * why not block signals in first thread calling this function
	 * and from then on inherit signal mask to created threads?
	 */
	st = malloc(sizeof(*st));
	if (st == NULL) {
		fprintf(stderr, "%s() malloc failure (sz=%zu)\n",
			__func__, sizeof(*st));
		abort();
	}
	st->func = (void *(*)(void *))function;
	st->arg = arg;

	/*
	 * Create thread and let it begin execution in 'sys_thread_wrapper()'.
	 */
	(void)pthread_mutex_lock(&tattr_mutex);
	if (name)
		pthread_attr_setname_np(&tattr, name);
	else
		pthread_attr_setname_np(&tattr, DEFAULT_THREAD_NAME);
	if (stacksize)
		pthread_attr_setstacksize(&tattr, stacksize);
	else
		pthread_attr_setstacksize(&tattr,
					  DEFAULT_THREAD_STACKSIZE);
	sp.sched_priority = prio ? prio : DEFAULT_THREAD_PRIO;
	pthread_attr_setschedparam(&tattr, &sp);
	r = pthread_create(&st->pthread, &tattr, sys_thread_wrapper, st);
	(void)pthread_mutex_unlock(&tattr_mutex);
	if (r != 0) {
		fprintf(stderr, "%s() pthread_create failed: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	}

	LWIP_DEBUGF(ARCH_DEBUG, ("sys_thread_new: \"%s\" %p(%p)\n",
				 name != NULL ? name : "(null)",
				 (void *)function, arg));
	return st;
}

/*----------------------------------------------------------------------*/
err_t
lwip_sys_mbox_new(struct sys_mbox **mb, int size)
{
	struct sys_mbox *mbox;

	mbox = malloc(sizeof(*mbox));
	if (mbox == NULL) {
		LWIP_DEBUGF(ARCH_DEBUG, ("%s() malloc failure (sz=%zu)\n",
					 __func__, sizeof(*mbox)));
		return ERR_MEM;
	}
	if (size == 0)
		size = SYS_MBOX_SIZE;
	mbox->msgs = malloc(size * sizeof(*mbox->msgs));
	if (mbox->msgs == NULL) {
		free(mbox);
		LWIP_DEBUGF(ARCH_DEBUG, ("%s() malloc failure (sz=%zu)\n",
					 __func__, size * sizeof(*mbox->msgs)));
		return ERR_MEM;
	}

	mbox->first = mbox->last = mbox->count = 0;
	mbox->size = size;
	(void)pthread_mutex_init(&mbox->mtx, NULL);
	(void)pthread_mutex_setname_np(&mbox->mtx, "lwipMbMutex");
	(void)pthread_cond_init(&mbox->cv, NULL);
	(void)pthread_cond_setname_np(&mbox->cv, "lwipMbMail");

	LWIP_DEBUGF(ARCH_DEBUG, ("sys_mbox_new: mbox %p\n", mbox));
	SYS_STATS_INC_USED(mbox);
	*mb = mbox;
	return ERR_OK;
}

/*----------------------------------------------------------------------*/
void
lwip_sys_mbox_name(struct sys_mbox **mb, const char *name)
{
	struct sys_mbox * const mbox = *mb;
	char nbuf[_PTHREAD_NAME_LEN];

	strlcpy(nbuf, name, sizeof(nbuf));
	strlcat(nbuf, "MbMail", sizeof(nbuf));
	pthread_cond_setname_np(&mbox->cv, nbuf);
	strlcpy(nbuf, name, sizeof(nbuf));
	strlcat(nbuf, "MbMtx", sizeof(nbuf));
	pthread_mutex_setname_np(&mbox->mtx, nbuf);
}

/*----------------------------------------------------------------------*/
void
lwip_sys_mbox_free(struct sys_mbox **mb)
{
	struct sys_mbox * const mbox = *mb;

	if (mbox != SYS_MBOX_NULL) {
		STATS_DEC(sys.mbox.used);
		pthread_cond_destroy(&mbox->cv);
		pthread_mutex_destroy(&mbox->mtx);
		free(mbox->msgs);
		LWIP_DEBUGF(ARCH_DEBUG, ("sys_mbox_free: mbox %p\n", mbox));
		free(mbox);
	}
}

/*----------------------------------------------------------------------*/
err_t
lwip_sys_mbox_trypost(struct sys_mbox **mb, void *msg)
{
	struct sys_mbox * const mbox = *mb;
	int ret;

	pthread_mutex_lock(&mbox->mtx);
	if (mbox->count < mbox->size) {
		mbox->msgs[mbox->last] = msg;
		mbox->last = (mbox->last + 1) % mbox->size;
		mbox->count++;
		ret = 0;
	} else {
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("sys_mbox_trypost: mbox %p mbox full\n",
			     mbox));
		ret = ERR_MEM;
	}
	pthread_mutex_unlock(&mbox->mtx);
	pthread_cond_signal(&mbox->cv);
	return ret;
}

/*----------------------------------------------------------------------*/
void
lwip_sys_mbox_post(struct sys_mbox **mb, void *msg)
{
	struct sys_mbox * const mbox = *mb;

#if 0
	LWIP_DEBUGF(ARCH_DEBUG,
		    ("sys_mbox_post: mbox %p msg %p\n",
		     mbox, msg));
#endif

	pthread_mutex_lock(&mbox->mtx);
	while (mbox->count == mbox->size) {
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("sys_mbox_post: mbox %p suspend on full mbox\n",
			     mbox));
		pthread_cond_wait(&mbox->cv, &mbox->mtx);
	}

	mbox->msgs[mbox->last] = msg;
	mbox->last = (mbox->last + 1) % mbox->size;
	mbox->count++;

	pthread_mutex_unlock(&mbox->mtx);
	pthread_cond_signal(&mbox->cv);
}

/*----------------------------------------------------------------------*/
static u32_t
_sys_arch_mbox_fetch(struct sys_mbox *mbox, void **msg,
		     u32_t timeout, int try)
{
	struct timespec now;
	struct timespec to;
	struct timespec then;
	u32_t time;

	/* The mutex lock is quick so we don't bother with the timeout
	 * stuff here.
	 */
	time = 0;
	if (!try)
		clock_gettime(CLOCK_REALTIME, &now);

	pthread_mutex_lock(&mbox->mtx);
	if (mbox->count == 0) {
		if (try) {
			pthread_mutex_unlock(&mbox->mtx);
			LWIP_DEBUGF(ARCH_DEBUG,
				    ("_sys_arch_mbox_fetch: "
				     "mbox %p tryfetch empty\n", mbox));
#ifdef SYS_MBOX_EMPTY
			return SYS_MBOX_EMPTY;
#else
			return SYS_ARCH_TIMEOUT;
#endif
		}

		/* We block while waiting for a mail to arrive in the
		 * mailbox. We must be prepared to timeout.
		 */
		lwto2absto(timeout, &now, &to);
		do {
			if (timeout != 0) {
				if (pthread_cond_timedwait(
					    &mbox->cv, &mbox->mtx,
					    &to) == ETIMEDOUT) {
					pthread_mutex_unlock(&mbox->mtx);
					return SYS_ARCH_TIMEOUT;
				}
			} else {
				pthread_cond_wait(&mbox->cv, &mbox->mtx);
			}
		} while (mbox->count == 0);
	}

	if (msg != NULL) {
#if 0
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("_sys_arch_mbox_fetch: mbox %p msg %p\n",
			     mbox, *msg));
#endif
		*msg = mbox->msgs[mbox->first % mbox->size];
	} else {
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("_sys_arch_mbox_fetch: mbox %p, null msg\n",
			     mbox));
	}

	mbox->first = (mbox->first + 1) % mbox->size;
	mbox->count--;

	if (!try) {
		clock_gettime(CLOCK_REALTIME, &then);
		time = tsdiff(&then, &now);
	}

	pthread_mutex_unlock(&mbox->mtx);
	pthread_cond_signal(&mbox->cv);

	return time;
}

u32_t
lwip_sys_arch_mbox_fetch(struct sys_mbox **mb, void **msg, u32_t timeout)
{
	struct sys_mbox * const mbox = *mb;

	return _sys_arch_mbox_fetch(mbox, msg, timeout, 0);
}

u32_t
lwip_sys_arch_mbox_tryfetch(struct sys_mbox **mb, void **msg)
{
	struct sys_mbox * const mbox = *mb;

	return _sys_arch_mbox_fetch(mbox, msg, 0, 1);
}

#if !LWIP_COMPAT_MUTEX
/*----------------------------------------------------------------------*/
err_t
lwip_sys_mutex_new(sys_mutex_t *m)
{
	pthread_mutex_t mtx;
	err_t ret;

	if (pthread_mutex_init(&mtx, NULL) == 0) {
		*m = mtx;
		SYS_STATS_INC_USED(mutex);
		ret = ERR_OK;
	} else
		ret = ERR_MEM;

	return ret;
}

/*----------------------------------------------------------------------*/
void
lwip_sys_mutex_free(sys_mutex_t *m)
{
	pthread_mutex_t * const mtx = (pthread_mutex_t *)m;

	if (pthread_mutex_destroy(mtx) == 0) {
		STATS_DEC(sys.mutex.used);
	}
}

/*----------------------------------------------------------------------*/
void
lwip_sys_mutex_name(sys_mutex_t *m, const char *name)
{
	pthread_mutex_t * const mtx = (pthread_mutex_t *)m;

	pthread_mutex_setname_np(mtx, name);
}
#endif

/*----------------------------------------------------------------------*/
err_t
lwip_sys_sem_new(struct sys_sem **sem, u8_t count)
{

	*sem = sys_sem_new_(count, "lwipSysSem");
	if (*sem == NULL) {
		return ERR_MEM;
	}
	SYS_STATS_INC_USED(sem);
	return ERR_OK;
}

/*----------------------------------------------------------------------*/
static struct sys_sem *
sys_sem_new_(u8_t count, const char *name)
{
	struct sys_sem *sem;

	sem = malloc(sizeof(struct sys_sem));
	if (sem == NULL) {
		LWIP_DEBUGF(ARCH_DEBUG, ("%s() malloc failure (sz=%zu)\n",
					 __func__, sizeof(struct sys_sem)));
		return SYS_SEM_NULL;
	}
#ifdef USE_SEMAPHORE
	if (sem_init(&sem->sema, 0, count) == -1) {
		LWIP_DEBUGF(ARCH_DEBUG, ("%s() sem_init: %s\n",
					 __func__, _sys_errlist[errno]));
		free(sem);
		return SYS_SEM_NULL;
	}
	sem_setname_np(&sem->sema, name);
#else
	sem->c = count;

	pthread_cond_init(&sem->cond, NULL);
	pthread_mutex_init(&sem->mutex, NULL);
	pthread_cond_setname_np(&sem->cond, name);
	pthread_mutex_setname_np(&sem->mutex, name);
#endif
	return sem;
}

/*----------------------------------------------------------------------*/
void
lwip_sys_sem_name(struct sys_sem **s, const char *name)
{
	struct sys_sem * const sem = *s;

#ifndef USE_SEMAPHORE
	pthread_cond_setname_np(&sem->cond, name);
	pthread_mutex_setname_np(&sem->mutex, name);
#else
	sem_setname_np(&sem->sema, name);
#endif
}

#ifndef USE_SEMAPHORE
/*----------------------------------------------------------------------*/
u32_t
lwip_sys_arch_sem_wait(struct sys_sem **s, u32_t timeout)
{
	struct sys_sem * const sem = *s;
	struct timespec start;
	struct timespec end;
	struct timespec to;
	s32_t time;
	u32_t ret;

	pthread_mutex_lock(&sem->mutex);
	/* Unlock mutex when thread gets canceled on the way. */
	pthread_cleanup_push((void (*)(void *))pthread_mutex_unlock,
			     &sem->mutex);

	/* Default return value for timeout==0 */
	ret = 0;
	if (sem->c <= 0) {	/* Semaphore available? */
		if (timeout == 0) {
			while (sem->c <= 0)
				pthread_cond_wait(&sem->cond, &sem->mutex);
		} else {
			/* Compute (absolute) timeout. */
			clock_gettime(CLOCK_REALTIME, &start);
			lwto2absto(timeout, &start, &to);
			while (sem->c <= 0) {
				if (pthread_cond_timedwait(
					    &sem->cond, &sem->mutex,
					    &to) == ETIMEDOUT) {
					ret = SYS_ARCH_TIMEOUT;
					break;
				}
			}
			if (ret == 0) {
				/* Compute time elapsed while waiting. */
				clock_gettime(CLOCK_REALTIME, &end);
				time = tsdiff(&end, &start);
				if (time < 0)
					ret = 0;
				else
					ret = (u32_t)time;
			}
		}
	} else {
		/* Semaphore available without blocking, good! */
	}
	/* Do the actual semaphore operation, unless timeout occurred. */
	if (ret != SYS_ARCH_TIMEOUT)
		sem->c--;
	pthread_cleanup_pop(1);
	return ret;
}
#else
/*----------------------------------------------------------------------*/
u32_t
lwip_sys_arch_sem_wait(struct sys_sem **s, u32_t timeout)
{
	struct sys_sem * const sem = *s;
	struct timespec now;
	struct timespec to;
	struct timespec then;
	u32_t time;
	int r;

	if (timeout) {
		/* timeout is in mulliseconds. */
		clock_gettime(CLOCK_REALTIME, &now);
		lwto2absto(timeout, &now, &to);
		r = sem_timedwait(&sem->sema, &to);
		clock_gettime(CLOCK_REALTIME, &then);

		if (r == 0) {
			time = (then.tv_sec - now.tv_sec) * 1000;
			time += (then.tv_nsec - now.tv_nsec) / (1000 * 1000);
		} else {
			if (errno != ETIMEDOUT) {
				fprintf(stderr, "%s() ", __func__);
				perror("sem_timedwait");
			}
			time = SYS_ARCH_TIMEOUT;
		}

	} else {
		r = sem_wait(&sem->sema);
		if (r == 0)
			time = 0;
		else {
			if (errno != ETIMEDOUT) {
				fprintf(stderr, "%s() ", __func__);
				perror("sem_wait");
			}
			time = SYS_ARCH_TIMEOUT;
		}
	}
	return time;
}
#endif

/*----------------------------------------------------------------------*/
void
lwip_sys_sem_signal(struct sys_sem **s)
{
	struct sys_sem * const sem = *s;

#ifdef USE_SEMAPHORE
	sem_post(&sem->sema);
#else
	pthread_mutex_lock(&(sem->mutex));
	sem->c++;

	if (sem->c > 1)
		sem->c = 1;

	pthread_cond_broadcast(&(sem->cond));
	pthread_mutex_unlock(&(sem->mutex));
#endif
}

/*----------------------------------------------------------------------*/
void
lwip_sys_sem_free(struct sys_sem **s)
{
	struct sys_sem * const sem = *s;

	if (sem != SYS_SEM_NULL) {
#if SYS_STATS
		lwip_stats.sys.sem.used--;
#endif							/* SYS_STATS */
		sys_sem_free_(sem);
	}
}

/*----------------------------------------------------------------------*/
static void
sys_sem_free_(struct sys_sem *sem)
{
#ifdef USE_SEMAPHORE
	sem_destroy(&sem->sema);
	free(sem);
#else
	(void)pthread_cond_destroy(&sem->cond);
	(void)pthread_mutex_destroy(&sem->mutex);
	free(sem);
#endif
}

/*----------------------------------------------------------------------*/
static void
_sys_arch_init(void)
{
	pthread_mutexattr_t mattr;
	struct sched_param sp;
	int r;

	if ((r = pthread_mutex_init(&tattr_mutex, NULL)) != 0) {
		fprintf(stderr, "%s(): pthread_mutex_init tattr_mutex: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	}
	if ((r = pthread_mutexattr_init(&mattr)) != 0) {
		fprintf(stderr, "%s(): pthread_mutexattr_init: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	}
	if ((r = pthread_mutexattr_settype(&mattr,
					   PTHREAD_MUTEX_RECURSIVE)) != 0) {
		fprintf(stderr, "%s(): pthread_mutexattr_settype: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	}
	if ((r = pthread_mutex_init(&lwprot_mutex, &mattr)) != 0) {
		fprintf(stderr, "%s(): pthread_mutex_init lwprot_mutex: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	}
	(void)pthread_mutexattr_destroy(&mattr);

	sp.sched_priority = DEFAULT_THREAD_PRIO;
	if ((r = pthread_attr_init(&tattr)) != 0) {
		fprintf(stderr, "%s(): pthread_attr_init: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	} else if ((r = pthread_attr_setdetachstate(
			    &tattr, PTHREAD_CREATE_DETACHED)) != 0) {
		fprintf(stderr, "%s(): pthread_attr_setdetachstate: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	} else if ((r = pthread_attr_setinheritsched(
			    &tattr, PTHREAD_EXPLICIT_SCHED)) != 0) {
		fprintf(stderr, "%s(): pthread_attr_setinheritsched: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	} else if ((r = pthread_attr_setschedpolicy(
			    &tattr, SCHED_FIFO)) != 0) {
		fprintf(stderr, "%s(): pthread_attr_setschedpolicy: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	} else if ((r = pthread_attr_setschedparam(
			    &tattr, &sp)) != 0) {
		fprintf(stderr, "%s(): pthread_attr_setschedparam: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	} else {
		(void)pthread_mutex_lock(&tattr_mutex);
		(void)pthread_mutex_unlock(&tattr_mutex);
	}
#if LWIP_DNS_API_HOSTENT_STORAGE
	if ((r = pthread_key_create(&hostdata_key, hostdata_free)) != 0) {
		fprintf(stderr, "%s(): pthread_key_create: %s\n",
			__func__, _sys_errlist[r]);
		abort();
	}
	(void)pthread_key_setname_np(hostdata_key, "HostDB");
#endif
}

void
lwip_sys_init(void)
{
	static pthread_once_t init_once = PTHREAD_ONCE_INIT;

	pthread_once(&init_once, _sys_arch_init);
}

/*----------------------------------------------------------------------*/

sys_prot_t
lwip_sys_arch_protect(void)
{
	int r;

	r = pthread_mutex_lock(&lwprot_mutex);
	LWIP_ASSERT("sys_arch_protect: pthread_mutex_lock success", r == 0);
#ifdef LWIP_NOASSERT
	(void)r;
#endif
	return 0;
}

/*----------------------------------------------------------------------*/

void
lwip_sys_arch_unprotect(sys_prot_t pval __unused)
{
	int r;

	r = pthread_mutex_unlock(&lwprot_mutex);
	LWIP_ASSERT("sys_arch_unprotect: pthread_mutex_unlock success", r == 0);
#ifdef LWIP_NOASSERT
	(void)r;
#endif
}

/*----------------------------------------------------------------------*/
u32_t
lwip_sys_now(void)
{
	struct timespec now;
	u32_t ret;

	if (clock_gettime(CLOCK_REALTIME, &now) == 0) {
		ret = now.tv_sec * 1000;
		ret += now.tv_nsec / 1000000;
	} else {
		ret = (u32_t)-1;
	}

	return ret;
}

/*----------------------------------------------------------------------*/

/* XXX only used by PPP */
/**
 * Sleep for some ms. Timeouts are NOT processed while sleeping.
 *
 * @param ms number of milliseconds to sleep
 */
void
lwip_sys_msleep(u32_t ms)
{
	if (ms > 0) {
		struct timespec to;

		to.tv_sec = ms / 1000;
		to.tv_nsec = (ms % 1000) * 1000 * 1000;
		nanosleep(&to, NULL);
	}
}

#ifdef LWIP_PPP

#ifndef MAX_JIFFY_OFFSET
#define MAX_JIFFY_OFFSET ((~0UL >> 1)-1)
#endif

#ifndef HZ
#define HZ 100
#endif

unsigned long
lwip_sys_jiffies(void)
{
	struct timeval tv;
	unsigned long sec = tv.tv_sec;
	long usec = tv.tv_usec;

	gettimeofday(&tv, NULL);

	if (sec >= (MAX_JIFFY_OFFSET / HZ))
		return MAX_JIFFY_OFFSET;
	usec += 1000000L / HZ - 1;
	usec /= 1000000L / HZ;
	return HZ * sec + usec;
}

#endif

/* ********************************************************************
 * HACK Alert: Provide glue for P4FS layer so that we don't need the
 *             notorious <lwipopts.h> when compiling it. The P4FS
 *             /lwip/ layer is currently fixed at 64 (PROC_NFILES)
 *             sockets.
 */

#include "lwip/sockets.h"
#include "lwip/netif.h"
#include "lwip/ip.h"
#include "arch/ethernetif.h"
#include "lwip/dhcp.h"

/* Bring up an ethernet interface. */
int
lwip_sys_ethifup(int ifnum)
{
	struct _lwip_config_if * const config =
		&__lwip_config.interfaces[ifnum];
#if LWIP_IPV6
	struct ip6_addr ip6addr[LWIP_NUM_ADDR_IPV6];
	int n;
#endif
	struct ip4_addr ipaddr;
	struct ip4_addr netmask;
	struct ip4_addr gw;
	struct netif *netif;
	netif = calloc(1, sizeof(struct netif));
	if (netif == NULL) {
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("%s: calloc() size %zu failed\n",
			     __func__, sizeof(struct netif)));
		return 1;
	}

	if (config->enable_ipv4) {
#if !LWIP_IPV4
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("%s: enable_ipv4 set for if%d but LWIP_IPV4=0\n",
			     __func__, ifnum));
#endif
		IP4_ADDR(&ipaddr,
			 config->target_ip[0], config->target_ip[1],
			 config->target_ip[2], config->target_ip[3]);
#if LWIP_IPV4 && LWIP_DHCP
		if (ipaddr.addr == lwip_htonl(0)) {
			/*
			 * If IP address is set 0.0.0.0, DHCP configuration is
			 * requested. Netmask and Gateway must be set to zero
			 * too, before calling netif_add().
			 */
			netmask.addr = lwip_htonl(0);
			gw.addr = lwip_htonl(0);
		} else
#endif
		{
			IP4_ADDR(&netmask,
				 config->netmask[0], config->netmask[1],
				 config->netmask[2], config->netmask[3]);
			IP4_ADDR(&gw,
				 config->gateway_ip[0], config->gateway_ip[1],
				 config->gateway_ip[2], config->gateway_ip[3]);
		}
	} else {
		ipaddr.addr = lwip_htonl(0);
		netmask.addr = lwip_htonl(0);
		gw.addr = lwip_htonl(0);
	}
	if (config->enable_ipv6) {
#if LWIP_IPV6
		struct in6_addr in6;

		memset(ip6addr, '\0', sizeof(ip6addr));
		for (n = 0; n < LWIP_NUM_ADDR_IPV6; n++) {
			memcpy(in6.s6_addr, config->target_ipv6[n], 16);
			inet6_addr_to_ip6addr(&ip6addr[n], &in6);
		}
#else
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("%s: enable_ipv6 set for if%d but LWIP_IPV6=0\n",
			     __func__, ifnum));
#endif
	}

	/* ifnum is an index into __lwip_config.interfaces[]. */
	if (netif_add(netif,
#if LWIP_IPV4
		      &ipaddr, &netmask, &gw,
#endif
		      (void *)(uintptr_t)ifnum, lwip_ethernetif_init,
		      tcpip_input) == NULL) {
		LWIP_DEBUGF(ARCH_DEBUG, ("%s: netif_add() if%d failed\n",
					 __func__, ifnum));
		free(netif);
		return 1;
	}
#if LWIP_IPV4
	if (config->enable_ipv4) {	
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("Interface %d %c%c\n",
			     ifnum, netif->name[0], netif->name[1]));
		LWIP_DEBUGF(ARCH_DEBUG,
			    (" IPv4 address: %u.%u.%u.%u netmask %u.%u.%u.%u "
			     "gw: %u.%u.%u.%u\n",
			     ip4_addr1(&ipaddr), ip4_addr2(&ipaddr),
			     ip4_addr3(&ipaddr), ip4_addr4(&ipaddr),
			     ip4_addr1(&netmask), ip4_addr2(&netmask),
			     ip4_addr3(&netmask), ip4_addr4(&netmask),
			     ip4_addr1(&gw), ip4_addr2(&gw),
			     ip4_addr3(&gw), ip4_addr4(&gw)));
	}
#endif
#if LWIP_IPV6 && LWIP_IPV6_MLD
	if (config->enable_ipv6) {
		LWIP_DEBUGF(ARCH_DEBUG,
			    ("Interface %d %c%c\n",
			     ifnum, netif->name[0], netif->name[1]));
		if (!(netif->flags & NETIF_FLAG_MLD6) ||
		    netif->mld_mac_filter == NULL) {
			LWIP_DEBUGF(ARCH_DEBUG,
				    (" not multicast capable, "
				     "IPv6 not configured\n"));
		} else {
			ip6_addr_t ip6_allnodes_ll;
			int added;

			/* XXX 
			 * - Make interface configuration display a stack
			 *  configuration conditional.
			 * - Add it for IPv4 as well.
			 */
			netif_create_ip6_linklocal_address(netif, 1);
#ifdef LWIP_DEBUG
			const struct ip6_addr *addr = netif_ip6_addr(netif, 0);
			LWIP_DEBUGF(ARCH_DEBUG,
				    (" IPv6 linklocal address: "
				     "%04x:%04x:%04x:%04x:%04x:%04x:%04x:%04x "
				     "index 0\n",
				     IP6_ADDR_BLOCK1(addr),
				     IP6_ADDR_BLOCK2(addr),
				     IP6_ADDR_BLOCK3(addr),
				     IP6_ADDR_BLOCK4(addr),
				     IP6_ADDR_BLOCK5(addr),
				     IP6_ADDR_BLOCK6(addr),
				     IP6_ADDR_BLOCK7(addr),
				     IP6_ADDR_BLOCK8(addr)));
#endif
			for (n = 0, added = 0; n < LWIP_NUM_ADDR_IPV6; n++) {
				s8_t s8;
				int r;

				if (ip6_addr_isany_val(ip6addr[n]))
					continue;
				r = netif_add_ip6_address(netif,
							  &ip6addr[n], &s8);
				LWIP_DEBUGF(ARCH_DEBUG,
					    (" IPv6 address: "
					     "%04x:%04x:%04x:%04x:"
					     "%04x:%04x:%04x:%04x ",
					     IP6_ADDR_BLOCK1(&ip6addr[n]),
					     IP6_ADDR_BLOCK2(&ip6addr[n]),
					     IP6_ADDR_BLOCK3(&ip6addr[n]),
					     IP6_ADDR_BLOCK4(&ip6addr[n]),
					     IP6_ADDR_BLOCK5(&ip6addr[n]),
					     IP6_ADDR_BLOCK6(&ip6addr[n]),
					     IP6_ADDR_BLOCK7(&ip6addr[n]),
					     IP6_ADDR_BLOCK8(&ip6addr[n])));
				if (r == ERR_OK) {
					++added;
					LWIP_DEBUGF(ARCH_DEBUG,
						    ("index %d\n", s8));
				} else {
					LWIP_DEBUGF(ARCH_DEBUG,
						    ("FAILED\n"));
				}
			}

#if LWIP_IPV6_AUTOCONFIG
			if (config->enable_auto_config &&
			    added < LWIP_NUM_ADDR_IPV6) {
				LWIP_DEBUGF(ARCH_DEBUG,
					    (" autoconfig enabled\n"));
				netif_set_ip6_autoconfig_enabled(netif, 1);
			}
#endif

			ip6_addr_set_allnodes_linklocal(&ip6_allnodes_ll);
			netif->mld_mac_filter(netif, &ip6_allnodes_ll,
					      NETIF_ADD_MAC_FILTER);
		}
	}
#endif

	netif_set_up(netif);
	
#if LWIP_IPV4
#if LWIP_DHCP
	if (ipaddr.addr == lwip_htonl(0)) {
		netifapi_dhcp_start(netif);
	} else
#endif
	/*
	 * XXX
	 * This only sets a global variable, IPv4 gateway is set in the
	 * netif_add() call above.
	 */ 
	if (gw.addr != lwip_htonl(0))
		netif_set_default(netif);
#endif

	return 0;
}

/* Provide API services with function linkage for binary modules of
 * liblwip.a and librpc_lwip.a.
 */

int
_inet_pton_func(int af, const char *src, void *dst)
{
	return inet_pton(af, src, dst);
}

const char *
_inet_ntop_func(int af, const void *src, char *dst, u32_t /*socklen_t*/ size)
{
	return inet_ntop(af, src, dst, size);
}

/*----------------------------------------------------------------------*/
#if LWIP_DNS_API_HOSTENT_STORAGE
struct hostent *
sys_thread_hostent(struct hostent *h)
{
	struct hostdata *hd;
	struct hostent *ret;
	int r;

	if ((hd = pthread_getspecific(hostdata_key)) == NULL) {
		if ((hd = calloc(1, sizeof(*hd))) == NULL) {
			fprintf(stderr, "%s: calloc failure (sz=%zu)\n",
				__func__, sizeof(*hd));
			return NULL;
		}
		LWIP_DEBUGF(ARCH_DEBUG | DNS_DEBUG,
			    ("%s: thread ID %p hostdata %p\n",
			     __func__, pthread_self(), hd));
		if ((r = pthread_setspecific(hostdata_key, hd)) != 0) {
			fprintf(stderr, "%s: pthread_setspecific failed: %s\n",
				__func__, _sys_errlist[r]);
			free(hd);
			return NULL;
		}
	}
	ret = &hd->host;
	ret->h_addrtype = h->h_addrtype;
	ret->h_length = h->h_length;
	strcpy(hd->data.name, h->h_name);
	ret->h_name = hd->data.name;
	hd->data.addr_ptrs[0] = &hd->data.addr;
	hd->data.addr_ptrs[1] = NULL;
	LWIP_ASSERT("sys_thread_hostent: h_addr_list[0]",
		    h->h_addr_list[0] != NULL);
	hd->data.addr = *(ip_addr_t *)h->h_addr_list[0];
	ret->h_addr_list = (char **)hd->data.addr_ptrs;
	hd->data.aliases[0] = NULL;
	ret->h_aliases = hd->data.aliases;

	return ret;
}
#endif

/*----------------------------------------------------------------------*/
#if LWIP_PERF
void
_lwip_arch_perf_print(uint64_t t0, uint64_t t1, const char *s)
{
	fprintf(stderr, "%s: %llu nsec\n", s, t1 - t0);
}
#endif
