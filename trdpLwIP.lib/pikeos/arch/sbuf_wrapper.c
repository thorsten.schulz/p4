/* -------------------------- FILE PROLOGUE -------------------------------- */

/**
 *  @if INCLUDE_HEADER
 *  @copyright
 *    (C) Copyright SYSGO AG.
 *    Klein-Winternheim, Germany
 *    All rights reserved.
 *  @endif
 *
 *  @file
 *    sbuf.c
 *
 *  @purpose
 *    libsbuf API wrapper for POSIX/lwip
 *
 *  @if INCLUDE_HEADER
 *  @cfg_management
 *    $Id: sbuf_wrapper.c,v 1.9 2017/01/18 15:01:18 tmu Exp $
 *    $Author: tmu $
 *    $Date: 2017/01/18 15:01:18 $
 *    $Revision: 1.9 $
 *    $State: Exp $
 *  @endif
 *
 */

/* ------------------------- RCS ID ---------------------------------------- */

#include <sys/cdefs.h>
__RCSID("$Id: sbuf_wrapper.c,v 1.9 2017/01/18 15:01:18 tmu Exp $");

/* ------------------------- FILE INCLUSION -------------------------------- */

#include "arch/sbuf_wrapper.h"

#include <vm.h>
#include <drv/drv_net_api.h>
#include <vm_io_sbuf.h>

#include <stdio.h>

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <errno.h>

/* ------------------- MACRO / CONSTANT DEFINITIONS ------------------------ */

/*
 * Switches between the native and the POSIX implementation (default is: 'on')
 * NOTE: Only undef this if you know what your are doing...
 */
#define SBUF_USE_P4FS

/* ------------------ GLOBAL FUNCTION DEFINITIONS -------------------------- */

/*
 * call ioctl in the driver to switch to sbuf-mode
 *
 * re-implements libdrv/sbuf/app_sbuf_api.c:vm_io_sbuf_init if
 * SBUF_USE_P4FS is set
 */
int
_sbuf_init(int fd, size_t *vsize)
{
#if !defined(SBUF_USE_P4FS)
	vm_file_desc_t *ssw_fd;
	P4_e_t verr;

	if (ioctl(fd, FIOGETFD, &ssw_fd) < 0) {
		return -errno;
	} else {
		P4_size_t size;

		verr = vm_io_sbuf_init(ssw_fd, &size);
		if (verr == P4_E_OK) {
			*vsize = size;
			return 0;
		} else
			return -(1000 + verr);
	}
#else
	drv_net_shm_info_cmd_t sbuf_info;

	if (ioctl(fd, DRV_NET_IOCTL_SHM_MODE, NULL) == -1 ||
	    ioctl(fd, DRV_NET_IOCTL_SHM_INFO, &sbuf_info) == -1) {
		return -errno;
	}

	*vsize = 2 * (sbuf_info.ro_map_size + sbuf_info.rw_map_size);
	return 0;
#endif
}

/*
 * map the buffers from the drivers memory
 *
 * re-implements libdrv/sbuf/app_sbuf_api.c:vm_io_sbuf_init if USE_P4FS is set
 */
int
_sbuf_map(int fd, void *vaddr, drv_sbuf_desc_t *shm)
{
#if !defined(SBUF_USE_P4FS)
	vm_file_desc_t *ssw_fd;
	P4_e_t verr;

	if (ioctl(fd, FIOGETFD, &ssw_fd) == -1) {
		return -errno;
	} else {
		verr = vm_io_sbuf_map(ssw_fd, (P4_address_t)vaddr, shm);
		if (verr == P4_E_OK)
			return 0;
		else
			return -(1000 + verr);
	}
#else
	drv_net_shm_info_cmd_t sbuf_info;
	char *map_addr;

	if (ioctl(fd, DRV_NET_IOCTL_SHM_INFO, &sbuf_info) == -1) {
		return -errno;
	}

	/*
	 * SHM offsets (these are bogus, they're not the real address offsets):
	 *  DRV_SHM_MAP_OFFSET_RX_RO - RX RO header
	 *  DRV_SHM_MAP_OFFSET_RX_RW - RX RW header + RX buffers
	 *  DRV_SHM_MAP_OFFSET_TX_RO - TX RO header
	 *  DRV_SHM_MAP_OFFSET_TX_RW - TX RW header + TX buffers
	 */
	map_addr = vaddr;
	if (MAP_FAILED == mmap(
			map_addr,
			(size_t)sbuf_info.ro_map_size,
			PROT_READ,
			MAP_FIXED | MAP_SHARED,
			fd,
			DRV_SHM_MAP_OFFSET_RX_RO)) {
		return -errno;
	}

	map_addr += sbuf_info.ro_map_size;
	if (MAP_FAILED == mmap(
			map_addr,
			(size_t)sbuf_info.rw_map_size,
			PROT_READ | PROT_WRITE,
			MAP_FIXED | MAP_SHARED,
			fd,
			DRV_SHM_MAP_OFFSET_RX_RW)) {
		return -errno;
	}

	map_addr += sbuf_info.rw_map_size;
	if (MAP_FAILED == mmap(
			map_addr,
			(size_t)sbuf_info.ro_map_size,
			PROT_READ,
			MAP_FIXED | MAP_SHARED,
			fd,
			DRV_SHM_MAP_OFFSET_TX_RO)) {
		return -errno;
	}

	map_addr += sbuf_info.ro_map_size;
	if (MAP_FAILED == mmap(
			map_addr,
			(size_t)sbuf_info.rw_map_size,
			PROT_READ | PROT_WRITE,
			MAP_FIXED | MAP_SHARED,
			fd,
			DRV_SHM_MAP_OFFSET_TX_RW)) {
		return -errno;
	}

	shm->rx = (drv_sbuf_header_t *)vaddr;
	shm->tx = (drv_sbuf_header_t *)
		((char *)vaddr + sbuf_info.ro_map_size + sbuf_info.rw_map_size);

	return 0;
#endif
}

/* -------------------------- LOCAL FUNCTIONS ------------------------------ */
