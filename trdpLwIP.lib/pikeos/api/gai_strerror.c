
#include <lwip/netdb.h>

static const char *ai_errlist[] = {
	"Success",					/* 0 */
	"hostname nor servname provided, or not known",	/* EAI_NONAME */
	"servname not supported for ai_socktype",	/* EAI_SERVICE */
	"Non-recoverable failure in name resolution",	/* EAI_FAIL */
	"Memory allocation failure", 			/* EAI_MEMORY */
	"ai_family not supported",			/* EAI_FAMILY */
};

#define EAI_MAX (sizeof(ai_errlist) / sizeof(ai_errlist[0]))

const char *
gai_strerror(int ecode)
{
	ecode -= LWIP_GAIERROR_MIN;
	if (ecode >= 0 && ecode < (int)EAI_MAX)
		return ai_errlist[ecode];
	return "Unknown error";
}
