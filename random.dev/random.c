/* -*- Mode: C -*- */
/* Based on PikeOS example "/dev/zero device" and the bus-driver to read an
 * infinite stream of 0 bytes. Modified to output QEMU's RAND-PCI-device.
 *
 *
 *   Thorsten Schulz, July 2018, <thorsten.schulz@uni-rostock.de>
 */

#include <drv_core_types.h>
#include <drv_base_svc.h>

#include <p4_vmit.h>
#include <kdev/assert.h>
#include <kdev/callback.h>
#include <kdev/service.h>

#include <p4.h>

typedef P4_uint32_t token_t;

extern P4_time_t rtc_get_time( void );

/* -------------------- LOCAL FUNCTION DECLARATIONS ------------------------ */


/* declare driver */

static P4_e_t random_init_gate( drv_gate_t *gate);
static P4_e_t random_read( drv_gd_t *gd, void * u_buff, P4_size_t buff_sz, void * u_ctrl, void * u_meta, P4_size_t *read_sz);

void   seed_xoroshiro128plus(P4_uint64_t seed);
P4_uint64_t xoroshiro128plus(void);

static drv_prov_ops_t prov_ops = {
    .init_gate = random_init_gate,
};

static drv_gate_ops_t gate_ops = {
    .read    = random_read,
};

DRV_DECLARE_DRV(devrandom, &prov_ops);

/* implementation */

static P4_e_t random_init_gate(drv_gate_t *gate) {
	drv_gate_set_ops (gate, &gate_ops);
//	seed_xoroshiro128plus(p4_get_time()); /* this is actually soooo badly predictable that it could be a constant value. */
	seed_xoroshiro128plus( rtc_get_time() ); /* still predictable though */

	return P4_E_OK;
}

#ifndef MIN
#define MIN(a,b) ((a)<(b))?(a):(b)
#endif
/* Do not use from kernel space. Dark segmentation clouds will rise upon you! */

static P4_e_t random_read( drv_gd_t *gd __unused, void * u_buff, P4_size_t buff_sz, void * u_ctrl __unused, void * u_meta __unused, P4_size_t *read_sz) {

	if (!u_buff || buff_sz <= 0 || !read_sz) return P4_E_SIZE;

    P4_e_t rc = P4_E_OK;
	P4_uint64_t local_buff[8];
    P4_size_t sz = (buff_sz+sizeof(local_buff[0])-1) / sizeof(local_buff[0]);
	while (rc==P4_E_OK && sz > 0) {
		P4_size_t todo = MIN(p4_countof(local_buff), sz);

		for (P4_size_t i=0; i<todo; i++) local_buff[i] = xoroshiro128plus();

		rc = drv_memcpy_out(u_buff, local_buff, todo);

	    if (rc == P4_E_OK) sz -= todo;
	}

	if (rc == P4_E_OK) {
		rc = drv_memcpy_out(read_sz, &buff_sz, sizeof(buff_sz));
	}
   	return rc;
}

/* EOF */
